/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model.impl;

import LTPDatabase.model.LTPSignUp;
import LTPDatabase.model.LTPSignUpModel;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

import java.sql.Types;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the LTPSignUp service. Represents a row in the &quot;LTPSignUp&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link LTPSignUpModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link LTPSignUpImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpImpl
 * @see LTPSignUp
 * @see LTPSignUpModel
 * @generated
 */
@ProviderType
public class LTPSignUpModelImpl extends BaseModelImpl<LTPSignUp>
	implements LTPSignUpModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a LTPSignUp model instance should use the {@link LTPSignUp} interface instead.
	 */
	public static final String TABLE_NAME = "LTPSignUp";
	public static final Object[][] TABLE_COLUMNS = {
			{ "ID", Types.BIGINT },
			{ "FirstName", Types.VARCHAR },
			{ "LastName", Types.VARCHAR },
			{ "MobileNumber", Types.VARCHAR },
			{ "CreatedDate", Types.TIMESTAMP },
			{ "UpdatedDate", Types.TIMESTAMP }
		};
	public static final Map<String, Integer> TABLE_COLUMNS_MAP = new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("ID", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("FirstName", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("LastName", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("MobileNumber", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("CreatedDate", Types.TIMESTAMP);
		TABLE_COLUMNS_MAP.put("UpdatedDate", Types.TIMESTAMP);
	}

	public static final String TABLE_SQL_CREATE = "create table LTPSignUp (ID LONG not null primary key,FirstName VARCHAR(75) null,LastName VARCHAR(75) null,MobileNumber VARCHAR(75) null,CreatedDate DATE null,UpdatedDate DATE null)";
	public static final String TABLE_SQL_DROP = "drop table LTPSignUp";
	public static final String ORDER_BY_JPQL = " ORDER BY ltpSignUp.ID ASC";
	public static final String ORDER_BY_SQL = " ORDER BY LTPSignUp.ID ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(LTPDatabase.service.util.ServiceProps.get(
				"value.object.entity.cache.enabled.LTPDatabase.model.LTPSignUp"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(LTPDatabase.service.util.ServiceProps.get(
				"value.object.finder.cache.enabled.LTPDatabase.model.LTPSignUp"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = false;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(LTPDatabase.service.util.ServiceProps.get(
				"lock.expiration.time.LTPDatabase.model.LTPSignUp"));

	public LTPSignUpModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _ID;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setID(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ID;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return LTPSignUp.class;
	}

	@Override
	public String getModelClassName() {
		return LTPSignUp.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("FirstName", getFirstName());
		attributes.put("LastName", getLastName());
		attributes.put("MobileNumber", getMobileNumber());
		attributes.put("CreatedDate", getCreatedDate());
		attributes.put("UpdatedDate", getUpdatedDate());

		attributes.put("entityCacheEnabled", isEntityCacheEnabled());
		attributes.put("finderCacheEnabled", isFinderCacheEnabled());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ID = (Long)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		String FirstName = (String)attributes.get("FirstName");

		if (FirstName != null) {
			setFirstName(FirstName);
		}

		String LastName = (String)attributes.get("LastName");

		if (LastName != null) {
			setLastName(LastName);
		}

		String MobileNumber = (String)attributes.get("MobileNumber");

		if (MobileNumber != null) {
			setMobileNumber(MobileNumber);
		}

		Date CreatedDate = (Date)attributes.get("CreatedDate");

		if (CreatedDate != null) {
			setCreatedDate(CreatedDate);
		}

		Date UpdatedDate = (Date)attributes.get("UpdatedDate");

		if (UpdatedDate != null) {
			setUpdatedDate(UpdatedDate);
		}
	}

	@Override
	public long getID() {
		return _ID;
	}

	@Override
	public void setID(long ID) {
		_ID = ID;
	}

	@Override
	public String getFirstName() {
		if (_FirstName == null) {
			return StringPool.BLANK;
		}
		else {
			return _FirstName;
		}
	}

	@Override
	public void setFirstName(String FirstName) {
		_FirstName = FirstName;
	}

	@Override
	public String getLastName() {
		if (_LastName == null) {
			return StringPool.BLANK;
		}
		else {
			return _LastName;
		}
	}

	@Override
	public void setLastName(String LastName) {
		_LastName = LastName;
	}

	@Override
	public String getMobileNumber() {
		if (_MobileNumber == null) {
			return StringPool.BLANK;
		}
		else {
			return _MobileNumber;
		}
	}

	@Override
	public void setMobileNumber(String MobileNumber) {
		_MobileNumber = MobileNumber;
	}

	@Override
	public Date getCreatedDate() {
		return _CreatedDate;
	}

	@Override
	public void setCreatedDate(Date CreatedDate) {
		_CreatedDate = CreatedDate;
	}

	@Override
	public Date getUpdatedDate() {
		return _UpdatedDate;
	}

	@Override
	public void setUpdatedDate(Date UpdatedDate) {
		_UpdatedDate = UpdatedDate;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			LTPSignUp.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public LTPSignUp toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (LTPSignUp)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		LTPSignUpImpl ltpSignUpImpl = new LTPSignUpImpl();

		ltpSignUpImpl.setID(getID());
		ltpSignUpImpl.setFirstName(getFirstName());
		ltpSignUpImpl.setLastName(getLastName());
		ltpSignUpImpl.setMobileNumber(getMobileNumber());
		ltpSignUpImpl.setCreatedDate(getCreatedDate());
		ltpSignUpImpl.setUpdatedDate(getUpdatedDate());

		ltpSignUpImpl.resetOriginalValues();

		return ltpSignUpImpl;
	}

	@Override
	public int compareTo(LTPSignUp ltpSignUp) {
		long primaryKey = ltpSignUp.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LTPSignUp)) {
			return false;
		}

		LTPSignUp ltpSignUp = (LTPSignUp)obj;

		long primaryKey = ltpSignUp.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return ENTITY_CACHE_ENABLED;
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return FINDER_CACHE_ENABLED;
	}

	@Override
	public void resetOriginalValues() {
	}

	@Override
	public CacheModel<LTPSignUp> toCacheModel() {
		LTPSignUpCacheModel ltpSignUpCacheModel = new LTPSignUpCacheModel();

		ltpSignUpCacheModel.ID = getID();

		ltpSignUpCacheModel.FirstName = getFirstName();

		String FirstName = ltpSignUpCacheModel.FirstName;

		if ((FirstName != null) && (FirstName.length() == 0)) {
			ltpSignUpCacheModel.FirstName = null;
		}

		ltpSignUpCacheModel.LastName = getLastName();

		String LastName = ltpSignUpCacheModel.LastName;

		if ((LastName != null) && (LastName.length() == 0)) {
			ltpSignUpCacheModel.LastName = null;
		}

		ltpSignUpCacheModel.MobileNumber = getMobileNumber();

		String MobileNumber = ltpSignUpCacheModel.MobileNumber;

		if ((MobileNumber != null) && (MobileNumber.length() == 0)) {
			ltpSignUpCacheModel.MobileNumber = null;
		}

		Date CreatedDate = getCreatedDate();

		if (CreatedDate != null) {
			ltpSignUpCacheModel.CreatedDate = CreatedDate.getTime();
		}
		else {
			ltpSignUpCacheModel.CreatedDate = Long.MIN_VALUE;
		}

		Date UpdatedDate = getUpdatedDate();

		if (UpdatedDate != null) {
			ltpSignUpCacheModel.UpdatedDate = UpdatedDate.getTime();
		}
		else {
			ltpSignUpCacheModel.UpdatedDate = Long.MIN_VALUE;
		}

		return ltpSignUpCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{ID=");
		sb.append(getID());
		sb.append(", FirstName=");
		sb.append(getFirstName());
		sb.append(", LastName=");
		sb.append(getLastName());
		sb.append(", MobileNumber=");
		sb.append(getMobileNumber());
		sb.append(", CreatedDate=");
		sb.append(getCreatedDate());
		sb.append(", UpdatedDate=");
		sb.append(getUpdatedDate());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("LTPDatabase.model.LTPSignUp");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ID</column-name><column-value><![CDATA[");
		sb.append(getID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>FirstName</column-name><column-value><![CDATA[");
		sb.append(getFirstName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>LastName</column-name><column-value><![CDATA[");
		sb.append(getLastName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>MobileNumber</column-name><column-value><![CDATA[");
		sb.append(getMobileNumber());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>CreatedDate</column-name><column-value><![CDATA[");
		sb.append(getCreatedDate());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>UpdatedDate</column-name><column-value><![CDATA[");
		sb.append(getUpdatedDate());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static final ClassLoader _classLoader = LTPSignUp.class.getClassLoader();
	private static final Class<?>[] _escapedModelInterfaces = new Class[] {
			LTPSignUp.class
		};
	private long _ID;
	private String _FirstName;
	private String _LastName;
	private String _MobileNumber;
	private Date _CreatedDate;
	private Date _UpdatedDate;
	private LTPSignUp _escapedModel;
}