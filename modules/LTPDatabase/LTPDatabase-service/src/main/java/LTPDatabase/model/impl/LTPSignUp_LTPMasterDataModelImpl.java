/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model.impl;

import LTPDatabase.model.LTPSignUp_LTPMasterData;
import LTPDatabase.model.LTPSignUp_LTPMasterDataModel;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;
import com.liferay.expando.kernel.util.ExpandoBridgeFactoryUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.model.impl.BaseModelImpl;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Serializable;

import java.sql.Types;

import java.util.HashMap;
import java.util.Map;

/**
 * The base model implementation for the LTPSignUp_LTPMasterData service. Represents a row in the &quot;LTPSignUp_LTPMasterData&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link LTPSignUp_LTPMasterDataModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link LTPSignUp_LTPMasterDataImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUp_LTPMasterDataImpl
 * @see LTPSignUp_LTPMasterData
 * @see LTPSignUp_LTPMasterDataModel
 * @generated
 */
@ProviderType
public class LTPSignUp_LTPMasterDataModelImpl extends BaseModelImpl<LTPSignUp_LTPMasterData>
	implements LTPSignUp_LTPMasterDataModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a LTPSignUp_LTPMasterData model instance should use the {@link LTPSignUp_LTPMasterData} interface instead.
	 */
	public static final String TABLE_NAME = "LTPSignUp_LTPMasterData";
	public static final Object[][] TABLE_COLUMNS = {
			{ "ID", Types.BIGINT },
			{ "LTPSignUpID", Types.BIGINT },
			{ "LTPMasterDataID", Types.BIGINT },
			{ "DinnerPrasadItem1", Types.VARCHAR },
			{ "DinnerPrasadItem2", Types.VARCHAR }
		};
	public static final Map<String, Integer> TABLE_COLUMNS_MAP = new HashMap<String, Integer>();

	static {
		TABLE_COLUMNS_MAP.put("ID", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("LTPSignUpID", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("LTPMasterDataID", Types.BIGINT);
		TABLE_COLUMNS_MAP.put("DinnerPrasadItem1", Types.VARCHAR);
		TABLE_COLUMNS_MAP.put("DinnerPrasadItem2", Types.VARCHAR);
	}

	public static final String TABLE_SQL_CREATE = "create table LTPSignUp_LTPMasterData (ID LONG not null primary key,LTPSignUpID LONG,LTPMasterDataID LONG,DinnerPrasadItem1 VARCHAR(75) null,DinnerPrasadItem2 VARCHAR(75) null)";
	public static final String TABLE_SQL_DROP = "drop table LTPSignUp_LTPMasterData";
	public static final String ORDER_BY_JPQL = " ORDER BY ltpSignUp_LTPMasterData.ID ASC";
	public static final String ORDER_BY_SQL = " ORDER BY LTPSignUp_LTPMasterData.ID ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(LTPDatabase.service.util.ServiceProps.get(
				"value.object.entity.cache.enabled.LTPDatabase.model.LTPSignUp_LTPMasterData"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(LTPDatabase.service.util.ServiceProps.get(
				"value.object.finder.cache.enabled.LTPDatabase.model.LTPSignUp_LTPMasterData"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(LTPDatabase.service.util.ServiceProps.get(
				"value.object.column.bitmask.enabled.LTPDatabase.model.LTPSignUp_LTPMasterData"),
			true);
	public static final long LTPMASTERDATAID_COLUMN_BITMASK = 1L;
	public static final long LTPSIGNUPID_COLUMN_BITMASK = 2L;
	public static final long ID_COLUMN_BITMASK = 4L;
	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(LTPDatabase.service.util.ServiceProps.get(
				"lock.expiration.time.LTPDatabase.model.LTPSignUp_LTPMasterData"));

	public LTPSignUp_LTPMasterDataModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _ID;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setID(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ID;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return LTPSignUp_LTPMasterData.class;
	}

	@Override
	public String getModelClassName() {
		return LTPSignUp_LTPMasterData.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("LTPSignUpID", getLTPSignUpID());
		attributes.put("LTPMasterDataID", getLTPMasterDataID());
		attributes.put("DinnerPrasadItem1", getDinnerPrasadItem1());
		attributes.put("DinnerPrasadItem2", getDinnerPrasadItem2());

		attributes.put("entityCacheEnabled", isEntityCacheEnabled());
		attributes.put("finderCacheEnabled", isFinderCacheEnabled());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ID = (Long)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		Long LTPSignUpID = (Long)attributes.get("LTPSignUpID");

		if (LTPSignUpID != null) {
			setLTPSignUpID(LTPSignUpID);
		}

		Long LTPMasterDataID = (Long)attributes.get("LTPMasterDataID");

		if (LTPMasterDataID != null) {
			setLTPMasterDataID(LTPMasterDataID);
		}

		String DinnerPrasadItem1 = (String)attributes.get("DinnerPrasadItem1");

		if (DinnerPrasadItem1 != null) {
			setDinnerPrasadItem1(DinnerPrasadItem1);
		}

		String DinnerPrasadItem2 = (String)attributes.get("DinnerPrasadItem2");

		if (DinnerPrasadItem2 != null) {
			setDinnerPrasadItem2(DinnerPrasadItem2);
		}
	}

	@Override
	public long getID() {
		return _ID;
	}

	@Override
	public void setID(long ID) {
		_ID = ID;
	}

	@Override
	public long getLTPSignUpID() {
		return _LTPSignUpID;
	}

	@Override
	public void setLTPSignUpID(long LTPSignUpID) {
		_columnBitmask |= LTPSIGNUPID_COLUMN_BITMASK;

		if (!_setOriginalLTPSignUpID) {
			_setOriginalLTPSignUpID = true;

			_originalLTPSignUpID = _LTPSignUpID;
		}

		_LTPSignUpID = LTPSignUpID;
	}

	public long getOriginalLTPSignUpID() {
		return _originalLTPSignUpID;
	}

	@Override
	public long getLTPMasterDataID() {
		return _LTPMasterDataID;
	}

	@Override
	public void setLTPMasterDataID(long LTPMasterDataID) {
		_columnBitmask |= LTPMASTERDATAID_COLUMN_BITMASK;

		if (!_setOriginalLTPMasterDataID) {
			_setOriginalLTPMasterDataID = true;

			_originalLTPMasterDataID = _LTPMasterDataID;
		}

		_LTPMasterDataID = LTPMasterDataID;
	}

	public long getOriginalLTPMasterDataID() {
		return _originalLTPMasterDataID;
	}

	@Override
	public String getDinnerPrasadItem1() {
		if (_DinnerPrasadItem1 == null) {
			return StringPool.BLANK;
		}
		else {
			return _DinnerPrasadItem1;
		}
	}

	@Override
	public void setDinnerPrasadItem1(String DinnerPrasadItem1) {
		_DinnerPrasadItem1 = DinnerPrasadItem1;
	}

	@Override
	public String getDinnerPrasadItem2() {
		if (_DinnerPrasadItem2 == null) {
			return StringPool.BLANK;
		}
		else {
			return _DinnerPrasadItem2;
		}
	}

	@Override
	public void setDinnerPrasadItem2(String DinnerPrasadItem2) {
		_DinnerPrasadItem2 = DinnerPrasadItem2;
	}

	public long getColumnBitmask() {
		return _columnBitmask;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			LTPSignUp_LTPMasterData.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public LTPSignUp_LTPMasterData toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (LTPSignUp_LTPMasterData)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		LTPSignUp_LTPMasterDataImpl ltpSignUp_LTPMasterDataImpl = new LTPSignUp_LTPMasterDataImpl();

		ltpSignUp_LTPMasterDataImpl.setID(getID());
		ltpSignUp_LTPMasterDataImpl.setLTPSignUpID(getLTPSignUpID());
		ltpSignUp_LTPMasterDataImpl.setLTPMasterDataID(getLTPMasterDataID());
		ltpSignUp_LTPMasterDataImpl.setDinnerPrasadItem1(getDinnerPrasadItem1());
		ltpSignUp_LTPMasterDataImpl.setDinnerPrasadItem2(getDinnerPrasadItem2());

		ltpSignUp_LTPMasterDataImpl.resetOriginalValues();

		return ltpSignUp_LTPMasterDataImpl;
	}

	@Override
	public int compareTo(LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		long primaryKey = ltpSignUp_LTPMasterData.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LTPSignUp_LTPMasterData)) {
			return false;
		}

		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = (LTPSignUp_LTPMasterData)obj;

		long primaryKey = ltpSignUp_LTPMasterData.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return ENTITY_CACHE_ENABLED;
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return FINDER_CACHE_ENABLED;
	}

	@Override
	public void resetOriginalValues() {
		LTPSignUp_LTPMasterDataModelImpl ltpSignUp_LTPMasterDataModelImpl = this;

		ltpSignUp_LTPMasterDataModelImpl._originalLTPSignUpID = ltpSignUp_LTPMasterDataModelImpl._LTPSignUpID;

		ltpSignUp_LTPMasterDataModelImpl._setOriginalLTPSignUpID = false;

		ltpSignUp_LTPMasterDataModelImpl._originalLTPMasterDataID = ltpSignUp_LTPMasterDataModelImpl._LTPMasterDataID;

		ltpSignUp_LTPMasterDataModelImpl._setOriginalLTPMasterDataID = false;

		ltpSignUp_LTPMasterDataModelImpl._columnBitmask = 0;
	}

	@Override
	public CacheModel<LTPSignUp_LTPMasterData> toCacheModel() {
		LTPSignUp_LTPMasterDataCacheModel ltpSignUp_LTPMasterDataCacheModel = new LTPSignUp_LTPMasterDataCacheModel();

		ltpSignUp_LTPMasterDataCacheModel.ID = getID();

		ltpSignUp_LTPMasterDataCacheModel.LTPSignUpID = getLTPSignUpID();

		ltpSignUp_LTPMasterDataCacheModel.LTPMasterDataID = getLTPMasterDataID();

		ltpSignUp_LTPMasterDataCacheModel.DinnerPrasadItem1 = getDinnerPrasadItem1();

		String DinnerPrasadItem1 = ltpSignUp_LTPMasterDataCacheModel.DinnerPrasadItem1;

		if ((DinnerPrasadItem1 != null) && (DinnerPrasadItem1.length() == 0)) {
			ltpSignUp_LTPMasterDataCacheModel.DinnerPrasadItem1 = null;
		}

		ltpSignUp_LTPMasterDataCacheModel.DinnerPrasadItem2 = getDinnerPrasadItem2();

		String DinnerPrasadItem2 = ltpSignUp_LTPMasterDataCacheModel.DinnerPrasadItem2;

		if ((DinnerPrasadItem2 != null) && (DinnerPrasadItem2.length() == 0)) {
			ltpSignUp_LTPMasterDataCacheModel.DinnerPrasadItem2 = null;
		}

		return ltpSignUp_LTPMasterDataCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{ID=");
		sb.append(getID());
		sb.append(", LTPSignUpID=");
		sb.append(getLTPSignUpID());
		sb.append(", LTPMasterDataID=");
		sb.append(getLTPMasterDataID());
		sb.append(", DinnerPrasadItem1=");
		sb.append(getDinnerPrasadItem1());
		sb.append(", DinnerPrasadItem2=");
		sb.append(getDinnerPrasadItem2());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("LTPDatabase.model.LTPSignUp_LTPMasterData");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>ID</column-name><column-value><![CDATA[");
		sb.append(getID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>LTPSignUpID</column-name><column-value><![CDATA[");
		sb.append(getLTPSignUpID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>LTPMasterDataID</column-name><column-value><![CDATA[");
		sb.append(getLTPMasterDataID());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>DinnerPrasadItem1</column-name><column-value><![CDATA[");
		sb.append(getDinnerPrasadItem1());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>DinnerPrasadItem2</column-name><column-value><![CDATA[");
		sb.append(getDinnerPrasadItem2());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static final ClassLoader _classLoader = LTPSignUp_LTPMasterData.class.getClassLoader();
	private static final Class<?>[] _escapedModelInterfaces = new Class[] {
			LTPSignUp_LTPMasterData.class
		};
	private long _ID;
	private long _LTPSignUpID;
	private long _originalLTPSignUpID;
	private boolean _setOriginalLTPSignUpID;
	private long _LTPMasterDataID;
	private long _originalLTPMasterDataID;
	private boolean _setOriginalLTPMasterDataID;
	private String _DinnerPrasadItem1;
	private String _DinnerPrasadItem2;
	private long _columnBitmask;
	private LTPSignUp_LTPMasterData _escapedModel;
}