/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model.impl;

import LTPDatabase.model.LTPSignUpMasterData;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing LTPSignUpMasterData in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpMasterData
 * @generated
 */
@ProviderType
public class LTPSignUpMasterDataCacheModel implements CacheModel<LTPSignUpMasterData>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LTPSignUpMasterDataCacheModel)) {
			return false;
		}

		LTPSignUpMasterDataCacheModel ltpSignUpMasterDataCacheModel = (LTPSignUpMasterDataCacheModel)obj;

		if (ID == ltpSignUpMasterDataCacheModel.ID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{ID=");
		sb.append(ID);
		sb.append(", EventDate=");
		sb.append(EventDate);
		sb.append(", ExtraText=");
		sb.append(ExtraText);
		sb.append(", isMorningWalk=");
		sb.append(isMorningWalk);
		sb.append(", isVolunteer=");
		sb.append(isVolunteer);
		sb.append(", isDinnerPrasad=");
		sb.append(isDinnerPrasad);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public LTPSignUpMasterData toEntityModel() {
		LTPSignUpMasterDataImpl ltpSignUpMasterDataImpl = new LTPSignUpMasterDataImpl();

		ltpSignUpMasterDataImpl.setID(ID);

		if (EventDate == Long.MIN_VALUE) {
			ltpSignUpMasterDataImpl.setEventDate(null);
		}
		else {
			ltpSignUpMasterDataImpl.setEventDate(new Date(EventDate));
		}

		if (ExtraText == null) {
			ltpSignUpMasterDataImpl.setExtraText(StringPool.BLANK);
		}
		else {
			ltpSignUpMasterDataImpl.setExtraText(ExtraText);
		}

		ltpSignUpMasterDataImpl.setIsMorningWalk(isMorningWalk);
		ltpSignUpMasterDataImpl.setIsVolunteer(isVolunteer);
		ltpSignUpMasterDataImpl.setIsDinnerPrasad(isDinnerPrasad);

		ltpSignUpMasterDataImpl.resetOriginalValues();

		return ltpSignUpMasterDataImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ID = objectInput.readLong();
		EventDate = objectInput.readLong();
		ExtraText = objectInput.readUTF();

		isMorningWalk = objectInput.readBoolean();

		isVolunteer = objectInput.readBoolean();

		isDinnerPrasad = objectInput.readBoolean();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ID);
		objectOutput.writeLong(EventDate);

		if (ExtraText == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ExtraText);
		}

		objectOutput.writeBoolean(isMorningWalk);

		objectOutput.writeBoolean(isVolunteer);

		objectOutput.writeBoolean(isDinnerPrasad);
	}

	public long ID;
	public long EventDate;
	public String ExtraText;
	public boolean isMorningWalk;
	public boolean isVolunteer;
	public boolean isDinnerPrasad;
}