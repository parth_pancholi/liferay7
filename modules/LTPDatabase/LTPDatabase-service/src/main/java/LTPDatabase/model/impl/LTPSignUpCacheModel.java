/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model.impl;

import LTPDatabase.model.LTPSignUp;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing LTPSignUp in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUp
 * @generated
 */
@ProviderType
public class LTPSignUpCacheModel implements CacheModel<LTPSignUp>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LTPSignUpCacheModel)) {
			return false;
		}

		LTPSignUpCacheModel ltpSignUpCacheModel = (LTPSignUpCacheModel)obj;

		if (ID == ltpSignUpCacheModel.ID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{ID=");
		sb.append(ID);
		sb.append(", FirstName=");
		sb.append(FirstName);
		sb.append(", LastName=");
		sb.append(LastName);
		sb.append(", MobileNumber=");
		sb.append(MobileNumber);
		sb.append(", CreatedDate=");
		sb.append(CreatedDate);
		sb.append(", UpdatedDate=");
		sb.append(UpdatedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public LTPSignUp toEntityModel() {
		LTPSignUpImpl ltpSignUpImpl = new LTPSignUpImpl();

		ltpSignUpImpl.setID(ID);

		if (FirstName == null) {
			ltpSignUpImpl.setFirstName(StringPool.BLANK);
		}
		else {
			ltpSignUpImpl.setFirstName(FirstName);
		}

		if (LastName == null) {
			ltpSignUpImpl.setLastName(StringPool.BLANK);
		}
		else {
			ltpSignUpImpl.setLastName(LastName);
		}

		if (MobileNumber == null) {
			ltpSignUpImpl.setMobileNumber(StringPool.BLANK);
		}
		else {
			ltpSignUpImpl.setMobileNumber(MobileNumber);
		}

		if (CreatedDate == Long.MIN_VALUE) {
			ltpSignUpImpl.setCreatedDate(null);
		}
		else {
			ltpSignUpImpl.setCreatedDate(new Date(CreatedDate));
		}

		if (UpdatedDate == Long.MIN_VALUE) {
			ltpSignUpImpl.setUpdatedDate(null);
		}
		else {
			ltpSignUpImpl.setUpdatedDate(new Date(UpdatedDate));
		}

		ltpSignUpImpl.resetOriginalValues();

		return ltpSignUpImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ID = objectInput.readLong();
		FirstName = objectInput.readUTF();
		LastName = objectInput.readUTF();
		MobileNumber = objectInput.readUTF();
		CreatedDate = objectInput.readLong();
		UpdatedDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ID);

		if (FirstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(FirstName);
		}

		if (LastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(LastName);
		}

		if (MobileNumber == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(MobileNumber);
		}

		objectOutput.writeLong(CreatedDate);
		objectOutput.writeLong(UpdatedDate);
	}

	public long ID;
	public String FirstName;
	public String LastName;
	public String MobileNumber;
	public long CreatedDate;
	public long UpdatedDate;
}