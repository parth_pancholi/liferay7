/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model.impl;

import LTPDatabase.model.LTPSignUpPreferences;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing LTPSignUpPreferences in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpPreferences
 * @generated
 */
@ProviderType
public class LTPSignUpPreferencesCacheModel implements CacheModel<LTPSignUpPreferences>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LTPSignUpPreferencesCacheModel)) {
			return false;
		}

		LTPSignUpPreferencesCacheModel ltpSignUpPreferencesCacheModel = (LTPSignUpPreferencesCacheModel)obj;

		if (ID == ltpSignUpPreferencesCacheModel.ID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(9);

		sb.append("{ID=");
		sb.append(ID);
		sb.append(", MorningWalkTitle=");
		sb.append(MorningWalkTitle);
		sb.append(", DinnerPrasadTitle=");
		sb.append(DinnerPrasadTitle);
		sb.append(", VolunteerTitle=");
		sb.append(VolunteerTitle);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public LTPSignUpPreferences toEntityModel() {
		LTPSignUpPreferencesImpl ltpSignUpPreferencesImpl = new LTPSignUpPreferencesImpl();

		ltpSignUpPreferencesImpl.setID(ID);

		if (MorningWalkTitle == null) {
			ltpSignUpPreferencesImpl.setMorningWalkTitle(StringPool.BLANK);
		}
		else {
			ltpSignUpPreferencesImpl.setMorningWalkTitle(MorningWalkTitle);
		}

		if (DinnerPrasadTitle == null) {
			ltpSignUpPreferencesImpl.setDinnerPrasadTitle(StringPool.BLANK);
		}
		else {
			ltpSignUpPreferencesImpl.setDinnerPrasadTitle(DinnerPrasadTitle);
		}

		if (VolunteerTitle == null) {
			ltpSignUpPreferencesImpl.setVolunteerTitle(StringPool.BLANK);
		}
		else {
			ltpSignUpPreferencesImpl.setVolunteerTitle(VolunteerTitle);
		}

		ltpSignUpPreferencesImpl.resetOriginalValues();

		return ltpSignUpPreferencesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ID = objectInput.readLong();
		MorningWalkTitle = objectInput.readUTF();
		DinnerPrasadTitle = objectInput.readUTF();
		VolunteerTitle = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ID);

		if (MorningWalkTitle == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(MorningWalkTitle);
		}

		if (DinnerPrasadTitle == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(DinnerPrasadTitle);
		}

		if (VolunteerTitle == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(VolunteerTitle);
		}
	}

	public long ID;
	public String MorningWalkTitle;
	public String DinnerPrasadTitle;
	public String VolunteerTitle;
}