/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model.impl;

import LTPDatabase.model.LTPSignUp_LTPMasterData;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing LTPSignUp_LTPMasterData in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUp_LTPMasterData
 * @generated
 */
@ProviderType
public class LTPSignUp_LTPMasterDataCacheModel implements CacheModel<LTPSignUp_LTPMasterData>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LTPSignUp_LTPMasterDataCacheModel)) {
			return false;
		}

		LTPSignUp_LTPMasterDataCacheModel ltpSignUp_LTPMasterDataCacheModel = (LTPSignUp_LTPMasterDataCacheModel)obj;

		if (ID == ltpSignUp_LTPMasterDataCacheModel.ID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{ID=");
		sb.append(ID);
		sb.append(", LTPSignUpID=");
		sb.append(LTPSignUpID);
		sb.append(", LTPMasterDataID=");
		sb.append(LTPMasterDataID);
		sb.append(", DinnerPrasadItem1=");
		sb.append(DinnerPrasadItem1);
		sb.append(", DinnerPrasadItem2=");
		sb.append(DinnerPrasadItem2);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public LTPSignUp_LTPMasterData toEntityModel() {
		LTPSignUp_LTPMasterDataImpl ltpSignUp_LTPMasterDataImpl = new LTPSignUp_LTPMasterDataImpl();

		ltpSignUp_LTPMasterDataImpl.setID(ID);
		ltpSignUp_LTPMasterDataImpl.setLTPSignUpID(LTPSignUpID);
		ltpSignUp_LTPMasterDataImpl.setLTPMasterDataID(LTPMasterDataID);

		if (DinnerPrasadItem1 == null) {
			ltpSignUp_LTPMasterDataImpl.setDinnerPrasadItem1(StringPool.BLANK);
		}
		else {
			ltpSignUp_LTPMasterDataImpl.setDinnerPrasadItem1(DinnerPrasadItem1);
		}

		if (DinnerPrasadItem2 == null) {
			ltpSignUp_LTPMasterDataImpl.setDinnerPrasadItem2(StringPool.BLANK);
		}
		else {
			ltpSignUp_LTPMasterDataImpl.setDinnerPrasadItem2(DinnerPrasadItem2);
		}

		ltpSignUp_LTPMasterDataImpl.resetOriginalValues();

		return ltpSignUp_LTPMasterDataImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ID = objectInput.readLong();

		LTPSignUpID = objectInput.readLong();

		LTPMasterDataID = objectInput.readLong();
		DinnerPrasadItem1 = objectInput.readUTF();
		DinnerPrasadItem2 = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ID);

		objectOutput.writeLong(LTPSignUpID);

		objectOutput.writeLong(LTPMasterDataID);

		if (DinnerPrasadItem1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(DinnerPrasadItem1);
		}

		if (DinnerPrasadItem2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(DinnerPrasadItem2);
		}
	}

	public long ID;
	public long LTPSignUpID;
	public long LTPMasterDataID;
	public String DinnerPrasadItem1;
	public String DinnerPrasadItem2;
}