/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.impl;

import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.Validator;

import LTPDatabase.model.LTPSignUp;
import LTPDatabase.model.LTPSignUp_LTPMasterData;
import LTPDatabase.service.base.LTPSignUpLocalServiceBaseImpl;
import LTPDatabase.service.persistence.LTPSignUpPersistence;
import LTPDatabase.service.persistence.LTPSignUp_LTPMasterDataPersistence;

/**
 * The implementation of the LTPSignUp local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link LTPDatabase.service.LTPSignUpLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpLocalServiceBaseImpl
 * @see LTPDatabase.service.LTPSignUpLocalServiceUtil
 */
public class LTPSignUpLocalServiceImpl extends LTPSignUpLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * LTPDatabase.service.LTPSignUpLocalServiceUtil} to access the LTPSignUp
	 * local service.
	 */
	@BeanReference(type = LTPSignUpPersistence.class)
	protected LTPSignUpPersistence ltpSignUpPersistence;

	@BeanReference(type = LTPSignUp_LTPMasterDataPersistence.class)
	protected LTPSignUp_LTPMasterDataPersistence ltpSignUp_LTPMasterDataPersistence;

	/* (non-Javadoc)
	 * @see LTPDatabase.service.LTPSignUpLocalService#addLTPSignUp(java.lang.String, java.lang.String, java.lang.String, java.util.Map)
	 */
	@Transactional
	public LTPSignUp addLTPSignUp(String firstName, String lastName, String mobileNumber, Map<Integer, String> ltpMap)
			throws PortalException {

		long ltpSignUpId = counterLocalService.increment();
		LTPSignUp ltpSignUpEntry = ltpSignUpPersistence.create(ltpSignUpId);
		ltpSignUpEntry.setID(ltpSignUpId);
		ltpSignUpEntry.setFirstName(firstName);
		ltpSignUpEntry.setLastName(lastName);
		ltpSignUpEntry.setMobileNumber(mobileNumber);
		Date createdDate = new Date();
		ltpSignUpEntry.setCreatedDate(createdDate);
		ltpSignUpEntry.setUpdatedDate(createdDate);
		ltpSignUpPersistence.update(ltpSignUpEntry);

		// Add LTPsignUp_LTPsignUpMasterData
		for (Entry<Integer, String> entry : ltpMap.entrySet()) {
			System.out.println(entry.getKey() + "/" + entry.getValue());
			long ltpSignUp_LTPMasterDataId = counterLocalService.increment();
			LTPSignUp_LTPMasterData ltpSignUp_LTPMasterDataEntry = ltpSignUp_LTPMasterDataPersistence
					.create(ltpSignUp_LTPMasterDataId);
			ltpSignUp_LTPMasterDataEntry.setID(ltpSignUp_LTPMasterDataId);
			ltpSignUp_LTPMasterDataEntry.setLTPSignUpID(ltpSignUpId);
			ltpSignUp_LTPMasterDataEntry.setLTPMasterDataID(entry.getKey());
			ltpSignUp_LTPMasterDataEntry.setDinnerPrasadItem2(entry.getValue().split("##")[0]);
			if(entry.getValue().split("##").length > 1) {
				ltpSignUp_LTPMasterDataEntry.setDinnerPrasadItem1(entry.getValue().split("##")[1]);
			}
			ltpSignUp_LTPMasterDataPersistence.update(ltpSignUp_LTPMasterDataEntry);
		}

		return ltpSignUpEntry;
	}
	
	@Transactional
	public LTPSignUp updateLTPSignUp(String firstName, String lastName, String mobileNumber, Map<Integer, String> ltpMap, long ltpSignUpId)
			throws PortalException {

		LTPSignUp ltpSignUpEntry = ltpSignUpPersistence.fetchByPrimaryKey(ltpSignUpId);
		ltpSignUpEntry.setID(ltpSignUpId);
		ltpSignUpEntry.setFirstName(firstName);
		ltpSignUpEntry.setLastName(lastName);
		ltpSignUpEntry.setMobileNumber(mobileNumber);
		Date createdDate = new Date();
		ltpSignUpEntry.setUpdatedDate(createdDate);
		ltpSignUpPersistence.update(ltpSignUpEntry);

		// Update LTPsignUp_LTPsignUpMasterData
		ltpSignUp_LTPMasterDataPersistence.findByLTPSignUpID(ltpSignUpId).stream()
		.forEach(ltpSignUp_LTPMasterData -> {
			ltpSignUp_LTPMasterDataPersistence.remove(ltpSignUp_LTPMasterData);
		});
		
		for (Entry<Integer, String> entry : ltpMap.entrySet()) {
			System.out.println(entry.getKey() + "/" + entry.getValue());
			long ltpSignUp_LTPMasterDataId = counterLocalService.increment();
			LTPSignUp_LTPMasterData ltpSignUp_LTPMasterDataEntry = ltpSignUp_LTPMasterDataPersistence
					.create(ltpSignUp_LTPMasterDataId);
			ltpSignUp_LTPMasterDataEntry.setID(ltpSignUp_LTPMasterDataId);
			ltpSignUp_LTPMasterDataEntry.setLTPSignUpID(ltpSignUpId);
			ltpSignUp_LTPMasterDataEntry.setLTPMasterDataID(entry.getKey());
			ltpSignUp_LTPMasterDataEntry.setDinnerPrasadItem2(entry.getValue().split("##")[0]);
			if(entry.getValue().split("##").length > 1) {
				ltpSignUp_LTPMasterDataEntry.setDinnerPrasadItem1(entry.getValue().split("##")[1]);
			}
			ltpSignUp_LTPMasterDataPersistence.update(ltpSignUp_LTPMasterDataEntry);
		}

		return ltpSignUpEntry;
	}

	/**
	 * @param id
	 * @return
	 */
	@Transactional
	public LTPSignUp deleteLTPSignUpWithDependents(long id){
		LTPSignUp ltpSignUp = ltpSignUpPersistence.fetchByPrimaryKey(id);
		ltpSignUp_LTPMasterDataPersistence.findByLTPSignUpID(ltpSignUp.getID()).stream()
				.forEach(ltpSignUp_LTPMasterData -> {
					ltpSignUp_LTPMasterDataPersistence.remove(ltpSignUp_LTPMasterData);
				});
		ltpSignUp = ltpSignUpPersistence.remove(ltpSignUp);
		return ltpSignUp;
	}
}