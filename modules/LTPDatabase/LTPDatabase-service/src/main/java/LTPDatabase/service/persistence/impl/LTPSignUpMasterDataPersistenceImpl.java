/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.persistence.impl;

import LTPDatabase.exception.NoSuchLTPSignUpMasterDataException;

import LTPDatabase.model.LTPSignUpMasterData;

import LTPDatabase.model.impl.LTPSignUpMasterDataImpl;
import LTPDatabase.model.impl.LTPSignUpMasterDataModelImpl;

import LTPDatabase.service.persistence.LTPSignUpMasterDataPersistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the LTPSignUpMasterData service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpMasterDataPersistence
 * @see LTPDatabase.service.persistence.LTPSignUpMasterDataUtil
 * @generated
 */
@ProviderType
public class LTPSignUpMasterDataPersistenceImpl extends BasePersistenceImpl<LTPSignUpMasterData>
	implements LTPSignUpMasterDataPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LTPSignUpMasterDataUtil} to access the LTPSignUpMasterData persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LTPSignUpMasterDataImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpMasterDataModelImpl.FINDER_CACHE_ENABLED,
			LTPSignUpMasterDataImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpMasterDataModelImpl.FINDER_CACHE_ENABLED,
			LTPSignUpMasterDataImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpMasterDataModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public LTPSignUpMasterDataPersistenceImpl() {
		setModelClass(LTPSignUpMasterData.class);
	}

	/**
	 * Caches the LTPSignUpMasterData in the entity cache if it is enabled.
	 *
	 * @param ltpSignUpMasterData the LTPSignUpMasterData
	 */
	@Override
	public void cacheResult(LTPSignUpMasterData ltpSignUpMasterData) {
		entityCache.putResult(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpMasterDataImpl.class, ltpSignUpMasterData.getPrimaryKey(),
			ltpSignUpMasterData);

		ltpSignUpMasterData.resetOriginalValues();
	}

	/**
	 * Caches the LTPSignUpMasterDatas in the entity cache if it is enabled.
	 *
	 * @param ltpSignUpMasterDatas the LTPSignUpMasterDatas
	 */
	@Override
	public void cacheResult(List<LTPSignUpMasterData> ltpSignUpMasterDatas) {
		for (LTPSignUpMasterData ltpSignUpMasterData : ltpSignUpMasterDatas) {
			if (entityCache.getResult(
						LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
						LTPSignUpMasterDataImpl.class,
						ltpSignUpMasterData.getPrimaryKey()) == null) {
				cacheResult(ltpSignUpMasterData);
			}
			else {
				ltpSignUpMasterData.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all LTPSignUpMasterDatas.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(LTPSignUpMasterDataImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the LTPSignUpMasterData.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LTPSignUpMasterData ltpSignUpMasterData) {
		entityCache.removeResult(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpMasterDataImpl.class, ltpSignUpMasterData.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<LTPSignUpMasterData> ltpSignUpMasterDatas) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LTPSignUpMasterData ltpSignUpMasterData : ltpSignUpMasterDatas) {
			entityCache.removeResult(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
				LTPSignUpMasterDataImpl.class,
				ltpSignUpMasterData.getPrimaryKey());
		}
	}

	/**
	 * Creates a new LTPSignUpMasterData with the primary key. Does not add the LTPSignUpMasterData to the database.
	 *
	 * @param ID the primary key for the new LTPSignUpMasterData
	 * @return the new LTPSignUpMasterData
	 */
	@Override
	public LTPSignUpMasterData create(long ID) {
		LTPSignUpMasterData ltpSignUpMasterData = new LTPSignUpMasterDataImpl();

		ltpSignUpMasterData.setNew(true);
		ltpSignUpMasterData.setPrimaryKey(ID);

		return ltpSignUpMasterData;
	}

	/**
	 * Removes the LTPSignUpMasterData with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ID the primary key of the LTPSignUpMasterData
	 * @return the LTPSignUpMasterData that was removed
	 * @throws NoSuchLTPSignUpMasterDataException if a LTPSignUpMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUpMasterData remove(long ID)
		throws NoSuchLTPSignUpMasterDataException {
		return remove((Serializable)ID);
	}

	/**
	 * Removes the LTPSignUpMasterData with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the LTPSignUpMasterData
	 * @return the LTPSignUpMasterData that was removed
	 * @throws NoSuchLTPSignUpMasterDataException if a LTPSignUpMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUpMasterData remove(Serializable primaryKey)
		throws NoSuchLTPSignUpMasterDataException {
		Session session = null;

		try {
			session = openSession();

			LTPSignUpMasterData ltpSignUpMasterData = (LTPSignUpMasterData)session.get(LTPSignUpMasterDataImpl.class,
					primaryKey);

			if (ltpSignUpMasterData == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLTPSignUpMasterDataException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ltpSignUpMasterData);
		}
		catch (NoSuchLTPSignUpMasterDataException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LTPSignUpMasterData removeImpl(
		LTPSignUpMasterData ltpSignUpMasterData) {
		ltpSignUpMasterData = toUnwrappedModel(ltpSignUpMasterData);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ltpSignUpMasterData)) {
				ltpSignUpMasterData = (LTPSignUpMasterData)session.get(LTPSignUpMasterDataImpl.class,
						ltpSignUpMasterData.getPrimaryKeyObj());
			}

			if (ltpSignUpMasterData != null) {
				session.delete(ltpSignUpMasterData);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ltpSignUpMasterData != null) {
			clearCache(ltpSignUpMasterData);
		}

		return ltpSignUpMasterData;
	}

	@Override
	public LTPSignUpMasterData updateImpl(
		LTPSignUpMasterData ltpSignUpMasterData) {
		ltpSignUpMasterData = toUnwrappedModel(ltpSignUpMasterData);

		boolean isNew = ltpSignUpMasterData.isNew();

		Session session = null;

		try {
			session = openSession();

			if (ltpSignUpMasterData.isNew()) {
				session.save(ltpSignUpMasterData);

				ltpSignUpMasterData.setNew(false);
			}
			else {
				ltpSignUpMasterData = (LTPSignUpMasterData)session.merge(ltpSignUpMasterData);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpMasterDataImpl.class, ltpSignUpMasterData.getPrimaryKey(),
			ltpSignUpMasterData, false);

		ltpSignUpMasterData.resetOriginalValues();

		return ltpSignUpMasterData;
	}

	protected LTPSignUpMasterData toUnwrappedModel(
		LTPSignUpMasterData ltpSignUpMasterData) {
		if (ltpSignUpMasterData instanceof LTPSignUpMasterDataImpl) {
			return ltpSignUpMasterData;
		}

		LTPSignUpMasterDataImpl ltpSignUpMasterDataImpl = new LTPSignUpMasterDataImpl();

		ltpSignUpMasterDataImpl.setNew(ltpSignUpMasterData.isNew());
		ltpSignUpMasterDataImpl.setPrimaryKey(ltpSignUpMasterData.getPrimaryKey());

		ltpSignUpMasterDataImpl.setID(ltpSignUpMasterData.getID());
		ltpSignUpMasterDataImpl.setEventDate(ltpSignUpMasterData.getEventDate());
		ltpSignUpMasterDataImpl.setExtraText(ltpSignUpMasterData.getExtraText());
		ltpSignUpMasterDataImpl.setIsMorningWalk(ltpSignUpMasterData.isIsMorningWalk());
		ltpSignUpMasterDataImpl.setIsVolunteer(ltpSignUpMasterData.isIsVolunteer());
		ltpSignUpMasterDataImpl.setIsDinnerPrasad(ltpSignUpMasterData.isIsDinnerPrasad());

		return ltpSignUpMasterDataImpl;
	}

	/**
	 * Returns the LTPSignUpMasterData with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the LTPSignUpMasterData
	 * @return the LTPSignUpMasterData
	 * @throws NoSuchLTPSignUpMasterDataException if a LTPSignUpMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUpMasterData findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLTPSignUpMasterDataException {
		LTPSignUpMasterData ltpSignUpMasterData = fetchByPrimaryKey(primaryKey);

		if (ltpSignUpMasterData == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLTPSignUpMasterDataException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ltpSignUpMasterData;
	}

	/**
	 * Returns the LTPSignUpMasterData with the primary key or throws a {@link NoSuchLTPSignUpMasterDataException} if it could not be found.
	 *
	 * @param ID the primary key of the LTPSignUpMasterData
	 * @return the LTPSignUpMasterData
	 * @throws NoSuchLTPSignUpMasterDataException if a LTPSignUpMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUpMasterData findByPrimaryKey(long ID)
		throws NoSuchLTPSignUpMasterDataException {
		return findByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns the LTPSignUpMasterData with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the LTPSignUpMasterData
	 * @return the LTPSignUpMasterData, or <code>null</code> if a LTPSignUpMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUpMasterData fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
				LTPSignUpMasterDataImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		LTPSignUpMasterData ltpSignUpMasterData = (LTPSignUpMasterData)serializable;

		if (ltpSignUpMasterData == null) {
			Session session = null;

			try {
				session = openSession();

				ltpSignUpMasterData = (LTPSignUpMasterData)session.get(LTPSignUpMasterDataImpl.class,
						primaryKey);

				if (ltpSignUpMasterData != null) {
					cacheResult(ltpSignUpMasterData);
				}
				else {
					entityCache.putResult(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
						LTPSignUpMasterDataImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUpMasterDataImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ltpSignUpMasterData;
	}

	/**
	 * Returns the LTPSignUpMasterData with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ID the primary key of the LTPSignUpMasterData
	 * @return the LTPSignUpMasterData, or <code>null</code> if a LTPSignUpMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUpMasterData fetchByPrimaryKey(long ID) {
		return fetchByPrimaryKey((Serializable)ID);
	}

	@Override
	public Map<Serializable, LTPSignUpMasterData> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, LTPSignUpMasterData> map = new HashMap<Serializable, LTPSignUpMasterData>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			LTPSignUpMasterData ltpSignUpMasterData = fetchByPrimaryKey(primaryKey);

			if (ltpSignUpMasterData != null) {
				map.put(primaryKey, ltpSignUpMasterData);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUpMasterDataImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (LTPSignUpMasterData)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_LTPSIGNUPMASTERDATA_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (LTPSignUpMasterData ltpSignUpMasterData : (List<LTPSignUpMasterData>)q.list()) {
				map.put(ltpSignUpMasterData.getPrimaryKeyObj(),
					ltpSignUpMasterData);

				cacheResult(ltpSignUpMasterData);

				uncachedPrimaryKeys.remove(ltpSignUpMasterData.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(LTPSignUpMasterDataModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUpMasterDataImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the LTPSignUpMasterDatas.
	 *
	 * @return the LTPSignUpMasterDatas
	 */
	@Override
	public List<LTPSignUpMasterData> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the LTPSignUpMasterDatas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUpMasterDatas
	 * @param end the upper bound of the range of LTPSignUpMasterDatas (not inclusive)
	 * @return the range of LTPSignUpMasterDatas
	 */
	@Override
	public List<LTPSignUpMasterData> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the LTPSignUpMasterDatas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUpMasterDatas
	 * @param end the upper bound of the range of LTPSignUpMasterDatas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of LTPSignUpMasterDatas
	 */
	@Override
	public List<LTPSignUpMasterData> findAll(int start, int end,
		OrderByComparator<LTPSignUpMasterData> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the LTPSignUpMasterDatas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUpMasterDatas
	 * @param end the upper bound of the range of LTPSignUpMasterDatas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of LTPSignUpMasterDatas
	 */
	@Override
	public List<LTPSignUpMasterData> findAll(int start, int end,
		OrderByComparator<LTPSignUpMasterData> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LTPSignUpMasterData> list = null;

		if (retrieveFromCache) {
			list = (List<LTPSignUpMasterData>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_LTPSIGNUPMASTERDATA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LTPSIGNUPMASTERDATA;

				if (pagination) {
					sql = sql.concat(LTPSignUpMasterDataModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<LTPSignUpMasterData>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LTPSignUpMasterData>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the LTPSignUpMasterDatas from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (LTPSignUpMasterData ltpSignUpMasterData : findAll()) {
			remove(ltpSignUpMasterData);
		}
	}

	/**
	 * Returns the number of LTPSignUpMasterDatas.
	 *
	 * @return the number of LTPSignUpMasterDatas
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LTPSIGNUPMASTERDATA);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return LTPSignUpMasterDataModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the LTPSignUpMasterData persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(LTPSignUpMasterDataImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_LTPSIGNUPMASTERDATA = "SELECT ltpSignUpMasterData FROM LTPSignUpMasterData ltpSignUpMasterData";
	private static final String _SQL_SELECT_LTPSIGNUPMASTERDATA_WHERE_PKS_IN = "SELECT ltpSignUpMasterData FROM LTPSignUpMasterData ltpSignUpMasterData WHERE ID IN (";
	private static final String _SQL_COUNT_LTPSIGNUPMASTERDATA = "SELECT COUNT(ltpSignUpMasterData) FROM LTPSignUpMasterData ltpSignUpMasterData";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ltpSignUpMasterData.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LTPSignUpMasterData exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(LTPSignUpMasterDataPersistenceImpl.class);
}