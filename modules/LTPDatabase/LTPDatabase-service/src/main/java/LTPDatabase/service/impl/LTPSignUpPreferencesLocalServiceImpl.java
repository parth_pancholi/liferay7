/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.impl;

import java.util.List;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.Validator;

import LTPDatabase.model.LTPSignUpPreferences;
import LTPDatabase.service.base.LTPSignUpPreferencesLocalServiceBaseImpl;
import LTPDatabase.service.persistence.LTPSignUpPreferencesPersistence;

/**
 * The implementation of the LTPSignUpPreferences local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link LTPDatabase.service.LTPSignUpPreferencesLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpPreferencesLocalServiceBaseImpl
 * @see LTPDatabase.service.LTPSignUpPreferencesLocalServiceUtil
 */
public class LTPSignUpPreferencesLocalServiceImpl extends LTPSignUpPreferencesLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * LTPDatabase.service.LTPSignUpPreferencesLocalServiceUtil} to access the
	 * LTPSignUpPreferences local service.
	 */

	/**
	 * 
	 */
	@BeanReference(type = LTPSignUpPreferencesPersistence.class)
	protected LTPSignUpPreferencesPersistence ltpSignUpPreferencesPersistence;

	/**
	 * @param entry
	 * @return
	 * @throws PortalException
	 */
	public LTPSignUpPreferences deleteLTPSignUpPreference(LTPSignUpPreferences entry) throws PortalException {
		ltpSignUpPreferencesPersistence.remove(entry);
		return entry;
	}

	/**
	 * @param start
	 * @param end
	 * @return
	 */
	public List<LTPSignUpPreferences> getLTPSignUpPreferences(int start, int end) {
		return ltpSignUpPreferencesPersistence.findAll(start, end);
	}

	/**
	 * @return
	 */
	public int getLTPSignUpPreferencesCount() {
		return ltpSignUpPreferencesPersistence.countAll();
	}

	/**
	 * @param morningWalkTitle
	 * @param dinnerPrasadTitle
	 * @param volunteerTitle
	 * @param ltpSignUpPreferenceId
	 * @return
	 * @throws PortalException
	 */
	public LTPSignUpPreferences addUpdateLTPSignUpPreferences(String morningWalkTitle, String dinnerPrasadTitle,
			String volunteerTitle, long ltpSignUpPreferenceId) throws PortalException {

		LTPSignUpPreferences ltpSignUpPreferenceEntry = null;
		if(ltpSignUpPreferenceId != 0L) {
			ltpSignUpPreferenceEntry = ltpSignUpPreferencesPersistence.findByPrimaryKey(ltpSignUpPreferenceId);
		}else {
			ltpSignUpPreferenceId = counterLocalService.increment();
			ltpSignUpPreferenceEntry = ltpSignUpPreferencesPersistence.create(ltpSignUpPreferenceId);
		}
		ltpSignUpPreferenceEntry.setID(ltpSignUpPreferenceId);
		ltpSignUpPreferenceEntry.setMorningWalkTitle(morningWalkTitle);
		ltpSignUpPreferenceEntry.setDinnerPrasadTitle(dinnerPrasadTitle);
		ltpSignUpPreferenceEntry.setVolunteerTitle(volunteerTitle);

		ltpSignUpPreferencesPersistence.update(ltpSignUpPreferenceEntry);

		return ltpSignUpPreferenceEntry;
	}
}