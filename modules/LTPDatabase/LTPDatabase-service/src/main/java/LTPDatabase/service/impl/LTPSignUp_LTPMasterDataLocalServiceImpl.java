/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.impl;

import java.util.List;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.transaction.Transactional;

import LTPDatabase.model.LTPSignUp_LTPMasterData;
import LTPDatabase.service.base.LTPSignUp_LTPMasterDataLocalServiceBaseImpl;
import LTPDatabase.service.persistence.LTPSignUp_LTPMasterDataPersistence;

/**
 * The implementation of the LTPSignUp_LTPMasterData local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link LTPDatabase.service.LTPSignUp_LTPMasterDataLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUp_LTPMasterDataLocalServiceBaseImpl
 * @see LTPDatabase.service.LTPSignUp_LTPMasterDataLocalServiceUtil
 */
public class LTPSignUp_LTPMasterDataLocalServiceImpl
	extends LTPSignUp_LTPMasterDataLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link LTPDatabase.service.LTPSignUp_LTPMasterDataLocalServiceUtil} to access the LTPSignUp_LTPMasterData local service.
	 */
	
	@BeanReference(type = LTPSignUp_LTPMasterDataPersistence.class)
	protected LTPSignUp_LTPMasterDataPersistence ltpSignUp_LTPMasterDataPersistence;
	
	@Transactional
	public List<LTPSignUp_LTPMasterData> findByLTPSignUpID(long ltpSignUpId){
		return ltpSignUp_LTPMasterDataPersistence.findByLTPSignUpID(ltpSignUpId);
	}
	
	@Transactional
	public List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(long ltpSignUpMasterDataId){
		return ltpSignUp_LTPMasterDataPersistence.findByLTPSignUpMasterDataId(ltpSignUpMasterDataId);
	}
}