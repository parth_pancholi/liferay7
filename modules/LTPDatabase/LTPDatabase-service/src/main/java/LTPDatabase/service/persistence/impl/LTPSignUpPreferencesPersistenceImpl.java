/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.persistence.impl;

import LTPDatabase.exception.NoSuchLTPSignUpPreferencesException;

import LTPDatabase.model.LTPSignUpPreferences;

import LTPDatabase.model.impl.LTPSignUpPreferencesImpl;
import LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl;

import LTPDatabase.service.persistence.LTPSignUpPreferencesPersistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the LTPSignUpPreferences service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpPreferencesPersistence
 * @see LTPDatabase.service.persistence.LTPSignUpPreferencesUtil
 * @generated
 */
@ProviderType
public class LTPSignUpPreferencesPersistenceImpl extends BasePersistenceImpl<LTPSignUpPreferences>
	implements LTPSignUpPreferencesPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LTPSignUpPreferencesUtil} to access the LTPSignUpPreferences persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LTPSignUpPreferencesImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpPreferencesModelImpl.FINDER_CACHE_ENABLED,
			LTPSignUpPreferencesImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpPreferencesModelImpl.FINDER_CACHE_ENABLED,
			LTPSignUpPreferencesImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpPreferencesModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public LTPSignUpPreferencesPersistenceImpl() {
		setModelClass(LTPSignUpPreferences.class);
	}

	/**
	 * Caches the LTPSignUpPreferences in the entity cache if it is enabled.
	 *
	 * @param ltpSignUpPreferences the LTPSignUpPreferences
	 */
	@Override
	public void cacheResult(LTPSignUpPreferences ltpSignUpPreferences) {
		entityCache.putResult(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpPreferencesImpl.class,
			ltpSignUpPreferences.getPrimaryKey(), ltpSignUpPreferences);

		ltpSignUpPreferences.resetOriginalValues();
	}

	/**
	 * Caches the LTPSignUpPreferenceses in the entity cache if it is enabled.
	 *
	 * @param ltpSignUpPreferenceses the LTPSignUpPreferenceses
	 */
	@Override
	public void cacheResult(List<LTPSignUpPreferences> ltpSignUpPreferenceses) {
		for (LTPSignUpPreferences ltpSignUpPreferences : ltpSignUpPreferenceses) {
			if (entityCache.getResult(
						LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
						LTPSignUpPreferencesImpl.class,
						ltpSignUpPreferences.getPrimaryKey()) == null) {
				cacheResult(ltpSignUpPreferences);
			}
			else {
				ltpSignUpPreferences.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all LTPSignUpPreferenceses.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(LTPSignUpPreferencesImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the LTPSignUpPreferences.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LTPSignUpPreferences ltpSignUpPreferences) {
		entityCache.removeResult(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpPreferencesImpl.class, ltpSignUpPreferences.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<LTPSignUpPreferences> ltpSignUpPreferenceses) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LTPSignUpPreferences ltpSignUpPreferences : ltpSignUpPreferenceses) {
			entityCache.removeResult(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
				LTPSignUpPreferencesImpl.class,
				ltpSignUpPreferences.getPrimaryKey());
		}
	}

	/**
	 * Creates a new LTPSignUpPreferences with the primary key. Does not add the LTPSignUpPreferences to the database.
	 *
	 * @param ID the primary key for the new LTPSignUpPreferences
	 * @return the new LTPSignUpPreferences
	 */
	@Override
	public LTPSignUpPreferences create(long ID) {
		LTPSignUpPreferences ltpSignUpPreferences = new LTPSignUpPreferencesImpl();

		ltpSignUpPreferences.setNew(true);
		ltpSignUpPreferences.setPrimaryKey(ID);

		return ltpSignUpPreferences;
	}

	/**
	 * Removes the LTPSignUpPreferences with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ID the primary key of the LTPSignUpPreferences
	 * @return the LTPSignUpPreferences that was removed
	 * @throws NoSuchLTPSignUpPreferencesException if a LTPSignUpPreferences with the primary key could not be found
	 */
	@Override
	public LTPSignUpPreferences remove(long ID)
		throws NoSuchLTPSignUpPreferencesException {
		return remove((Serializable)ID);
	}

	/**
	 * Removes the LTPSignUpPreferences with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the LTPSignUpPreferences
	 * @return the LTPSignUpPreferences that was removed
	 * @throws NoSuchLTPSignUpPreferencesException if a LTPSignUpPreferences with the primary key could not be found
	 */
	@Override
	public LTPSignUpPreferences remove(Serializable primaryKey)
		throws NoSuchLTPSignUpPreferencesException {
		Session session = null;

		try {
			session = openSession();

			LTPSignUpPreferences ltpSignUpPreferences = (LTPSignUpPreferences)session.get(LTPSignUpPreferencesImpl.class,
					primaryKey);

			if (ltpSignUpPreferences == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLTPSignUpPreferencesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ltpSignUpPreferences);
		}
		catch (NoSuchLTPSignUpPreferencesException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LTPSignUpPreferences removeImpl(
		LTPSignUpPreferences ltpSignUpPreferences) {
		ltpSignUpPreferences = toUnwrappedModel(ltpSignUpPreferences);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ltpSignUpPreferences)) {
				ltpSignUpPreferences = (LTPSignUpPreferences)session.get(LTPSignUpPreferencesImpl.class,
						ltpSignUpPreferences.getPrimaryKeyObj());
			}

			if (ltpSignUpPreferences != null) {
				session.delete(ltpSignUpPreferences);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ltpSignUpPreferences != null) {
			clearCache(ltpSignUpPreferences);
		}

		return ltpSignUpPreferences;
	}

	@Override
	public LTPSignUpPreferences updateImpl(
		LTPSignUpPreferences ltpSignUpPreferences) {
		ltpSignUpPreferences = toUnwrappedModel(ltpSignUpPreferences);

		boolean isNew = ltpSignUpPreferences.isNew();

		Session session = null;

		try {
			session = openSession();

			if (ltpSignUpPreferences.isNew()) {
				session.save(ltpSignUpPreferences);

				ltpSignUpPreferences.setNew(false);
			}
			else {
				ltpSignUpPreferences = (LTPSignUpPreferences)session.merge(ltpSignUpPreferences);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpPreferencesImpl.class,
			ltpSignUpPreferences.getPrimaryKey(), ltpSignUpPreferences, false);

		ltpSignUpPreferences.resetOriginalValues();

		return ltpSignUpPreferences;
	}

	protected LTPSignUpPreferences toUnwrappedModel(
		LTPSignUpPreferences ltpSignUpPreferences) {
		if (ltpSignUpPreferences instanceof LTPSignUpPreferencesImpl) {
			return ltpSignUpPreferences;
		}

		LTPSignUpPreferencesImpl ltpSignUpPreferencesImpl = new LTPSignUpPreferencesImpl();

		ltpSignUpPreferencesImpl.setNew(ltpSignUpPreferences.isNew());
		ltpSignUpPreferencesImpl.setPrimaryKey(ltpSignUpPreferences.getPrimaryKey());

		ltpSignUpPreferencesImpl.setID(ltpSignUpPreferences.getID());
		ltpSignUpPreferencesImpl.setMorningWalkTitle(ltpSignUpPreferences.getMorningWalkTitle());
		ltpSignUpPreferencesImpl.setDinnerPrasadTitle(ltpSignUpPreferences.getDinnerPrasadTitle());
		ltpSignUpPreferencesImpl.setVolunteerTitle(ltpSignUpPreferences.getVolunteerTitle());

		return ltpSignUpPreferencesImpl;
	}

	/**
	 * Returns the LTPSignUpPreferences with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the LTPSignUpPreferences
	 * @return the LTPSignUpPreferences
	 * @throws NoSuchLTPSignUpPreferencesException if a LTPSignUpPreferences with the primary key could not be found
	 */
	@Override
	public LTPSignUpPreferences findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLTPSignUpPreferencesException {
		LTPSignUpPreferences ltpSignUpPreferences = fetchByPrimaryKey(primaryKey);

		if (ltpSignUpPreferences == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLTPSignUpPreferencesException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ltpSignUpPreferences;
	}

	/**
	 * Returns the LTPSignUpPreferences with the primary key or throws a {@link NoSuchLTPSignUpPreferencesException} if it could not be found.
	 *
	 * @param ID the primary key of the LTPSignUpPreferences
	 * @return the LTPSignUpPreferences
	 * @throws NoSuchLTPSignUpPreferencesException if a LTPSignUpPreferences with the primary key could not be found
	 */
	@Override
	public LTPSignUpPreferences findByPrimaryKey(long ID)
		throws NoSuchLTPSignUpPreferencesException {
		return findByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns the LTPSignUpPreferences with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the LTPSignUpPreferences
	 * @return the LTPSignUpPreferences, or <code>null</code> if a LTPSignUpPreferences with the primary key could not be found
	 */
	@Override
	public LTPSignUpPreferences fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
				LTPSignUpPreferencesImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		LTPSignUpPreferences ltpSignUpPreferences = (LTPSignUpPreferences)serializable;

		if (ltpSignUpPreferences == null) {
			Session session = null;

			try {
				session = openSession();

				ltpSignUpPreferences = (LTPSignUpPreferences)session.get(LTPSignUpPreferencesImpl.class,
						primaryKey);

				if (ltpSignUpPreferences != null) {
					cacheResult(ltpSignUpPreferences);
				}
				else {
					entityCache.putResult(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
						LTPSignUpPreferencesImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUpPreferencesImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ltpSignUpPreferences;
	}

	/**
	 * Returns the LTPSignUpPreferences with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ID the primary key of the LTPSignUpPreferences
	 * @return the LTPSignUpPreferences, or <code>null</code> if a LTPSignUpPreferences with the primary key could not be found
	 */
	@Override
	public LTPSignUpPreferences fetchByPrimaryKey(long ID) {
		return fetchByPrimaryKey((Serializable)ID);
	}

	@Override
	public Map<Serializable, LTPSignUpPreferences> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, LTPSignUpPreferences> map = new HashMap<Serializable, LTPSignUpPreferences>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			LTPSignUpPreferences ltpSignUpPreferences = fetchByPrimaryKey(primaryKey);

			if (ltpSignUpPreferences != null) {
				map.put(primaryKey, ltpSignUpPreferences);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUpPreferencesImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (LTPSignUpPreferences)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_LTPSIGNUPPREFERENCES_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (LTPSignUpPreferences ltpSignUpPreferences : (List<LTPSignUpPreferences>)q.list()) {
				map.put(ltpSignUpPreferences.getPrimaryKeyObj(),
					ltpSignUpPreferences);

				cacheResult(ltpSignUpPreferences);

				uncachedPrimaryKeys.remove(ltpSignUpPreferences.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(LTPSignUpPreferencesModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUpPreferencesImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the LTPSignUpPreferenceses.
	 *
	 * @return the LTPSignUpPreferenceses
	 */
	@Override
	public List<LTPSignUpPreferences> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the LTPSignUpPreferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUpPreferenceses
	 * @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	 * @return the range of LTPSignUpPreferenceses
	 */
	@Override
	public List<LTPSignUpPreferences> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the LTPSignUpPreferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUpPreferenceses
	 * @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of LTPSignUpPreferenceses
	 */
	@Override
	public List<LTPSignUpPreferences> findAll(int start, int end,
		OrderByComparator<LTPSignUpPreferences> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the LTPSignUpPreferenceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUpPreferenceses
	 * @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of LTPSignUpPreferenceses
	 */
	@Override
	public List<LTPSignUpPreferences> findAll(int start, int end,
		OrderByComparator<LTPSignUpPreferences> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LTPSignUpPreferences> list = null;

		if (retrieveFromCache) {
			list = (List<LTPSignUpPreferences>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_LTPSIGNUPPREFERENCES);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LTPSIGNUPPREFERENCES;

				if (pagination) {
					sql = sql.concat(LTPSignUpPreferencesModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<LTPSignUpPreferences>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LTPSignUpPreferences>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the LTPSignUpPreferenceses from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (LTPSignUpPreferences ltpSignUpPreferences : findAll()) {
			remove(ltpSignUpPreferences);
		}
	}

	/**
	 * Returns the number of LTPSignUpPreferenceses.
	 *
	 * @return the number of LTPSignUpPreferenceses
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LTPSIGNUPPREFERENCES);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return LTPSignUpPreferencesModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the LTPSignUpPreferences persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(LTPSignUpPreferencesImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_LTPSIGNUPPREFERENCES = "SELECT ltpSignUpPreferences FROM LTPSignUpPreferences ltpSignUpPreferences";
	private static final String _SQL_SELECT_LTPSIGNUPPREFERENCES_WHERE_PKS_IN = "SELECT ltpSignUpPreferences FROM LTPSignUpPreferences ltpSignUpPreferences WHERE ID IN (";
	private static final String _SQL_COUNT_LTPSIGNUPPREFERENCES = "SELECT COUNT(ltpSignUpPreferences) FROM LTPSignUpPreferences ltpSignUpPreferences";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ltpSignUpPreferences.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LTPSignUpPreferences exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(LTPSignUpPreferencesPersistenceImpl.class);
}