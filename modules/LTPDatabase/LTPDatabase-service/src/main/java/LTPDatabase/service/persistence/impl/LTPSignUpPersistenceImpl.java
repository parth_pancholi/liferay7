/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.persistence.impl;

import LTPDatabase.exception.NoSuchLTPSignUpException;

import LTPDatabase.model.LTPSignUp;

import LTPDatabase.model.impl.LTPSignUpImpl;
import LTPDatabase.model.impl.LTPSignUpModelImpl;

import LTPDatabase.service.persistence.LTPSignUpPersistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the LTPSignUp service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpPersistence
 * @see LTPDatabase.service.persistence.LTPSignUpUtil
 * @generated
 */
@ProviderType
public class LTPSignUpPersistenceImpl extends BasePersistenceImpl<LTPSignUp>
	implements LTPSignUpPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LTPSignUpUtil} to access the LTPSignUp persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LTPSignUpImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpModelImpl.FINDER_CACHE_ENABLED, LTPSignUpImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpModelImpl.FINDER_CACHE_ENABLED, LTPSignUpImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public LTPSignUpPersistenceImpl() {
		setModelClass(LTPSignUp.class);
	}

	/**
	 * Caches the LTPSignUp in the entity cache if it is enabled.
	 *
	 * @param ltpSignUp the LTPSignUp
	 */
	@Override
	public void cacheResult(LTPSignUp ltpSignUp) {
		entityCache.putResult(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpImpl.class, ltpSignUp.getPrimaryKey(), ltpSignUp);

		ltpSignUp.resetOriginalValues();
	}

	/**
	 * Caches the LTPSignUps in the entity cache if it is enabled.
	 *
	 * @param ltpSignUps the LTPSignUps
	 */
	@Override
	public void cacheResult(List<LTPSignUp> ltpSignUps) {
		for (LTPSignUp ltpSignUp : ltpSignUps) {
			if (entityCache.getResult(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
						LTPSignUpImpl.class, ltpSignUp.getPrimaryKey()) == null) {
				cacheResult(ltpSignUp);
			}
			else {
				ltpSignUp.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all LTPSignUps.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(LTPSignUpImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the LTPSignUp.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LTPSignUp ltpSignUp) {
		entityCache.removeResult(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpImpl.class, ltpSignUp.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<LTPSignUp> ltpSignUps) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LTPSignUp ltpSignUp : ltpSignUps) {
			entityCache.removeResult(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
				LTPSignUpImpl.class, ltpSignUp.getPrimaryKey());
		}
	}

	/**
	 * Creates a new LTPSignUp with the primary key. Does not add the LTPSignUp to the database.
	 *
	 * @param ID the primary key for the new LTPSignUp
	 * @return the new LTPSignUp
	 */
	@Override
	public LTPSignUp create(long ID) {
		LTPSignUp ltpSignUp = new LTPSignUpImpl();

		ltpSignUp.setNew(true);
		ltpSignUp.setPrimaryKey(ID);

		return ltpSignUp;
	}

	/**
	 * Removes the LTPSignUp with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ID the primary key of the LTPSignUp
	 * @return the LTPSignUp that was removed
	 * @throws NoSuchLTPSignUpException if a LTPSignUp with the primary key could not be found
	 */
	@Override
	public LTPSignUp remove(long ID) throws NoSuchLTPSignUpException {
		return remove((Serializable)ID);
	}

	/**
	 * Removes the LTPSignUp with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the LTPSignUp
	 * @return the LTPSignUp that was removed
	 * @throws NoSuchLTPSignUpException if a LTPSignUp with the primary key could not be found
	 */
	@Override
	public LTPSignUp remove(Serializable primaryKey)
		throws NoSuchLTPSignUpException {
		Session session = null;

		try {
			session = openSession();

			LTPSignUp ltpSignUp = (LTPSignUp)session.get(LTPSignUpImpl.class,
					primaryKey);

			if (ltpSignUp == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLTPSignUpException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ltpSignUp);
		}
		catch (NoSuchLTPSignUpException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LTPSignUp removeImpl(LTPSignUp ltpSignUp) {
		ltpSignUp = toUnwrappedModel(ltpSignUp);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ltpSignUp)) {
				ltpSignUp = (LTPSignUp)session.get(LTPSignUpImpl.class,
						ltpSignUp.getPrimaryKeyObj());
			}

			if (ltpSignUp != null) {
				session.delete(ltpSignUp);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ltpSignUp != null) {
			clearCache(ltpSignUp);
		}

		return ltpSignUp;
	}

	@Override
	public LTPSignUp updateImpl(LTPSignUp ltpSignUp) {
		ltpSignUp = toUnwrappedModel(ltpSignUp);

		boolean isNew = ltpSignUp.isNew();

		Session session = null;

		try {
			session = openSession();

			if (ltpSignUp.isNew()) {
				session.save(ltpSignUp);

				ltpSignUp.setNew(false);
			}
			else {
				ltpSignUp = (LTPSignUp)session.merge(ltpSignUp);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUpImpl.class, ltpSignUp.getPrimaryKey(), ltpSignUp, false);

		ltpSignUp.resetOriginalValues();

		return ltpSignUp;
	}

	protected LTPSignUp toUnwrappedModel(LTPSignUp ltpSignUp) {
		if (ltpSignUp instanceof LTPSignUpImpl) {
			return ltpSignUp;
		}

		LTPSignUpImpl ltpSignUpImpl = new LTPSignUpImpl();

		ltpSignUpImpl.setNew(ltpSignUp.isNew());
		ltpSignUpImpl.setPrimaryKey(ltpSignUp.getPrimaryKey());

		ltpSignUpImpl.setID(ltpSignUp.getID());
		ltpSignUpImpl.setFirstName(ltpSignUp.getFirstName());
		ltpSignUpImpl.setLastName(ltpSignUp.getLastName());
		ltpSignUpImpl.setMobileNumber(ltpSignUp.getMobileNumber());
		ltpSignUpImpl.setCreatedDate(ltpSignUp.getCreatedDate());
		ltpSignUpImpl.setUpdatedDate(ltpSignUp.getUpdatedDate());

		return ltpSignUpImpl;
	}

	/**
	 * Returns the LTPSignUp with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the LTPSignUp
	 * @return the LTPSignUp
	 * @throws NoSuchLTPSignUpException if a LTPSignUp with the primary key could not be found
	 */
	@Override
	public LTPSignUp findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLTPSignUpException {
		LTPSignUp ltpSignUp = fetchByPrimaryKey(primaryKey);

		if (ltpSignUp == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLTPSignUpException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ltpSignUp;
	}

	/**
	 * Returns the LTPSignUp with the primary key or throws a {@link NoSuchLTPSignUpException} if it could not be found.
	 *
	 * @param ID the primary key of the LTPSignUp
	 * @return the LTPSignUp
	 * @throws NoSuchLTPSignUpException if a LTPSignUp with the primary key could not be found
	 */
	@Override
	public LTPSignUp findByPrimaryKey(long ID) throws NoSuchLTPSignUpException {
		return findByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns the LTPSignUp with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the LTPSignUp
	 * @return the LTPSignUp, or <code>null</code> if a LTPSignUp with the primary key could not be found
	 */
	@Override
	public LTPSignUp fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
				LTPSignUpImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		LTPSignUp ltpSignUp = (LTPSignUp)serializable;

		if (ltpSignUp == null) {
			Session session = null;

			try {
				session = openSession();

				ltpSignUp = (LTPSignUp)session.get(LTPSignUpImpl.class,
						primaryKey);

				if (ltpSignUp != null) {
					cacheResult(ltpSignUp);
				}
				else {
					entityCache.putResult(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
						LTPSignUpImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUpImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ltpSignUp;
	}

	/**
	 * Returns the LTPSignUp with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ID the primary key of the LTPSignUp
	 * @return the LTPSignUp, or <code>null</code> if a LTPSignUp with the primary key could not be found
	 */
	@Override
	public LTPSignUp fetchByPrimaryKey(long ID) {
		return fetchByPrimaryKey((Serializable)ID);
	}

	@Override
	public Map<Serializable, LTPSignUp> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, LTPSignUp> map = new HashMap<Serializable, LTPSignUp>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			LTPSignUp ltpSignUp = fetchByPrimaryKey(primaryKey);

			if (ltpSignUp != null) {
				map.put(primaryKey, ltpSignUp);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUpImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (LTPSignUp)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_LTPSIGNUP_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (LTPSignUp ltpSignUp : (List<LTPSignUp>)q.list()) {
				map.put(ltpSignUp.getPrimaryKeyObj(), ltpSignUp);

				cacheResult(ltpSignUp);

				uncachedPrimaryKeys.remove(ltpSignUp.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(LTPSignUpModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUpImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the LTPSignUps.
	 *
	 * @return the LTPSignUps
	 */
	@Override
	public List<LTPSignUp> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the LTPSignUps.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUps
	 * @param end the upper bound of the range of LTPSignUps (not inclusive)
	 * @return the range of LTPSignUps
	 */
	@Override
	public List<LTPSignUp> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the LTPSignUps.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUps
	 * @param end the upper bound of the range of LTPSignUps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of LTPSignUps
	 */
	@Override
	public List<LTPSignUp> findAll(int start, int end,
		OrderByComparator<LTPSignUp> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the LTPSignUps.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUps
	 * @param end the upper bound of the range of LTPSignUps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of LTPSignUps
	 */
	@Override
	public List<LTPSignUp> findAll(int start, int end,
		OrderByComparator<LTPSignUp> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LTPSignUp> list = null;

		if (retrieveFromCache) {
			list = (List<LTPSignUp>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_LTPSIGNUP);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LTPSIGNUP;

				if (pagination) {
					sql = sql.concat(LTPSignUpModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<LTPSignUp>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LTPSignUp>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the LTPSignUps from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (LTPSignUp ltpSignUp : findAll()) {
			remove(ltpSignUp);
		}
	}

	/**
	 * Returns the number of LTPSignUps.
	 *
	 * @return the number of LTPSignUps
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LTPSIGNUP);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return LTPSignUpModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the LTPSignUp persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(LTPSignUpImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_LTPSIGNUP = "SELECT ltpSignUp FROM LTPSignUp ltpSignUp";
	private static final String _SQL_SELECT_LTPSIGNUP_WHERE_PKS_IN = "SELECT ltpSignUp FROM LTPSignUp ltpSignUp WHERE ID IN (";
	private static final String _SQL_COUNT_LTPSIGNUP = "SELECT COUNT(ltpSignUp) FROM LTPSignUp ltpSignUp";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ltpSignUp.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LTPSignUp exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(LTPSignUpPersistenceImpl.class);
}