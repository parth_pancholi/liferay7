/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.exception.PortalException;

import LTPDatabase.model.LTPSignUpMasterData;
import LTPDatabase.service.base.LTPSignUpMasterDataLocalServiceBaseImpl;
import LTPDatabase.service.persistence.LTPSignUpMasterDataPersistence;
import LTPDatabase.service.persistence.LTPSignUp_LTPMasterDataPersistence;

/**
 * The implementation of the LTPSignUpMasterData local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link LTPDatabase.service.LTPSignUpMasterDataLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpMasterDataLocalServiceBaseImpl
 * @see LTPDatabase.service.LTPSignUpMasterDataLocalServiceUtil
 */
public class LTPSignUpMasterDataLocalServiceImpl extends LTPSignUpMasterDataLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * LTPDatabase.service.LTPSignUpMasterDataLocalServiceUtil} to access the
	 * LTPSignUpMasterData local service.
	 */
	@BeanReference(type = LTPSignUpMasterDataPersistence.class)
	protected LTPSignUpMasterDataPersistence ltpSignUpMasterDataPersistence;
	
	@BeanReference(type = LTPSignUp_LTPMasterDataPersistence.class)
	protected LTPSignUp_LTPMasterDataPersistence ltpSignUp_LTPMasterDataPersistence;

	/**
	 * @param entry
	 * @return
	 */
	public LTPSignUpMasterData deleteLTPSignUpMasterData(LTPSignUpMasterData entry) {
		ltpSignUpMasterDataPersistence.remove(entry);
		return entry;
	}

	/**
	 * @param start
	 * @param end
	 * @return
	 */
	public List<LTPSignUpMasterData> getAllLTPSignUpMasterData(int start, int end) {
		return ltpSignUpMasterDataPersistence.findAll(start, end);
	}

	/* (non-Javadoc)
	 * @see LTPDatabase.service.LTPSignUpMasterDataLocalService#getLTPSignUpMasterData(int, int)
	 */
	public List<LTPSignUpMasterData> getLTPSignUpMasterData(int start, int end) {
		List<LTPSignUpMasterData> ltpSignUpMasterDataList = new ArrayList<>();
		for (LTPSignUpMasterData ltpSignUpMasterData:ltpSignUpMasterDataPersistence.findAll(start, end)) {
			if(ltpSignUp_LTPMasterDataPersistence.findByLTPSignUpMasterDataId(ltpSignUpMasterData.getID()).size() <= 0) {
				ltpSignUpMasterDataList.add(ltpSignUpMasterData);
			};
		}
		return ltpSignUpMasterDataList;
	}
	/**
	 * @return
	 */
	public int getLTPSignUpMasterDataCount() {
		return ltpSignUpMasterDataPersistence.countAll();
	}

	/**
	 * @param morningWalkTitle
	 * @param dinnerPrasadTitle
	 * @param volunteerTitle
	 * @param ltpSignUpMasterDataId
	 * @return
	 * @throws PortalException
	 */
	public LTPSignUpMasterData addUpdateLTPSignUpMasterData(long ltpSignUpMasterDataId, String extraText,
			Date eventDate, boolean isMorningWalk, boolean isVolunteer, boolean isDinnerPrasad) throws PortalException {

		LTPSignUpMasterData ltpSignUpMasterDataEntry = null;
		if (ltpSignUpMasterDataId != 0L) {
			ltpSignUpMasterDataEntry = ltpSignUpMasterDataPersistence.findByPrimaryKey(ltpSignUpMasterDataId);
		} else {
			ltpSignUpMasterDataId = counterLocalService.increment();
			ltpSignUpMasterDataEntry = ltpSignUpMasterDataPersistence.create(ltpSignUpMasterDataId);
		}
		ltpSignUpMasterDataEntry.setID(ltpSignUpMasterDataId);
		ltpSignUpMasterDataEntry.setEventDate(eventDate);
		ltpSignUpMasterDataEntry.setExtraText(extraText);
		ltpSignUpMasterDataEntry.setIsMorningWalk(isMorningWalk);
		ltpSignUpMasterDataEntry.setIsDinnerPrasad(isDinnerPrasad);
		ltpSignUpMasterDataEntry.setIsVolunteer(isVolunteer);
		ltpSignUpMasterDataPersistence.update(ltpSignUpMasterDataEntry);

		return ltpSignUpMasterDataEntry;
	}
}