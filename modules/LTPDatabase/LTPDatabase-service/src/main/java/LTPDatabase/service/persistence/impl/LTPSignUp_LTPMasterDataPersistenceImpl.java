/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.persistence.impl;

import LTPDatabase.exception.NoSuchLTPSignUp_LTPMasterDataException;

import LTPDatabase.model.LTPSignUp_LTPMasterData;

import LTPDatabase.model.impl.LTPSignUp_LTPMasterDataImpl;
import LTPDatabase.model.impl.LTPSignUp_LTPMasterDataModelImpl;

import LTPDatabase.service.persistence.LTPSignUp_LTPMasterDataPersistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the LTPSignUp_LTPMasterData service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUp_LTPMasterDataPersistence
 * @see LTPDatabase.service.persistence.LTPSignUp_LTPMasterDataUtil
 * @generated
 */
@ProviderType
public class LTPSignUp_LTPMasterDataPersistenceImpl extends BasePersistenceImpl<LTPSignUp_LTPMasterData>
	implements LTPSignUp_LTPMasterDataPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link LTPSignUp_LTPMasterDataUtil} to access the LTPSignUp_LTPMasterData persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = LTPSignUp_LTPMasterDataImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataModelImpl.FINDER_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataModelImpl.FINDER_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LTPSIGNUPID =
		new FinderPath(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataModelImpl.FINDER_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLTPSignUpID",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPID =
		new FinderPath(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataModelImpl.FINDER_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByLTPSignUpID",
			new String[] { Long.class.getName() },
			LTPSignUp_LTPMasterDataModelImpl.LTPSIGNUPID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LTPSIGNUPID = new FinderPath(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLTPSignUpID",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	 *
	 * @param LTPSignUpID the ltp sign up ID
	 * @return the matching LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findByLTPSignUpID(long LTPSignUpID) {
		return findByLTPSignUpID(LTPSignUpID, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param LTPSignUpID the ltp sign up ID
	 * @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	 * @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	 * @return the range of matching LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findByLTPSignUpID(long LTPSignUpID,
		int start, int end) {
		return findByLTPSignUpID(LTPSignUpID, start, end, null);
	}

	/**
	 * Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param LTPSignUpID the ltp sign up ID
	 * @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	 * @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findByLTPSignUpID(long LTPSignUpID,
		int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		return findByLTPSignUpID(LTPSignUpID, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param LTPSignUpID the ltp sign up ID
	 * @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	 * @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findByLTPSignUpID(long LTPSignUpID,
		int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPID;
			finderArgs = new Object[] { LTPSignUpID };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LTPSIGNUPID;
			finderArgs = new Object[] { LTPSignUpID, start, end, orderByComparator };
		}

		List<LTPSignUp_LTPMasterData> list = null;

		if (retrieveFromCache) {
			list = (List<LTPSignUp_LTPMasterData>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData : list) {
					if ((LTPSignUpID != ltpSignUp_LTPMasterData.getLTPSignUpID())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LTPSIGNUP_LTPMASTERDATA_WHERE);

			query.append(_FINDER_COLUMN_LTPSIGNUPID_LTPSIGNUPID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LTPSignUp_LTPMasterDataModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(LTPSignUpID);

				if (!pagination) {
					list = (List<LTPSignUp_LTPMasterData>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LTPSignUp_LTPMasterData>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	 *
	 * @param LTPSignUpID the ltp sign up ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching LTPSignUp_LTPMasterData
	 * @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData findByLTPSignUpID_First(long LTPSignUpID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException {
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = fetchByLTPSignUpID_First(LTPSignUpID,
				orderByComparator);

		if (ltpSignUp_LTPMasterData != null) {
			return ltpSignUp_LTPMasterData;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("LTPSignUpID=");
		msg.append(LTPSignUpID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLTPSignUp_LTPMasterDataException(msg.toString());
	}

	/**
	 * Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	 *
	 * @param LTPSignUpID the ltp sign up ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData fetchByLTPSignUpID_First(long LTPSignUpID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		List<LTPSignUp_LTPMasterData> list = findByLTPSignUpID(LTPSignUpID, 0,
				1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	 *
	 * @param LTPSignUpID the ltp sign up ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching LTPSignUp_LTPMasterData
	 * @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData findByLTPSignUpID_Last(long LTPSignUpID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException {
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = fetchByLTPSignUpID_Last(LTPSignUpID,
				orderByComparator);

		if (ltpSignUp_LTPMasterData != null) {
			return ltpSignUp_LTPMasterData;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("LTPSignUpID=");
		msg.append(LTPSignUpID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLTPSignUp_LTPMasterDataException(msg.toString());
	}

	/**
	 * Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	 *
	 * @param LTPSignUpID the ltp sign up ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData fetchByLTPSignUpID_Last(long LTPSignUpID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		int count = countByLTPSignUpID(LTPSignUpID);

		if (count == 0) {
			return null;
		}

		List<LTPSignUp_LTPMasterData> list = findByLTPSignUpID(LTPSignUpID,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the LTPSignUp_LTPMasterDatas before and after the current LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	 *
	 * @param ID the primary key of the current LTPSignUp_LTPMasterData
	 * @param LTPSignUpID the ltp sign up ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next LTPSignUp_LTPMasterData
	 * @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData[] findByLTPSignUpID_PrevAndNext(long ID,
		long LTPSignUpID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException {
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = findByPrimaryKey(ID);

		Session session = null;

		try {
			session = openSession();

			LTPSignUp_LTPMasterData[] array = new LTPSignUp_LTPMasterDataImpl[3];

			array[0] = getByLTPSignUpID_PrevAndNext(session,
					ltpSignUp_LTPMasterData, LTPSignUpID, orderByComparator,
					true);

			array[1] = ltpSignUp_LTPMasterData;

			array[2] = getByLTPSignUpID_PrevAndNext(session,
					ltpSignUp_LTPMasterData, LTPSignUpID, orderByComparator,
					false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LTPSignUp_LTPMasterData getByLTPSignUpID_PrevAndNext(
		Session session, LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData,
		long LTPSignUpID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LTPSIGNUP_LTPMASTERDATA_WHERE);

		query.append(_FINDER_COLUMN_LTPSIGNUPID_LTPSIGNUPID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LTPSignUp_LTPMasterDataModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(LTPSignUpID);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ltpSignUp_LTPMasterData);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LTPSignUp_LTPMasterData> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63; from the database.
	 *
	 * @param LTPSignUpID the ltp sign up ID
	 */
	@Override
	public void removeByLTPSignUpID(long LTPSignUpID) {
		for (LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData : findByLTPSignUpID(
				LTPSignUpID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ltpSignUp_LTPMasterData);
		}
	}

	/**
	 * Returns the number of LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	 *
	 * @param LTPSignUpID the ltp sign up ID
	 * @return the number of matching LTPSignUp_LTPMasterDatas
	 */
	@Override
	public int countByLTPSignUpID(long LTPSignUpID) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LTPSIGNUPID;

		Object[] finderArgs = new Object[] { LTPSignUpID };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LTPSIGNUP_LTPMASTERDATA_WHERE);

			query.append(_FINDER_COLUMN_LTPSIGNUPID_LTPSIGNUPID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(LTPSignUpID);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LTPSIGNUPID_LTPSIGNUPID_2 = "ltpSignUp_LTPMasterData.LTPSignUpID = ?";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LTPSIGNUPMASTERDATAID =
		new FinderPath(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataModelImpl.FINDER_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findByLTPSignUpMasterDataId",
			new String[] {
				Long.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPMASTERDATAID =
		new FinderPath(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataModelImpl.FINDER_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"findByLTPSignUpMasterDataId",
			new String[] { Long.class.getName() },
			LTPSignUp_LTPMasterDataModelImpl.LTPMASTERDATAID_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_LTPSIGNUPMASTERDATAID = new FinderPath(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
			"countByLTPSignUpMasterDataId",
			new String[] { Long.class.getName() });

	/**
	 * Returns all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	 *
	 * @param LTPMasterDataID the ltp master data ID
	 * @return the matching LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID) {
		return findByLTPSignUpMasterDataId(LTPMasterDataID, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param LTPMasterDataID the ltp master data ID
	 * @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	 * @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	 * @return the range of matching LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID, int start, int end) {
		return findByLTPSignUpMasterDataId(LTPMasterDataID, start, end, null);
	}

	/**
	 * Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param LTPMasterDataID the ltp master data ID
	 * @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	 * @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID, int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		return findByLTPSignUpMasterDataId(LTPMasterDataID, start, end,
			orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param LTPMasterDataID the ltp master data ID
	 * @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	 * @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID, int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPMASTERDATAID;
			finderArgs = new Object[] { LTPMasterDataID };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LTPSIGNUPMASTERDATAID;
			finderArgs = new Object[] {
					LTPMasterDataID,
					
					start, end, orderByComparator
				};
		}

		List<LTPSignUp_LTPMasterData> list = null;

		if (retrieveFromCache) {
			list = (List<LTPSignUp_LTPMasterData>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData : list) {
					if ((LTPMasterDataID != ltpSignUp_LTPMasterData.getLTPMasterDataID())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_LTPSIGNUP_LTPMASTERDATA_WHERE);

			query.append(_FINDER_COLUMN_LTPSIGNUPMASTERDATAID_LTPMASTERDATAID_2);

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(LTPSignUp_LTPMasterDataModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(LTPMasterDataID);

				if (!pagination) {
					list = (List<LTPSignUp_LTPMasterData>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LTPSignUp_LTPMasterData>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	 *
	 * @param LTPMasterDataID the ltp master data ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching LTPSignUp_LTPMasterData
	 * @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData findByLTPSignUpMasterDataId_First(
		long LTPMasterDataID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException {
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = fetchByLTPSignUpMasterDataId_First(LTPMasterDataID,
				orderByComparator);

		if (ltpSignUp_LTPMasterData != null) {
			return ltpSignUp_LTPMasterData;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("LTPMasterDataID=");
		msg.append(LTPMasterDataID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLTPSignUp_LTPMasterDataException(msg.toString());
	}

	/**
	 * Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	 *
	 * @param LTPMasterDataID the ltp master data ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData fetchByLTPSignUpMasterDataId_First(
		long LTPMasterDataID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		List<LTPSignUp_LTPMasterData> list = findByLTPSignUpMasterDataId(LTPMasterDataID,
				0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	 *
	 * @param LTPMasterDataID the ltp master data ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching LTPSignUp_LTPMasterData
	 * @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData findByLTPSignUpMasterDataId_Last(
		long LTPMasterDataID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException {
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = fetchByLTPSignUpMasterDataId_Last(LTPMasterDataID,
				orderByComparator);

		if (ltpSignUp_LTPMasterData != null) {
			return ltpSignUp_LTPMasterData;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("LTPMasterDataID=");
		msg.append(LTPMasterDataID);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchLTPSignUp_LTPMasterDataException(msg.toString());
	}

	/**
	 * Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	 *
	 * @param LTPMasterDataID the ltp master data ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData fetchByLTPSignUpMasterDataId_Last(
		long LTPMasterDataID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		int count = countByLTPSignUpMasterDataId(LTPMasterDataID);

		if (count == 0) {
			return null;
		}

		List<LTPSignUp_LTPMasterData> list = findByLTPSignUpMasterDataId(LTPMasterDataID,
				count - 1, count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the LTPSignUp_LTPMasterDatas before and after the current LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	 *
	 * @param ID the primary key of the current LTPSignUp_LTPMasterData
	 * @param LTPMasterDataID the ltp master data ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next LTPSignUp_LTPMasterData
	 * @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData[] findByLTPSignUpMasterDataId_PrevAndNext(
		long ID, long LTPMasterDataID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException {
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = findByPrimaryKey(ID);

		Session session = null;

		try {
			session = openSession();

			LTPSignUp_LTPMasterData[] array = new LTPSignUp_LTPMasterDataImpl[3];

			array[0] = getByLTPSignUpMasterDataId_PrevAndNext(session,
					ltpSignUp_LTPMasterData, LTPMasterDataID,
					orderByComparator, true);

			array[1] = ltpSignUp_LTPMasterData;

			array[2] = getByLTPSignUpMasterDataId_PrevAndNext(session,
					ltpSignUp_LTPMasterData, LTPMasterDataID,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected LTPSignUp_LTPMasterData getByLTPSignUpMasterDataId_PrevAndNext(
		Session session, LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData,
		long LTPMasterDataID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator,
		boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_LTPSIGNUP_LTPMASTERDATA_WHERE);

		query.append(_FINDER_COLUMN_LTPSIGNUPMASTERDATAID_LTPMASTERDATAID_2);

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(LTPSignUp_LTPMasterDataModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		qPos.add(LTPMasterDataID);

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(ltpSignUp_LTPMasterData);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<LTPSignUp_LTPMasterData> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63; from the database.
	 *
	 * @param LTPMasterDataID the ltp master data ID
	 */
	@Override
	public void removeByLTPSignUpMasterDataId(long LTPMasterDataID) {
		for (LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData : findByLTPSignUpMasterDataId(
				LTPMasterDataID, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(ltpSignUp_LTPMasterData);
		}
	}

	/**
	 * Returns the number of LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	 *
	 * @param LTPMasterDataID the ltp master data ID
	 * @return the number of matching LTPSignUp_LTPMasterDatas
	 */
	@Override
	public int countByLTPSignUpMasterDataId(long LTPMasterDataID) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_LTPSIGNUPMASTERDATAID;

		Object[] finderArgs = new Object[] { LTPMasterDataID };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_LTPSIGNUP_LTPMASTERDATA_WHERE);

			query.append(_FINDER_COLUMN_LTPSIGNUPMASTERDATAID_LTPMASTERDATAID_2);

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				qPos.add(LTPMasterDataID);

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LTPSIGNUPMASTERDATAID_LTPMASTERDATAID_2 =
		"ltpSignUp_LTPMasterData.LTPMasterDataID = ?";

	public LTPSignUp_LTPMasterDataPersistenceImpl() {
		setModelClass(LTPSignUp_LTPMasterData.class);
	}

	/**
	 * Caches the LTPSignUp_LTPMasterData in the entity cache if it is enabled.
	 *
	 * @param ltpSignUp_LTPMasterData the LTPSignUp_LTPMasterData
	 */
	@Override
	public void cacheResult(LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		entityCache.putResult(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataImpl.class,
			ltpSignUp_LTPMasterData.getPrimaryKey(), ltpSignUp_LTPMasterData);

		ltpSignUp_LTPMasterData.resetOriginalValues();
	}

	/**
	 * Caches the LTPSignUp_LTPMasterDatas in the entity cache if it is enabled.
	 *
	 * @param ltpSignUp_LTPMasterDatas the LTPSignUp_LTPMasterDatas
	 */
	@Override
	public void cacheResult(
		List<LTPSignUp_LTPMasterData> ltpSignUp_LTPMasterDatas) {
		for (LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData : ltpSignUp_LTPMasterDatas) {
			if (entityCache.getResult(
						LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
						LTPSignUp_LTPMasterDataImpl.class,
						ltpSignUp_LTPMasterData.getPrimaryKey()) == null) {
				cacheResult(ltpSignUp_LTPMasterData);
			}
			else {
				ltpSignUp_LTPMasterData.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all LTPSignUp_LTPMasterDatas.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(LTPSignUp_LTPMasterDataImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the LTPSignUp_LTPMasterData.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		entityCache.removeResult(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataImpl.class,
			ltpSignUp_LTPMasterData.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(
		List<LTPSignUp_LTPMasterData> ltpSignUp_LTPMasterDatas) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData : ltpSignUp_LTPMasterDatas) {
			entityCache.removeResult(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
				LTPSignUp_LTPMasterDataImpl.class,
				ltpSignUp_LTPMasterData.getPrimaryKey());
		}
	}

	/**
	 * Creates a new LTPSignUp_LTPMasterData with the primary key. Does not add the LTPSignUp_LTPMasterData to the database.
	 *
	 * @param ID the primary key for the new LTPSignUp_LTPMasterData
	 * @return the new LTPSignUp_LTPMasterData
	 */
	@Override
	public LTPSignUp_LTPMasterData create(long ID) {
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = new LTPSignUp_LTPMasterDataImpl();

		ltpSignUp_LTPMasterData.setNew(true);
		ltpSignUp_LTPMasterData.setPrimaryKey(ID);

		return ltpSignUp_LTPMasterData;
	}

	/**
	 * Removes the LTPSignUp_LTPMasterData with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ID the primary key of the LTPSignUp_LTPMasterData
	 * @return the LTPSignUp_LTPMasterData that was removed
	 * @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData remove(long ID)
		throws NoSuchLTPSignUp_LTPMasterDataException {
		return remove((Serializable)ID);
	}

	/**
	 * Removes the LTPSignUp_LTPMasterData with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the LTPSignUp_LTPMasterData
	 * @return the LTPSignUp_LTPMasterData that was removed
	 * @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData remove(Serializable primaryKey)
		throws NoSuchLTPSignUp_LTPMasterDataException {
		Session session = null;

		try {
			session = openSession();

			LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = (LTPSignUp_LTPMasterData)session.get(LTPSignUp_LTPMasterDataImpl.class,
					primaryKey);

			if (ltpSignUp_LTPMasterData == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchLTPSignUp_LTPMasterDataException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(ltpSignUp_LTPMasterData);
		}
		catch (NoSuchLTPSignUp_LTPMasterDataException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected LTPSignUp_LTPMasterData removeImpl(
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		ltpSignUp_LTPMasterData = toUnwrappedModel(ltpSignUp_LTPMasterData);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(ltpSignUp_LTPMasterData)) {
				ltpSignUp_LTPMasterData = (LTPSignUp_LTPMasterData)session.get(LTPSignUp_LTPMasterDataImpl.class,
						ltpSignUp_LTPMasterData.getPrimaryKeyObj());
			}

			if (ltpSignUp_LTPMasterData != null) {
				session.delete(ltpSignUp_LTPMasterData);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (ltpSignUp_LTPMasterData != null) {
			clearCache(ltpSignUp_LTPMasterData);
		}

		return ltpSignUp_LTPMasterData;
	}

	@Override
	public LTPSignUp_LTPMasterData updateImpl(
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		ltpSignUp_LTPMasterData = toUnwrappedModel(ltpSignUp_LTPMasterData);

		boolean isNew = ltpSignUp_LTPMasterData.isNew();

		LTPSignUp_LTPMasterDataModelImpl ltpSignUp_LTPMasterDataModelImpl = (LTPSignUp_LTPMasterDataModelImpl)ltpSignUp_LTPMasterData;

		Session session = null;

		try {
			session = openSession();

			if (ltpSignUp_LTPMasterData.isNew()) {
				session.save(ltpSignUp_LTPMasterData);

				ltpSignUp_LTPMasterData.setNew(false);
			}
			else {
				ltpSignUp_LTPMasterData = (LTPSignUp_LTPMasterData)session.merge(ltpSignUp_LTPMasterData);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!LTPSignUp_LTPMasterDataModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] {
					ltpSignUp_LTPMasterDataModelImpl.getLTPSignUpID()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_LTPSIGNUPID, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPID,
				args);

			args = new Object[] {
					ltpSignUp_LTPMasterDataModelImpl.getLTPMasterDataID()
				};

			finderCache.removeResult(FINDER_PATH_COUNT_BY_LTPSIGNUPMASTERDATAID,
				args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPMASTERDATAID,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((ltpSignUp_LTPMasterDataModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ltpSignUp_LTPMasterDataModelImpl.getOriginalLTPSignUpID()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_LTPSIGNUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPID,
					args);

				args = new Object[] {
						ltpSignUp_LTPMasterDataModelImpl.getLTPSignUpID()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_LTPSIGNUPID, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPID,
					args);
			}

			if ((ltpSignUp_LTPMasterDataModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPMASTERDATAID.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						ltpSignUp_LTPMasterDataModelImpl.getOriginalLTPMasterDataID()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_LTPSIGNUPMASTERDATAID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPMASTERDATAID,
					args);

				args = new Object[] {
						ltpSignUp_LTPMasterDataModelImpl.getLTPMasterDataID()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_LTPSIGNUPMASTERDATAID,
					args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_LTPSIGNUPMASTERDATAID,
					args);
			}
		}

		entityCache.putResult(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
			LTPSignUp_LTPMasterDataImpl.class,
			ltpSignUp_LTPMasterData.getPrimaryKey(), ltpSignUp_LTPMasterData,
			false);

		ltpSignUp_LTPMasterData.resetOriginalValues();

		return ltpSignUp_LTPMasterData;
	}

	protected LTPSignUp_LTPMasterData toUnwrappedModel(
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		if (ltpSignUp_LTPMasterData instanceof LTPSignUp_LTPMasterDataImpl) {
			return ltpSignUp_LTPMasterData;
		}

		LTPSignUp_LTPMasterDataImpl ltpSignUp_LTPMasterDataImpl = new LTPSignUp_LTPMasterDataImpl();

		ltpSignUp_LTPMasterDataImpl.setNew(ltpSignUp_LTPMasterData.isNew());
		ltpSignUp_LTPMasterDataImpl.setPrimaryKey(ltpSignUp_LTPMasterData.getPrimaryKey());

		ltpSignUp_LTPMasterDataImpl.setID(ltpSignUp_LTPMasterData.getID());
		ltpSignUp_LTPMasterDataImpl.setLTPSignUpID(ltpSignUp_LTPMasterData.getLTPSignUpID());
		ltpSignUp_LTPMasterDataImpl.setLTPMasterDataID(ltpSignUp_LTPMasterData.getLTPMasterDataID());
		ltpSignUp_LTPMasterDataImpl.setDinnerPrasadItem1(ltpSignUp_LTPMasterData.getDinnerPrasadItem1());
		ltpSignUp_LTPMasterDataImpl.setDinnerPrasadItem2(ltpSignUp_LTPMasterData.getDinnerPrasadItem2());

		return ltpSignUp_LTPMasterDataImpl;
	}

	/**
	 * Returns the LTPSignUp_LTPMasterData with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the LTPSignUp_LTPMasterData
	 * @return the LTPSignUp_LTPMasterData
	 * @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData findByPrimaryKey(Serializable primaryKey)
		throws NoSuchLTPSignUp_LTPMasterDataException {
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = fetchByPrimaryKey(primaryKey);

		if (ltpSignUp_LTPMasterData == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchLTPSignUp_LTPMasterDataException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return ltpSignUp_LTPMasterData;
	}

	/**
	 * Returns the LTPSignUp_LTPMasterData with the primary key or throws a {@link NoSuchLTPSignUp_LTPMasterDataException} if it could not be found.
	 *
	 * @param ID the primary key of the LTPSignUp_LTPMasterData
	 * @return the LTPSignUp_LTPMasterData
	 * @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData findByPrimaryKey(long ID)
		throws NoSuchLTPSignUp_LTPMasterDataException {
		return findByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns the LTPSignUp_LTPMasterData with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the LTPSignUp_LTPMasterData
	 * @return the LTPSignUp_LTPMasterData, or <code>null</code> if a LTPSignUp_LTPMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
				LTPSignUp_LTPMasterDataImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = (LTPSignUp_LTPMasterData)serializable;

		if (ltpSignUp_LTPMasterData == null) {
			Session session = null;

			try {
				session = openSession();

				ltpSignUp_LTPMasterData = (LTPSignUp_LTPMasterData)session.get(LTPSignUp_LTPMasterDataImpl.class,
						primaryKey);

				if (ltpSignUp_LTPMasterData != null) {
					cacheResult(ltpSignUp_LTPMasterData);
				}
				else {
					entityCache.putResult(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
						LTPSignUp_LTPMasterDataImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUp_LTPMasterDataImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return ltpSignUp_LTPMasterData;
	}

	/**
	 * Returns the LTPSignUp_LTPMasterData with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ID the primary key of the LTPSignUp_LTPMasterData
	 * @return the LTPSignUp_LTPMasterData, or <code>null</code> if a LTPSignUp_LTPMasterData with the primary key could not be found
	 */
	@Override
	public LTPSignUp_LTPMasterData fetchByPrimaryKey(long ID) {
		return fetchByPrimaryKey((Serializable)ID);
	}

	@Override
	public Map<Serializable, LTPSignUp_LTPMasterData> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, LTPSignUp_LTPMasterData> map = new HashMap<Serializable, LTPSignUp_LTPMasterData>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData = fetchByPrimaryKey(primaryKey);

			if (ltpSignUp_LTPMasterData != null) {
				map.put(primaryKey, ltpSignUp_LTPMasterData);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUp_LTPMasterDataImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (LTPSignUp_LTPMasterData)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_LTPSIGNUP_LTPMASTERDATA_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData : (List<LTPSignUp_LTPMasterData>)q.list()) {
				map.put(ltpSignUp_LTPMasterData.getPrimaryKeyObj(),
					ltpSignUp_LTPMasterData);

				cacheResult(ltpSignUp_LTPMasterData);

				uncachedPrimaryKeys.remove(ltpSignUp_LTPMasterData.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(LTPSignUp_LTPMasterDataModelImpl.ENTITY_CACHE_ENABLED,
					LTPSignUp_LTPMasterDataImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the LTPSignUp_LTPMasterDatas.
	 *
	 * @return the LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the LTPSignUp_LTPMasterDatas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	 * @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	 * @return the range of LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the LTPSignUp_LTPMasterDatas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	 * @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findAll(int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the LTPSignUp_LTPMasterDatas.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	 * @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of LTPSignUp_LTPMasterDatas
	 */
	@Override
	public List<LTPSignUp_LTPMasterData> findAll(int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<LTPSignUp_LTPMasterData> list = null;

		if (retrieveFromCache) {
			list = (List<LTPSignUp_LTPMasterData>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_LTPSIGNUP_LTPMASTERDATA);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_LTPSIGNUP_LTPMASTERDATA;

				if (pagination) {
					sql = sql.concat(LTPSignUp_LTPMasterDataModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<LTPSignUp_LTPMasterData>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<LTPSignUp_LTPMasterData>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the LTPSignUp_LTPMasterDatas from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData : findAll()) {
			remove(ltpSignUp_LTPMasterData);
		}
	}

	/**
	 * Returns the number of LTPSignUp_LTPMasterDatas.
	 *
	 * @return the number of LTPSignUp_LTPMasterDatas
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_LTPSIGNUP_LTPMASTERDATA);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return LTPSignUp_LTPMasterDataModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the LTPSignUp_LTPMasterData persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(LTPSignUp_LTPMasterDataImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_LTPSIGNUP_LTPMASTERDATA = "SELECT ltpSignUp_LTPMasterData FROM LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData";
	private static final String _SQL_SELECT_LTPSIGNUP_LTPMASTERDATA_WHERE_PKS_IN =
		"SELECT ltpSignUp_LTPMasterData FROM LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData WHERE ID IN (";
	private static final String _SQL_SELECT_LTPSIGNUP_LTPMASTERDATA_WHERE = "SELECT ltpSignUp_LTPMasterData FROM LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData WHERE ";
	private static final String _SQL_COUNT_LTPSIGNUP_LTPMASTERDATA = "SELECT COUNT(ltpSignUp_LTPMasterData) FROM LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData";
	private static final String _SQL_COUNT_LTPSIGNUP_LTPMASTERDATA_WHERE = "SELECT COUNT(ltpSignUp_LTPMasterData) FROM LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "ltpSignUp_LTPMasterData.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No LTPSignUp_LTPMasterData exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No LTPSignUp_LTPMasterData exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(LTPSignUp_LTPMasterDataPersistenceImpl.class);
}