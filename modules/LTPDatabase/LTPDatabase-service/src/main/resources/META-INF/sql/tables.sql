create table LTPSignUp (
	ID LONG not null primary key,
	FirstName VARCHAR(75) null,
	LastName VARCHAR(75) null,
	MobileNumber VARCHAR(75) null,
	CreatedDate DATE null,
	UpdatedDate DATE null
);

create table LTPSignUpMasterData (
	ID LONG not null primary key,
	EventDate DATE null,
	ExtraText VARCHAR(75) null,
	isMorningWalk BOOLEAN,
	isVolunteer BOOLEAN,
	isDinnerPrasad BOOLEAN
);

create table LTPSignUpPreferences (
	ID LONG not null primary key,
	MorningWalkTitle VARCHAR(75) null,
	DinnerPrasadTitle VARCHAR(75) null,
	VolunteerTitle VARCHAR(75) null
);

create table LTPSignUp_LTPMasterData (
	ID LONG not null primary key,
	LTPSignUpID LONG,
	LTPMasterDataID LONG,
	DinnerPrasadItem1 VARCHAR(75) null,
	DinnerPrasadItem2 VARCHAR(75) null
);