/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for LTPSignUp. This utility wraps
 * {@link LTPDatabase.service.impl.LTPSignUpLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpLocalService
 * @see LTPDatabase.service.base.LTPSignUpLocalServiceBaseImpl
 * @see LTPDatabase.service.impl.LTPSignUpLocalServiceImpl
 * @generated
 */
@ProviderType
public class LTPSignUpLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link LTPDatabase.service.impl.LTPSignUpLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the LTPSignUp to the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUp the LTPSignUp
	* @return the LTPSignUp that was added
	*/
	public static LTPDatabase.model.LTPSignUp addLTPSignUp(
		LTPDatabase.model.LTPSignUp ltpSignUp) {
		return getService().addLTPSignUp(ltpSignUp);
	}

	public static LTPDatabase.model.LTPSignUp addLTPSignUp(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String mobileNumber,
		java.util.Map<java.lang.Integer, java.lang.String> ltpMap)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .addLTPSignUp(firstName, lastName, mobileNumber, ltpMap);
	}

	/**
	* Creates a new LTPSignUp with the primary key. Does not add the LTPSignUp to the database.
	*
	* @param ID the primary key for the new LTPSignUp
	* @return the new LTPSignUp
	*/
	public static LTPDatabase.model.LTPSignUp createLTPSignUp(long ID) {
		return getService().createLTPSignUp(ID);
	}

	/**
	* Deletes the LTPSignUp from the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUp the LTPSignUp
	* @return the LTPSignUp that was removed
	*/
	public static LTPDatabase.model.LTPSignUp deleteLTPSignUp(
		LTPDatabase.model.LTPSignUp ltpSignUp) {
		return getService().deleteLTPSignUp(ltpSignUp);
	}

	/**
	* Deletes the LTPSignUp with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUp
	* @return the LTPSignUp that was removed
	* @throws PortalException if a LTPSignUp with the primary key could not be found
	*/
	public static LTPDatabase.model.LTPSignUp deleteLTPSignUp(long ID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteLTPSignUp(ID);
	}

	/**
	* @param id
	* @return
	*/
	public static LTPDatabase.model.LTPSignUp deleteLTPSignUpWithDependents(
		long id) {
		return getService().deleteLTPSignUpWithDependents(id);
	}

	public static LTPDatabase.model.LTPSignUp fetchLTPSignUp(long ID) {
		return getService().fetchLTPSignUp(ID);
	}

	/**
	* Returns the LTPSignUp with the primary key.
	*
	* @param ID the primary key of the LTPSignUp
	* @return the LTPSignUp
	* @throws PortalException if a LTPSignUp with the primary key could not be found
	*/
	public static LTPDatabase.model.LTPSignUp getLTPSignUp(long ID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getLTPSignUp(ID);
	}

	/**
	* Updates the LTPSignUp in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUp the LTPSignUp
	* @return the LTPSignUp that was updated
	*/
	public static LTPDatabase.model.LTPSignUp updateLTPSignUp(
		LTPDatabase.model.LTPSignUp ltpSignUp) {
		return getService().updateLTPSignUp(ltpSignUp);
	}

	public static LTPDatabase.model.LTPSignUp updateLTPSignUp(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String mobileNumber,
		java.util.Map<java.lang.Integer, java.lang.String> ltpMap,
		long ltpSignUpId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .updateLTPSignUp(firstName, lastName, mobileNumber, ltpMap,
			ltpSignUpId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of LTPSignUps.
	*
	* @return the number of LTPSignUps
	*/
	public static int getLTPSignUpsCount() {
		return getService().getLTPSignUpsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns a range of all the LTPSignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUps
	* @param end the upper bound of the range of LTPSignUps (not inclusive)
	* @return the range of LTPSignUps
	*/
	public static java.util.List<LTPDatabase.model.LTPSignUp> getLTPSignUps(
		int start, int end) {
		return getService().getLTPSignUps(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static LTPSignUpLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LTPSignUpLocalService, LTPSignUpLocalService> _serviceTracker =
		ServiceTrackerFactory.open(LTPSignUpLocalService.class);
}