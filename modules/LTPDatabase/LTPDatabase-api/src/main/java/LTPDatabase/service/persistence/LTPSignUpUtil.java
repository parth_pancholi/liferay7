/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.persistence;

import LTPDatabase.model.LTPSignUp;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the LTPSignUp service. This utility wraps {@link LTPDatabase.service.persistence.impl.LTPSignUpPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpPersistence
 * @see LTPDatabase.service.persistence.impl.LTPSignUpPersistenceImpl
 * @generated
 */
@ProviderType
public class LTPSignUpUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(LTPSignUp ltpSignUp) {
		getPersistence().clearCache(ltpSignUp);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LTPSignUp> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LTPSignUp> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LTPSignUp> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<LTPSignUp> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static LTPSignUp update(LTPSignUp ltpSignUp) {
		return getPersistence().update(ltpSignUp);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static LTPSignUp update(LTPSignUp ltpSignUp,
		ServiceContext serviceContext) {
		return getPersistence().update(ltpSignUp, serviceContext);
	}

	/**
	* Caches the LTPSignUp in the entity cache if it is enabled.
	*
	* @param ltpSignUp the LTPSignUp
	*/
	public static void cacheResult(LTPSignUp ltpSignUp) {
		getPersistence().cacheResult(ltpSignUp);
	}

	/**
	* Caches the LTPSignUps in the entity cache if it is enabled.
	*
	* @param ltpSignUps the LTPSignUps
	*/
	public static void cacheResult(List<LTPSignUp> ltpSignUps) {
		getPersistence().cacheResult(ltpSignUps);
	}

	/**
	* Creates a new LTPSignUp with the primary key. Does not add the LTPSignUp to the database.
	*
	* @param ID the primary key for the new LTPSignUp
	* @return the new LTPSignUp
	*/
	public static LTPSignUp create(long ID) {
		return getPersistence().create(ID);
	}

	/**
	* Removes the LTPSignUp with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUp
	* @return the LTPSignUp that was removed
	* @throws NoSuchLTPSignUpException if a LTPSignUp with the primary key could not be found
	*/
	public static LTPSignUp remove(long ID)
		throws LTPDatabase.exception.NoSuchLTPSignUpException {
		return getPersistence().remove(ID);
	}

	public static LTPSignUp updateImpl(LTPSignUp ltpSignUp) {
		return getPersistence().updateImpl(ltpSignUp);
	}

	/**
	* Returns the LTPSignUp with the primary key or throws a {@link NoSuchLTPSignUpException} if it could not be found.
	*
	* @param ID the primary key of the LTPSignUp
	* @return the LTPSignUp
	* @throws NoSuchLTPSignUpException if a LTPSignUp with the primary key could not be found
	*/
	public static LTPSignUp findByPrimaryKey(long ID)
		throws LTPDatabase.exception.NoSuchLTPSignUpException {
		return getPersistence().findByPrimaryKey(ID);
	}

	/**
	* Returns the LTPSignUp with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the LTPSignUp
	* @return the LTPSignUp, or <code>null</code> if a LTPSignUp with the primary key could not be found
	*/
	public static LTPSignUp fetchByPrimaryKey(long ID) {
		return getPersistence().fetchByPrimaryKey(ID);
	}

	public static java.util.Map<java.io.Serializable, LTPSignUp> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the LTPSignUps.
	*
	* @return the LTPSignUps
	*/
	public static List<LTPSignUp> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the LTPSignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUps
	* @param end the upper bound of the range of LTPSignUps (not inclusive)
	* @return the range of LTPSignUps
	*/
	public static List<LTPSignUp> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the LTPSignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUps
	* @param end the upper bound of the range of LTPSignUps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of LTPSignUps
	*/
	public static List<LTPSignUp> findAll(int start, int end,
		OrderByComparator<LTPSignUp> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the LTPSignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUps
	* @param end the upper bound of the range of LTPSignUps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of LTPSignUps
	*/
	public static List<LTPSignUp> findAll(int start, int end,
		OrderByComparator<LTPSignUp> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the LTPSignUps from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of LTPSignUps.
	*
	* @return the number of LTPSignUps
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static LTPSignUpPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LTPSignUpPersistence, LTPSignUpPersistence> _serviceTracker =
		ServiceTrackerFactory.open(LTPSignUpPersistence.class);
}