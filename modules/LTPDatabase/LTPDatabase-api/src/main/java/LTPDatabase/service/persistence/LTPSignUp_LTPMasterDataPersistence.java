/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.persistence;

import LTPDatabase.exception.NoSuchLTPSignUp_LTPMasterDataException;

import LTPDatabase.model.LTPSignUp_LTPMasterData;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the LTPSignUp_LTPMasterData service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPDatabase.service.persistence.impl.LTPSignUp_LTPMasterDataPersistenceImpl
 * @see LTPSignUp_LTPMasterDataUtil
 * @generated
 */
@ProviderType
public interface LTPSignUp_LTPMasterDataPersistence extends BasePersistence<LTPSignUp_LTPMasterData> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LTPSignUp_LTPMasterDataUtil} to access the LTPSignUp_LTPMasterData persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @return the matching LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findByLTPSignUpID(
		long LTPSignUpID);

	/**
	* Returns a range of all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @return the range of matching LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findByLTPSignUpID(
		long LTPSignUpID, int start, int end);

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findByLTPSignUpID(
		long LTPSignUpID, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator);

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findByLTPSignUpID(
		long LTPSignUpID, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public LTPSignUp_LTPMasterData findByLTPSignUpID_First(long LTPSignUpID,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException;

	/**
	* Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public LTPSignUp_LTPMasterData fetchByLTPSignUpID_First(long LTPSignUpID,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator);

	/**
	* Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public LTPSignUp_LTPMasterData findByLTPSignUpID_Last(long LTPSignUpID,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException;

	/**
	* Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public LTPSignUp_LTPMasterData fetchByLTPSignUpID_Last(long LTPSignUpID,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator);

	/**
	* Returns the LTPSignUp_LTPMasterDatas before and after the current LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	*
	* @param ID the primary key of the current LTPSignUp_LTPMasterData
	* @param LTPSignUpID the ltp sign up ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	public LTPSignUp_LTPMasterData[] findByLTPSignUpID_PrevAndNext(long ID,
		long LTPSignUpID,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException;

	/**
	* Removes all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63; from the database.
	*
	* @param LTPSignUpID the ltp sign up ID
	*/
	public void removeByLTPSignUpID(long LTPSignUpID);

	/**
	* Returns the number of LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @return the number of matching LTPSignUp_LTPMasterDatas
	*/
	public int countByLTPSignUpID(long LTPSignUpID);

	/**
	* Returns all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @return the matching LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID);

	/**
	* Returns a range of all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @return the range of matching LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID, int start, int end);

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator);

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public LTPSignUp_LTPMasterData findByLTPSignUpMasterDataId_First(
		long LTPMasterDataID,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException;

	/**
	* Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public LTPSignUp_LTPMasterData fetchByLTPSignUpMasterDataId_First(
		long LTPMasterDataID,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator);

	/**
	* Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public LTPSignUp_LTPMasterData findByLTPSignUpMasterDataId_Last(
		long LTPMasterDataID,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException;

	/**
	* Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public LTPSignUp_LTPMasterData fetchByLTPSignUpMasterDataId_Last(
		long LTPMasterDataID,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator);

	/**
	* Returns the LTPSignUp_LTPMasterDatas before and after the current LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	*
	* @param ID the primary key of the current LTPSignUp_LTPMasterData
	* @param LTPMasterDataID the ltp master data ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	public LTPSignUp_LTPMasterData[] findByLTPSignUpMasterDataId_PrevAndNext(
		long ID, long LTPMasterDataID,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws NoSuchLTPSignUp_LTPMasterDataException;

	/**
	* Removes all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63; from the database.
	*
	* @param LTPMasterDataID the ltp master data ID
	*/
	public void removeByLTPSignUpMasterDataId(long LTPMasterDataID);

	/**
	* Returns the number of LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @return the number of matching LTPSignUp_LTPMasterDatas
	*/
	public int countByLTPSignUpMasterDataId(long LTPMasterDataID);

	/**
	* Caches the LTPSignUp_LTPMasterData in the entity cache if it is enabled.
	*
	* @param ltpSignUp_LTPMasterData the LTPSignUp_LTPMasterData
	*/
	public void cacheResult(LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData);

	/**
	* Caches the LTPSignUp_LTPMasterDatas in the entity cache if it is enabled.
	*
	* @param ltpSignUp_LTPMasterDatas the LTPSignUp_LTPMasterDatas
	*/
	public void cacheResult(
		java.util.List<LTPSignUp_LTPMasterData> ltpSignUp_LTPMasterDatas);

	/**
	* Creates a new LTPSignUp_LTPMasterData with the primary key. Does not add the LTPSignUp_LTPMasterData to the database.
	*
	* @param ID the primary key for the new LTPSignUp_LTPMasterData
	* @return the new LTPSignUp_LTPMasterData
	*/
	public LTPSignUp_LTPMasterData create(long ID);

	/**
	* Removes the LTPSignUp_LTPMasterData with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUp_LTPMasterData
	* @return the LTPSignUp_LTPMasterData that was removed
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	public LTPSignUp_LTPMasterData remove(long ID)
		throws NoSuchLTPSignUp_LTPMasterDataException;

	public LTPSignUp_LTPMasterData updateImpl(
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData);

	/**
	* Returns the LTPSignUp_LTPMasterData with the primary key or throws a {@link NoSuchLTPSignUp_LTPMasterDataException} if it could not be found.
	*
	* @param ID the primary key of the LTPSignUp_LTPMasterData
	* @return the LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	public LTPSignUp_LTPMasterData findByPrimaryKey(long ID)
		throws NoSuchLTPSignUp_LTPMasterDataException;

	/**
	* Returns the LTPSignUp_LTPMasterData with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the LTPSignUp_LTPMasterData
	* @return the LTPSignUp_LTPMasterData, or <code>null</code> if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	public LTPSignUp_LTPMasterData fetchByPrimaryKey(long ID);

	@Override
	public java.util.Map<java.io.Serializable, LTPSignUp_LTPMasterData> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the LTPSignUp_LTPMasterDatas.
	*
	* @return the LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findAll();

	/**
	* Returns a range of all the LTPSignUp_LTPMasterDatas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @return the range of LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findAll(int start, int end);

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator);

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of LTPSignUp_LTPMasterDatas
	*/
	public java.util.List<LTPSignUp_LTPMasterData> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the LTPSignUp_LTPMasterDatas from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of LTPSignUp_LTPMasterDatas.
	*
	* @return the number of LTPSignUp_LTPMasterDatas
	*/
	public int countAll();
}