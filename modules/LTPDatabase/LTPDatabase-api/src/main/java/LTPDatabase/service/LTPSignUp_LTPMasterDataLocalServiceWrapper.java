/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link LTPSignUp_LTPMasterDataLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUp_LTPMasterDataLocalService
 * @generated
 */
@ProviderType
public class LTPSignUp_LTPMasterDataLocalServiceWrapper
	implements LTPSignUp_LTPMasterDataLocalService,
		ServiceWrapper<LTPSignUp_LTPMasterDataLocalService> {
	public LTPSignUp_LTPMasterDataLocalServiceWrapper(
		LTPSignUp_LTPMasterDataLocalService ltpSignUp_LTPMasterDataLocalService) {
		_ltpSignUp_LTPMasterDataLocalService = ltpSignUp_LTPMasterDataLocalService;
	}

	/**
	* Adds the LTPSignUp_LTPMasterData to the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUp_LTPMasterData the LTPSignUp_LTPMasterData
	* @return the LTPSignUp_LTPMasterData that was added
	*/
	@Override
	public LTPDatabase.model.LTPSignUp_LTPMasterData addLTPSignUp_LTPMasterData(
		LTPDatabase.model.LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		return _ltpSignUp_LTPMasterDataLocalService.addLTPSignUp_LTPMasterData(ltpSignUp_LTPMasterData);
	}

	/**
	* Creates a new LTPSignUp_LTPMasterData with the primary key. Does not add the LTPSignUp_LTPMasterData to the database.
	*
	* @param ID the primary key for the new LTPSignUp_LTPMasterData
	* @return the new LTPSignUp_LTPMasterData
	*/
	@Override
	public LTPDatabase.model.LTPSignUp_LTPMasterData createLTPSignUp_LTPMasterData(
		long ID) {
		return _ltpSignUp_LTPMasterDataLocalService.createLTPSignUp_LTPMasterData(ID);
	}

	/**
	* Deletes the LTPSignUp_LTPMasterData from the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUp_LTPMasterData the LTPSignUp_LTPMasterData
	* @return the LTPSignUp_LTPMasterData that was removed
	*/
	@Override
	public LTPDatabase.model.LTPSignUp_LTPMasterData deleteLTPSignUp_LTPMasterData(
		LTPDatabase.model.LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		return _ltpSignUp_LTPMasterDataLocalService.deleteLTPSignUp_LTPMasterData(ltpSignUp_LTPMasterData);
	}

	/**
	* Deletes the LTPSignUp_LTPMasterData with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUp_LTPMasterData
	* @return the LTPSignUp_LTPMasterData that was removed
	* @throws PortalException if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	@Override
	public LTPDatabase.model.LTPSignUp_LTPMasterData deleteLTPSignUp_LTPMasterData(
		long ID) throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUp_LTPMasterDataLocalService.deleteLTPSignUp_LTPMasterData(ID);
	}

	@Override
	public LTPDatabase.model.LTPSignUp_LTPMasterData fetchLTPSignUp_LTPMasterData(
		long ID) {
		return _ltpSignUp_LTPMasterDataLocalService.fetchLTPSignUp_LTPMasterData(ID);
	}

	/**
	* Returns the LTPSignUp_LTPMasterData with the primary key.
	*
	* @param ID the primary key of the LTPSignUp_LTPMasterData
	* @return the LTPSignUp_LTPMasterData
	* @throws PortalException if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	@Override
	public LTPDatabase.model.LTPSignUp_LTPMasterData getLTPSignUp_LTPMasterData(
		long ID) throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUp_LTPMasterDataLocalService.getLTPSignUp_LTPMasterData(ID);
	}

	/**
	* Updates the LTPSignUp_LTPMasterData in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUp_LTPMasterData the LTPSignUp_LTPMasterData
	* @return the LTPSignUp_LTPMasterData that was updated
	*/
	@Override
	public LTPDatabase.model.LTPSignUp_LTPMasterData updateLTPSignUp_LTPMasterData(
		LTPDatabase.model.LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		return _ltpSignUp_LTPMasterDataLocalService.updateLTPSignUp_LTPMasterData(ltpSignUp_LTPMasterData);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _ltpSignUp_LTPMasterDataLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ltpSignUp_LTPMasterDataLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _ltpSignUp_LTPMasterDataLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUp_LTPMasterDataLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUp_LTPMasterDataLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of LTPSignUp_LTPMasterDatas.
	*
	* @return the number of LTPSignUp_LTPMasterDatas
	*/
	@Override
	public int getLTPSignUp_LTPMasterDatasCount() {
		return _ltpSignUp_LTPMasterDataLocalService.getLTPSignUp_LTPMasterDatasCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _ltpSignUp_LTPMasterDataLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ltpSignUp_LTPMasterDataLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _ltpSignUp_LTPMasterDataLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _ltpSignUp_LTPMasterDataLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	@Override
	public java.util.List<LTPDatabase.model.LTPSignUp_LTPMasterData> findByLTPSignUpID(
		long ltpSignUpId) {
		return _ltpSignUp_LTPMasterDataLocalService.findByLTPSignUpID(ltpSignUpId);
	}

	@Override
	public java.util.List<LTPDatabase.model.LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long ltpSignUpMasterDataId) {
		return _ltpSignUp_LTPMasterDataLocalService.findByLTPSignUpMasterDataId(ltpSignUpMasterDataId);
	}

	/**
	* Returns a range of all the LTPSignUp_LTPMasterDatas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @return the range of LTPSignUp_LTPMasterDatas
	*/
	@Override
	public java.util.List<LTPDatabase.model.LTPSignUp_LTPMasterData> getLTPSignUp_LTPMasterDatas(
		int start, int end) {
		return _ltpSignUp_LTPMasterDataLocalService.getLTPSignUp_LTPMasterDatas(start,
			end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ltpSignUp_LTPMasterDataLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _ltpSignUp_LTPMasterDataLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public LTPSignUp_LTPMasterDataLocalService getWrappedService() {
		return _ltpSignUp_LTPMasterDataLocalService;
	}

	@Override
	public void setWrappedService(
		LTPSignUp_LTPMasterDataLocalService ltpSignUp_LTPMasterDataLocalService) {
		_ltpSignUp_LTPMasterDataLocalService = ltpSignUp_LTPMasterDataLocalService;
	}

	private LTPSignUp_LTPMasterDataLocalService _ltpSignUp_LTPMasterDataLocalService;
}