/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link LTPSignUpLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpLocalService
 * @generated
 */
@ProviderType
public class LTPSignUpLocalServiceWrapper implements LTPSignUpLocalService,
	ServiceWrapper<LTPSignUpLocalService> {
	public LTPSignUpLocalServiceWrapper(
		LTPSignUpLocalService ltpSignUpLocalService) {
		_ltpSignUpLocalService = ltpSignUpLocalService;
	}

	/**
	* Adds the LTPSignUp to the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUp the LTPSignUp
	* @return the LTPSignUp that was added
	*/
	@Override
	public LTPDatabase.model.LTPSignUp addLTPSignUp(
		LTPDatabase.model.LTPSignUp ltpSignUp) {
		return _ltpSignUpLocalService.addLTPSignUp(ltpSignUp);
	}

	@Override
	public LTPDatabase.model.LTPSignUp addLTPSignUp(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String mobileNumber,
		java.util.Map<java.lang.Integer, java.lang.String> ltpMap)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpLocalService.addLTPSignUp(firstName, lastName,
			mobileNumber, ltpMap);
	}

	/**
	* Creates a new LTPSignUp with the primary key. Does not add the LTPSignUp to the database.
	*
	* @param ID the primary key for the new LTPSignUp
	* @return the new LTPSignUp
	*/
	@Override
	public LTPDatabase.model.LTPSignUp createLTPSignUp(long ID) {
		return _ltpSignUpLocalService.createLTPSignUp(ID);
	}

	/**
	* Deletes the LTPSignUp from the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUp the LTPSignUp
	* @return the LTPSignUp that was removed
	*/
	@Override
	public LTPDatabase.model.LTPSignUp deleteLTPSignUp(
		LTPDatabase.model.LTPSignUp ltpSignUp) {
		return _ltpSignUpLocalService.deleteLTPSignUp(ltpSignUp);
	}

	/**
	* Deletes the LTPSignUp with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUp
	* @return the LTPSignUp that was removed
	* @throws PortalException if a LTPSignUp with the primary key could not be found
	*/
	@Override
	public LTPDatabase.model.LTPSignUp deleteLTPSignUp(long ID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpLocalService.deleteLTPSignUp(ID);
	}

	/**
	* @param id
	* @return
	*/
	@Override
	public LTPDatabase.model.LTPSignUp deleteLTPSignUpWithDependents(long id) {
		return _ltpSignUpLocalService.deleteLTPSignUpWithDependents(id);
	}

	@Override
	public LTPDatabase.model.LTPSignUp fetchLTPSignUp(long ID) {
		return _ltpSignUpLocalService.fetchLTPSignUp(ID);
	}

	/**
	* Returns the LTPSignUp with the primary key.
	*
	* @param ID the primary key of the LTPSignUp
	* @return the LTPSignUp
	* @throws PortalException if a LTPSignUp with the primary key could not be found
	*/
	@Override
	public LTPDatabase.model.LTPSignUp getLTPSignUp(long ID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpLocalService.getLTPSignUp(ID);
	}

	/**
	* Updates the LTPSignUp in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUp the LTPSignUp
	* @return the LTPSignUp that was updated
	*/
	@Override
	public LTPDatabase.model.LTPSignUp updateLTPSignUp(
		LTPDatabase.model.LTPSignUp ltpSignUp) {
		return _ltpSignUpLocalService.updateLTPSignUp(ltpSignUp);
	}

	@Override
	public LTPDatabase.model.LTPSignUp updateLTPSignUp(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String mobileNumber,
		java.util.Map<java.lang.Integer, java.lang.String> ltpMap,
		long ltpSignUpId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpLocalService.updateLTPSignUp(firstName, lastName,
			mobileNumber, ltpMap, ltpSignUpId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _ltpSignUpLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ltpSignUpLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _ltpSignUpLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns the number of LTPSignUps.
	*
	* @return the number of LTPSignUps
	*/
	@Override
	public int getLTPSignUpsCount() {
		return _ltpSignUpLocalService.getLTPSignUpsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _ltpSignUpLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ltpSignUpLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _ltpSignUpLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _ltpSignUpLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns a range of all the LTPSignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUps
	* @param end the upper bound of the range of LTPSignUps (not inclusive)
	* @return the range of LTPSignUps
	*/
	@Override
	public java.util.List<LTPDatabase.model.LTPSignUp> getLTPSignUps(
		int start, int end) {
		return _ltpSignUpLocalService.getLTPSignUps(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ltpSignUpLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _ltpSignUpLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public LTPSignUpLocalService getWrappedService() {
		return _ltpSignUpLocalService;
	}

	@Override
	public void setWrappedService(LTPSignUpLocalService ltpSignUpLocalService) {
		_ltpSignUpLocalService = ltpSignUpLocalService;
	}

	private LTPSignUpLocalService _ltpSignUpLocalService;
}