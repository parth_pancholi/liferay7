/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service;

import LTPDatabase.model.LTPSignUpPreferences;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for LTPSignUpPreferences. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpPreferencesLocalServiceUtil
 * @see LTPDatabase.service.base.LTPSignUpPreferencesLocalServiceBaseImpl
 * @see LTPDatabase.service.impl.LTPSignUpPreferencesLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface LTPSignUpPreferencesLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LTPSignUpPreferencesLocalServiceUtil} to access the LTPSignUpPreferences local service. Add custom service methods to {@link LTPDatabase.service.impl.LTPSignUpPreferencesLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Adds the LTPSignUpPreferences to the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpPreferences the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public LTPSignUpPreferences addLTPSignUpPreferences(
		LTPSignUpPreferences ltpSignUpPreferences);

	/**
	* @param morningWalkTitle
	* @param dinnerPrasadTitle
	* @param volunteerTitle
	* @param ltpSignUpPreferenceId
	* @return
	* @throws PortalException
	*/
	public LTPSignUpPreferences addUpdateLTPSignUpPreferences(
		java.lang.String morningWalkTitle, java.lang.String dinnerPrasadTitle,
		java.lang.String volunteerTitle, long ltpSignUpPreferenceId)
		throws PortalException;

	/**
	* Creates a new LTPSignUpPreferences with the primary key. Does not add the LTPSignUpPreferences to the database.
	*
	* @param ID the primary key for the new LTPSignUpPreferences
	* @return the new LTPSignUpPreferences
	*/
	public LTPSignUpPreferences createLTPSignUpPreferences(long ID);

	/**
	* @param entry
	* @return
	* @throws PortalException
	*/
	public LTPSignUpPreferences deleteLTPSignUpPreference(
		LTPSignUpPreferences entry) throws PortalException;

	/**
	* Deletes the LTPSignUpPreferences from the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpPreferences the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public LTPSignUpPreferences deleteLTPSignUpPreferences(
		LTPSignUpPreferences ltpSignUpPreferences);

	/**
	* Deletes the LTPSignUpPreferences with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was removed
	* @throws PortalException if a LTPSignUpPreferences with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public LTPSignUpPreferences deleteLTPSignUpPreferences(long ID)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public LTPSignUpPreferences fetchLTPSignUpPreferences(long ID);

	/**
	* Returns the LTPSignUpPreferences with the primary key.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences
	* @throws PortalException if a LTPSignUpPreferences with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public LTPSignUpPreferences getLTPSignUpPreferences(long ID)
		throws PortalException;

	/**
	* Updates the LTPSignUpPreferences in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpPreferences the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public LTPSignUpPreferences updateLTPSignUpPreferences(
		LTPSignUpPreferences ltpSignUpPreferences);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* @return
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getLTPSignUpPreferencesCount();

	/**
	* Returns the number of LTPSignUpPreferenceses.
	*
	* @return the number of LTPSignUpPreferenceses
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getLTPSignUpPreferencesesCount();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	/**
	* @param start
	* @param end
	* @return
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LTPSignUpPreferences> getLTPSignUpPreferences(int start, int end);

	/**
	* Returns a range of all the LTPSignUpPreferenceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpPreferenceses
	* @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	* @return the range of LTPSignUpPreferenceses
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LTPSignUpPreferences> getLTPSignUpPreferenceses(int start,
		int end);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);
}