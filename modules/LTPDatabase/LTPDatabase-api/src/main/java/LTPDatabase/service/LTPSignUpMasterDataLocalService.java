/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service;

import LTPDatabase.model.LTPSignUpMasterData;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.Date;
import java.util.List;

/**
 * Provides the local service interface for LTPSignUpMasterData. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpMasterDataLocalServiceUtil
 * @see LTPDatabase.service.base.LTPSignUpMasterDataLocalServiceBaseImpl
 * @see LTPDatabase.service.impl.LTPSignUpMasterDataLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface LTPSignUpMasterDataLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LTPSignUpMasterDataLocalServiceUtil} to access the LTPSignUpMasterData local service. Add custom service methods to {@link LTPDatabase.service.impl.LTPSignUpMasterDataLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* Adds the LTPSignUpMasterData to the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpMasterData the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public LTPSignUpMasterData addLTPSignUpMasterData(
		LTPSignUpMasterData ltpSignUpMasterData);

	/**
	* @param morningWalkTitle
	* @param dinnerPrasadTitle
	* @param volunteerTitle
	* @param ltpSignUpMasterDataId
	* @return
	* @throws PortalException
	*/
	public LTPSignUpMasterData addUpdateLTPSignUpMasterData(
		long ltpSignUpMasterDataId, java.lang.String extraText, Date eventDate,
		boolean isMorningWalk, boolean isVolunteer, boolean isDinnerPrasad)
		throws PortalException;

	/**
	* Creates a new LTPSignUpMasterData with the primary key. Does not add the LTPSignUpMasterData to the database.
	*
	* @param ID the primary key for the new LTPSignUpMasterData
	* @return the new LTPSignUpMasterData
	*/
	public LTPSignUpMasterData createLTPSignUpMasterData(long ID);

	/**
	* Deletes the LTPSignUpMasterData from the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpMasterData the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public LTPSignUpMasterData deleteLTPSignUpMasterData(
		LTPSignUpMasterData ltpSignUpMasterData);

	/**
	* Deletes the LTPSignUpMasterData with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was removed
	* @throws PortalException if a LTPSignUpMasterData with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public LTPSignUpMasterData deleteLTPSignUpMasterData(long ID)
		throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public LTPSignUpMasterData fetchLTPSignUpMasterData(long ID);

	/**
	* Returns the LTPSignUpMasterData with the primary key.
	*
	* @param ID the primary key of the LTPSignUpMasterData
	* @return the LTPSignUpMasterData
	* @throws PortalException if a LTPSignUpMasterData with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public LTPSignUpMasterData getLTPSignUpMasterData(long ID)
		throws PortalException;

	/**
	* Updates the LTPSignUpMasterData in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpMasterData the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public LTPSignUpMasterData updateLTPSignUpMasterData(
		LTPSignUpMasterData ltpSignUpMasterData);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* @return
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getLTPSignUpMasterDataCount();

	/**
	* Returns the number of LTPSignUpMasterDatas.
	*
	* @return the number of LTPSignUpMasterDatas
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getLTPSignUpMasterDatasCount();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	/**
	* @param start
	* @param end
	* @return
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LTPSignUpMasterData> getAllLTPSignUpMasterData(int start,
		int end);

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LTPSignUpMasterData> getLTPSignUpMasterData(int start, int end);

	/**
	* Returns a range of all the LTPSignUpMasterDatas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpMasterDatas
	* @param end the upper bound of the range of LTPSignUpMasterDatas (not inclusive)
	* @return the range of LTPSignUpMasterDatas
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<LTPSignUpMasterData> getLTPSignUpMasterDatas(int start, int end);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);
}