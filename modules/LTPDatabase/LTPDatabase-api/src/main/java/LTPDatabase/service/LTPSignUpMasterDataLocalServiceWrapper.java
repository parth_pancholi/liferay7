/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link LTPSignUpMasterDataLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpMasterDataLocalService
 * @generated
 */
@ProviderType
public class LTPSignUpMasterDataLocalServiceWrapper
	implements LTPSignUpMasterDataLocalService,
		ServiceWrapper<LTPSignUpMasterDataLocalService> {
	public LTPSignUpMasterDataLocalServiceWrapper(
		LTPSignUpMasterDataLocalService ltpSignUpMasterDataLocalService) {
		_ltpSignUpMasterDataLocalService = ltpSignUpMasterDataLocalService;
	}

	/**
	* Adds the LTPSignUpMasterData to the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpMasterData the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was added
	*/
	@Override
	public LTPDatabase.model.LTPSignUpMasterData addLTPSignUpMasterData(
		LTPDatabase.model.LTPSignUpMasterData ltpSignUpMasterData) {
		return _ltpSignUpMasterDataLocalService.addLTPSignUpMasterData(ltpSignUpMasterData);
	}

	/**
	* @param morningWalkTitle
	* @param dinnerPrasadTitle
	* @param volunteerTitle
	* @param ltpSignUpMasterDataId
	* @return
	* @throws PortalException
	*/
	@Override
	public LTPDatabase.model.LTPSignUpMasterData addUpdateLTPSignUpMasterData(
		long ltpSignUpMasterDataId, java.lang.String extraText,
		java.util.Date eventDate, boolean isMorningWalk, boolean isVolunteer,
		boolean isDinnerPrasad)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpMasterDataLocalService.addUpdateLTPSignUpMasterData(ltpSignUpMasterDataId,
			extraText, eventDate, isMorningWalk, isVolunteer, isDinnerPrasad);
	}

	/**
	* Creates a new LTPSignUpMasterData with the primary key. Does not add the LTPSignUpMasterData to the database.
	*
	* @param ID the primary key for the new LTPSignUpMasterData
	* @return the new LTPSignUpMasterData
	*/
	@Override
	public LTPDatabase.model.LTPSignUpMasterData createLTPSignUpMasterData(
		long ID) {
		return _ltpSignUpMasterDataLocalService.createLTPSignUpMasterData(ID);
	}

	/**
	* Deletes the LTPSignUpMasterData from the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpMasterData the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was removed
	*/
	@Override
	public LTPDatabase.model.LTPSignUpMasterData deleteLTPSignUpMasterData(
		LTPDatabase.model.LTPSignUpMasterData ltpSignUpMasterData) {
		return _ltpSignUpMasterDataLocalService.deleteLTPSignUpMasterData(ltpSignUpMasterData);
	}

	/**
	* Deletes the LTPSignUpMasterData with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was removed
	* @throws PortalException if a LTPSignUpMasterData with the primary key could not be found
	*/
	@Override
	public LTPDatabase.model.LTPSignUpMasterData deleteLTPSignUpMasterData(
		long ID) throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpMasterDataLocalService.deleteLTPSignUpMasterData(ID);
	}

	@Override
	public LTPDatabase.model.LTPSignUpMasterData fetchLTPSignUpMasterData(
		long ID) {
		return _ltpSignUpMasterDataLocalService.fetchLTPSignUpMasterData(ID);
	}

	/**
	* Returns the LTPSignUpMasterData with the primary key.
	*
	* @param ID the primary key of the LTPSignUpMasterData
	* @return the LTPSignUpMasterData
	* @throws PortalException if a LTPSignUpMasterData with the primary key could not be found
	*/
	@Override
	public LTPDatabase.model.LTPSignUpMasterData getLTPSignUpMasterData(long ID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpMasterDataLocalService.getLTPSignUpMasterData(ID);
	}

	/**
	* Updates the LTPSignUpMasterData in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpMasterData the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was updated
	*/
	@Override
	public LTPDatabase.model.LTPSignUpMasterData updateLTPSignUpMasterData(
		LTPDatabase.model.LTPSignUpMasterData ltpSignUpMasterData) {
		return _ltpSignUpMasterDataLocalService.updateLTPSignUpMasterData(ltpSignUpMasterData);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _ltpSignUpMasterDataLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ltpSignUpMasterDataLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _ltpSignUpMasterDataLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpMasterDataLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpMasterDataLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* @return
	*/
	@Override
	public int getLTPSignUpMasterDataCount() {
		return _ltpSignUpMasterDataLocalService.getLTPSignUpMasterDataCount();
	}

	/**
	* Returns the number of LTPSignUpMasterDatas.
	*
	* @return the number of LTPSignUpMasterDatas
	*/
	@Override
	public int getLTPSignUpMasterDatasCount() {
		return _ltpSignUpMasterDataLocalService.getLTPSignUpMasterDatasCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _ltpSignUpMasterDataLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ltpSignUpMasterDataLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _ltpSignUpMasterDataLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _ltpSignUpMasterDataLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* @param start
	* @param end
	* @return
	*/
	@Override
	public java.util.List<LTPDatabase.model.LTPSignUpMasterData> getAllLTPSignUpMasterData(
		int start, int end) {
		return _ltpSignUpMasterDataLocalService.getAllLTPSignUpMasterData(start,
			end);
	}

	@Override
	public java.util.List<LTPDatabase.model.LTPSignUpMasterData> getLTPSignUpMasterData(
		int start, int end) {
		return _ltpSignUpMasterDataLocalService.getLTPSignUpMasterData(start,
			end);
	}

	/**
	* Returns a range of all the LTPSignUpMasterDatas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpMasterDatas
	* @param end the upper bound of the range of LTPSignUpMasterDatas (not inclusive)
	* @return the range of LTPSignUpMasterDatas
	*/
	@Override
	public java.util.List<LTPDatabase.model.LTPSignUpMasterData> getLTPSignUpMasterDatas(
		int start, int end) {
		return _ltpSignUpMasterDataLocalService.getLTPSignUpMasterDatas(start,
			end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ltpSignUpMasterDataLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _ltpSignUpMasterDataLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public LTPSignUpMasterDataLocalService getWrappedService() {
		return _ltpSignUpMasterDataLocalService;
	}

	@Override
	public void setWrappedService(
		LTPSignUpMasterDataLocalService ltpSignUpMasterDataLocalService) {
		_ltpSignUpMasterDataLocalService = ltpSignUpMasterDataLocalService;
	}

	private LTPSignUpMasterDataLocalService _ltpSignUpMasterDataLocalService;
}