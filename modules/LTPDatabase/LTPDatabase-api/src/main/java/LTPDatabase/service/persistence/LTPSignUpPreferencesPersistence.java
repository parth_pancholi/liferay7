/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.persistence;

import LTPDatabase.exception.NoSuchLTPSignUpPreferencesException;

import LTPDatabase.model.LTPSignUpPreferences;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the LTPSignUpPreferences service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPDatabase.service.persistence.impl.LTPSignUpPreferencesPersistenceImpl
 * @see LTPSignUpPreferencesUtil
 * @generated
 */
@ProviderType
public interface LTPSignUpPreferencesPersistence extends BasePersistence<LTPSignUpPreferences> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LTPSignUpPreferencesUtil} to access the LTPSignUpPreferences persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the LTPSignUpPreferences in the entity cache if it is enabled.
	*
	* @param ltpSignUpPreferences the LTPSignUpPreferences
	*/
	public void cacheResult(LTPSignUpPreferences ltpSignUpPreferences);

	/**
	* Caches the LTPSignUpPreferenceses in the entity cache if it is enabled.
	*
	* @param ltpSignUpPreferenceses the LTPSignUpPreferenceses
	*/
	public void cacheResult(
		java.util.List<LTPSignUpPreferences> ltpSignUpPreferenceses);

	/**
	* Creates a new LTPSignUpPreferences with the primary key. Does not add the LTPSignUpPreferences to the database.
	*
	* @param ID the primary key for the new LTPSignUpPreferences
	* @return the new LTPSignUpPreferences
	*/
	public LTPSignUpPreferences create(long ID);

	/**
	* Removes the LTPSignUpPreferences with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was removed
	* @throws NoSuchLTPSignUpPreferencesException if a LTPSignUpPreferences with the primary key could not be found
	*/
	public LTPSignUpPreferences remove(long ID)
		throws NoSuchLTPSignUpPreferencesException;

	public LTPSignUpPreferences updateImpl(
		LTPSignUpPreferences ltpSignUpPreferences);

	/**
	* Returns the LTPSignUpPreferences with the primary key or throws a {@link NoSuchLTPSignUpPreferencesException} if it could not be found.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences
	* @throws NoSuchLTPSignUpPreferencesException if a LTPSignUpPreferences with the primary key could not be found
	*/
	public LTPSignUpPreferences findByPrimaryKey(long ID)
		throws NoSuchLTPSignUpPreferencesException;

	/**
	* Returns the LTPSignUpPreferences with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences, or <code>null</code> if a LTPSignUpPreferences with the primary key could not be found
	*/
	public LTPSignUpPreferences fetchByPrimaryKey(long ID);

	@Override
	public java.util.Map<java.io.Serializable, LTPSignUpPreferences> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the LTPSignUpPreferenceses.
	*
	* @return the LTPSignUpPreferenceses
	*/
	public java.util.List<LTPSignUpPreferences> findAll();

	/**
	* Returns a range of all the LTPSignUpPreferenceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpPreferenceses
	* @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	* @return the range of LTPSignUpPreferenceses
	*/
	public java.util.List<LTPSignUpPreferences> findAll(int start, int end);

	/**
	* Returns an ordered range of all the LTPSignUpPreferenceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpPreferenceses
	* @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of LTPSignUpPreferenceses
	*/
	public java.util.List<LTPSignUpPreferences> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUpPreferences> orderByComparator);

	/**
	* Returns an ordered range of all the LTPSignUpPreferenceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpPreferenceses
	* @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of LTPSignUpPreferenceses
	*/
	public java.util.List<LTPSignUpPreferences> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUpPreferences> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the LTPSignUpPreferenceses from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of LTPSignUpPreferenceses.
	*
	* @return the number of LTPSignUpPreferenceses
	*/
	public int countAll();
}