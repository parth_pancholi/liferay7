/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for LTPSignUpPreferences. This utility wraps
 * {@link LTPDatabase.service.impl.LTPSignUpPreferencesLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpPreferencesLocalService
 * @see LTPDatabase.service.base.LTPSignUpPreferencesLocalServiceBaseImpl
 * @see LTPDatabase.service.impl.LTPSignUpPreferencesLocalServiceImpl
 * @generated
 */
@ProviderType
public class LTPSignUpPreferencesLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link LTPDatabase.service.impl.LTPSignUpPreferencesLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the LTPSignUpPreferences to the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpPreferences the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was added
	*/
	public static LTPDatabase.model.LTPSignUpPreferences addLTPSignUpPreferences(
		LTPDatabase.model.LTPSignUpPreferences ltpSignUpPreferences) {
		return getService().addLTPSignUpPreferences(ltpSignUpPreferences);
	}

	/**
	* @param morningWalkTitle
	* @param dinnerPrasadTitle
	* @param volunteerTitle
	* @param ltpSignUpPreferenceId
	* @return
	* @throws PortalException
	*/
	public static LTPDatabase.model.LTPSignUpPreferences addUpdateLTPSignUpPreferences(
		java.lang.String morningWalkTitle, java.lang.String dinnerPrasadTitle,
		java.lang.String volunteerTitle, long ltpSignUpPreferenceId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .addUpdateLTPSignUpPreferences(morningWalkTitle,
			dinnerPrasadTitle, volunteerTitle, ltpSignUpPreferenceId);
	}

	/**
	* Creates a new LTPSignUpPreferences with the primary key. Does not add the LTPSignUpPreferences to the database.
	*
	* @param ID the primary key for the new LTPSignUpPreferences
	* @return the new LTPSignUpPreferences
	*/
	public static LTPDatabase.model.LTPSignUpPreferences createLTPSignUpPreferences(
		long ID) {
		return getService().createLTPSignUpPreferences(ID);
	}

	/**
	* @param entry
	* @return
	* @throws PortalException
	*/
	public static LTPDatabase.model.LTPSignUpPreferences deleteLTPSignUpPreference(
		LTPDatabase.model.LTPSignUpPreferences entry)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteLTPSignUpPreference(entry);
	}

	/**
	* Deletes the LTPSignUpPreferences from the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpPreferences the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was removed
	*/
	public static LTPDatabase.model.LTPSignUpPreferences deleteLTPSignUpPreferences(
		LTPDatabase.model.LTPSignUpPreferences ltpSignUpPreferences) {
		return getService().deleteLTPSignUpPreferences(ltpSignUpPreferences);
	}

	/**
	* Deletes the LTPSignUpPreferences with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was removed
	* @throws PortalException if a LTPSignUpPreferences with the primary key could not be found
	*/
	public static LTPDatabase.model.LTPSignUpPreferences deleteLTPSignUpPreferences(
		long ID) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteLTPSignUpPreferences(ID);
	}

	public static LTPDatabase.model.LTPSignUpPreferences fetchLTPSignUpPreferences(
		long ID) {
		return getService().fetchLTPSignUpPreferences(ID);
	}

	/**
	* Returns the LTPSignUpPreferences with the primary key.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences
	* @throws PortalException if a LTPSignUpPreferences with the primary key could not be found
	*/
	public static LTPDatabase.model.LTPSignUpPreferences getLTPSignUpPreferences(
		long ID) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getLTPSignUpPreferences(ID);
	}

	/**
	* Updates the LTPSignUpPreferences in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpPreferences the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was updated
	*/
	public static LTPDatabase.model.LTPSignUpPreferences updateLTPSignUpPreferences(
		LTPDatabase.model.LTPSignUpPreferences ltpSignUpPreferences) {
		return getService().updateLTPSignUpPreferences(ltpSignUpPreferences);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* @return
	*/
	public static int getLTPSignUpPreferencesCount() {
		return getService().getLTPSignUpPreferencesCount();
	}

	/**
	* Returns the number of LTPSignUpPreferenceses.
	*
	* @return the number of LTPSignUpPreferenceses
	*/
	public static int getLTPSignUpPreferencesesCount() {
		return getService().getLTPSignUpPreferencesesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* @param start
	* @param end
	* @return
	*/
	public static java.util.List<LTPDatabase.model.LTPSignUpPreferences> getLTPSignUpPreferences(
		int start, int end) {
		return getService().getLTPSignUpPreferences(start, end);
	}

	/**
	* Returns a range of all the LTPSignUpPreferenceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpPreferenceses
	* @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	* @return the range of LTPSignUpPreferenceses
	*/
	public static java.util.List<LTPDatabase.model.LTPSignUpPreferences> getLTPSignUpPreferenceses(
		int start, int end) {
		return getService().getLTPSignUpPreferenceses(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static LTPSignUpPreferencesLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LTPSignUpPreferencesLocalService, LTPSignUpPreferencesLocalService> _serviceTracker =
		ServiceTrackerFactory.open(LTPSignUpPreferencesLocalService.class);
}