/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for LTPSignUpMasterData. This utility wraps
 * {@link LTPDatabase.service.impl.LTPSignUpMasterDataLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpMasterDataLocalService
 * @see LTPDatabase.service.base.LTPSignUpMasterDataLocalServiceBaseImpl
 * @see LTPDatabase.service.impl.LTPSignUpMasterDataLocalServiceImpl
 * @generated
 */
@ProviderType
public class LTPSignUpMasterDataLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link LTPDatabase.service.impl.LTPSignUpMasterDataLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the LTPSignUpMasterData to the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpMasterData the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was added
	*/
	public static LTPDatabase.model.LTPSignUpMasterData addLTPSignUpMasterData(
		LTPDatabase.model.LTPSignUpMasterData ltpSignUpMasterData) {
		return getService().addLTPSignUpMasterData(ltpSignUpMasterData);
	}

	/**
	* @param morningWalkTitle
	* @param dinnerPrasadTitle
	* @param volunteerTitle
	* @param ltpSignUpMasterDataId
	* @return
	* @throws PortalException
	*/
	public static LTPDatabase.model.LTPSignUpMasterData addUpdateLTPSignUpMasterData(
		long ltpSignUpMasterDataId, java.lang.String extraText,
		java.util.Date eventDate, boolean isMorningWalk, boolean isVolunteer,
		boolean isDinnerPrasad)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .addUpdateLTPSignUpMasterData(ltpSignUpMasterDataId,
			extraText, eventDate, isMorningWalk, isVolunteer, isDinnerPrasad);
	}

	/**
	* Creates a new LTPSignUpMasterData with the primary key. Does not add the LTPSignUpMasterData to the database.
	*
	* @param ID the primary key for the new LTPSignUpMasterData
	* @return the new LTPSignUpMasterData
	*/
	public static LTPDatabase.model.LTPSignUpMasterData createLTPSignUpMasterData(
		long ID) {
		return getService().createLTPSignUpMasterData(ID);
	}

	/**
	* Deletes the LTPSignUpMasterData from the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpMasterData the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was removed
	*/
	public static LTPDatabase.model.LTPSignUpMasterData deleteLTPSignUpMasterData(
		LTPDatabase.model.LTPSignUpMasterData ltpSignUpMasterData) {
		return getService().deleteLTPSignUpMasterData(ltpSignUpMasterData);
	}

	/**
	* Deletes the LTPSignUpMasterData with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was removed
	* @throws PortalException if a LTPSignUpMasterData with the primary key could not be found
	*/
	public static LTPDatabase.model.LTPSignUpMasterData deleteLTPSignUpMasterData(
		long ID) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteLTPSignUpMasterData(ID);
	}

	public static LTPDatabase.model.LTPSignUpMasterData fetchLTPSignUpMasterData(
		long ID) {
		return getService().fetchLTPSignUpMasterData(ID);
	}

	/**
	* Returns the LTPSignUpMasterData with the primary key.
	*
	* @param ID the primary key of the LTPSignUpMasterData
	* @return the LTPSignUpMasterData
	* @throws PortalException if a LTPSignUpMasterData with the primary key could not be found
	*/
	public static LTPDatabase.model.LTPSignUpMasterData getLTPSignUpMasterData(
		long ID) throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getLTPSignUpMasterData(ID);
	}

	/**
	* Updates the LTPSignUpMasterData in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpMasterData the LTPSignUpMasterData
	* @return the LTPSignUpMasterData that was updated
	*/
	public static LTPDatabase.model.LTPSignUpMasterData updateLTPSignUpMasterData(
		LTPDatabase.model.LTPSignUpMasterData ltpSignUpMasterData) {
		return getService().updateLTPSignUpMasterData(ltpSignUpMasterData);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* @return
	*/
	public static int getLTPSignUpMasterDataCount() {
		return getService().getLTPSignUpMasterDataCount();
	}

	/**
	* Returns the number of LTPSignUpMasterDatas.
	*
	* @return the number of LTPSignUpMasterDatas
	*/
	public static int getLTPSignUpMasterDatasCount() {
		return getService().getLTPSignUpMasterDatasCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* @param start
	* @param end
	* @return
	*/
	public static java.util.List<LTPDatabase.model.LTPSignUpMasterData> getAllLTPSignUpMasterData(
		int start, int end) {
		return getService().getAllLTPSignUpMasterData(start, end);
	}

	public static java.util.List<LTPDatabase.model.LTPSignUpMasterData> getLTPSignUpMasterData(
		int start, int end) {
		return getService().getLTPSignUpMasterData(start, end);
	}

	/**
	* Returns a range of all the LTPSignUpMasterDatas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpMasterDatas
	* @param end the upper bound of the range of LTPSignUpMasterDatas (not inclusive)
	* @return the range of LTPSignUpMasterDatas
	*/
	public static java.util.List<LTPDatabase.model.LTPSignUpMasterData> getLTPSignUpMasterDatas(
		int start, int end) {
		return getService().getLTPSignUpMasterDatas(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static LTPSignUpMasterDataLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LTPSignUpMasterDataLocalService, LTPSignUpMasterDataLocalService> _serviceTracker =
		ServiceTrackerFactory.open(LTPSignUpMasterDataLocalService.class);
}