/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.persistence;

import LTPDatabase.model.LTPSignUp_LTPMasterData;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the LTPSignUp_LTPMasterData service. This utility wraps {@link LTPDatabase.service.persistence.impl.LTPSignUp_LTPMasterDataPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUp_LTPMasterDataPersistence
 * @see LTPDatabase.service.persistence.impl.LTPSignUp_LTPMasterDataPersistenceImpl
 * @generated
 */
@ProviderType
public class LTPSignUp_LTPMasterDataUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		getPersistence().clearCache(ltpSignUp_LTPMasterData);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LTPSignUp_LTPMasterData> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LTPSignUp_LTPMasterData> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LTPSignUp_LTPMasterData> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static LTPSignUp_LTPMasterData update(
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		return getPersistence().update(ltpSignUp_LTPMasterData);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static LTPSignUp_LTPMasterData update(
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData,
		ServiceContext serviceContext) {
		return getPersistence().update(ltpSignUp_LTPMasterData, serviceContext);
	}

	/**
	* Returns all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @return the matching LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findByLTPSignUpID(
		long LTPSignUpID) {
		return getPersistence().findByLTPSignUpID(LTPSignUpID);
	}

	/**
	* Returns a range of all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @return the range of matching LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findByLTPSignUpID(
		long LTPSignUpID, int start, int end) {
		return getPersistence().findByLTPSignUpID(LTPSignUpID, start, end);
	}

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findByLTPSignUpID(
		long LTPSignUpID, int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		return getPersistence()
				   .findByLTPSignUpID(LTPSignUpID, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findByLTPSignUpID(
		long LTPSignUpID, int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByLTPSignUpID(LTPSignUpID, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public static LTPSignUp_LTPMasterData findByLTPSignUpID_First(
		long LTPSignUpID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws LTPDatabase.exception.NoSuchLTPSignUp_LTPMasterDataException {
		return getPersistence()
				   .findByLTPSignUpID_First(LTPSignUpID, orderByComparator);
	}

	/**
	* Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public static LTPSignUp_LTPMasterData fetchByLTPSignUpID_First(
		long LTPSignUpID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		return getPersistence()
				   .fetchByLTPSignUpID_First(LTPSignUpID, orderByComparator);
	}

	/**
	* Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public static LTPSignUp_LTPMasterData findByLTPSignUpID_Last(
		long LTPSignUpID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws LTPDatabase.exception.NoSuchLTPSignUp_LTPMasterDataException {
		return getPersistence()
				   .findByLTPSignUpID_Last(LTPSignUpID, orderByComparator);
	}

	/**
	* Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public static LTPSignUp_LTPMasterData fetchByLTPSignUpID_Last(
		long LTPSignUpID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		return getPersistence()
				   .fetchByLTPSignUpID_Last(LTPSignUpID, orderByComparator);
	}

	/**
	* Returns the LTPSignUp_LTPMasterDatas before and after the current LTPSignUp_LTPMasterData in the ordered set where LTPSignUpID = &#63;.
	*
	* @param ID the primary key of the current LTPSignUp_LTPMasterData
	* @param LTPSignUpID the ltp sign up ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	public static LTPSignUp_LTPMasterData[] findByLTPSignUpID_PrevAndNext(
		long ID, long LTPSignUpID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws LTPDatabase.exception.NoSuchLTPSignUp_LTPMasterDataException {
		return getPersistence()
				   .findByLTPSignUpID_PrevAndNext(ID, LTPSignUpID,
			orderByComparator);
	}

	/**
	* Removes all the LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63; from the database.
	*
	* @param LTPSignUpID the ltp sign up ID
	*/
	public static void removeByLTPSignUpID(long LTPSignUpID) {
		getPersistence().removeByLTPSignUpID(LTPSignUpID);
	}

	/**
	* Returns the number of LTPSignUp_LTPMasterDatas where LTPSignUpID = &#63;.
	*
	* @param LTPSignUpID the ltp sign up ID
	* @return the number of matching LTPSignUp_LTPMasterDatas
	*/
	public static int countByLTPSignUpID(long LTPSignUpID) {
		return getPersistence().countByLTPSignUpID(LTPSignUpID);
	}

	/**
	* Returns all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @return the matching LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID) {
		return getPersistence().findByLTPSignUpMasterDataId(LTPMasterDataID);
	}

	/**
	* Returns a range of all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @return the range of matching LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID, int start, int end) {
		return getPersistence()
				   .findByLTPSignUpMasterDataId(LTPMasterDataID, start, end);
	}

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID, int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		return getPersistence()
				   .findByLTPSignUpMasterDataId(LTPMasterDataID, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findByLTPSignUpMasterDataId(
		long LTPMasterDataID, int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByLTPSignUpMasterDataId(LTPMasterDataID, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public static LTPSignUp_LTPMasterData findByLTPSignUpMasterDataId_First(
		long LTPMasterDataID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws LTPDatabase.exception.NoSuchLTPSignUp_LTPMasterDataException {
		return getPersistence()
				   .findByLTPSignUpMasterDataId_First(LTPMasterDataID,
			orderByComparator);
	}

	/**
	* Returns the first LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public static LTPSignUp_LTPMasterData fetchByLTPSignUpMasterDataId_First(
		long LTPMasterDataID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		return getPersistence()
				   .fetchByLTPSignUpMasterDataId_First(LTPMasterDataID,
			orderByComparator);
	}

	/**
	* Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public static LTPSignUp_LTPMasterData findByLTPSignUpMasterDataId_Last(
		long LTPMasterDataID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws LTPDatabase.exception.NoSuchLTPSignUp_LTPMasterDataException {
		return getPersistence()
				   .findByLTPSignUpMasterDataId_Last(LTPMasterDataID,
			orderByComparator);
	}

	/**
	* Returns the last LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching LTPSignUp_LTPMasterData, or <code>null</code> if a matching LTPSignUp_LTPMasterData could not be found
	*/
	public static LTPSignUp_LTPMasterData fetchByLTPSignUpMasterDataId_Last(
		long LTPMasterDataID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		return getPersistence()
				   .fetchByLTPSignUpMasterDataId_Last(LTPMasterDataID,
			orderByComparator);
	}

	/**
	* Returns the LTPSignUp_LTPMasterDatas before and after the current LTPSignUp_LTPMasterData in the ordered set where LTPMasterDataID = &#63;.
	*
	* @param ID the primary key of the current LTPSignUp_LTPMasterData
	* @param LTPMasterDataID the ltp master data ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	public static LTPSignUp_LTPMasterData[] findByLTPSignUpMasterDataId_PrevAndNext(
		long ID, long LTPMasterDataID,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator)
		throws LTPDatabase.exception.NoSuchLTPSignUp_LTPMasterDataException {
		return getPersistence()
				   .findByLTPSignUpMasterDataId_PrevAndNext(ID,
			LTPMasterDataID, orderByComparator);
	}

	/**
	* Removes all the LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63; from the database.
	*
	* @param LTPMasterDataID the ltp master data ID
	*/
	public static void removeByLTPSignUpMasterDataId(long LTPMasterDataID) {
		getPersistence().removeByLTPSignUpMasterDataId(LTPMasterDataID);
	}

	/**
	* Returns the number of LTPSignUp_LTPMasterDatas where LTPMasterDataID = &#63;.
	*
	* @param LTPMasterDataID the ltp master data ID
	* @return the number of matching LTPSignUp_LTPMasterDatas
	*/
	public static int countByLTPSignUpMasterDataId(long LTPMasterDataID) {
		return getPersistence().countByLTPSignUpMasterDataId(LTPMasterDataID);
	}

	/**
	* Caches the LTPSignUp_LTPMasterData in the entity cache if it is enabled.
	*
	* @param ltpSignUp_LTPMasterData the LTPSignUp_LTPMasterData
	*/
	public static void cacheResult(
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		getPersistence().cacheResult(ltpSignUp_LTPMasterData);
	}

	/**
	* Caches the LTPSignUp_LTPMasterDatas in the entity cache if it is enabled.
	*
	* @param ltpSignUp_LTPMasterDatas the LTPSignUp_LTPMasterDatas
	*/
	public static void cacheResult(
		List<LTPSignUp_LTPMasterData> ltpSignUp_LTPMasterDatas) {
		getPersistence().cacheResult(ltpSignUp_LTPMasterDatas);
	}

	/**
	* Creates a new LTPSignUp_LTPMasterData with the primary key. Does not add the LTPSignUp_LTPMasterData to the database.
	*
	* @param ID the primary key for the new LTPSignUp_LTPMasterData
	* @return the new LTPSignUp_LTPMasterData
	*/
	public static LTPSignUp_LTPMasterData create(long ID) {
		return getPersistence().create(ID);
	}

	/**
	* Removes the LTPSignUp_LTPMasterData with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUp_LTPMasterData
	* @return the LTPSignUp_LTPMasterData that was removed
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	public static LTPSignUp_LTPMasterData remove(long ID)
		throws LTPDatabase.exception.NoSuchLTPSignUp_LTPMasterDataException {
		return getPersistence().remove(ID);
	}

	public static LTPSignUp_LTPMasterData updateImpl(
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		return getPersistence().updateImpl(ltpSignUp_LTPMasterData);
	}

	/**
	* Returns the LTPSignUp_LTPMasterData with the primary key or throws a {@link NoSuchLTPSignUp_LTPMasterDataException} if it could not be found.
	*
	* @param ID the primary key of the LTPSignUp_LTPMasterData
	* @return the LTPSignUp_LTPMasterData
	* @throws NoSuchLTPSignUp_LTPMasterDataException if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	public static LTPSignUp_LTPMasterData findByPrimaryKey(long ID)
		throws LTPDatabase.exception.NoSuchLTPSignUp_LTPMasterDataException {
		return getPersistence().findByPrimaryKey(ID);
	}

	/**
	* Returns the LTPSignUp_LTPMasterData with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the LTPSignUp_LTPMasterData
	* @return the LTPSignUp_LTPMasterData, or <code>null</code> if a LTPSignUp_LTPMasterData with the primary key could not be found
	*/
	public static LTPSignUp_LTPMasterData fetchByPrimaryKey(long ID) {
		return getPersistence().fetchByPrimaryKey(ID);
	}

	public static java.util.Map<java.io.Serializable, LTPSignUp_LTPMasterData> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the LTPSignUp_LTPMasterDatas.
	*
	* @return the LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the LTPSignUp_LTPMasterDatas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @return the range of LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findAll(int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the LTPSignUp_LTPMasterDatas.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUp_LTPMasterDataModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUp_LTPMasterDatas
	* @param end the upper bound of the range of LTPSignUp_LTPMasterDatas (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of LTPSignUp_LTPMasterDatas
	*/
	public static List<LTPSignUp_LTPMasterData> findAll(int start, int end,
		OrderByComparator<LTPSignUp_LTPMasterData> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the LTPSignUp_LTPMasterDatas from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of LTPSignUp_LTPMasterDatas.
	*
	* @return the number of LTPSignUp_LTPMasterDatas
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static LTPSignUp_LTPMasterDataPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LTPSignUp_LTPMasterDataPersistence, LTPSignUp_LTPMasterDataPersistence> _serviceTracker =
		ServiceTrackerFactory.open(LTPSignUp_LTPMasterDataPersistence.class);
}