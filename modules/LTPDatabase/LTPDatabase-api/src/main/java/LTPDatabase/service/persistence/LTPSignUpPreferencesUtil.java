/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.persistence;

import LTPDatabase.model.LTPSignUpPreferences;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the LTPSignUpPreferences service. This utility wraps {@link LTPDatabase.service.persistence.impl.LTPSignUpPreferencesPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpPreferencesPersistence
 * @see LTPDatabase.service.persistence.impl.LTPSignUpPreferencesPersistenceImpl
 * @generated
 */
@ProviderType
public class LTPSignUpPreferencesUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(LTPSignUpPreferences ltpSignUpPreferences) {
		getPersistence().clearCache(ltpSignUpPreferences);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<LTPSignUpPreferences> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<LTPSignUpPreferences> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<LTPSignUpPreferences> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<LTPSignUpPreferences> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static LTPSignUpPreferences update(
		LTPSignUpPreferences ltpSignUpPreferences) {
		return getPersistence().update(ltpSignUpPreferences);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static LTPSignUpPreferences update(
		LTPSignUpPreferences ltpSignUpPreferences, ServiceContext serviceContext) {
		return getPersistence().update(ltpSignUpPreferences, serviceContext);
	}

	/**
	* Caches the LTPSignUpPreferences in the entity cache if it is enabled.
	*
	* @param ltpSignUpPreferences the LTPSignUpPreferences
	*/
	public static void cacheResult(LTPSignUpPreferences ltpSignUpPreferences) {
		getPersistence().cacheResult(ltpSignUpPreferences);
	}

	/**
	* Caches the LTPSignUpPreferenceses in the entity cache if it is enabled.
	*
	* @param ltpSignUpPreferenceses the LTPSignUpPreferenceses
	*/
	public static void cacheResult(
		List<LTPSignUpPreferences> ltpSignUpPreferenceses) {
		getPersistence().cacheResult(ltpSignUpPreferenceses);
	}

	/**
	* Creates a new LTPSignUpPreferences with the primary key. Does not add the LTPSignUpPreferences to the database.
	*
	* @param ID the primary key for the new LTPSignUpPreferences
	* @return the new LTPSignUpPreferences
	*/
	public static LTPSignUpPreferences create(long ID) {
		return getPersistence().create(ID);
	}

	/**
	* Removes the LTPSignUpPreferences with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was removed
	* @throws NoSuchLTPSignUpPreferencesException if a LTPSignUpPreferences with the primary key could not be found
	*/
	public static LTPSignUpPreferences remove(long ID)
		throws LTPDatabase.exception.NoSuchLTPSignUpPreferencesException {
		return getPersistence().remove(ID);
	}

	public static LTPSignUpPreferences updateImpl(
		LTPSignUpPreferences ltpSignUpPreferences) {
		return getPersistence().updateImpl(ltpSignUpPreferences);
	}

	/**
	* Returns the LTPSignUpPreferences with the primary key or throws a {@link NoSuchLTPSignUpPreferencesException} if it could not be found.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences
	* @throws NoSuchLTPSignUpPreferencesException if a LTPSignUpPreferences with the primary key could not be found
	*/
	public static LTPSignUpPreferences findByPrimaryKey(long ID)
		throws LTPDatabase.exception.NoSuchLTPSignUpPreferencesException {
		return getPersistence().findByPrimaryKey(ID);
	}

	/**
	* Returns the LTPSignUpPreferences with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences, or <code>null</code> if a LTPSignUpPreferences with the primary key could not be found
	*/
	public static LTPSignUpPreferences fetchByPrimaryKey(long ID) {
		return getPersistence().fetchByPrimaryKey(ID);
	}

	public static java.util.Map<java.io.Serializable, LTPSignUpPreferences> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the LTPSignUpPreferenceses.
	*
	* @return the LTPSignUpPreferenceses
	*/
	public static List<LTPSignUpPreferences> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the LTPSignUpPreferenceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpPreferenceses
	* @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	* @return the range of LTPSignUpPreferenceses
	*/
	public static List<LTPSignUpPreferences> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the LTPSignUpPreferenceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpPreferenceses
	* @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of LTPSignUpPreferenceses
	*/
	public static List<LTPSignUpPreferences> findAll(int start, int end,
		OrderByComparator<LTPSignUpPreferences> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the LTPSignUpPreferenceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpPreferenceses
	* @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of LTPSignUpPreferenceses
	*/
	public static List<LTPSignUpPreferences> findAll(int start, int end,
		OrderByComparator<LTPSignUpPreferences> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the LTPSignUpPreferenceses from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of LTPSignUpPreferenceses.
	*
	* @return the number of LTPSignUpPreferenceses
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static LTPSignUpPreferencesPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<LTPSignUpPreferencesPersistence, LTPSignUpPreferencesPersistence> _serviceTracker =
		ServiceTrackerFactory.open(LTPSignUpPreferencesPersistence.class);
}