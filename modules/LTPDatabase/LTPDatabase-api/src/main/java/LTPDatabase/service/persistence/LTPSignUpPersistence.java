/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service.persistence;

import LTPDatabase.exception.NoSuchLTPSignUpException;

import LTPDatabase.model.LTPSignUp;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the LTPSignUp service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPDatabase.service.persistence.impl.LTPSignUpPersistenceImpl
 * @see LTPSignUpUtil
 * @generated
 */
@ProviderType
public interface LTPSignUpPersistence extends BasePersistence<LTPSignUp> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link LTPSignUpUtil} to access the LTPSignUp persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the LTPSignUp in the entity cache if it is enabled.
	*
	* @param ltpSignUp the LTPSignUp
	*/
	public void cacheResult(LTPSignUp ltpSignUp);

	/**
	* Caches the LTPSignUps in the entity cache if it is enabled.
	*
	* @param ltpSignUps the LTPSignUps
	*/
	public void cacheResult(java.util.List<LTPSignUp> ltpSignUps);

	/**
	* Creates a new LTPSignUp with the primary key. Does not add the LTPSignUp to the database.
	*
	* @param ID the primary key for the new LTPSignUp
	* @return the new LTPSignUp
	*/
	public LTPSignUp create(long ID);

	/**
	* Removes the LTPSignUp with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUp
	* @return the LTPSignUp that was removed
	* @throws NoSuchLTPSignUpException if a LTPSignUp with the primary key could not be found
	*/
	public LTPSignUp remove(long ID) throws NoSuchLTPSignUpException;

	public LTPSignUp updateImpl(LTPSignUp ltpSignUp);

	/**
	* Returns the LTPSignUp with the primary key or throws a {@link NoSuchLTPSignUpException} if it could not be found.
	*
	* @param ID the primary key of the LTPSignUp
	* @return the LTPSignUp
	* @throws NoSuchLTPSignUpException if a LTPSignUp with the primary key could not be found
	*/
	public LTPSignUp findByPrimaryKey(long ID) throws NoSuchLTPSignUpException;

	/**
	* Returns the LTPSignUp with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the LTPSignUp
	* @return the LTPSignUp, or <code>null</code> if a LTPSignUp with the primary key could not be found
	*/
	public LTPSignUp fetchByPrimaryKey(long ID);

	@Override
	public java.util.Map<java.io.Serializable, LTPSignUp> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the LTPSignUps.
	*
	* @return the LTPSignUps
	*/
	public java.util.List<LTPSignUp> findAll();

	/**
	* Returns a range of all the LTPSignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUps
	* @param end the upper bound of the range of LTPSignUps (not inclusive)
	* @return the range of LTPSignUps
	*/
	public java.util.List<LTPSignUp> findAll(int start, int end);

	/**
	* Returns an ordered range of all the LTPSignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUps
	* @param end the upper bound of the range of LTPSignUps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of LTPSignUps
	*/
	public java.util.List<LTPSignUp> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp> orderByComparator);

	/**
	* Returns an ordered range of all the LTPSignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPSignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUps
	* @param end the upper bound of the range of LTPSignUps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of LTPSignUps
	*/
	public java.util.List<LTPSignUp> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<LTPSignUp> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the LTPSignUps from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of LTPSignUps.
	*
	* @return the number of LTPSignUps
	*/
	public int countAll();
}