/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link LTPSignUpPreferencesLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpPreferencesLocalService
 * @generated
 */
@ProviderType
public class LTPSignUpPreferencesLocalServiceWrapper
	implements LTPSignUpPreferencesLocalService,
		ServiceWrapper<LTPSignUpPreferencesLocalService> {
	public LTPSignUpPreferencesLocalServiceWrapper(
		LTPSignUpPreferencesLocalService ltpSignUpPreferencesLocalService) {
		_ltpSignUpPreferencesLocalService = ltpSignUpPreferencesLocalService;
	}

	/**
	* Adds the LTPSignUpPreferences to the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpPreferences the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was added
	*/
	@Override
	public LTPDatabase.model.LTPSignUpPreferences addLTPSignUpPreferences(
		LTPDatabase.model.LTPSignUpPreferences ltpSignUpPreferences) {
		return _ltpSignUpPreferencesLocalService.addLTPSignUpPreferences(ltpSignUpPreferences);
	}

	/**
	* @param morningWalkTitle
	* @param dinnerPrasadTitle
	* @param volunteerTitle
	* @param ltpSignUpPreferenceId
	* @return
	* @throws PortalException
	*/
	@Override
	public LTPDatabase.model.LTPSignUpPreferences addUpdateLTPSignUpPreferences(
		java.lang.String morningWalkTitle, java.lang.String dinnerPrasadTitle,
		java.lang.String volunteerTitle, long ltpSignUpPreferenceId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpPreferencesLocalService.addUpdateLTPSignUpPreferences(morningWalkTitle,
			dinnerPrasadTitle, volunteerTitle, ltpSignUpPreferenceId);
	}

	/**
	* Creates a new LTPSignUpPreferences with the primary key. Does not add the LTPSignUpPreferences to the database.
	*
	* @param ID the primary key for the new LTPSignUpPreferences
	* @return the new LTPSignUpPreferences
	*/
	@Override
	public LTPDatabase.model.LTPSignUpPreferences createLTPSignUpPreferences(
		long ID) {
		return _ltpSignUpPreferencesLocalService.createLTPSignUpPreferences(ID);
	}

	/**
	* @param entry
	* @return
	* @throws PortalException
	*/
	@Override
	public LTPDatabase.model.LTPSignUpPreferences deleteLTPSignUpPreference(
		LTPDatabase.model.LTPSignUpPreferences entry)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpPreferencesLocalService.deleteLTPSignUpPreference(entry);
	}

	/**
	* Deletes the LTPSignUpPreferences from the database. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpPreferences the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was removed
	*/
	@Override
	public LTPDatabase.model.LTPSignUpPreferences deleteLTPSignUpPreferences(
		LTPDatabase.model.LTPSignUpPreferences ltpSignUpPreferences) {
		return _ltpSignUpPreferencesLocalService.deleteLTPSignUpPreferences(ltpSignUpPreferences);
	}

	/**
	* Deletes the LTPSignUpPreferences with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was removed
	* @throws PortalException if a LTPSignUpPreferences with the primary key could not be found
	*/
	@Override
	public LTPDatabase.model.LTPSignUpPreferences deleteLTPSignUpPreferences(
		long ID) throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpPreferencesLocalService.deleteLTPSignUpPreferences(ID);
	}

	@Override
	public LTPDatabase.model.LTPSignUpPreferences fetchLTPSignUpPreferences(
		long ID) {
		return _ltpSignUpPreferencesLocalService.fetchLTPSignUpPreferences(ID);
	}

	/**
	* Returns the LTPSignUpPreferences with the primary key.
	*
	* @param ID the primary key of the LTPSignUpPreferences
	* @return the LTPSignUpPreferences
	* @throws PortalException if a LTPSignUpPreferences with the primary key could not be found
	*/
	@Override
	public LTPDatabase.model.LTPSignUpPreferences getLTPSignUpPreferences(
		long ID) throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpPreferencesLocalService.getLTPSignUpPreferences(ID);
	}

	/**
	* Updates the LTPSignUpPreferences in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param ltpSignUpPreferences the LTPSignUpPreferences
	* @return the LTPSignUpPreferences that was updated
	*/
	@Override
	public LTPDatabase.model.LTPSignUpPreferences updateLTPSignUpPreferences(
		LTPDatabase.model.LTPSignUpPreferences ltpSignUpPreferences) {
		return _ltpSignUpPreferencesLocalService.updateLTPSignUpPreferences(ltpSignUpPreferences);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _ltpSignUpPreferencesLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _ltpSignUpPreferencesLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _ltpSignUpPreferencesLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpPreferencesLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _ltpSignUpPreferencesLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* @return
	*/
	@Override
	public int getLTPSignUpPreferencesCount() {
		return _ltpSignUpPreferencesLocalService.getLTPSignUpPreferencesCount();
	}

	/**
	* Returns the number of LTPSignUpPreferenceses.
	*
	* @return the number of LTPSignUpPreferenceses
	*/
	@Override
	public int getLTPSignUpPreferencesesCount() {
		return _ltpSignUpPreferencesLocalService.getLTPSignUpPreferencesesCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _ltpSignUpPreferencesLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ltpSignUpPreferencesLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _ltpSignUpPreferencesLocalService.dynamicQuery(dynamicQuery,
			start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _ltpSignUpPreferencesLocalService.dynamicQuery(dynamicQuery,
			start, end, orderByComparator);
	}

	/**
	* @param start
	* @param end
	* @return
	*/
	@Override
	public java.util.List<LTPDatabase.model.LTPSignUpPreferences> getLTPSignUpPreferences(
		int start, int end) {
		return _ltpSignUpPreferencesLocalService.getLTPSignUpPreferences(start,
			end);
	}

	/**
	* Returns a range of all the LTPSignUpPreferenceses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of LTPSignUpPreferenceses
	* @param end the upper bound of the range of LTPSignUpPreferenceses (not inclusive)
	* @return the range of LTPSignUpPreferenceses
	*/
	@Override
	public java.util.List<LTPDatabase.model.LTPSignUpPreferences> getLTPSignUpPreferenceses(
		int start, int end) {
		return _ltpSignUpPreferencesLocalService.getLTPSignUpPreferenceses(start,
			end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _ltpSignUpPreferencesLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _ltpSignUpPreferencesLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public LTPSignUpPreferencesLocalService getWrappedService() {
		return _ltpSignUpPreferencesLocalService;
	}

	@Override
	public void setWrappedService(
		LTPSignUpPreferencesLocalService ltpSignUpPreferencesLocalService) {
		_ltpSignUpPreferencesLocalService = ltpSignUpPreferencesLocalService;
	}

	private LTPSignUpPreferencesLocalService _ltpSignUpPreferencesLocalService;
}