/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

/**
 * The base model interface for the LTPSignUpPreferences service. Represents a row in the &quot;LTPSignUpPreferences&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link LTPDatabase.model.impl.LTPSignUpPreferencesImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpPreferences
 * @see LTPDatabase.model.impl.LTPSignUpPreferencesImpl
 * @see LTPDatabase.model.impl.LTPSignUpPreferencesModelImpl
 * @generated
 */
@ProviderType
public interface LTPSignUpPreferencesModel extends BaseModel<LTPSignUpPreferences> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a LTPSignUpPreferences model instance should use the {@link LTPSignUpPreferences} interface instead.
	 */

	/**
	 * Returns the primary key of this LTPSignUpPreferences.
	 *
	 * @return the primary key of this LTPSignUpPreferences
	 */
	public long getPrimaryKey();

	/**
	 * Sets the primary key of this LTPSignUpPreferences.
	 *
	 * @param primaryKey the primary key of this LTPSignUpPreferences
	 */
	public void setPrimaryKey(long primaryKey);

	/**
	 * Returns the ID of this LTPSignUpPreferences.
	 *
	 * @return the ID of this LTPSignUpPreferences
	 */
	public long getID();

	/**
	 * Sets the ID of this LTPSignUpPreferences.
	 *
	 * @param ID the ID of this LTPSignUpPreferences
	 */
	public void setID(long ID);

	/**
	 * Returns the morning walk title of this LTPSignUpPreferences.
	 *
	 * @return the morning walk title of this LTPSignUpPreferences
	 */
	@AutoEscape
	public String getMorningWalkTitle();

	/**
	 * Sets the morning walk title of this LTPSignUpPreferences.
	 *
	 * @param MorningWalkTitle the morning walk title of this LTPSignUpPreferences
	 */
	public void setMorningWalkTitle(String MorningWalkTitle);

	/**
	 * Returns the dinner prasad title of this LTPSignUpPreferences.
	 *
	 * @return the dinner prasad title of this LTPSignUpPreferences
	 */
	@AutoEscape
	public String getDinnerPrasadTitle();

	/**
	 * Sets the dinner prasad title of this LTPSignUpPreferences.
	 *
	 * @param DinnerPrasadTitle the dinner prasad title of this LTPSignUpPreferences
	 */
	public void setDinnerPrasadTitle(String DinnerPrasadTitle);

	/**
	 * Returns the volunteer title of this LTPSignUpPreferences.
	 *
	 * @return the volunteer title of this LTPSignUpPreferences
	 */
	@AutoEscape
	public String getVolunteerTitle();

	/**
	 * Sets the volunteer title of this LTPSignUpPreferences.
	 *
	 * @param VolunteerTitle the volunteer title of this LTPSignUpPreferences
	 */
	public void setVolunteerTitle(String VolunteerTitle);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(
		LTPDatabase.model.LTPSignUpPreferences ltpSignUpPreferences);

	@Override
	public int hashCode();

	@Override
	public CacheModel<LTPDatabase.model.LTPSignUpPreferences> toCacheModel();

	@Override
	public LTPDatabase.model.LTPSignUpPreferences toEscapedModel();

	@Override
	public LTPDatabase.model.LTPSignUpPreferences toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}