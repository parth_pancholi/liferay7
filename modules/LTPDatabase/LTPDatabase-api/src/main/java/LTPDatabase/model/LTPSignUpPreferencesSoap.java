/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class LTPSignUpPreferencesSoap implements Serializable {
	public static LTPSignUpPreferencesSoap toSoapModel(
		LTPSignUpPreferences model) {
		LTPSignUpPreferencesSoap soapModel = new LTPSignUpPreferencesSoap();

		soapModel.setID(model.getID());
		soapModel.setMorningWalkTitle(model.getMorningWalkTitle());
		soapModel.setDinnerPrasadTitle(model.getDinnerPrasadTitle());
		soapModel.setVolunteerTitle(model.getVolunteerTitle());

		return soapModel;
	}

	public static LTPSignUpPreferencesSoap[] toSoapModels(
		LTPSignUpPreferences[] models) {
		LTPSignUpPreferencesSoap[] soapModels = new LTPSignUpPreferencesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LTPSignUpPreferencesSoap[][] toSoapModels(
		LTPSignUpPreferences[][] models) {
		LTPSignUpPreferencesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LTPSignUpPreferencesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LTPSignUpPreferencesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LTPSignUpPreferencesSoap[] toSoapModels(
		List<LTPSignUpPreferences> models) {
		List<LTPSignUpPreferencesSoap> soapModels = new ArrayList<LTPSignUpPreferencesSoap>(models.size());

		for (LTPSignUpPreferences model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LTPSignUpPreferencesSoap[soapModels.size()]);
	}

	public LTPSignUpPreferencesSoap() {
	}

	public long getPrimaryKey() {
		return _ID;
	}

	public void setPrimaryKey(long pk) {
		setID(pk);
	}

	public long getID() {
		return _ID;
	}

	public void setID(long ID) {
		_ID = ID;
	}

	public String getMorningWalkTitle() {
		return _MorningWalkTitle;
	}

	public void setMorningWalkTitle(String MorningWalkTitle) {
		_MorningWalkTitle = MorningWalkTitle;
	}

	public String getDinnerPrasadTitle() {
		return _DinnerPrasadTitle;
	}

	public void setDinnerPrasadTitle(String DinnerPrasadTitle) {
		_DinnerPrasadTitle = DinnerPrasadTitle;
	}

	public String getVolunteerTitle() {
		return _VolunteerTitle;
	}

	public void setVolunteerTitle(String VolunteerTitle) {
		_VolunteerTitle = VolunteerTitle;
	}

	private long _ID;
	private String _MorningWalkTitle;
	private String _DinnerPrasadTitle;
	private String _VolunteerTitle;
}