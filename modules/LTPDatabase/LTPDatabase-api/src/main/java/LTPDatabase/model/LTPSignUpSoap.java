/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class LTPSignUpSoap implements Serializable {
	public static LTPSignUpSoap toSoapModel(LTPSignUp model) {
		LTPSignUpSoap soapModel = new LTPSignUpSoap();

		soapModel.setID(model.getID());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setLastName(model.getLastName());
		soapModel.setMobileNumber(model.getMobileNumber());
		soapModel.setCreatedDate(model.getCreatedDate());
		soapModel.setUpdatedDate(model.getUpdatedDate());

		return soapModel;
	}

	public static LTPSignUpSoap[] toSoapModels(LTPSignUp[] models) {
		LTPSignUpSoap[] soapModels = new LTPSignUpSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LTPSignUpSoap[][] toSoapModels(LTPSignUp[][] models) {
		LTPSignUpSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LTPSignUpSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LTPSignUpSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LTPSignUpSoap[] toSoapModels(List<LTPSignUp> models) {
		List<LTPSignUpSoap> soapModels = new ArrayList<LTPSignUpSoap>(models.size());

		for (LTPSignUp model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LTPSignUpSoap[soapModels.size()]);
	}

	public LTPSignUpSoap() {
	}

	public long getPrimaryKey() {
		return _ID;
	}

	public void setPrimaryKey(long pk) {
		setID(pk);
	}

	public long getID() {
		return _ID;
	}

	public void setID(long ID) {
		_ID = ID;
	}

	public String getFirstName() {
		return _FirstName;
	}

	public void setFirstName(String FirstName) {
		_FirstName = FirstName;
	}

	public String getLastName() {
		return _LastName;
	}

	public void setLastName(String LastName) {
		_LastName = LastName;
	}

	public String getMobileNumber() {
		return _MobileNumber;
	}

	public void setMobileNumber(String MobileNumber) {
		_MobileNumber = MobileNumber;
	}

	public Date getCreatedDate() {
		return _CreatedDate;
	}

	public void setCreatedDate(Date CreatedDate) {
		_CreatedDate = CreatedDate;
	}

	public Date getUpdatedDate() {
		return _UpdatedDate;
	}

	public void setUpdatedDate(Date UpdatedDate) {
		_UpdatedDate = UpdatedDate;
	}

	private long _ID;
	private String _FirstName;
	private String _LastName;
	private String _MobileNumber;
	private Date _CreatedDate;
	private Date _UpdatedDate;
}