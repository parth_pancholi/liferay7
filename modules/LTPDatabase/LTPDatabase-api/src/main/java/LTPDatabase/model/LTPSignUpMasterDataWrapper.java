/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link LTPSignUpMasterData}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpMasterData
 * @generated
 */
@ProviderType
public class LTPSignUpMasterDataWrapper implements LTPSignUpMasterData,
	ModelWrapper<LTPSignUpMasterData> {
	public LTPSignUpMasterDataWrapper(LTPSignUpMasterData ltpSignUpMasterData) {
		_ltpSignUpMasterData = ltpSignUpMasterData;
	}

	@Override
	public Class<?> getModelClass() {
		return LTPSignUpMasterData.class;
	}

	@Override
	public String getModelClassName() {
		return LTPSignUpMasterData.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("EventDate", getEventDate());
		attributes.put("ExtraText", getExtraText());
		attributes.put("isMorningWalk", getIsMorningWalk());
		attributes.put("isVolunteer", getIsVolunteer());
		attributes.put("isDinnerPrasad", getIsDinnerPrasad());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ID = (Long)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		Date EventDate = (Date)attributes.get("EventDate");

		if (EventDate != null) {
			setEventDate(EventDate);
		}

		String ExtraText = (String)attributes.get("ExtraText");

		if (ExtraText != null) {
			setExtraText(ExtraText);
		}

		Boolean isMorningWalk = (Boolean)attributes.get("isMorningWalk");

		if (isMorningWalk != null) {
			setIsMorningWalk(isMorningWalk);
		}

		Boolean isVolunteer = (Boolean)attributes.get("isVolunteer");

		if (isVolunteer != null) {
			setIsVolunteer(isVolunteer);
		}

		Boolean isDinnerPrasad = (Boolean)attributes.get("isDinnerPrasad");

		if (isDinnerPrasad != null) {
			setIsDinnerPrasad(isDinnerPrasad);
		}
	}

	@Override
	public LTPDatabase.model.LTPSignUpMasterData toEscapedModel() {
		return new LTPSignUpMasterDataWrapper(_ltpSignUpMasterData.toEscapedModel());
	}

	@Override
	public LTPDatabase.model.LTPSignUpMasterData toUnescapedModel() {
		return new LTPSignUpMasterDataWrapper(_ltpSignUpMasterData.toUnescapedModel());
	}

	/**
	* Returns the is dinner prasad of this LTPSignUpMasterData.
	*
	* @return the is dinner prasad of this LTPSignUpMasterData
	*/
	@Override
	public boolean getIsDinnerPrasad() {
		return _ltpSignUpMasterData.getIsDinnerPrasad();
	}

	/**
	* Returns the is morning walk of this LTPSignUpMasterData.
	*
	* @return the is morning walk of this LTPSignUpMasterData
	*/
	@Override
	public boolean getIsMorningWalk() {
		return _ltpSignUpMasterData.getIsMorningWalk();
	}

	/**
	* Returns the is volunteer of this LTPSignUpMasterData.
	*
	* @return the is volunteer of this LTPSignUpMasterData
	*/
	@Override
	public boolean getIsVolunteer() {
		return _ltpSignUpMasterData.getIsVolunteer();
	}

	@Override
	public boolean isCachedModel() {
		return _ltpSignUpMasterData.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _ltpSignUpMasterData.isEscapedModel();
	}

	/**
	* Returns <code>true</code> if this LTPSignUpMasterData is is dinner prasad.
	*
	* @return <code>true</code> if this LTPSignUpMasterData is is dinner prasad; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsDinnerPrasad() {
		return _ltpSignUpMasterData.isIsDinnerPrasad();
	}

	/**
	* Returns <code>true</code> if this LTPSignUpMasterData is is morning walk.
	*
	* @return <code>true</code> if this LTPSignUpMasterData is is morning walk; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsMorningWalk() {
		return _ltpSignUpMasterData.isIsMorningWalk();
	}

	/**
	* Returns <code>true</code> if this LTPSignUpMasterData is is volunteer.
	*
	* @return <code>true</code> if this LTPSignUpMasterData is is volunteer; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsVolunteer() {
		return _ltpSignUpMasterData.isIsVolunteer();
	}

	@Override
	public boolean isNew() {
		return _ltpSignUpMasterData.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _ltpSignUpMasterData.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<LTPDatabase.model.LTPSignUpMasterData> toCacheModel() {
		return _ltpSignUpMasterData.toCacheModel();
	}

	@Override
	public int compareTo(
		LTPDatabase.model.LTPSignUpMasterData ltpSignUpMasterData) {
		return _ltpSignUpMasterData.compareTo(ltpSignUpMasterData);
	}

	@Override
	public int hashCode() {
		return _ltpSignUpMasterData.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ltpSignUpMasterData.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new LTPSignUpMasterDataWrapper((LTPSignUpMasterData)_ltpSignUpMasterData.clone());
	}

	/**
	* Returns the extra text of this LTPSignUpMasterData.
	*
	* @return the extra text of this LTPSignUpMasterData
	*/
	@Override
	public java.lang.String getExtraText() {
		return _ltpSignUpMasterData.getExtraText();
	}

	@Override
	public java.lang.String toString() {
		return _ltpSignUpMasterData.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ltpSignUpMasterData.toXmlString();
	}

	/**
	* Returns the event date of this LTPSignUpMasterData.
	*
	* @return the event date of this LTPSignUpMasterData
	*/
	@Override
	public Date getEventDate() {
		return _ltpSignUpMasterData.getEventDate();
	}

	/**
	* Returns the ID of this LTPSignUpMasterData.
	*
	* @return the ID of this LTPSignUpMasterData
	*/
	@Override
	public long getID() {
		return _ltpSignUpMasterData.getID();
	}

	/**
	* Returns the primary key of this LTPSignUpMasterData.
	*
	* @return the primary key of this LTPSignUpMasterData
	*/
	@Override
	public long getPrimaryKey() {
		return _ltpSignUpMasterData.getPrimaryKey();
	}

	@Override
	public void persist() {
		_ltpSignUpMasterData.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ltpSignUpMasterData.setCachedModel(cachedModel);
	}

	/**
	* Sets the event date of this LTPSignUpMasterData.
	*
	* @param EventDate the event date of this LTPSignUpMasterData
	*/
	@Override
	public void setEventDate(Date EventDate) {
		_ltpSignUpMasterData.setEventDate(EventDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_ltpSignUpMasterData.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_ltpSignUpMasterData.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_ltpSignUpMasterData.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the extra text of this LTPSignUpMasterData.
	*
	* @param ExtraText the extra text of this LTPSignUpMasterData
	*/
	@Override
	public void setExtraText(java.lang.String ExtraText) {
		_ltpSignUpMasterData.setExtraText(ExtraText);
	}

	/**
	* Sets the ID of this LTPSignUpMasterData.
	*
	* @param ID the ID of this LTPSignUpMasterData
	*/
	@Override
	public void setID(long ID) {
		_ltpSignUpMasterData.setID(ID);
	}

	/**
	* Sets whether this LTPSignUpMasterData is is dinner prasad.
	*
	* @param isDinnerPrasad the is dinner prasad of this LTPSignUpMasterData
	*/
	@Override
	public void setIsDinnerPrasad(boolean isDinnerPrasad) {
		_ltpSignUpMasterData.setIsDinnerPrasad(isDinnerPrasad);
	}

	/**
	* Sets whether this LTPSignUpMasterData is is morning walk.
	*
	* @param isMorningWalk the is morning walk of this LTPSignUpMasterData
	*/
	@Override
	public void setIsMorningWalk(boolean isMorningWalk) {
		_ltpSignUpMasterData.setIsMorningWalk(isMorningWalk);
	}

	/**
	* Sets whether this LTPSignUpMasterData is is volunteer.
	*
	* @param isVolunteer the is volunteer of this LTPSignUpMasterData
	*/
	@Override
	public void setIsVolunteer(boolean isVolunteer) {
		_ltpSignUpMasterData.setIsVolunteer(isVolunteer);
	}

	@Override
	public void setNew(boolean n) {
		_ltpSignUpMasterData.setNew(n);
	}

	/**
	* Sets the primary key of this LTPSignUpMasterData.
	*
	* @param primaryKey the primary key of this LTPSignUpMasterData
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_ltpSignUpMasterData.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_ltpSignUpMasterData.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LTPSignUpMasterDataWrapper)) {
			return false;
		}

		LTPSignUpMasterDataWrapper ltpSignUpMasterDataWrapper = (LTPSignUpMasterDataWrapper)obj;

		if (Objects.equals(_ltpSignUpMasterData,
					ltpSignUpMasterDataWrapper._ltpSignUpMasterData)) {
			return true;
		}

		return false;
	}

	@Override
	public LTPSignUpMasterData getWrappedModel() {
		return _ltpSignUpMasterData;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _ltpSignUpMasterData.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _ltpSignUpMasterData.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_ltpSignUpMasterData.resetOriginalValues();
	}

	private final LTPSignUpMasterData _ltpSignUpMasterData;
}