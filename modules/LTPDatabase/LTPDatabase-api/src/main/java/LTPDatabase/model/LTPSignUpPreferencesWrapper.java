/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link LTPSignUpPreferences}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpPreferences
 * @generated
 */
@ProviderType
public class LTPSignUpPreferencesWrapper implements LTPSignUpPreferences,
	ModelWrapper<LTPSignUpPreferences> {
	public LTPSignUpPreferencesWrapper(
		LTPSignUpPreferences ltpSignUpPreferences) {
		_ltpSignUpPreferences = ltpSignUpPreferences;
	}

	@Override
	public Class<?> getModelClass() {
		return LTPSignUpPreferences.class;
	}

	@Override
	public String getModelClassName() {
		return LTPSignUpPreferences.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("MorningWalkTitle", getMorningWalkTitle());
		attributes.put("DinnerPrasadTitle", getDinnerPrasadTitle());
		attributes.put("VolunteerTitle", getVolunteerTitle());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ID = (Long)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		String MorningWalkTitle = (String)attributes.get("MorningWalkTitle");

		if (MorningWalkTitle != null) {
			setMorningWalkTitle(MorningWalkTitle);
		}

		String DinnerPrasadTitle = (String)attributes.get("DinnerPrasadTitle");

		if (DinnerPrasadTitle != null) {
			setDinnerPrasadTitle(DinnerPrasadTitle);
		}

		String VolunteerTitle = (String)attributes.get("VolunteerTitle");

		if (VolunteerTitle != null) {
			setVolunteerTitle(VolunteerTitle);
		}
	}

	@Override
	public LTPDatabase.model.LTPSignUpPreferences toEscapedModel() {
		return new LTPSignUpPreferencesWrapper(_ltpSignUpPreferences.toEscapedModel());
	}

	@Override
	public LTPDatabase.model.LTPSignUpPreferences toUnescapedModel() {
		return new LTPSignUpPreferencesWrapper(_ltpSignUpPreferences.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _ltpSignUpPreferences.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _ltpSignUpPreferences.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _ltpSignUpPreferences.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _ltpSignUpPreferences.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<LTPDatabase.model.LTPSignUpPreferences> toCacheModel() {
		return _ltpSignUpPreferences.toCacheModel();
	}

	@Override
	public int compareTo(
		LTPDatabase.model.LTPSignUpPreferences ltpSignUpPreferences) {
		return _ltpSignUpPreferences.compareTo(ltpSignUpPreferences);
	}

	@Override
	public int hashCode() {
		return _ltpSignUpPreferences.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ltpSignUpPreferences.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new LTPSignUpPreferencesWrapper((LTPSignUpPreferences)_ltpSignUpPreferences.clone());
	}

	/**
	* Returns the dinner prasad title of this LTPSignUpPreferences.
	*
	* @return the dinner prasad title of this LTPSignUpPreferences
	*/
	@Override
	public java.lang.String getDinnerPrasadTitle() {
		return _ltpSignUpPreferences.getDinnerPrasadTitle();
	}

	/**
	* Returns the morning walk title of this LTPSignUpPreferences.
	*
	* @return the morning walk title of this LTPSignUpPreferences
	*/
	@Override
	public java.lang.String getMorningWalkTitle() {
		return _ltpSignUpPreferences.getMorningWalkTitle();
	}

	/**
	* Returns the volunteer title of this LTPSignUpPreferences.
	*
	* @return the volunteer title of this LTPSignUpPreferences
	*/
	@Override
	public java.lang.String getVolunteerTitle() {
		return _ltpSignUpPreferences.getVolunteerTitle();
	}

	@Override
	public java.lang.String toString() {
		return _ltpSignUpPreferences.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ltpSignUpPreferences.toXmlString();
	}

	/**
	* Returns the ID of this LTPSignUpPreferences.
	*
	* @return the ID of this LTPSignUpPreferences
	*/
	@Override
	public long getID() {
		return _ltpSignUpPreferences.getID();
	}

	/**
	* Returns the primary key of this LTPSignUpPreferences.
	*
	* @return the primary key of this LTPSignUpPreferences
	*/
	@Override
	public long getPrimaryKey() {
		return _ltpSignUpPreferences.getPrimaryKey();
	}

	@Override
	public void persist() {
		_ltpSignUpPreferences.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ltpSignUpPreferences.setCachedModel(cachedModel);
	}

	/**
	* Sets the dinner prasad title of this LTPSignUpPreferences.
	*
	* @param DinnerPrasadTitle the dinner prasad title of this LTPSignUpPreferences
	*/
	@Override
	public void setDinnerPrasadTitle(java.lang.String DinnerPrasadTitle) {
		_ltpSignUpPreferences.setDinnerPrasadTitle(DinnerPrasadTitle);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_ltpSignUpPreferences.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_ltpSignUpPreferences.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_ltpSignUpPreferences.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the ID of this LTPSignUpPreferences.
	*
	* @param ID the ID of this LTPSignUpPreferences
	*/
	@Override
	public void setID(long ID) {
		_ltpSignUpPreferences.setID(ID);
	}

	/**
	* Sets the morning walk title of this LTPSignUpPreferences.
	*
	* @param MorningWalkTitle the morning walk title of this LTPSignUpPreferences
	*/
	@Override
	public void setMorningWalkTitle(java.lang.String MorningWalkTitle) {
		_ltpSignUpPreferences.setMorningWalkTitle(MorningWalkTitle);
	}

	@Override
	public void setNew(boolean n) {
		_ltpSignUpPreferences.setNew(n);
	}

	/**
	* Sets the primary key of this LTPSignUpPreferences.
	*
	* @param primaryKey the primary key of this LTPSignUpPreferences
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_ltpSignUpPreferences.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_ltpSignUpPreferences.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the volunteer title of this LTPSignUpPreferences.
	*
	* @param VolunteerTitle the volunteer title of this LTPSignUpPreferences
	*/
	@Override
	public void setVolunteerTitle(java.lang.String VolunteerTitle) {
		_ltpSignUpPreferences.setVolunteerTitle(VolunteerTitle);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LTPSignUpPreferencesWrapper)) {
			return false;
		}

		LTPSignUpPreferencesWrapper ltpSignUpPreferencesWrapper = (LTPSignUpPreferencesWrapper)obj;

		if (Objects.equals(_ltpSignUpPreferences,
					ltpSignUpPreferencesWrapper._ltpSignUpPreferences)) {
			return true;
		}

		return false;
	}

	@Override
	public LTPSignUpPreferences getWrappedModel() {
		return _ltpSignUpPreferences;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _ltpSignUpPreferences.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _ltpSignUpPreferences.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_ltpSignUpPreferences.resetOriginalValues();
	}

	private final LTPSignUpPreferences _ltpSignUpPreferences;
}