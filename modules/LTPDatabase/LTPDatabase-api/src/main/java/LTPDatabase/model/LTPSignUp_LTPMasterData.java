/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the LTPSignUp_LTPMasterData service. Represents a row in the &quot;LTPSignUp_LTPMasterData&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUp_LTPMasterDataModel
 * @see LTPDatabase.model.impl.LTPSignUp_LTPMasterDataImpl
 * @see LTPDatabase.model.impl.LTPSignUp_LTPMasterDataModelImpl
 * @generated
 */
@ImplementationClassName("LTPDatabase.model.impl.LTPSignUp_LTPMasterDataImpl")
@ProviderType
public interface LTPSignUp_LTPMasterData extends LTPSignUp_LTPMasterDataModel,
	PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link LTPDatabase.model.impl.LTPSignUp_LTPMasterDataImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<LTPSignUp_LTPMasterData, Long> ID_ACCESSOR = new Accessor<LTPSignUp_LTPMasterData, Long>() {
			@Override
			public Long get(LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
				return ltpSignUp_LTPMasterData.getID();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<LTPSignUp_LTPMasterData> getTypeClass() {
				return LTPSignUp_LTPMasterData.class;
			}
		};
}