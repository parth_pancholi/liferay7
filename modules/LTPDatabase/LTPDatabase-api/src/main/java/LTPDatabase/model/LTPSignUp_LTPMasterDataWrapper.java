/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link LTPSignUp_LTPMasterData}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUp_LTPMasterData
 * @generated
 */
@ProviderType
public class LTPSignUp_LTPMasterDataWrapper implements LTPSignUp_LTPMasterData,
	ModelWrapper<LTPSignUp_LTPMasterData> {
	public LTPSignUp_LTPMasterDataWrapper(
		LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		_ltpSignUp_LTPMasterData = ltpSignUp_LTPMasterData;
	}

	@Override
	public Class<?> getModelClass() {
		return LTPSignUp_LTPMasterData.class;
	}

	@Override
	public String getModelClassName() {
		return LTPSignUp_LTPMasterData.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("LTPSignUpID", getLTPSignUpID());
		attributes.put("LTPMasterDataID", getLTPMasterDataID());
		attributes.put("DinnerPrasadItem1", getDinnerPrasadItem1());
		attributes.put("DinnerPrasadItem2", getDinnerPrasadItem2());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ID = (Long)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		Long LTPSignUpID = (Long)attributes.get("LTPSignUpID");

		if (LTPSignUpID != null) {
			setLTPSignUpID(LTPSignUpID);
		}

		Long LTPMasterDataID = (Long)attributes.get("LTPMasterDataID");

		if (LTPMasterDataID != null) {
			setLTPMasterDataID(LTPMasterDataID);
		}

		String DinnerPrasadItem1 = (String)attributes.get("DinnerPrasadItem1");

		if (DinnerPrasadItem1 != null) {
			setDinnerPrasadItem1(DinnerPrasadItem1);
		}

		String DinnerPrasadItem2 = (String)attributes.get("DinnerPrasadItem2");

		if (DinnerPrasadItem2 != null) {
			setDinnerPrasadItem2(DinnerPrasadItem2);
		}
	}

	@Override
	public LTPDatabase.model.LTPSignUp_LTPMasterData toEscapedModel() {
		return new LTPSignUp_LTPMasterDataWrapper(_ltpSignUp_LTPMasterData.toEscapedModel());
	}

	@Override
	public LTPDatabase.model.LTPSignUp_LTPMasterData toUnescapedModel() {
		return new LTPSignUp_LTPMasterDataWrapper(_ltpSignUp_LTPMasterData.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _ltpSignUp_LTPMasterData.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _ltpSignUp_LTPMasterData.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _ltpSignUp_LTPMasterData.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _ltpSignUp_LTPMasterData.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<LTPDatabase.model.LTPSignUp_LTPMasterData> toCacheModel() {
		return _ltpSignUp_LTPMasterData.toCacheModel();
	}

	@Override
	public int compareTo(
		LTPDatabase.model.LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData) {
		return _ltpSignUp_LTPMasterData.compareTo(ltpSignUp_LTPMasterData);
	}

	@Override
	public int hashCode() {
		return _ltpSignUp_LTPMasterData.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ltpSignUp_LTPMasterData.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new LTPSignUp_LTPMasterDataWrapper((LTPSignUp_LTPMasterData)_ltpSignUp_LTPMasterData.clone());
	}

	/**
	* Returns the dinner prasad item1 of this LTPSignUp_LTPMasterData.
	*
	* @return the dinner prasad item1 of this LTPSignUp_LTPMasterData
	*/
	@Override
	public java.lang.String getDinnerPrasadItem1() {
		return _ltpSignUp_LTPMasterData.getDinnerPrasadItem1();
	}

	/**
	* Returns the dinner prasad item2 of this LTPSignUp_LTPMasterData.
	*
	* @return the dinner prasad item2 of this LTPSignUp_LTPMasterData
	*/
	@Override
	public java.lang.String getDinnerPrasadItem2() {
		return _ltpSignUp_LTPMasterData.getDinnerPrasadItem2();
	}

	@Override
	public java.lang.String toString() {
		return _ltpSignUp_LTPMasterData.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ltpSignUp_LTPMasterData.toXmlString();
	}

	/**
	* Returns the ID of this LTPSignUp_LTPMasterData.
	*
	* @return the ID of this LTPSignUp_LTPMasterData
	*/
	@Override
	public long getID() {
		return _ltpSignUp_LTPMasterData.getID();
	}

	/**
	* Returns the ltp master data ID of this LTPSignUp_LTPMasterData.
	*
	* @return the ltp master data ID of this LTPSignUp_LTPMasterData
	*/
	@Override
	public long getLTPMasterDataID() {
		return _ltpSignUp_LTPMasterData.getLTPMasterDataID();
	}

	/**
	* Returns the ltp sign up ID of this LTPSignUp_LTPMasterData.
	*
	* @return the ltp sign up ID of this LTPSignUp_LTPMasterData
	*/
	@Override
	public long getLTPSignUpID() {
		return _ltpSignUp_LTPMasterData.getLTPSignUpID();
	}

	/**
	* Returns the primary key of this LTPSignUp_LTPMasterData.
	*
	* @return the primary key of this LTPSignUp_LTPMasterData
	*/
	@Override
	public long getPrimaryKey() {
		return _ltpSignUp_LTPMasterData.getPrimaryKey();
	}

	@Override
	public void persist() {
		_ltpSignUp_LTPMasterData.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ltpSignUp_LTPMasterData.setCachedModel(cachedModel);
	}

	/**
	* Sets the dinner prasad item1 of this LTPSignUp_LTPMasterData.
	*
	* @param DinnerPrasadItem1 the dinner prasad item1 of this LTPSignUp_LTPMasterData
	*/
	@Override
	public void setDinnerPrasadItem1(java.lang.String DinnerPrasadItem1) {
		_ltpSignUp_LTPMasterData.setDinnerPrasadItem1(DinnerPrasadItem1);
	}

	/**
	* Sets the dinner prasad item2 of this LTPSignUp_LTPMasterData.
	*
	* @param DinnerPrasadItem2 the dinner prasad item2 of this LTPSignUp_LTPMasterData
	*/
	@Override
	public void setDinnerPrasadItem2(java.lang.String DinnerPrasadItem2) {
		_ltpSignUp_LTPMasterData.setDinnerPrasadItem2(DinnerPrasadItem2);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_ltpSignUp_LTPMasterData.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_ltpSignUp_LTPMasterData.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_ltpSignUp_LTPMasterData.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the ID of this LTPSignUp_LTPMasterData.
	*
	* @param ID the ID of this LTPSignUp_LTPMasterData
	*/
	@Override
	public void setID(long ID) {
		_ltpSignUp_LTPMasterData.setID(ID);
	}

	/**
	* Sets the ltp master data ID of this LTPSignUp_LTPMasterData.
	*
	* @param LTPMasterDataID the ltp master data ID of this LTPSignUp_LTPMasterData
	*/
	@Override
	public void setLTPMasterDataID(long LTPMasterDataID) {
		_ltpSignUp_LTPMasterData.setLTPMasterDataID(LTPMasterDataID);
	}

	/**
	* Sets the ltp sign up ID of this LTPSignUp_LTPMasterData.
	*
	* @param LTPSignUpID the ltp sign up ID of this LTPSignUp_LTPMasterData
	*/
	@Override
	public void setLTPSignUpID(long LTPSignUpID) {
		_ltpSignUp_LTPMasterData.setLTPSignUpID(LTPSignUpID);
	}

	@Override
	public void setNew(boolean n) {
		_ltpSignUp_LTPMasterData.setNew(n);
	}

	/**
	* Sets the primary key of this LTPSignUp_LTPMasterData.
	*
	* @param primaryKey the primary key of this LTPSignUp_LTPMasterData
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_ltpSignUp_LTPMasterData.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_ltpSignUp_LTPMasterData.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LTPSignUp_LTPMasterDataWrapper)) {
			return false;
		}

		LTPSignUp_LTPMasterDataWrapper ltpSignUp_LTPMasterDataWrapper = (LTPSignUp_LTPMasterDataWrapper)obj;

		if (Objects.equals(_ltpSignUp_LTPMasterData,
					ltpSignUp_LTPMasterDataWrapper._ltpSignUp_LTPMasterData)) {
			return true;
		}

		return false;
	}

	@Override
	public LTPSignUp_LTPMasterData getWrappedModel() {
		return _ltpSignUp_LTPMasterData;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _ltpSignUp_LTPMasterData.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _ltpSignUp_LTPMasterData.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_ltpSignUp_LTPMasterData.resetOriginalValues();
	}

	private final LTPSignUp_LTPMasterData _ltpSignUp_LTPMasterData;
}