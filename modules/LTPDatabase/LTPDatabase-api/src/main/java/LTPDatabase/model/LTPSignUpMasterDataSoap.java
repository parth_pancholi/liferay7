/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class LTPSignUpMasterDataSoap implements Serializable {
	public static LTPSignUpMasterDataSoap toSoapModel(LTPSignUpMasterData model) {
		LTPSignUpMasterDataSoap soapModel = new LTPSignUpMasterDataSoap();

		soapModel.setID(model.getID());
		soapModel.setEventDate(model.getEventDate());
		soapModel.setExtraText(model.getExtraText());
		soapModel.setIsMorningWalk(model.getIsMorningWalk());
		soapModel.setIsVolunteer(model.getIsVolunteer());
		soapModel.setIsDinnerPrasad(model.getIsDinnerPrasad());

		return soapModel;
	}

	public static LTPSignUpMasterDataSoap[] toSoapModels(
		LTPSignUpMasterData[] models) {
		LTPSignUpMasterDataSoap[] soapModels = new LTPSignUpMasterDataSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LTPSignUpMasterDataSoap[][] toSoapModels(
		LTPSignUpMasterData[][] models) {
		LTPSignUpMasterDataSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LTPSignUpMasterDataSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LTPSignUpMasterDataSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LTPSignUpMasterDataSoap[] toSoapModels(
		List<LTPSignUpMasterData> models) {
		List<LTPSignUpMasterDataSoap> soapModels = new ArrayList<LTPSignUpMasterDataSoap>(models.size());

		for (LTPSignUpMasterData model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LTPSignUpMasterDataSoap[soapModels.size()]);
	}

	public LTPSignUpMasterDataSoap() {
	}

	public long getPrimaryKey() {
		return _ID;
	}

	public void setPrimaryKey(long pk) {
		setID(pk);
	}

	public long getID() {
		return _ID;
	}

	public void setID(long ID) {
		_ID = ID;
	}

	public Date getEventDate() {
		return _EventDate;
	}

	public void setEventDate(Date EventDate) {
		_EventDate = EventDate;
	}

	public String getExtraText() {
		return _ExtraText;
	}

	public void setExtraText(String ExtraText) {
		_ExtraText = ExtraText;
	}

	public boolean getIsMorningWalk() {
		return _isMorningWalk;
	}

	public boolean isIsMorningWalk() {
		return _isMorningWalk;
	}

	public void setIsMorningWalk(boolean isMorningWalk) {
		_isMorningWalk = isMorningWalk;
	}

	public boolean getIsVolunteer() {
		return _isVolunteer;
	}

	public boolean isIsVolunteer() {
		return _isVolunteer;
	}

	public void setIsVolunteer(boolean isVolunteer) {
		_isVolunteer = isVolunteer;
	}

	public boolean getIsDinnerPrasad() {
		return _isDinnerPrasad;
	}

	public boolean isIsDinnerPrasad() {
		return _isDinnerPrasad;
	}

	public void setIsDinnerPrasad(boolean isDinnerPrasad) {
		_isDinnerPrasad = isDinnerPrasad;
	}

	private long _ID;
	private Date _EventDate;
	private String _ExtraText;
	private boolean _isMorningWalk;
	private boolean _isVolunteer;
	private boolean _isDinnerPrasad;
}