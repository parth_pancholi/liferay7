/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class LTPSignUp_LTPMasterDataSoap implements Serializable {
	public static LTPSignUp_LTPMasterDataSoap toSoapModel(
		LTPSignUp_LTPMasterData model) {
		LTPSignUp_LTPMasterDataSoap soapModel = new LTPSignUp_LTPMasterDataSoap();

		soapModel.setID(model.getID());
		soapModel.setLTPSignUpID(model.getLTPSignUpID());
		soapModel.setLTPMasterDataID(model.getLTPMasterDataID());
		soapModel.setDinnerPrasadItem1(model.getDinnerPrasadItem1());
		soapModel.setDinnerPrasadItem2(model.getDinnerPrasadItem2());

		return soapModel;
	}

	public static LTPSignUp_LTPMasterDataSoap[] toSoapModels(
		LTPSignUp_LTPMasterData[] models) {
		LTPSignUp_LTPMasterDataSoap[] soapModels = new LTPSignUp_LTPMasterDataSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static LTPSignUp_LTPMasterDataSoap[][] toSoapModels(
		LTPSignUp_LTPMasterData[][] models) {
		LTPSignUp_LTPMasterDataSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new LTPSignUp_LTPMasterDataSoap[models.length][models[0].length];
		}
		else {
			soapModels = new LTPSignUp_LTPMasterDataSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static LTPSignUp_LTPMasterDataSoap[] toSoapModels(
		List<LTPSignUp_LTPMasterData> models) {
		List<LTPSignUp_LTPMasterDataSoap> soapModels = new ArrayList<LTPSignUp_LTPMasterDataSoap>(models.size());

		for (LTPSignUp_LTPMasterData model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new LTPSignUp_LTPMasterDataSoap[soapModels.size()]);
	}

	public LTPSignUp_LTPMasterDataSoap() {
	}

	public long getPrimaryKey() {
		return _ID;
	}

	public void setPrimaryKey(long pk) {
		setID(pk);
	}

	public long getID() {
		return _ID;
	}

	public void setID(long ID) {
		_ID = ID;
	}

	public long getLTPSignUpID() {
		return _LTPSignUpID;
	}

	public void setLTPSignUpID(long LTPSignUpID) {
		_LTPSignUpID = LTPSignUpID;
	}

	public long getLTPMasterDataID() {
		return _LTPMasterDataID;
	}

	public void setLTPMasterDataID(long LTPMasterDataID) {
		_LTPMasterDataID = LTPMasterDataID;
	}

	public String getDinnerPrasadItem1() {
		return _DinnerPrasadItem1;
	}

	public void setDinnerPrasadItem1(String DinnerPrasadItem1) {
		_DinnerPrasadItem1 = DinnerPrasadItem1;
	}

	public String getDinnerPrasadItem2() {
		return _DinnerPrasadItem2;
	}

	public void setDinnerPrasadItem2(String DinnerPrasadItem2) {
		_DinnerPrasadItem2 = DinnerPrasadItem2;
	}

	private long _ID;
	private long _LTPSignUpID;
	private long _LTPMasterDataID;
	private String _DinnerPrasadItem1;
	private String _DinnerPrasadItem2;
}