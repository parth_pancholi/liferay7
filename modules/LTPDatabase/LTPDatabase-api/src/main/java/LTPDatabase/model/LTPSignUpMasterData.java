/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the LTPSignUpMasterData service. Represents a row in the &quot;LTPSignUpMasterData&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUpMasterDataModel
 * @see LTPDatabase.model.impl.LTPSignUpMasterDataImpl
 * @see LTPDatabase.model.impl.LTPSignUpMasterDataModelImpl
 * @generated
 */
@ImplementationClassName("LTPDatabase.model.impl.LTPSignUpMasterDataImpl")
@ProviderType
public interface LTPSignUpMasterData extends LTPSignUpMasterDataModel,
	PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link LTPDatabase.model.impl.LTPSignUpMasterDataImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<LTPSignUpMasterData, Long> ID_ACCESSOR = new Accessor<LTPSignUpMasterData, Long>() {
			@Override
			public Long get(LTPSignUpMasterData ltpSignUpMasterData) {
				return ltpSignUpMasterData.getID();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<LTPSignUpMasterData> getTypeClass() {
				return LTPSignUpMasterData.class;
			}
		};
}