/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package LTPDatabase.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link LTPSignUp}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see LTPSignUp
 * @generated
 */
@ProviderType
public class LTPSignUpWrapper implements LTPSignUp, ModelWrapper<LTPSignUp> {
	public LTPSignUpWrapper(LTPSignUp ltpSignUp) {
		_ltpSignUp = ltpSignUp;
	}

	@Override
	public Class<?> getModelClass() {
		return LTPSignUp.class;
	}

	@Override
	public String getModelClassName() {
		return LTPSignUp.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("FirstName", getFirstName());
		attributes.put("LastName", getLastName());
		attributes.put("MobileNumber", getMobileNumber());
		attributes.put("CreatedDate", getCreatedDate());
		attributes.put("UpdatedDate", getUpdatedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ID = (Long)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		String FirstName = (String)attributes.get("FirstName");

		if (FirstName != null) {
			setFirstName(FirstName);
		}

		String LastName = (String)attributes.get("LastName");

		if (LastName != null) {
			setLastName(LastName);
		}

		String MobileNumber = (String)attributes.get("MobileNumber");

		if (MobileNumber != null) {
			setMobileNumber(MobileNumber);
		}

		Date CreatedDate = (Date)attributes.get("CreatedDate");

		if (CreatedDate != null) {
			setCreatedDate(CreatedDate);
		}

		Date UpdatedDate = (Date)attributes.get("UpdatedDate");

		if (UpdatedDate != null) {
			setUpdatedDate(UpdatedDate);
		}
	}

	@Override
	public LTPDatabase.model.LTPSignUp toEscapedModel() {
		return new LTPSignUpWrapper(_ltpSignUp.toEscapedModel());
	}

	@Override
	public LTPDatabase.model.LTPSignUp toUnescapedModel() {
		return new LTPSignUpWrapper(_ltpSignUp.toUnescapedModel());
	}

	@Override
	public boolean isCachedModel() {
		return _ltpSignUp.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _ltpSignUp.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _ltpSignUp.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _ltpSignUp.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<LTPDatabase.model.LTPSignUp> toCacheModel() {
		return _ltpSignUp.toCacheModel();
	}

	@Override
	public int compareTo(LTPDatabase.model.LTPSignUp ltpSignUp) {
		return _ltpSignUp.compareTo(ltpSignUp);
	}

	@Override
	public int hashCode() {
		return _ltpSignUp.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _ltpSignUp.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new LTPSignUpWrapper((LTPSignUp)_ltpSignUp.clone());
	}

	/**
	* Returns the first name of this LTPSignUp.
	*
	* @return the first name of this LTPSignUp
	*/
	@Override
	public java.lang.String getFirstName() {
		return _ltpSignUp.getFirstName();
	}

	/**
	* Returns the last name of this LTPSignUp.
	*
	* @return the last name of this LTPSignUp
	*/
	@Override
	public java.lang.String getLastName() {
		return _ltpSignUp.getLastName();
	}

	/**
	* Returns the mobile number of this LTPSignUp.
	*
	* @return the mobile number of this LTPSignUp
	*/
	@Override
	public java.lang.String getMobileNumber() {
		return _ltpSignUp.getMobileNumber();
	}

	@Override
	public java.lang.String toString() {
		return _ltpSignUp.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _ltpSignUp.toXmlString();
	}

	/**
	* Returns the created date of this LTPSignUp.
	*
	* @return the created date of this LTPSignUp
	*/
	@Override
	public Date getCreatedDate() {
		return _ltpSignUp.getCreatedDate();
	}

	/**
	* Returns the updated date of this LTPSignUp.
	*
	* @return the updated date of this LTPSignUp
	*/
	@Override
	public Date getUpdatedDate() {
		return _ltpSignUp.getUpdatedDate();
	}

	/**
	* Returns the ID of this LTPSignUp.
	*
	* @return the ID of this LTPSignUp
	*/
	@Override
	public long getID() {
		return _ltpSignUp.getID();
	}

	/**
	* Returns the primary key of this LTPSignUp.
	*
	* @return the primary key of this LTPSignUp
	*/
	@Override
	public long getPrimaryKey() {
		return _ltpSignUp.getPrimaryKey();
	}

	@Override
	public void persist() {
		_ltpSignUp.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_ltpSignUp.setCachedModel(cachedModel);
	}

	/**
	* Sets the created date of this LTPSignUp.
	*
	* @param CreatedDate the created date of this LTPSignUp
	*/
	@Override
	public void setCreatedDate(Date CreatedDate) {
		_ltpSignUp.setCreatedDate(CreatedDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_ltpSignUp.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_ltpSignUp.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_ltpSignUp.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the first name of this LTPSignUp.
	*
	* @param FirstName the first name of this LTPSignUp
	*/
	@Override
	public void setFirstName(java.lang.String FirstName) {
		_ltpSignUp.setFirstName(FirstName);
	}

	/**
	* Sets the ID of this LTPSignUp.
	*
	* @param ID the ID of this LTPSignUp
	*/
	@Override
	public void setID(long ID) {
		_ltpSignUp.setID(ID);
	}

	/**
	* Sets the last name of this LTPSignUp.
	*
	* @param LastName the last name of this LTPSignUp
	*/
	@Override
	public void setLastName(java.lang.String LastName) {
		_ltpSignUp.setLastName(LastName);
	}

	/**
	* Sets the mobile number of this LTPSignUp.
	*
	* @param MobileNumber the mobile number of this LTPSignUp
	*/
	@Override
	public void setMobileNumber(java.lang.String MobileNumber) {
		_ltpSignUp.setMobileNumber(MobileNumber);
	}

	@Override
	public void setNew(boolean n) {
		_ltpSignUp.setNew(n);
	}

	/**
	* Sets the primary key of this LTPSignUp.
	*
	* @param primaryKey the primary key of this LTPSignUp
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_ltpSignUp.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_ltpSignUp.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the updated date of this LTPSignUp.
	*
	* @param UpdatedDate the updated date of this LTPSignUp
	*/
	@Override
	public void setUpdatedDate(Date UpdatedDate) {
		_ltpSignUp.setUpdatedDate(UpdatedDate);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof LTPSignUpWrapper)) {
			return false;
		}

		LTPSignUpWrapper ltpSignUpWrapper = (LTPSignUpWrapper)obj;

		if (Objects.equals(_ltpSignUp, ltpSignUpWrapper._ltpSignUp)) {
			return true;
		}

		return false;
	}

	@Override
	public LTPSignUp getWrappedModel() {
		return _ltpSignUp;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _ltpSignUp.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _ltpSignUp.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_ltpSignUp.resetOriginalValues();
	}

	private final LTPSignUp _ltpSignUp;
}