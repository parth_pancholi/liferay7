<%@ include file="/init.jsp"%>
<style>
	.dataTables_filter input{
 	    width: 500px;
   		height: 36px;
	}
</style>
<portlet:renderURL var="addActivitySignUpURL">
	<portlet:param name="mvcPath" value="/view.jsp"></portlet:param>
</portlet:renderURL>
<portlet:renderURL var="updateActivityURL">
	<portlet:param name="mvcPath" value="/view.jsp"></portlet:param>
</portlet:renderURL>
<portlet:resourceURL var="deleteActivityURL" id="deleteActivity" />
<aui:nav cssClass="container-fluid-1280">
	<aui:button-row>
		<aui:a href="<%=addActivitySignUpURL%>"
			cssClass="btn btn-primary btn-default">
			<span class="lfr-btn-label">New Activity Sign Up</span>
		</aui:a>
	</aui:button-row>
</aui:nav>
<table id="activity" class="display container-fluid-1280">
	<thead>
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Mobile Number</th>
			<th>Created Date</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${data}" var="activity">
			<tr class="${activity.getID()}">
				<td>${activity.getFirstName()}</td>
				<td>${activity.getLastName()}</td>
				<td>${activity.getMobileNumber()}</td>
				<td>
					<fmt:formatDate pattern = "MM/dd/yyyy"  value = "${activity.getCreatedDate()}" />
				</td>
				<td>
					<aui:a href="${updateActivityURL}&${renderResponse.getNamespace()}id=${activity.getID()}"
						cssClass="btn btn-primary btn-default">
						<span class="lfr-btn-label">Edit</span>
					</aui:a>
					<aui:a href="#" onclick="deleteUser(${activity.getID()})"
						cssClass="btn btn-primary btn-default">
						<span class="lfr-btn-label">Delete</span>
					</aui:a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script type="text/javascript">
	function deleteUser(userId){
		$.ajax({
			url: "${deleteActivityURL}&${renderResponse.getNamespace()}id="+userId, 
			success: function(result){
				location.reload();
		  	}
		});
	}
	$(document).ready(function() {
		var userTable=$('#activity').DataTable({
							"oLanguage" : {
								"sSearch" : "Search:",
								"sEmptyTable" : "<div class='portlet-msg-alert'>No Activity Found</div>"
							}
						});
	});
</script>