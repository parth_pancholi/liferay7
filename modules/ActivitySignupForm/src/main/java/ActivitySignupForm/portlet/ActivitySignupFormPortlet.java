package ActivitySignupForm.portlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.google.gson.Gson;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import ActivitySignupForm.constants.ActivitySignupFormPortletKeys;
import Database.model.ActivitySignUp;
import Database.model.Register;
import Database.service.ActivitySignUpLocalServiceUtil;
import Database.service.RegisterLocalServiceUtil;

/**
 * @author parth
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=ActivitySignupForm Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ActivitySignupFormPortletKeys.ActivitySignupForm,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class ActivitySignupFormPortlet extends MVCPortlet {

	@Override
	public void serveResource(javax.portlet.ResourceRequest resourceRequest,
			javax.portlet.ResourceResponse resourceResponse)
			throws java.io.IOException, javax.portlet.PortletException {
		if ("deleteActivity".equals(resourceRequest.getResourceID())) {
			try {
				ActivitySignUp activitySignUp = ActivitySignUpLocalServiceUtil
						.deleteActivitySignUp(Long.parseLong(resourceRequest.getParameter("id")));
				System.out.println("Activity Deleted Successfully.." + activitySignUp);
				resourceResponse.createRenderURL().addProperty("mvcPath", "/listing.jsp");
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if ("searchUser".equals(resourceRequest.getResourceID())) {
			System.out.println("searchUser" + resourceRequest.getResourceID());
			System.out.println(ParamUtil.getString(resourceRequest, "searchLastName"));
			System.out.println(ParamUtil.getString(resourceRequest, "searchMobileNumber"));
			List<Register> userList = RegisterLocalServiceUtil.findByLastNameOrMobile(
					ParamUtil.getString(resourceRequest, "searchLastName"),
					ParamUtil.getString(resourceRequest, "searchMobileNumber"), QueryUtil.ALL_POS, QueryUtil.ALL_POS,
					null);
			System.out.println(userList.size());
			Gson gson = new Gson();
			PrintWriter printout = resourceResponse.getWriter();
			printout.print(gson.toJson(userList).toString());
		} else {

			long id = ParamUtil.getLong(resourceRequest, "id");
			String firstName = ParamUtil.getString(resourceRequest, "firstName");
			String lastName = ParamUtil.getString(resourceRequest, "lastName");
			String mobileNumber = ParamUtil.getString(resourceRequest, "mobileNumber");
			boolean isSatsangBiWeeklyMeeting = ParamUtil.getBoolean(resourceRequest, "isSatsangBiWeeklyMeeting");
			boolean isBalmukund = ParamUtil.getBoolean(resourceRequest, "isBalmukund");
			boolean isBhagvatGitaRecitation = ParamUtil.getBoolean(resourceRequest, "isBhagvatGitaRecitation");
			boolean isYoga = ParamUtil.getBoolean(resourceRequest, "isYoga");
			boolean is7MindSet = ParamUtil.getBoolean(resourceRequest, "is7MindSet");
			boolean isOnlineBhagvatGitaDiscussion = ParamUtil.getBoolean(resourceRequest,
					"isOnlineBhagvatGitaDiscussion");
			boolean isOnlineKidsMeditation = ParamUtil.getBoolean(resourceRequest, "isOnlineKidsMeditation");
			boolean isOnlineYouthClub = ParamUtil.getBoolean(resourceRequest, "isOnlineYouthClub");

			try {
				if (Validator.isNotNull(id)) {
					ActivitySignUpLocalServiceUtil.updateActivitySignUp(id, firstName, lastName, mobileNumber,
							isSatsangBiWeeklyMeeting, isBalmukund, isBhagvatGitaRecitation, isYoga, is7MindSet,
							isOnlineBhagvatGitaDiscussion, isOnlineKidsMeditation, isOnlineYouthClub);
				} else {
					ActivitySignUpLocalServiceUtil.addActivitySignUp(firstName, lastName, mobileNumber,
							isSatsangBiWeeklyMeeting, isBalmukund, isBhagvatGitaRecitation, isYoga, is7MindSet,
							isOnlineBhagvatGitaDiscussion, isOnlineKidsMeditation, isOnlineYouthClub);
				}
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	};

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		// Get all the data for the register page.
		if (Validator.isNotNull(renderRequest.getParameter("id"))) {
			try {
				renderRequest.setAttribute("activityData", ActivitySignUpLocalServiceUtil
						.getActivitySignUp(Long.parseLong(renderRequest.getParameter("id"))));
			} catch (NumberFormatException | PortalException e) {
				e.printStackTrace();
			}
		}
		renderRequest.setAttribute("searchUsers", RegisterLocalServiceUtil.getRegisterMembers(QueryUtil.ALL_POS, QueryUtil.ALL_POS));
		renderRequest.setAttribute("data",
				ActivitySignUpLocalServiceUtil.getActivitySignUp(QueryUtil.ALL_POS, QueryUtil.ALL_POS));
		renderRequest.setAttribute("isAdmin", themeDisplay.isSignedIn());
		super.render(renderRequest, renderResponse);
	}
}