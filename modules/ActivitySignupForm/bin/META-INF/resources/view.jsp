<%@ include file="/init.jsp"%>

<portlet:renderURL var="viewActivitySignupURL">
	<portlet:param name="mvcPath" value="/listing.jsp"></portlet:param>
</portlet:renderURL>

<portlet:resourceURL id="addOrUpdateActivitySignUp"
	var="addOrUpdateActivitySignUpURL" />

<portlet:resourceURL id="searchUser" var="searchUserURL" />
<c:if test="${isAdmin eq true}">
	<aui:nav cssClass="container-fluid-1280">
		<aui:button-row>
			<aui:a href="<%=viewActivitySignupURL%>"
				cssClass="btn btn-primary btn-default">
				<span class="lfr-btn-label">Show All Activity Sign up</span>
			</aui:a>
		</aui:button-row>
		<table id="searchUser" class="display container-fluid-1280">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Mobile Number</th>
					<th>Created Date</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${searchUsers}" var="user">
					<tr class="${user.getID()}">
						<td>${user.getFirstName()}</td>
						<td>${user.getLastName()}</td>
						<td>${user.getMobileNumber()}</td>
						<td><fmt:formatDate pattern="MM/dd/yyyy"
								value="${user.getCreatedDate()}" /></td>
						<td><aui:a href="javascript:void(0)"
								onClick="loadUser('${user.getFirstName()}', '${user.getLastName()}', '${user.getMobileNumber()}')"
								cssClass="btn btn-primary btn-default">
								<span class="lfr-btn-label">Load User</span>
							</aui:a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</aui:nav>
</c:if>
<hr>
<aui:form action="javascript:validate()" method="post"
	name="registerUserActivityForm" id="ActivityForm"
	class="container-fluid-1280">
	<aui:fieldset-group markupView="lexicon">
		<aui:fieldset>
			<h1 align="center">Activity Signup Form</h1>
			<div class="alert alert-danger hide" id="errorMessage">
				<strong>Error!</strong> You should select atleast one activity from
				below list.</a>
			</div>
			<div class="alert hide" id="errorAjaxMessage">
				<button type="button" class="close" data-dismiss="alert">x</button>
			</div>
		</aui:fieldset>
		<aui:fieldset>
			<aui:row>
				<aui:col width="33">
					<label>First Name:  <span id="labelFirstName">${activityData.firstName}</span></label>
					<aui:input label="First Name" name="firstName" type="hidden"
						placeholder="Enter your first name"
						value="${activityData.firstName}" readonly="true">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="33">
					<label>Last Name:  <span id="labelLastName">${activityData.lastName}</span></label>
					<aui:input label="Last Name" name="lastName" type="hidden"
						placeholder="Enter your last name"
						value="${activityData.lastName}" readonly="true">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="33">
					<label>MobileNumber:  <span id="labelMobileNumber">${activityData.mobileNumber}</span></label>
					<aui:input label="MobileNumber" name="mobileNumber" type="hidden"
						placeholder="Enter your mobileNumber"
						value="${activityData.mobileNumber}" readonly="true">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="number"></aui:validator>
						<aui:validator name="maxLength">10</aui:validator>
					</aui:input>
				</aui:col>
			</aui:row>
			<br/>
			<aui:input name="isSatsangBiWeeklyMeeting" type="checkbox"
				label="Satsang - BiWeekly Meet" onClick="validate()"
				checked="${activityData.isSatsangBiWeeklyMeeting}" />
			<aui:input name="isBalmukund" onClick="validate()"
				checked="${activityData.isBalmukund}" type="checkbox"
				label="Balmukund(Weekly) - Character building program for children" />
			<aui:input name="isBhagvatGitaRecitation" onClick="validate()"
				checked="${activityData.isBhagvatGitaRecitation}" type="checkbox"
				label="Bhagvat Gita(Weekly) - Recitation & Memorization Classes" />
			<aui:input name="isYoga" type="checkbox" onClick="validate()"
				checked="${activityData.isYoga}" label="Yoga" />
			<aui:input name="is7MindSet" onClick="validate()"
				checked="${activityData.is7MindSet}" type="checkbox"
				label="7 MindSet - For Mental wellness of Youth" />
			<aui:input name="isOnlineBhagvatGitaDiscussion" onClick="validate()"
				type="checkbox"
				checked="${activityData.isOnlineBhagvatGitaDiscussion}"
				label="Online - Bhagvat Gita Discussion" />
			<aui:input name="isOnlineKidsMeditation" onClick="validate()"
				type="checkbox" checked="${activityData.isOnlineKidsMeditation}"
				label="Online - Meditation for Kids" />
			<aui:input name="isOnlineYouthClub" onClick="validate()"
				type="checkbox" checked="${activityData.isOnlineYouthClub}"
				label="Online - Youth Club" />
			<aui:input name="id" type="hidden" value="${activityData.ID}" />
		</aui:fieldset>
	</aui:fieldset-group>
	<aui:button-row>
		<aui:button type="submit"
			value='${activityData.ID ne null? "update" : "submit" }'
			onClick="validate(this)" primary="<%=true%>" id="saveButton" />
	</aui:button-row>
</aui:form>
<script type="text/javascript">
var searchUsers= '${searchUsers}';
console.log(searchUsers);
	$(document)
			.ready(
					function() {
						var userTable = $('#searchUser')
								.DataTable(
										{
											"bLengthChange" : false,
											"oLanguage" : {
												"sSearch" : "Search by your name:",
												"sEmptyTable" : "<div class='portlet-msg-alert'>No Activity Found</div>",
												"sZeroRecords": "No matching records found. <br/>  Not a registered member. Please register on the welcome page."
											}
										});
						var condition = '${activityData.ID}';
						if (condition != "") {
							$('#searchUser').parents('div.dataTables_wrapper')
									.first().hide();
						}
						$('#searchUser_filter input').keyup(function(event) {
							if (event.target.value !== "") {
								$("#searchUser").show();
								$('#searchUser_length').show();
								$("#searchUser + .dataTables_info").show();
								$("#searchUser_paginate").show();
							}else{
								$("#searchUser").hide();
								$('#searchUser_length').hide();
								$("#searchUser + .dataTables_info").hide();
								$("#searchUser_paginate").hide();
								$("form[id$='registerUserActivityForm']").hide();
							}
						});
						$("#searchUser").hide();
						$('#searchUser_length').hide();
						$("#searchUser + .dataTables_info").hide();
						$("#searchUser_paginate").hide();
						if(!'${activityData.ID}')
							$("form[id$='registerUserActivityForm']").hide();
					});
</script>
<aui:script>
	function validate(element) {
		var isValid = false;
		$("form[id$='ActivityForm']").find('input[type="checkbox"]').each(
				function(index) {
					if (this.checked) {
						isValid = true;
					}
				});
		if (!isValid) {
			//Highlight the error message showing atleast one checkbox must be selected.
			$("#errorMessage").removeClass('hide');
		} else {
			$("#errorMessage").addClass('hide');
			//Call the action method for the form from here with the form data
			if ($(element).attr('type') === "submit") {
				AUI()
						.use(
								'aui-io-request',
								function(A) {
									A.io
											.request(
													'${addOrUpdateActivitySignUpURL}',
													{
														method : 'post',
														form : {
															id : $(
																	"form[id$='ActivityForm']")
																	.attr('id')
														},
														on : {
															success : function() {
																$(
																		"form[id$='ActivityForm']")
																		.find(
																				'input')
																		.val('');
																$(
																		"form[id$='ActivityForm']")
																		.find(
																				'input')
																		.selected(
																				false);
																$(
																		"#errorAjaxMessage")
																		.html(
																				"Activity Signup Updated Successfull.")
																		.removeClass(
																				'hide')
																		.addClass(
																				"alert-success");
															},
															failure : function() {
																$(
																		"#errorAjaxMessage")
																		.html(
																				"Activity Signup updated failed.")
																		.removeClass(
																				'hide')
																		.addClass(
																				"alert-danger");
															}
														}
													})
								});
			}
		}
		$("button[id$='saveButton']").enable().removeAttr("style");
	};
	function loadUser(firstName, lastName, mobileNumber) {
		$('input[name$="firstName"]').val(firstName);
		$('input[name$="lastName"]').val(lastName);
		$('input[name$="mobileNumber"]').val(mobileNumber);
		$('#labelFirstName').text(firstName);
		$('#labelLastName').text(lastName);
		$('#labelMobileNumber').text(mobileNumber);
		$("form[id$='registerUserActivityForm']").show();
	}
</aui:script>