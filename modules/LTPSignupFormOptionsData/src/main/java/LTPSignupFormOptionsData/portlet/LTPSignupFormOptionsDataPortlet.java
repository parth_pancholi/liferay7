package LTPSignupFormOptionsData.portlet;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import LTPDatabase.model.LTPSignUpMasterData;
import LTPDatabase.service.LTPSignUpMasterDataLocalServiceUtil;
import LTPSignupFormOptionsData.constants.LTPSignupFormOptionsDataPortletKeys;

/**
 * @author parth
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=LTPSignupFormOptionsData Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + LTPSignupFormOptionsDataPortletKeys.LTPSignupFormOptionsData,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class LTPSignupFormOptionsDataPortlet extends MVCPortlet {
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		// TODO Auto-generated method stub
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		if (Validator.isNotNull(renderRequest.getParameter("id"))) {
			try {
				renderRequest.setAttribute("data", LTPSignUpMasterDataLocalServiceUtil
						.getLTPSignUpMasterData(Long.parseLong(renderRequest.getParameter("id"))));
			} catch (NumberFormatException | PortalException e) {
				e.printStackTrace();
			}
		}
		renderRequest.setAttribute("dataList",
				LTPSignUpMasterDataLocalServiceUtil.getAllLTPSignUpMasterData(QueryUtil.ALL_POS, QueryUtil.ALL_POS));
		renderRequest.setAttribute("editableDataList",
				LTPSignUpMasterDataLocalServiceUtil.getLTPSignUpMasterData(QueryUtil.ALL_POS, QueryUtil.ALL_POS));
		renderRequest.setAttribute("isAdmin", themeDisplay.isSignedIn());
		super.render(renderRequest, renderResponse);
	}

	@ProcessAction(name = "submitMasterData")
	public void submitMasterData(ActionRequest actionRequest, ActionResponse actionResponse) {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String eventDate = actionRequest.getParameter("eventDate");
		String isEventType = actionRequest.getParameter("isEventType");
		String extraText = null;
		boolean isMorningWalk = false, isVolunteer = false, isDinnerPrasad = false;
		if ("Morning Walk".equals(isEventType)) {
			extraText = actionRequest.getParameter("extraText");
			isMorningWalk = true;
		} else if ("Volunteer".equals(isEventType)) {
			isVolunteer = true;
		} else {
			isDinnerPrasad = true;
		}
		Long ltpSignUpMasterDataId = Validator.isNotNull(actionRequest.getParameter("id"))
				? Long.parseLong(actionRequest.getParameter("id"))
				: 0L;
		try {
			LTPSignUpMasterDataLocalServiceUtil.addUpdateLTPSignUpMasterData(ltpSignUpMasterDataId, extraText,
					formatter.parse(eventDate), isMorningWalk, isVolunteer, isDinnerPrasad);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		try {
			LTPSignUpMasterData ltpSignUpMasterData = LTPSignUpMasterDataLocalServiceUtil
					.deleteLTPSignUpMasterData(ParamUtil.getLong(resourceRequest, "id"));
			System.out.println("Successfully deleted...." + ltpSignUpMasterData);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.serveResource(resourceRequest, resourceResponse);
	}
}