<%@ include file="/init.jsp"%>
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css" />
<script type="text/javascript" charset="utf8"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<portlet:renderURL var="updateDataURL">
	<portlet:param name="mvcPath" value="/view.jsp"></portlet:param>
</portlet:renderURL>
<portlet:resourceURL var="deleteURL" id="delete" />
<aui:nav cssClass="container-fluid-1280">
	<aui:button-row>
		<aui:a href="<%=updateDataURL%>"
			cssClass="btn btn-primary btn-default">
			<span class="lfr-btn-label">Add New</span>
		</aui:a>
		<label style="float:right;">N/A : Not Editable because atleast one LTP Sign Up is done for that event type.</label>
	</aui:button-row>
</aui:nav>
<table id="dataList" class="display container-fluid-1280">
	<thead>
		<tr>
			<th>Date</th>
			<th>Event Type</th>
			<th>Address</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${dataList}" var="data">
			<tr class="${data.getID()}">
				<td>
					<fmt:formatDate pattern = "MM/dd/yyyy"  value = "${data.getEventDate()}" />
				</td>
				<td>${data.getIsMorningWalk() ? "Morning Walk" : data.getIsDinnerPrasad()? "Dinner Prasad" : "Volunteer"}</td>
				<td>${data.getIsMorningWalk() ? data.getExtraText() : "N/A"}</td>
				<td>
					<c:if test="${editableDataList.contains(data)}">
						<aui:a href="${updateDataURL}&${renderResponse.getNamespace()}id=${data.getID()}"
							cssClass="btn btn-primary btn-default">
							<span class="lfr-btn-label">Edit</span>
						</aui:a>
						<aui:a href="#" onclick="deleteEvent(${data.getID()})"
							cssClass="btn btn-primary btn-default">
							<span class="lfr-btn-label">Delete</span>
						</aui:a>
					</c:if>
					<c:if test="${!editableDataList.contains(data)}">
						N/A
					</c:if>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script type="text/javascript">
	function deleteEvent(id){
		$.ajax({
			url: "${deleteURL}&${renderResponse.getNamespace()}id="+id, 
			success: function(result){
				location.reload();
		  	}
		});
	}
	$(document).ready(function() {
		var dataTable=$('#dataList').DataTable({
							"oLanguage" : {
								"sSearch" : "Search: ",
								"sEmptyTable" : "<div class='portlet-msg-alert'>No Data Found</div>" // default message for no data
							}
						});
	});
</script>