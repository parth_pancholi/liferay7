<%@page import="LTPDatabase.model.LTPSignUpMasterData"%>
<%@ include file="/init.jsp"%>
<%@ page import="java.util.Date"%>
<%
	Date defaultDate;
	if(request.getAttribute("data") == null){
		defaultDate = new Date();
	}
	else{
		defaultDate = ((LTPSignUpMasterData)request.getAttribute("data")).getEventDate();
	}
	int startDay, startMonth, startYear;
	startDay = defaultDate.getDate();
	// For some reason the input-date subtracts 1900 from the year it is given,
	// so we have to add that 1900 back to it
	startYear = defaultDate.getYear() + 1900;
	// However it does work with java.util.Date counting months from 0, 
	// so you don't have to add one extra month to it
	startMonth = defaultDate.getMonth();
%>
<portlet:renderURL var="viewDataURL">
	<portlet:param name="mvcPath" value="/listing.jsp"></portlet:param>
</portlet:renderURL>
<portlet:actionURL name="submitMasterData" var="submitMasterDataURL" />
<c:if test="${isAdmin eq true}">
	<aui:nav cssClass="container-fluid-1280">
		<aui:button-row>
			<aui:a href="<%=viewDataURL%>"
				cssClass="btn btn-primary btn-default">
				<span class="lfr-btn-label">Show Listing</span>
			</aui:a>
		</aui:button-row>
	</aui:nav>
	<aui:form action="${submitMasterDataURL}" method="post"
		name="submitMasterDataForm" cssClass="container-fluid-1280">
		<aui:fieldset-group markupView="lexicon">
			<aui:fieldset>
				<h1 align="center">LTP Signup Master Data Configuration</h1>
			</aui:fieldset>
			<aui:fieldset>
				<aui:row>
					<aui:col width="50">
						<liferay-ui:input-date name="eventDate"
							firstEnabledDate="<%=defaultDate%>" dayValue="<%=startDay%>"
							monthValue="<%=startMonth%>" yearValue="<%=startYear%>">Select Date</liferay-ui:input-date>
					</aui:col>
					<aui:col width="50">
						<aui:input name="extraText" label="Address" type="type" value="${data.extraText}" />
					</aui:col>
				</aui:row>
				<aui:input name="isEventType" label="Morning Walk" type="radio"
					value="Morning Walk"  />
				<aui:input name="isEventType" type="radio" value="Dinner Prasad" 
					label="Dinner Prasad"  />
				<aui:input name="isEventType" type="radio" value="Volunteer" 
					label="Volunteer"  />
				<aui:input name="id" type="hidden" value="${data.ID}" />
			</aui:fieldset>
		</aui:fieldset-group>
		<aui:button-row>
			<aui:button type="submit" value='Update' primary="<%=true%>" />
		</aui:button-row>
	</aui:form>
</c:if>
<c:if test="${isAdmin ne true}">
	Please login via admin account to use this portlet.
</c:if>
<script>
	function displayAddress(){
		$("input[name$='isEventType']").each(function(index) {
			if (this.checked && this.value === "Morning Walk") {
				console.log(this.checked + ":" + this.value);
				$('input[name$="extraText"]').parent().show();
				return false;
			} else {
				$('input[name$="extraText"]').parent().hide();
			}
		});
	}
	$(document).ready(function() {
		$('input[name$="isEventType"]').change(function() {
			displayAddress();
		});
		displayAddress();
		if('${data.ID}'){
			$('input[name$="isEventType"][value="${data.getIsMorningWalk() ? 'Morning Walk' : data.getIsDinnerPrasad()? 'Dinner Prasad' : 'Volunteer'}"]').trigger('click');
		}
		//default enable Volunteer
		if('${data.ID}'){
			console.log("update request")
		}else{
			$('input[name$="isEventType"][value="Volunteer"]').trigger('click');
			$('input[name$="isEventType"]').selected(false);
		}
	});
</script>