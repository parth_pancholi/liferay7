create table ActivitySignUp (
	ID LONG not null primary key,
	FirstName VARCHAR(75) null,
	LastName VARCHAR(75) null,
	MobileNumber VARCHAR(75) null,
	IsSatsangBiWeeklyMeeting BOOLEAN,
	IsBalmukund BOOLEAN,
	IsBhagvatGitaRecitation BOOLEAN,
	IsYoga BOOLEAN,
	Is7MindSet BOOLEAN,
	IsOnlineBhagvatGitaDiscussion BOOLEAN,
	IsOnlineKidsMeditation BOOLEAN,
	IsOnlineYouthClub BOOLEAN,
	CreatedDate DATE null,
	UpdatedDate DATE null
);

create table Register (
	ID LONG not null primary key,
	FirstName VARCHAR(75) null,
	LastName VARCHAR(75) null,
	IsMale BOOLEAN,
	Address VARCHAR(255) null,
	City VARCHAR(255) null,
	State VARCHAR(2) null,
	Zip VARCHAR(5) null,
	PhoneNumber VARCHAR(75) null,
	MobileNumber VARCHAR(75) null,
	Email VARCHAR(75) null,
	AdditionalMember1 VARCHAR(75) null,
	AdditionalMember2 VARCHAR(75) null,
	AdditionalMember3 VARCHAR(75) null,
	IsEmailCampain BOOLEAN,
	IsTemple BOOLEAN,
	IsOnlineNewsWebsite BOOLEAN,
	IsRadio BOOLEAN,
	IsYouTube BOOLEAN,
	IsFacebook BOOLEAN,
	IsMeetup BOOLEAN,
	IsOnlineEventCalendar BOOLEAN,
	IsFlyer BOOLEAN,
	IsNewspaper BOOLEAN,
	IsWordOfMouth BOOLEAN,
	Other VARCHAR(500) null,
	IsinterestedInVolunteringActivity BOOLEAN,
	IsinterestedInSatsangActivity BOOLEAN,
	IsinterestedInChildrenActivity BOOLEAN,
	CreatedDate DATE null,
	UpdatedDate DATE null,
	AdditionalMember1Age VARCHAR(75) null,
	AdditionalMember2Age VARCHAR(75) null,
	AdditionalMember3Age VARCHAR(75) null,
	SpouseFirstName VARCHAR(75) null,
	SpouseLastName VARCHAR(75) null,
	SpousePhoneNumber VARCHAR(75) null,
	MembershipPlan VARCHAR(75) null,
	PaymentMethod VARCHAR(75) null,
	StateCenter VARCHAR(75) null,
	ProgramName VARCHAR(75) null
);