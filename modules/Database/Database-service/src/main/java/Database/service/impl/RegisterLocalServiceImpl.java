/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.service.impl;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.Junction;
import com.liferay.portal.kernel.dao.orm.Property;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.Validator;

import Database.model.Register;
import Database.service.RegisterLocalServiceUtil;
import Database.service.base.RegisterLocalServiceBaseImpl;
import Database.service.persistence.RegisterPersistence;

/**
 * The implementation of the Register local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link Database.service.RegisterLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RegisterLocalServiceBaseImpl
 * @see Database.service.RegisterLocalServiceUtil
 */
public class RegisterLocalServiceImpl extends RegisterLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * Database.service.RegisterLocalServiceUtil} to access the Register local
	 * service.
	 */

	/**
	 * 
	 */
	@BeanReference(type = RegisterPersistence.class)
	protected RegisterPersistence registerPersistence;

	
	public List<Register> findByLastNameOrMobile(String lastName,
			String mobileNumber, int start, int end,
			OrderByComparator orderByComparator) throws SystemException {
		DynamicQuery dynamicQuery = buildRegisterDynamicQuery(lastName,
				mobileNumber);
		return RegisterLocalServiceUtil.dynamicQuery(dynamicQuery, start, end,
				orderByComparator);
	}


	protected DynamicQuery buildRegisterDynamicQuery(String lastName, String mobileNumber) {
		Junction junction = null;
			junction = RestrictionsFactoryUtil.disjunction();

		if (Validator.isNotNull(lastName)) {
			Property property = PropertyFactoryUtil.forName("LastName");
			String value = (new StringBuilder("%")).append(lastName)
					.append("%").toString();
			junction.add(property.like(value));
		}
		if(Validator.isNotNull(mobileNumber)) {
			Property property = PropertyFactoryUtil.forName("MobileNumber");
			String value = (new StringBuilder("%")).append(mobileNumber)
					.append("%").toString();
			junction.add(property.like(value));
		}
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(
				Register.class, getClassLoader());
		return dynamicQuery.add(junction);
	}
	/**
	 * @param firstName
	 * @param lastName
	 * @param isMale
	 * @param address
	 * @param city
	 * @param state
	 * @param zip
	 * @param phoneNumber
	 * @param mobileNumber
	 * @param email
	 * @param additionalMember1
	 * @param additionalMember2
	 * @param additionalMember3
	 * @param isEmailCampain
	 * @param isTemple
	 * @param isOnlineNewsWebsite
	 * @param isRadio
	 * @param isYouTube
	 * @param isFacebook
	 * @param isMeetup
	 * @param isOnlineEventCalendar
	 * @param isFlyer
	 * @param isNewspaper
	 * @param isWordOfMouth
	 * @param other
	 * @param isinterestedInVolunteringActivity
	 * @param isinterestedInSatsangActivity
	 * @param isinterestedInChildrenActivity
	 * @return
	 * @throws PortalException
	 */
	public Register RegisterMember(String firstName, String lastName, boolean isMale, String address, String city,
			String state, String zip, String phoneNumber, String mobileNumber, String email, String additionalMember1,
			String additionalMember2, String additionalMember3, boolean isEmailCampain, boolean isTemple,
			boolean isOnlineNewsWebsite, boolean isRadio, boolean isYouTube, boolean isFacebook, boolean isMeetup,
			boolean isOnlineEventCalendar, boolean isFlyer, boolean isNewspaper, boolean isWordOfMouth, String other,
			boolean isinterestedInVolunteringActivity, boolean isinterestedInSatsangActivity,
			boolean isinterestedInChildrenActivity, String additionalMember1Age, String additionalMember2Age,
			String additionalMember3Age, String spouseFirstName, String spouseLastName, String spousePhoneNumber,
			String membershipPlan, String paymentMethod) throws PortalException {

		long registerId = counterLocalService.increment();

		Register registerEntry = registerPersistence.create(registerId);

		registerEntry.setID(registerId);
		registerEntry.setFirstName(firstName);
		registerEntry.setLastName(lastName);
		registerEntry.setIsMale(isMale);
		registerEntry.setAddress(address);
		registerEntry.setCity(city);
		registerEntry.setState(state);
		registerEntry.setZip(zip);
		registerEntry.setPhoneNumber(phoneNumber);
		registerEntry.setMobileNumber(mobileNumber);
		registerEntry.setEmail(email);
		registerEntry.setSpouseFirstName(spouseFirstName);
		registerEntry.setSpouseLastName(spouseLastName);
		registerEntry.setSpousePhoneNumber(spousePhoneNumber);
		registerEntry.setAdditionalMember1(additionalMember1);
		registerEntry.setAdditionalMember1Age(additionalMember1Age);
		registerEntry.setAdditionalMember2(additionalMember2);
		registerEntry.setAdditionalMember2Age(additionalMember2Age);
		registerEntry.setAdditionalMember3(additionalMember3);
		registerEntry.setAdditionalMember3Age(additionalMember3Age);
		registerEntry.setIsEmailCampain(isEmailCampain);
		registerEntry.setIsTemple(isTemple);
		registerEntry.setIsOnlineNewsWebsite(isOnlineNewsWebsite);
		registerEntry.setIsRadio(isRadio);
		registerEntry.setIsYouTube(isYouTube);
		registerEntry.setIsFacebook(isFacebook);
		registerEntry.setIsMeetup(isMeetup);
		registerEntry.setIsOnlineEventCalendar(isOnlineEventCalendar);
		registerEntry.setIsFlyer(isFlyer);
		registerEntry.setIsNewspaper(isNewspaper);
		registerEntry.setIsWordOfMouth(isWordOfMouth);
		registerEntry.setOther(other);
		registerEntry.setIsinterestedInVolunteringActivity(isinterestedInVolunteringActivity);
		registerEntry.setIsinterestedInSatsangActivity(isinterestedInSatsangActivity);
		registerEntry.setIsinterestedInChildrenActivity(isinterestedInChildrenActivity);
		Date createdDate = new Date();
		registerEntry.setCreatedDate(createdDate);
		registerEntry.setUpdatedDate(createdDate);
		registerEntry.setMembershipPlan(membershipPlan);
		registerEntry.setPaymentMethod(paymentMethod);
		
		registerEntry.setStateCenter("VA");
		registerEntry.setProgramName("LTP-2");
		
		registerPersistence.update(registerEntry);

		return registerEntry;
	}

	/**
	 * @param registerId
	 * @param firstName
	 * @param lastName
	 * @param isMale
	 * @param address
	 * @param city
	 * @param state
	 * @param zip
	 * @param phoneNumber
	 * @param mobileNumber
	 * @param email
	 * @param additionalMember1
	 * @param additionalMember2
	 * @param additionalMember3
	 * @param isEmailCampain
	 * @param isTemple
	 * @param isOnlineNewsWebsite
	 * @param isRadio
	 * @param isYouTube
	 * @param isFacebook
	 * @param isMeetup
	 * @param isOnlineEventCalendar
	 * @param isFlyer
	 * @param isNewspaper
	 * @param isWordOfMouth
	 * @param other
	 * @param isinterestedInVolunteringActivity
	 * @param isinterestedInSatsangActivity
	 * @param isinterestedInChildrenActivity
	 * @return
	 * @throws PortalException
	 */
	public Register updateRegisterMember(long registerId, String firstName, String lastName, boolean isMale,
			String address, String city, String state, String zip, String phoneNumber, String mobileNumber,
			String email, String additionalMember1, String additionalMember2, String additionalMember3,
			boolean isEmailCampain, boolean isTemple, boolean isOnlineNewsWebsite, boolean isRadio, boolean isYouTube,
			boolean isFacebook, boolean isMeetup, boolean isOnlineEventCalendar, boolean isFlyer, boolean isNewspaper,
			boolean isWordOfMouth, String other, boolean isinterestedInVolunteringActivity,
			boolean isinterestedInSatsangActivity, boolean isinterestedInChildrenActivity, String additionalMember1Age,
			String additionalMember2Age, String additionalMember3Age, String spouseFirstName, String spouseLastName,
			String spousePhoneNumber, String membershipPlan, String paymentMethod) throws PortalException {
		Register registerEntry = registerPersistence.findByPrimaryKey(registerId);

		registerEntry.setID(registerId);
		registerEntry.setFirstName(firstName);
		registerEntry.setLastName(lastName);
		registerEntry.setIsMale(isMale);
		registerEntry.setAddress(address);
		registerEntry.setCity(city);
		registerEntry.setState(state);
		registerEntry.setZip(zip);
		registerEntry.setPhoneNumber(phoneNumber);
		registerEntry.setMobileNumber(mobileNumber);
		registerEntry.setEmail(email);
		registerEntry.setSpouseFirstName(spouseFirstName);
		registerEntry.setSpouseLastName(spouseLastName);
		registerEntry.setSpousePhoneNumber(spousePhoneNumber);
		registerEntry.setAdditionalMember1(additionalMember1);
		registerEntry.setAdditionalMember1Age(additionalMember1Age);
		registerEntry.setAdditionalMember2(additionalMember2);
		registerEntry.setAdditionalMember2Age(additionalMember2Age);
		registerEntry.setAdditionalMember3(additionalMember3);
		registerEntry.setAdditionalMember3Age(additionalMember3Age);
		registerEntry.setIsEmailCampain(isEmailCampain);
		registerEntry.setIsTemple(isTemple);
		registerEntry.setIsOnlineNewsWebsite(isOnlineNewsWebsite);
		registerEntry.setIsRadio(isRadio);
		registerEntry.setIsYouTube(isYouTube);
		registerEntry.setIsFacebook(isFacebook);
		registerEntry.setIsMeetup(isMeetup);
		registerEntry.setIsOnlineEventCalendar(isOnlineEventCalendar);
		registerEntry.setIsFlyer(isFlyer);
		registerEntry.setIsNewspaper(isNewspaper);
		registerEntry.setIsWordOfMouth(isWordOfMouth);
		registerEntry.setOther(other);
		registerEntry.setIsinterestedInVolunteringActivity(isinterestedInVolunteringActivity);
		registerEntry.setIsinterestedInSatsangActivity(isinterestedInSatsangActivity);
		registerEntry.setIsinterestedInChildrenActivity(isinterestedInChildrenActivity);
		registerEntry.setCreatedDate(registerEntry.getCreatedDate());
		Date updatedDate = new Date();
		registerEntry.setUpdatedDate(updatedDate);
		registerEntry.setMembershipPlan(membershipPlan);
		registerEntry.setPaymentMethod(paymentMethod);

		registerEntry.setStateCenter("VA");
		registerEntry.setProgramName("LTP-2");
		
		registerPersistence.update(registerEntry);

		return registerEntry;
	}

	/**
	 * @param entry
	 * @return
	 * @throws PortalException
	 */
	public Register deleteEntry(Register entry) throws PortalException {
		registerPersistence.remove(entry);
		return entry;
	}

	/**
	 * @param start
	 * @param end
	 * @return
	 */
	public List<Register> getRegisterMembers(int start, int end) {
		return registerPersistence.findAll(start, end);
	}

	/**
	 * @return
	 */
	public int getRegisterMembersCount() {
		return registerPersistence.countAll();
	}
}