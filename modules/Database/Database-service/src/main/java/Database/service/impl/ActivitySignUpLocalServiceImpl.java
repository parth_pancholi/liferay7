/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.service.impl;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.exception.PortalException;

import Database.model.ActivitySignUp;
import Database.service.base.ActivitySignUpLocalServiceBaseImpl;
import Database.service.persistence.ActivitySignUpPersistence;

/**
 * The implementation of the ActivitySignUp local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are
 * added, rerun ServiceBuilder to copy their definitions into the
 * {@link Database.service.ActivitySignUpLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security
 * checks based on the propagated JAAS credentials because this service can only
 * be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ActivitySignUpLocalServiceBaseImpl
 * @see Database.service.ActivitySignUpLocalServiceUtil
 */
public class ActivitySignUpLocalServiceImpl extends ActivitySignUpLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link
	 * Database.service.ActivitySignUpLocalServiceUtil} to access the
	 * ActivitySignUp local service.
	 */
	@BeanReference(type = ActivitySignUpPersistence.class)
	protected ActivitySignUpPersistence activitySignUpPersistence;

	/**
	 * @param entry
	 * @return
	 * @throws PortalException
	 */
	public ActivitySignUp deleteEntry(ActivitySignUp entry) throws PortalException {
		activitySignUpPersistence.remove(entry);
		return entry;
	}

	/**
	 * @param start
	 * @param end
	 * @return
	 */
	public List<ActivitySignUp> getActivitySignUp(int start, int end) {
		return activitySignUpPersistence.findAll(start, end);
	}

	/**
	 * @return
	 */
	public int getActivitySignUpCount() {
		return activitySignUpPersistence.countAll();
	}

	/**
	 * @param firstName
	 * @param lastName
	 * @param phoneNumber
	 * @param isSatsangBiWeeklyMeeting
	 * @param isBalmukund
	 * @param isBhagvatGitaRecitation
	 * @param isYoga
	 * @param is7MindSet
	 * @param isOnlineBhagvatGitaDiscussion
	 * @param isOnlineKidsMeditation
	 * @param isOnlineYouthClub
	 * @return
	 * @throws PortalException
	 */
	public ActivitySignUp addActivitySignUp(String firstName, String lastName, String phoneNumber,
			boolean isSatsangBiWeeklyMeeting, boolean isBalmukund, boolean isBhagvatGitaRecitation, boolean isYoga,
			boolean is7MindSet, boolean isOnlineBhagvatGitaDiscussion, boolean isOnlineKidsMeditation,
			boolean isOnlineYouthClub) throws PortalException {

		long activitySignUpId = counterLocalService.increment();

		ActivitySignUp activitySignUpEntry = activitySignUpPersistence.create(activitySignUpId);

		activitySignUpEntry.setID(activitySignUpId);
		activitySignUpEntry.setFirstName(firstName);
		activitySignUpEntry.setLastName(lastName);
		activitySignUpEntry.setMobileNumber(phoneNumber);
		activitySignUpEntry.setIsSatsangBiWeeklyMeeting(isSatsangBiWeeklyMeeting);
		activitySignUpEntry.setIsBalmukund(isBalmukund);
		activitySignUpEntry.setIsBhagvatGitaRecitation(isBhagvatGitaRecitation);
		activitySignUpEntry.setIsYoga(isYoga);
		activitySignUpEntry.setIs7MindSet(is7MindSet);
		activitySignUpEntry.setIsOnlineBhagvatGitaDiscussion(isOnlineBhagvatGitaDiscussion);
		activitySignUpEntry.setIsOnlineKidsMeditation(isOnlineKidsMeditation);
		activitySignUpEntry.setIsOnlineYouthClub(isOnlineYouthClub);
		Date createdDate = new Date();
		activitySignUpEntry.setCreatedDate(createdDate);
		activitySignUpEntry.setUpdatedDate(createdDate);

		System.out.println("Entry Added into the DB");
		activitySignUpPersistence.update(activitySignUpEntry);

		return activitySignUpEntry;
	}
	
	/**
	 * @param activitySignUpId
	 * @param firstName
	 * @param lastName
	 * @param phoneNumber
	 * @param isSatsangBiWeeklyMeeting
	 * @param isBalmukund
	 * @param isBhagvatGitaRecitation
	 * @param isYoga
	 * @param is7MindSet
	 * @param isOnlineBhagvatGitaDiscussion
	 * @param isOnlineKidsMeditation
	 * @param isOnlineYouthClub
	 * @return
	 * @throws PortalException
	 */
	public ActivitySignUp updateActivitySignUp(long activitySignUpId,String firstName, String lastName, String phoneNumber,
			boolean isSatsangBiWeeklyMeeting, boolean isBalmukund, boolean isBhagvatGitaRecitation, boolean isYoga,
			boolean is7MindSet, boolean isOnlineBhagvatGitaDiscussion, boolean isOnlineKidsMeditation,
			boolean isOnlineYouthClub) throws PortalException {

		ActivitySignUp activitySignUpEntry = activitySignUpPersistence.findByPrimaryKey(activitySignUpId);

		activitySignUpEntry.setID(activitySignUpId);
		activitySignUpEntry.setFirstName(firstName);
		activitySignUpEntry.setLastName(lastName);
		activitySignUpEntry.setMobileNumber(phoneNumber);
		activitySignUpEntry.setIsSatsangBiWeeklyMeeting(isSatsangBiWeeklyMeeting);
		activitySignUpEntry.setIsBalmukund(isBalmukund);
		activitySignUpEntry.setIsBhagvatGitaRecitation(isBhagvatGitaRecitation);
		activitySignUpEntry.setIsYoga(isYoga);
		activitySignUpEntry.setIs7MindSet(is7MindSet);
		activitySignUpEntry.setIsOnlineBhagvatGitaDiscussion(isOnlineBhagvatGitaDiscussion);
		activitySignUpEntry.setIsOnlineKidsMeditation(isOnlineKidsMeditation);
		activitySignUpEntry.setIsOnlineYouthClub(isOnlineYouthClub);
		Date createdDate = new Date();
		activitySignUpEntry.setCreatedDate(createdDate);
		activitySignUpEntry.setUpdatedDate(createdDate);

		activitySignUpPersistence.update(activitySignUpEntry);

		return activitySignUpEntry;
	}
}