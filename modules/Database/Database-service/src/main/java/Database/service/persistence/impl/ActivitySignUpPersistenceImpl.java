/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.service.persistence.impl;

import Database.exception.NoSuchActivitySignUpException;

import Database.model.ActivitySignUp;

import Database.model.impl.ActivitySignUpImpl;
import Database.model.impl.ActivitySignUpModelImpl;

import Database.service.persistence.ActivitySignUpPersistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the ActivitySignUp service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ActivitySignUpPersistence
 * @see Database.service.persistence.ActivitySignUpUtil
 * @generated
 */
@ProviderType
public class ActivitySignUpPersistenceImpl extends BasePersistenceImpl<ActivitySignUp>
	implements ActivitySignUpPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link ActivitySignUpUtil} to access the ActivitySignUp persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = ActivitySignUpImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
			ActivitySignUpModelImpl.FINDER_CACHE_ENABLED,
			ActivitySignUpImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
			"findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
			ActivitySignUpModelImpl.FINDER_CACHE_ENABLED,
			ActivitySignUpImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
			ActivitySignUpModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public ActivitySignUpPersistenceImpl() {
		setModelClass(ActivitySignUp.class);
	}

	/**
	 * Caches the ActivitySignUp in the entity cache if it is enabled.
	 *
	 * @param activitySignUp the ActivitySignUp
	 */
	@Override
	public void cacheResult(ActivitySignUp activitySignUp) {
		entityCache.putResult(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
			ActivitySignUpImpl.class, activitySignUp.getPrimaryKey(),
			activitySignUp);

		activitySignUp.resetOriginalValues();
	}

	/**
	 * Caches the ActivitySignUps in the entity cache if it is enabled.
	 *
	 * @param activitySignUps the ActivitySignUps
	 */
	@Override
	public void cacheResult(List<ActivitySignUp> activitySignUps) {
		for (ActivitySignUp activitySignUp : activitySignUps) {
			if (entityCache.getResult(
						ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
						ActivitySignUpImpl.class, activitySignUp.getPrimaryKey()) == null) {
				cacheResult(activitySignUp);
			}
			else {
				activitySignUp.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all ActivitySignUps.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(ActivitySignUpImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the ActivitySignUp.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(ActivitySignUp activitySignUp) {
		entityCache.removeResult(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
			ActivitySignUpImpl.class, activitySignUp.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<ActivitySignUp> activitySignUps) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (ActivitySignUp activitySignUp : activitySignUps) {
			entityCache.removeResult(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
				ActivitySignUpImpl.class, activitySignUp.getPrimaryKey());
		}
	}

	/**
	 * Creates a new ActivitySignUp with the primary key. Does not add the ActivitySignUp to the database.
	 *
	 * @param ID the primary key for the new ActivitySignUp
	 * @return the new ActivitySignUp
	 */
	@Override
	public ActivitySignUp create(long ID) {
		ActivitySignUp activitySignUp = new ActivitySignUpImpl();

		activitySignUp.setNew(true);
		activitySignUp.setPrimaryKey(ID);

		return activitySignUp;
	}

	/**
	 * Removes the ActivitySignUp with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ID the primary key of the ActivitySignUp
	 * @return the ActivitySignUp that was removed
	 * @throws NoSuchActivitySignUpException if a ActivitySignUp with the primary key could not be found
	 */
	@Override
	public ActivitySignUp remove(long ID) throws NoSuchActivitySignUpException {
		return remove((Serializable)ID);
	}

	/**
	 * Removes the ActivitySignUp with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the ActivitySignUp
	 * @return the ActivitySignUp that was removed
	 * @throws NoSuchActivitySignUpException if a ActivitySignUp with the primary key could not be found
	 */
	@Override
	public ActivitySignUp remove(Serializable primaryKey)
		throws NoSuchActivitySignUpException {
		Session session = null;

		try {
			session = openSession();

			ActivitySignUp activitySignUp = (ActivitySignUp)session.get(ActivitySignUpImpl.class,
					primaryKey);

			if (activitySignUp == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchActivitySignUpException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(activitySignUp);
		}
		catch (NoSuchActivitySignUpException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected ActivitySignUp removeImpl(ActivitySignUp activitySignUp) {
		activitySignUp = toUnwrappedModel(activitySignUp);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(activitySignUp)) {
				activitySignUp = (ActivitySignUp)session.get(ActivitySignUpImpl.class,
						activitySignUp.getPrimaryKeyObj());
			}

			if (activitySignUp != null) {
				session.delete(activitySignUp);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (activitySignUp != null) {
			clearCache(activitySignUp);
		}

		return activitySignUp;
	}

	@Override
	public ActivitySignUp updateImpl(ActivitySignUp activitySignUp) {
		activitySignUp = toUnwrappedModel(activitySignUp);

		boolean isNew = activitySignUp.isNew();

		Session session = null;

		try {
			session = openSession();

			if (activitySignUp.isNew()) {
				session.save(activitySignUp);

				activitySignUp.setNew(false);
			}
			else {
				activitySignUp = (ActivitySignUp)session.merge(activitySignUp);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
			ActivitySignUpImpl.class, activitySignUp.getPrimaryKey(),
			activitySignUp, false);

		activitySignUp.resetOriginalValues();

		return activitySignUp;
	}

	protected ActivitySignUp toUnwrappedModel(ActivitySignUp activitySignUp) {
		if (activitySignUp instanceof ActivitySignUpImpl) {
			return activitySignUp;
		}

		ActivitySignUpImpl activitySignUpImpl = new ActivitySignUpImpl();

		activitySignUpImpl.setNew(activitySignUp.isNew());
		activitySignUpImpl.setPrimaryKey(activitySignUp.getPrimaryKey());

		activitySignUpImpl.setID(activitySignUp.getID());
		activitySignUpImpl.setFirstName(activitySignUp.getFirstName());
		activitySignUpImpl.setLastName(activitySignUp.getLastName());
		activitySignUpImpl.setMobileNumber(activitySignUp.getMobileNumber());
		activitySignUpImpl.setIsSatsangBiWeeklyMeeting(activitySignUp.isIsSatsangBiWeeklyMeeting());
		activitySignUpImpl.setIsBalmukund(activitySignUp.isIsBalmukund());
		activitySignUpImpl.setIsBhagvatGitaRecitation(activitySignUp.isIsBhagvatGitaRecitation());
		activitySignUpImpl.setIsYoga(activitySignUp.isIsYoga());
		activitySignUpImpl.setIs7MindSet(activitySignUp.isIs7MindSet());
		activitySignUpImpl.setIsOnlineBhagvatGitaDiscussion(activitySignUp.isIsOnlineBhagvatGitaDiscussion());
		activitySignUpImpl.setIsOnlineKidsMeditation(activitySignUp.isIsOnlineKidsMeditation());
		activitySignUpImpl.setIsOnlineYouthClub(activitySignUp.isIsOnlineYouthClub());
		activitySignUpImpl.setCreatedDate(activitySignUp.getCreatedDate());
		activitySignUpImpl.setUpdatedDate(activitySignUp.getUpdatedDate());

		return activitySignUpImpl;
	}

	/**
	 * Returns the ActivitySignUp with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the ActivitySignUp
	 * @return the ActivitySignUp
	 * @throws NoSuchActivitySignUpException if a ActivitySignUp with the primary key could not be found
	 */
	@Override
	public ActivitySignUp findByPrimaryKey(Serializable primaryKey)
		throws NoSuchActivitySignUpException {
		ActivitySignUp activitySignUp = fetchByPrimaryKey(primaryKey);

		if (activitySignUp == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchActivitySignUpException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return activitySignUp;
	}

	/**
	 * Returns the ActivitySignUp with the primary key or throws a {@link NoSuchActivitySignUpException} if it could not be found.
	 *
	 * @param ID the primary key of the ActivitySignUp
	 * @return the ActivitySignUp
	 * @throws NoSuchActivitySignUpException if a ActivitySignUp with the primary key could not be found
	 */
	@Override
	public ActivitySignUp findByPrimaryKey(long ID)
		throws NoSuchActivitySignUpException {
		return findByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns the ActivitySignUp with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the ActivitySignUp
	 * @return the ActivitySignUp, or <code>null</code> if a ActivitySignUp with the primary key could not be found
	 */
	@Override
	public ActivitySignUp fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
				ActivitySignUpImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		ActivitySignUp activitySignUp = (ActivitySignUp)serializable;

		if (activitySignUp == null) {
			Session session = null;

			try {
				session = openSession();

				activitySignUp = (ActivitySignUp)session.get(ActivitySignUpImpl.class,
						primaryKey);

				if (activitySignUp != null) {
					cacheResult(activitySignUp);
				}
				else {
					entityCache.putResult(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
						ActivitySignUpImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
					ActivitySignUpImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return activitySignUp;
	}

	/**
	 * Returns the ActivitySignUp with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ID the primary key of the ActivitySignUp
	 * @return the ActivitySignUp, or <code>null</code> if a ActivitySignUp with the primary key could not be found
	 */
	@Override
	public ActivitySignUp fetchByPrimaryKey(long ID) {
		return fetchByPrimaryKey((Serializable)ID);
	}

	@Override
	public Map<Serializable, ActivitySignUp> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, ActivitySignUp> map = new HashMap<Serializable, ActivitySignUp>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			ActivitySignUp activitySignUp = fetchByPrimaryKey(primaryKey);

			if (activitySignUp != null) {
				map.put(primaryKey, activitySignUp);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
					ActivitySignUpImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (ActivitySignUp)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_ACTIVITYSIGNUP_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (ActivitySignUp activitySignUp : (List<ActivitySignUp>)q.list()) {
				map.put(activitySignUp.getPrimaryKeyObj(), activitySignUp);

				cacheResult(activitySignUp);

				uncachedPrimaryKeys.remove(activitySignUp.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(ActivitySignUpModelImpl.ENTITY_CACHE_ENABLED,
					ActivitySignUpImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the ActivitySignUps.
	 *
	 * @return the ActivitySignUps
	 */
	@Override
	public List<ActivitySignUp> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the ActivitySignUps.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ActivitySignUps
	 * @param end the upper bound of the range of ActivitySignUps (not inclusive)
	 * @return the range of ActivitySignUps
	 */
	@Override
	public List<ActivitySignUp> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the ActivitySignUps.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ActivitySignUps
	 * @param end the upper bound of the range of ActivitySignUps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of ActivitySignUps
	 */
	@Override
	public List<ActivitySignUp> findAll(int start, int end,
		OrderByComparator<ActivitySignUp> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the ActivitySignUps.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of ActivitySignUps
	 * @param end the upper bound of the range of ActivitySignUps (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of ActivitySignUps
	 */
	@Override
	public List<ActivitySignUp> findAll(int start, int end,
		OrderByComparator<ActivitySignUp> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<ActivitySignUp> list = null;

		if (retrieveFromCache) {
			list = (List<ActivitySignUp>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_ACTIVITYSIGNUP);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_ACTIVITYSIGNUP;

				if (pagination) {
					sql = sql.concat(ActivitySignUpModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<ActivitySignUp>)QueryUtil.list(q,
							getDialect(), start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<ActivitySignUp>)QueryUtil.list(q,
							getDialect(), start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the ActivitySignUps from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (ActivitySignUp activitySignUp : findAll()) {
			remove(activitySignUp);
		}
	}

	/**
	 * Returns the number of ActivitySignUps.
	 *
	 * @return the number of ActivitySignUps
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_ACTIVITYSIGNUP);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return ActivitySignUpModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the ActivitySignUp persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(ActivitySignUpImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_ACTIVITYSIGNUP = "SELECT activitySignUp FROM ActivitySignUp activitySignUp";
	private static final String _SQL_SELECT_ACTIVITYSIGNUP_WHERE_PKS_IN = "SELECT activitySignUp FROM ActivitySignUp activitySignUp WHERE ID IN (";
	private static final String _SQL_COUNT_ACTIVITYSIGNUP = "SELECT COUNT(activitySignUp) FROM ActivitySignUp activitySignUp";
	private static final String _ORDER_BY_ENTITY_ALIAS = "activitySignUp.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No ActivitySignUp exists with the primary key ";
	private static final Log _log = LogFactoryUtil.getLog(ActivitySignUpPersistenceImpl.class);
}