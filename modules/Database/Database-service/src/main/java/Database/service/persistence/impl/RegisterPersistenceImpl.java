/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.service.persistence.impl;

import Database.exception.NoSuchRegisterException;

import Database.model.Register;

import Database.model.impl.RegisterImpl;
import Database.model.impl.RegisterModelImpl;

import Database.service.persistence.RegisterPersistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * The persistence implementation for the Register service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RegisterPersistence
 * @see Database.service.persistence.RegisterUtil
 * @generated
 */
@ProviderType
public class RegisterPersistenceImpl extends BasePersistenceImpl<Register>
	implements RegisterPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link RegisterUtil} to access the Register persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = RegisterImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(RegisterModelImpl.ENTITY_CACHE_ENABLED,
			RegisterModelImpl.FINDER_CACHE_ENABLED, RegisterImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(RegisterModelImpl.ENTITY_CACHE_ENABLED,
			RegisterModelImpl.FINDER_CACHE_ENABLED, RegisterImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(RegisterModelImpl.ENTITY_CACHE_ENABLED,
			RegisterModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_LASTNAME = new FinderPath(RegisterModelImpl.ENTITY_CACHE_ENABLED,
			RegisterModelImpl.FINDER_CACHE_ENABLED, RegisterImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByLastName",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_LASTNAME =
		new FinderPath(RegisterModelImpl.ENTITY_CACHE_ENABLED,
			RegisterModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByLastName",
			new String[] { String.class.getName() });

	/**
	 * Returns all the Registers where LastName LIKE &#63;.
	 *
	 * @param LastName the last name
	 * @return the matching Registers
	 */
	@Override
	public List<Register> findByLastName(String LastName) {
		return findByLastName(LastName, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
			null);
	}

	/**
	 * Returns a range of all the Registers where LastName LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param LastName the last name
	 * @param start the lower bound of the range of Registers
	 * @param end the upper bound of the range of Registers (not inclusive)
	 * @return the range of matching Registers
	 */
	@Override
	public List<Register> findByLastName(String LastName, int start, int end) {
		return findByLastName(LastName, start, end, null);
	}

	/**
	 * Returns an ordered range of all the Registers where LastName LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param LastName the last name
	 * @param start the lower bound of the range of Registers
	 * @param end the upper bound of the range of Registers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching Registers
	 */
	@Override
	public List<Register> findByLastName(String LastName, int start, int end,
		OrderByComparator<Register> orderByComparator) {
		return findByLastName(LastName, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the Registers where LastName LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param LastName the last name
	 * @param start the lower bound of the range of Registers
	 * @param end the upper bound of the range of Registers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching Registers
	 */
	@Override
	public List<Register> findByLastName(String LastName, int start, int end,
		OrderByComparator<Register> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_LASTNAME;
		finderArgs = new Object[] { LastName, start, end, orderByComparator };

		List<Register> list = null;

		if (retrieveFromCache) {
			list = (List<Register>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Register register : list) {
					if (!StringUtil.wildcardMatches(register.getLastName(),
								LastName, CharPool.UNDERLINE, CharPool.PERCENT,
								CharPool.BACK_SLASH, true)) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_REGISTER_WHERE);

			boolean bindLastName = false;

			if (LastName == null) {
				query.append(_FINDER_COLUMN_LASTNAME_LASTNAME_1);
			}
			else if (LastName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_LASTNAME_LASTNAME_3);
			}
			else {
				bindLastName = true;

				query.append(_FINDER_COLUMN_LASTNAME_LASTNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(RegisterModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindLastName) {
					qPos.add(LastName);
				}

				if (!pagination) {
					list = (List<Register>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Register>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first Register in the ordered set where LastName LIKE &#63;.
	 *
	 * @param LastName the last name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching Register
	 * @throws NoSuchRegisterException if a matching Register could not be found
	 */
	@Override
	public Register findByLastName_First(String LastName,
		OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException {
		Register register = fetchByLastName_First(LastName, orderByComparator);

		if (register != null) {
			return register;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("LastName=");
		msg.append(LastName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRegisterException(msg.toString());
	}

	/**
	 * Returns the first Register in the ordered set where LastName LIKE &#63;.
	 *
	 * @param LastName the last name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching Register, or <code>null</code> if a matching Register could not be found
	 */
	@Override
	public Register fetchByLastName_First(String LastName,
		OrderByComparator<Register> orderByComparator) {
		List<Register> list = findByLastName(LastName, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last Register in the ordered set where LastName LIKE &#63;.
	 *
	 * @param LastName the last name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching Register
	 * @throws NoSuchRegisterException if a matching Register could not be found
	 */
	@Override
	public Register findByLastName_Last(String LastName,
		OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException {
		Register register = fetchByLastName_Last(LastName, orderByComparator);

		if (register != null) {
			return register;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("LastName=");
		msg.append(LastName);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRegisterException(msg.toString());
	}

	/**
	 * Returns the last Register in the ordered set where LastName LIKE &#63;.
	 *
	 * @param LastName the last name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching Register, or <code>null</code> if a matching Register could not be found
	 */
	@Override
	public Register fetchByLastName_Last(String LastName,
		OrderByComparator<Register> orderByComparator) {
		int count = countByLastName(LastName);

		if (count == 0) {
			return null;
		}

		List<Register> list = findByLastName(LastName, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the Registers before and after the current Register in the ordered set where LastName LIKE &#63;.
	 *
	 * @param ID the primary key of the current Register
	 * @param LastName the last name
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next Register
	 * @throws NoSuchRegisterException if a Register with the primary key could not be found
	 */
	@Override
	public Register[] findByLastName_PrevAndNext(long ID, String LastName,
		OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException {
		Register register = findByPrimaryKey(ID);

		Session session = null;

		try {
			session = openSession();

			Register[] array = new RegisterImpl[3];

			array[0] = getByLastName_PrevAndNext(session, register, LastName,
					orderByComparator, true);

			array[1] = register;

			array[2] = getByLastName_PrevAndNext(session, register, LastName,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Register getByLastName_PrevAndNext(Session session,
		Register register, String LastName,
		OrderByComparator<Register> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_REGISTER_WHERE);

		boolean bindLastName = false;

		if (LastName == null) {
			query.append(_FINDER_COLUMN_LASTNAME_LASTNAME_1);
		}
		else if (LastName.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_LASTNAME_LASTNAME_3);
		}
		else {
			bindLastName = true;

			query.append(_FINDER_COLUMN_LASTNAME_LASTNAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(RegisterModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindLastName) {
			qPos.add(LastName);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(register);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Register> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the Registers where LastName LIKE &#63; from the database.
	 *
	 * @param LastName the last name
	 */
	@Override
	public void removeByLastName(String LastName) {
		for (Register register : findByLastName(LastName, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(register);
		}
	}

	/**
	 * Returns the number of Registers where LastName LIKE &#63;.
	 *
	 * @param LastName the last name
	 * @return the number of matching Registers
	 */
	@Override
	public int countByLastName(String LastName) {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_LASTNAME;

		Object[] finderArgs = new Object[] { LastName };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_REGISTER_WHERE);

			boolean bindLastName = false;

			if (LastName == null) {
				query.append(_FINDER_COLUMN_LASTNAME_LASTNAME_1);
			}
			else if (LastName.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_LASTNAME_LASTNAME_3);
			}
			else {
				bindLastName = true;

				query.append(_FINDER_COLUMN_LASTNAME_LASTNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindLastName) {
					qPos.add(LastName);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_LASTNAME_LASTNAME_1 = "register.LastName IS NULL";
	private static final String _FINDER_COLUMN_LASTNAME_LASTNAME_2 = "register.LastName LIKE ?";
	private static final String _FINDER_COLUMN_LASTNAME_LASTNAME_3 = "(register.LastName IS NULL OR register.LastName LIKE '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MOBILENUMBER =
		new FinderPath(RegisterModelImpl.ENTITY_CACHE_ENABLED,
			RegisterModelImpl.FINDER_CACHE_ENABLED, RegisterImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByMobileNumber",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_COUNT_BY_MOBILENUMBER =
		new FinderPath(RegisterModelImpl.ENTITY_CACHE_ENABLED,
			RegisterModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "countByMobileNumber",
			new String[] { String.class.getName() });

	/**
	 * Returns all the Registers where MobileNumber LIKE &#63;.
	 *
	 * @param MobileNumber the mobile number
	 * @return the matching Registers
	 */
	@Override
	public List<Register> findByMobileNumber(String MobileNumber) {
		return findByMobileNumber(MobileNumber, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the Registers where MobileNumber LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param MobileNumber the mobile number
	 * @param start the lower bound of the range of Registers
	 * @param end the upper bound of the range of Registers (not inclusive)
	 * @return the range of matching Registers
	 */
	@Override
	public List<Register> findByMobileNumber(String MobileNumber, int start,
		int end) {
		return findByMobileNumber(MobileNumber, start, end, null);
	}

	/**
	 * Returns an ordered range of all the Registers where MobileNumber LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param MobileNumber the mobile number
	 * @param start the lower bound of the range of Registers
	 * @param end the upper bound of the range of Registers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching Registers
	 */
	@Override
	public List<Register> findByMobileNumber(String MobileNumber, int start,
		int end, OrderByComparator<Register> orderByComparator) {
		return findByMobileNumber(MobileNumber, start, end, orderByComparator,
			true);
	}

	/**
	 * Returns an ordered range of all the Registers where MobileNumber LIKE &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param MobileNumber the mobile number
	 * @param start the lower bound of the range of Registers
	 * @param end the upper bound of the range of Registers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching Registers
	 */
	@Override
	public List<Register> findByMobileNumber(String MobileNumber, int start,
		int end, OrderByComparator<Register> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MOBILENUMBER;
		finderArgs = new Object[] { MobileNumber, start, end, orderByComparator };

		List<Register> list = null;

		if (retrieveFromCache) {
			list = (List<Register>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (Register register : list) {
					if (!StringUtil.wildcardMatches(
								register.getMobileNumber(), MobileNumber,
								CharPool.UNDERLINE, CharPool.PERCENT,
								CharPool.BACK_SLASH, true)) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_REGISTER_WHERE);

			boolean bindMobileNumber = false;

			if (MobileNumber == null) {
				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_1);
			}
			else if (MobileNumber.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_3);
			}
			else {
				bindMobileNumber = true;

				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(RegisterModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindMobileNumber) {
					qPos.add(MobileNumber);
				}

				if (!pagination) {
					list = (List<Register>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Register>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first Register in the ordered set where MobileNumber LIKE &#63;.
	 *
	 * @param MobileNumber the mobile number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching Register
	 * @throws NoSuchRegisterException if a matching Register could not be found
	 */
	@Override
	public Register findByMobileNumber_First(String MobileNumber,
		OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException {
		Register register = fetchByMobileNumber_First(MobileNumber,
				orderByComparator);

		if (register != null) {
			return register;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("MobileNumber=");
		msg.append(MobileNumber);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRegisterException(msg.toString());
	}

	/**
	 * Returns the first Register in the ordered set where MobileNumber LIKE &#63;.
	 *
	 * @param MobileNumber the mobile number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching Register, or <code>null</code> if a matching Register could not be found
	 */
	@Override
	public Register fetchByMobileNumber_First(String MobileNumber,
		OrderByComparator<Register> orderByComparator) {
		List<Register> list = findByMobileNumber(MobileNumber, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last Register in the ordered set where MobileNumber LIKE &#63;.
	 *
	 * @param MobileNumber the mobile number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching Register
	 * @throws NoSuchRegisterException if a matching Register could not be found
	 */
	@Override
	public Register findByMobileNumber_Last(String MobileNumber,
		OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException {
		Register register = fetchByMobileNumber_Last(MobileNumber,
				orderByComparator);

		if (register != null) {
			return register;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("MobileNumber=");
		msg.append(MobileNumber);

		msg.append(StringPool.CLOSE_CURLY_BRACE);

		throw new NoSuchRegisterException(msg.toString());
	}

	/**
	 * Returns the last Register in the ordered set where MobileNumber LIKE &#63;.
	 *
	 * @param MobileNumber the mobile number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching Register, or <code>null</code> if a matching Register could not be found
	 */
	@Override
	public Register fetchByMobileNumber_Last(String MobileNumber,
		OrderByComparator<Register> orderByComparator) {
		int count = countByMobileNumber(MobileNumber);

		if (count == 0) {
			return null;
		}

		List<Register> list = findByMobileNumber(MobileNumber, count - 1,
				count, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the Registers before and after the current Register in the ordered set where MobileNumber LIKE &#63;.
	 *
	 * @param ID the primary key of the current Register
	 * @param MobileNumber the mobile number
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next Register
	 * @throws NoSuchRegisterException if a Register with the primary key could not be found
	 */
	@Override
	public Register[] findByMobileNumber_PrevAndNext(long ID,
		String MobileNumber, OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException {
		Register register = findByPrimaryKey(ID);

		Session session = null;

		try {
			session = openSession();

			Register[] array = new RegisterImpl[3];

			array[0] = getByMobileNumber_PrevAndNext(session, register,
					MobileNumber, orderByComparator, true);

			array[1] = register;

			array[2] = getByMobileNumber_PrevAndNext(session, register,
					MobileNumber, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected Register getByMobileNumber_PrevAndNext(Session session,
		Register register, String MobileNumber,
		OrderByComparator<Register> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_REGISTER_WHERE);

		boolean bindMobileNumber = false;

		if (MobileNumber == null) {
			query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_1);
		}
		else if (MobileNumber.equals(StringPool.BLANK)) {
			query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_3);
		}
		else {
			bindMobileNumber = true;

			query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(RegisterModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindMobileNumber) {
			qPos.add(MobileNumber);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(register);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<Register> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the Registers where MobileNumber LIKE &#63; from the database.
	 *
	 * @param MobileNumber the mobile number
	 */
	@Override
	public void removeByMobileNumber(String MobileNumber) {
		for (Register register : findByMobileNumber(MobileNumber,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(register);
		}
	}

	/**
	 * Returns the number of Registers where MobileNumber LIKE &#63;.
	 *
	 * @param MobileNumber the mobile number
	 * @return the number of matching Registers
	 */
	@Override
	public int countByMobileNumber(String MobileNumber) {
		FinderPath finderPath = FINDER_PATH_WITH_PAGINATION_COUNT_BY_MOBILENUMBER;

		Object[] finderArgs = new Object[] { MobileNumber };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_REGISTER_WHERE);

			boolean bindMobileNumber = false;

			if (MobileNumber == null) {
				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_1);
			}
			else if (MobileNumber.equals(StringPool.BLANK)) {
				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_3);
			}
			else {
				bindMobileNumber = true;

				query.append(_FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindMobileNumber) {
					qPos.add(MobileNumber);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_1 = "register.MobileNumber IS NULL";
	private static final String _FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_2 = "register.MobileNumber LIKE ?";
	private static final String _FINDER_COLUMN_MOBILENUMBER_MOBILENUMBER_3 = "(register.MobileNumber IS NULL OR register.MobileNumber LIKE '')";

	public RegisterPersistenceImpl() {
		setModelClass(Register.class);
	}

	/**
	 * Caches the Register in the entity cache if it is enabled.
	 *
	 * @param register the Register
	 */
	@Override
	public void cacheResult(Register register) {
		entityCache.putResult(RegisterModelImpl.ENTITY_CACHE_ENABLED,
			RegisterImpl.class, register.getPrimaryKey(), register);

		register.resetOriginalValues();
	}

	/**
	 * Caches the Registers in the entity cache if it is enabled.
	 *
	 * @param registers the Registers
	 */
	@Override
	public void cacheResult(List<Register> registers) {
		for (Register register : registers) {
			if (entityCache.getResult(RegisterModelImpl.ENTITY_CACHE_ENABLED,
						RegisterImpl.class, register.getPrimaryKey()) == null) {
				cacheResult(register);
			}
			else {
				register.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all Registers.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(RegisterImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the Register.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(Register register) {
		entityCache.removeResult(RegisterModelImpl.ENTITY_CACHE_ENABLED,
			RegisterImpl.class, register.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<Register> registers) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (Register register : registers) {
			entityCache.removeResult(RegisterModelImpl.ENTITY_CACHE_ENABLED,
				RegisterImpl.class, register.getPrimaryKey());
		}
	}

	/**
	 * Creates a new Register with the primary key. Does not add the Register to the database.
	 *
	 * @param ID the primary key for the new Register
	 * @return the new Register
	 */
	@Override
	public Register create(long ID) {
		Register register = new RegisterImpl();

		register.setNew(true);
		register.setPrimaryKey(ID);

		return register;
	}

	/**
	 * Removes the Register with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param ID the primary key of the Register
	 * @return the Register that was removed
	 * @throws NoSuchRegisterException if a Register with the primary key could not be found
	 */
	@Override
	public Register remove(long ID) throws NoSuchRegisterException {
		return remove((Serializable)ID);
	}

	/**
	 * Removes the Register with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the Register
	 * @return the Register that was removed
	 * @throws NoSuchRegisterException if a Register with the primary key could not be found
	 */
	@Override
	public Register remove(Serializable primaryKey)
		throws NoSuchRegisterException {
		Session session = null;

		try {
			session = openSession();

			Register register = (Register)session.get(RegisterImpl.class,
					primaryKey);

			if (register == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchRegisterException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(register);
		}
		catch (NoSuchRegisterException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected Register removeImpl(Register register) {
		register = toUnwrappedModel(register);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(register)) {
				register = (Register)session.get(RegisterImpl.class,
						register.getPrimaryKeyObj());
			}

			if (register != null) {
				session.delete(register);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (register != null) {
			clearCache(register);
		}

		return register;
	}

	@Override
	public Register updateImpl(Register register) {
		register = toUnwrappedModel(register);

		boolean isNew = register.isNew();

		Session session = null;

		try {
			session = openSession();

			if (register.isNew()) {
				session.save(register);

				register.setNew(false);
			}
			else {
				register = (Register)session.merge(register);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!RegisterModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		entityCache.putResult(RegisterModelImpl.ENTITY_CACHE_ENABLED,
			RegisterImpl.class, register.getPrimaryKey(), register, false);

		register.resetOriginalValues();

		return register;
	}

	protected Register toUnwrappedModel(Register register) {
		if (register instanceof RegisterImpl) {
			return register;
		}

		RegisterImpl registerImpl = new RegisterImpl();

		registerImpl.setNew(register.isNew());
		registerImpl.setPrimaryKey(register.getPrimaryKey());

		registerImpl.setID(register.getID());
		registerImpl.setFirstName(register.getFirstName());
		registerImpl.setLastName(register.getLastName());
		registerImpl.setIsMale(register.isIsMale());
		registerImpl.setAddress(register.getAddress());
		registerImpl.setCity(register.getCity());
		registerImpl.setState(register.getState());
		registerImpl.setZip(register.getZip());
		registerImpl.setPhoneNumber(register.getPhoneNumber());
		registerImpl.setMobileNumber(register.getMobileNumber());
		registerImpl.setEmail(register.getEmail());
		registerImpl.setAdditionalMember1(register.getAdditionalMember1());
		registerImpl.setAdditionalMember2(register.getAdditionalMember2());
		registerImpl.setAdditionalMember3(register.getAdditionalMember3());
		registerImpl.setIsEmailCampain(register.isIsEmailCampain());
		registerImpl.setIsTemple(register.isIsTemple());
		registerImpl.setIsOnlineNewsWebsite(register.isIsOnlineNewsWebsite());
		registerImpl.setIsRadio(register.isIsRadio());
		registerImpl.setIsYouTube(register.isIsYouTube());
		registerImpl.setIsFacebook(register.isIsFacebook());
		registerImpl.setIsMeetup(register.isIsMeetup());
		registerImpl.setIsOnlineEventCalendar(register.isIsOnlineEventCalendar());
		registerImpl.setIsFlyer(register.isIsFlyer());
		registerImpl.setIsNewspaper(register.isIsNewspaper());
		registerImpl.setIsWordOfMouth(register.isIsWordOfMouth());
		registerImpl.setOther(register.getOther());
		registerImpl.setIsinterestedInVolunteringActivity(register.isIsinterestedInVolunteringActivity());
		registerImpl.setIsinterestedInSatsangActivity(register.isIsinterestedInSatsangActivity());
		registerImpl.setIsinterestedInChildrenActivity(register.isIsinterestedInChildrenActivity());
		registerImpl.setCreatedDate(register.getCreatedDate());
		registerImpl.setUpdatedDate(register.getUpdatedDate());
		registerImpl.setAdditionalMember1Age(register.getAdditionalMember1Age());
		registerImpl.setAdditionalMember2Age(register.getAdditionalMember2Age());
		registerImpl.setAdditionalMember3Age(register.getAdditionalMember3Age());
		registerImpl.setSpouseFirstName(register.getSpouseFirstName());
		registerImpl.setSpouseLastName(register.getSpouseLastName());
		registerImpl.setSpousePhoneNumber(register.getSpousePhoneNumber());
		registerImpl.setMembershipPlan(register.getMembershipPlan());
		registerImpl.setPaymentMethod(register.getPaymentMethod());
		registerImpl.setStateCenter(register.getStateCenter());
		registerImpl.setProgramName(register.getProgramName());

		return registerImpl;
	}

	/**
	 * Returns the Register with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the Register
	 * @return the Register
	 * @throws NoSuchRegisterException if a Register with the primary key could not be found
	 */
	@Override
	public Register findByPrimaryKey(Serializable primaryKey)
		throws NoSuchRegisterException {
		Register register = fetchByPrimaryKey(primaryKey);

		if (register == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchRegisterException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return register;
	}

	/**
	 * Returns the Register with the primary key or throws a {@link NoSuchRegisterException} if it could not be found.
	 *
	 * @param ID the primary key of the Register
	 * @return the Register
	 * @throws NoSuchRegisterException if a Register with the primary key could not be found
	 */
	@Override
	public Register findByPrimaryKey(long ID) throws NoSuchRegisterException {
		return findByPrimaryKey((Serializable)ID);
	}

	/**
	 * Returns the Register with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the Register
	 * @return the Register, or <code>null</code> if a Register with the primary key could not be found
	 */
	@Override
	public Register fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(RegisterModelImpl.ENTITY_CACHE_ENABLED,
				RegisterImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		Register register = (Register)serializable;

		if (register == null) {
			Session session = null;

			try {
				session = openSession();

				register = (Register)session.get(RegisterImpl.class, primaryKey);

				if (register != null) {
					cacheResult(register);
				}
				else {
					entityCache.putResult(RegisterModelImpl.ENTITY_CACHE_ENABLED,
						RegisterImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(RegisterModelImpl.ENTITY_CACHE_ENABLED,
					RegisterImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return register;
	}

	/**
	 * Returns the Register with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param ID the primary key of the Register
	 * @return the Register, or <code>null</code> if a Register with the primary key could not be found
	 */
	@Override
	public Register fetchByPrimaryKey(long ID) {
		return fetchByPrimaryKey((Serializable)ID);
	}

	@Override
	public Map<Serializable, Register> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, Register> map = new HashMap<Serializable, Register>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			Register register = fetchByPrimaryKey(primaryKey);

			if (register != null) {
				map.put(primaryKey, register);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(RegisterModelImpl.ENTITY_CACHE_ENABLED,
					RegisterImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (Register)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_REGISTER_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((long)primaryKey);

			query.append(StringPool.COMMA);
		}

		query.setIndex(query.index() - 1);

		query.append(StringPool.CLOSE_PARENTHESIS);

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (Register register : (List<Register>)q.list()) {
				map.put(register.getPrimaryKeyObj(), register);

				cacheResult(register);

				uncachedPrimaryKeys.remove(register.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(RegisterModelImpl.ENTITY_CACHE_ENABLED,
					RegisterImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the Registers.
	 *
	 * @return the Registers
	 */
	@Override
	public List<Register> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the Registers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of Registers
	 * @param end the upper bound of the range of Registers (not inclusive)
	 * @return the range of Registers
	 */
	@Override
	public List<Register> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the Registers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of Registers
	 * @param end the upper bound of the range of Registers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of Registers
	 */
	@Override
	public List<Register> findAll(int start, int end,
		OrderByComparator<Register> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the Registers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of Registers
	 * @param end the upper bound of the range of Registers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of Registers
	 */
	@Override
	public List<Register> findAll(int start, int end,
		OrderByComparator<Register> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<Register> list = null;

		if (retrieveFromCache) {
			list = (List<Register>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_REGISTER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_REGISTER;

				if (pagination) {
					sql = sql.concat(RegisterModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<Register>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<Register>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the Registers from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (Register register : findAll()) {
			remove(register);
		}
	}

	/**
	 * Returns the number of Registers.
	 *
	 * @return the number of Registers
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_REGISTER);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return RegisterModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the Register persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(RegisterImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_REGISTER = "SELECT register FROM Register register";
	private static final String _SQL_SELECT_REGISTER_WHERE_PKS_IN = "SELECT register FROM Register register WHERE ID IN (";
	private static final String _SQL_SELECT_REGISTER_WHERE = "SELECT register FROM Register register WHERE ";
	private static final String _SQL_COUNT_REGISTER = "SELECT COUNT(register) FROM Register register";
	private static final String _SQL_COUNT_REGISTER_WHERE = "SELECT COUNT(register) FROM Register register WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "register.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Register exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Register exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(RegisterPersistenceImpl.class);
}