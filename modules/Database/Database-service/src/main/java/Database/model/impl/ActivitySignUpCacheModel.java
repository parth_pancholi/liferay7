/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.model.impl;

import Database.model.ActivitySignUp;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ActivitySignUp in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see ActivitySignUp
 * @generated
 */
@ProviderType
public class ActivitySignUpCacheModel implements CacheModel<ActivitySignUp>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ActivitySignUpCacheModel)) {
			return false;
		}

		ActivitySignUpCacheModel activitySignUpCacheModel = (ActivitySignUpCacheModel)obj;

		if (ID == activitySignUpCacheModel.ID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(29);

		sb.append("{ID=");
		sb.append(ID);
		sb.append(", FirstName=");
		sb.append(FirstName);
		sb.append(", LastName=");
		sb.append(LastName);
		sb.append(", MobileNumber=");
		sb.append(MobileNumber);
		sb.append(", IsSatsangBiWeeklyMeeting=");
		sb.append(IsSatsangBiWeeklyMeeting);
		sb.append(", IsBalmukund=");
		sb.append(IsBalmukund);
		sb.append(", IsBhagvatGitaRecitation=");
		sb.append(IsBhagvatGitaRecitation);
		sb.append(", IsYoga=");
		sb.append(IsYoga);
		sb.append(", Is7MindSet=");
		sb.append(Is7MindSet);
		sb.append(", IsOnlineBhagvatGitaDiscussion=");
		sb.append(IsOnlineBhagvatGitaDiscussion);
		sb.append(", IsOnlineKidsMeditation=");
		sb.append(IsOnlineKidsMeditation);
		sb.append(", IsOnlineYouthClub=");
		sb.append(IsOnlineYouthClub);
		sb.append(", CreatedDate=");
		sb.append(CreatedDate);
		sb.append(", UpdatedDate=");
		sb.append(UpdatedDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ActivitySignUp toEntityModel() {
		ActivitySignUpImpl activitySignUpImpl = new ActivitySignUpImpl();

		activitySignUpImpl.setID(ID);

		if (FirstName == null) {
			activitySignUpImpl.setFirstName(StringPool.BLANK);
		}
		else {
			activitySignUpImpl.setFirstName(FirstName);
		}

		if (LastName == null) {
			activitySignUpImpl.setLastName(StringPool.BLANK);
		}
		else {
			activitySignUpImpl.setLastName(LastName);
		}

		if (MobileNumber == null) {
			activitySignUpImpl.setMobileNumber(StringPool.BLANK);
		}
		else {
			activitySignUpImpl.setMobileNumber(MobileNumber);
		}

		activitySignUpImpl.setIsSatsangBiWeeklyMeeting(IsSatsangBiWeeklyMeeting);
		activitySignUpImpl.setIsBalmukund(IsBalmukund);
		activitySignUpImpl.setIsBhagvatGitaRecitation(IsBhagvatGitaRecitation);
		activitySignUpImpl.setIsYoga(IsYoga);
		activitySignUpImpl.setIs7MindSet(Is7MindSet);
		activitySignUpImpl.setIsOnlineBhagvatGitaDiscussion(IsOnlineBhagvatGitaDiscussion);
		activitySignUpImpl.setIsOnlineKidsMeditation(IsOnlineKidsMeditation);
		activitySignUpImpl.setIsOnlineYouthClub(IsOnlineYouthClub);

		if (CreatedDate == Long.MIN_VALUE) {
			activitySignUpImpl.setCreatedDate(null);
		}
		else {
			activitySignUpImpl.setCreatedDate(new Date(CreatedDate));
		}

		if (UpdatedDate == Long.MIN_VALUE) {
			activitySignUpImpl.setUpdatedDate(null);
		}
		else {
			activitySignUpImpl.setUpdatedDate(new Date(UpdatedDate));
		}

		activitySignUpImpl.resetOriginalValues();

		return activitySignUpImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ID = objectInput.readLong();
		FirstName = objectInput.readUTF();
		LastName = objectInput.readUTF();
		MobileNumber = objectInput.readUTF();

		IsSatsangBiWeeklyMeeting = objectInput.readBoolean();

		IsBalmukund = objectInput.readBoolean();

		IsBhagvatGitaRecitation = objectInput.readBoolean();

		IsYoga = objectInput.readBoolean();

		Is7MindSet = objectInput.readBoolean();

		IsOnlineBhagvatGitaDiscussion = objectInput.readBoolean();

		IsOnlineKidsMeditation = objectInput.readBoolean();

		IsOnlineYouthClub = objectInput.readBoolean();
		CreatedDate = objectInput.readLong();
		UpdatedDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ID);

		if (FirstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(FirstName);
		}

		if (LastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(LastName);
		}

		if (MobileNumber == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(MobileNumber);
		}

		objectOutput.writeBoolean(IsSatsangBiWeeklyMeeting);

		objectOutput.writeBoolean(IsBalmukund);

		objectOutput.writeBoolean(IsBhagvatGitaRecitation);

		objectOutput.writeBoolean(IsYoga);

		objectOutput.writeBoolean(Is7MindSet);

		objectOutput.writeBoolean(IsOnlineBhagvatGitaDiscussion);

		objectOutput.writeBoolean(IsOnlineKidsMeditation);

		objectOutput.writeBoolean(IsOnlineYouthClub);
		objectOutput.writeLong(CreatedDate);
		objectOutput.writeLong(UpdatedDate);
	}

	public long ID;
	public String FirstName;
	public String LastName;
	public String MobileNumber;
	public boolean IsSatsangBiWeeklyMeeting;
	public boolean IsBalmukund;
	public boolean IsBhagvatGitaRecitation;
	public boolean IsYoga;
	public boolean Is7MindSet;
	public boolean IsOnlineBhagvatGitaDiscussion;
	public boolean IsOnlineKidsMeditation;
	public boolean IsOnlineYouthClub;
	public long CreatedDate;
	public long UpdatedDate;
}