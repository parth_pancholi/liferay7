/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.model.impl;

import Database.model.Register;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Register in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Register
 * @generated
 */
@ProviderType
public class RegisterCacheModel implements CacheModel<Register>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RegisterCacheModel)) {
			return false;
		}

		RegisterCacheModel registerCacheModel = (RegisterCacheModel)obj;

		if (ID == registerCacheModel.ID) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, ID);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(83);

		sb.append("{ID=");
		sb.append(ID);
		sb.append(", FirstName=");
		sb.append(FirstName);
		sb.append(", LastName=");
		sb.append(LastName);
		sb.append(", IsMale=");
		sb.append(IsMale);
		sb.append(", Address=");
		sb.append(Address);
		sb.append(", City=");
		sb.append(City);
		sb.append(", State=");
		sb.append(State);
		sb.append(", Zip=");
		sb.append(Zip);
		sb.append(", PhoneNumber=");
		sb.append(PhoneNumber);
		sb.append(", MobileNumber=");
		sb.append(MobileNumber);
		sb.append(", Email=");
		sb.append(Email);
		sb.append(", AdditionalMember1=");
		sb.append(AdditionalMember1);
		sb.append(", AdditionalMember2=");
		sb.append(AdditionalMember2);
		sb.append(", AdditionalMember3=");
		sb.append(AdditionalMember3);
		sb.append(", IsEmailCampain=");
		sb.append(IsEmailCampain);
		sb.append(", IsTemple=");
		sb.append(IsTemple);
		sb.append(", IsOnlineNewsWebsite=");
		sb.append(IsOnlineNewsWebsite);
		sb.append(", IsRadio=");
		sb.append(IsRadio);
		sb.append(", IsYouTube=");
		sb.append(IsYouTube);
		sb.append(", IsFacebook=");
		sb.append(IsFacebook);
		sb.append(", IsMeetup=");
		sb.append(IsMeetup);
		sb.append(", IsOnlineEventCalendar=");
		sb.append(IsOnlineEventCalendar);
		sb.append(", IsFlyer=");
		sb.append(IsFlyer);
		sb.append(", IsNewspaper=");
		sb.append(IsNewspaper);
		sb.append(", IsWordOfMouth=");
		sb.append(IsWordOfMouth);
		sb.append(", Other=");
		sb.append(Other);
		sb.append(", IsinterestedInVolunteringActivity=");
		sb.append(IsinterestedInVolunteringActivity);
		sb.append(", IsinterestedInSatsangActivity=");
		sb.append(IsinterestedInSatsangActivity);
		sb.append(", IsinterestedInChildrenActivity=");
		sb.append(IsinterestedInChildrenActivity);
		sb.append(", CreatedDate=");
		sb.append(CreatedDate);
		sb.append(", UpdatedDate=");
		sb.append(UpdatedDate);
		sb.append(", AdditionalMember1Age=");
		sb.append(AdditionalMember1Age);
		sb.append(", AdditionalMember2Age=");
		sb.append(AdditionalMember2Age);
		sb.append(", AdditionalMember3Age=");
		sb.append(AdditionalMember3Age);
		sb.append(", SpouseFirstName=");
		sb.append(SpouseFirstName);
		sb.append(", SpouseLastName=");
		sb.append(SpouseLastName);
		sb.append(", SpousePhoneNumber=");
		sb.append(SpousePhoneNumber);
		sb.append(", MembershipPlan=");
		sb.append(MembershipPlan);
		sb.append(", PaymentMethod=");
		sb.append(PaymentMethod);
		sb.append(", StateCenter=");
		sb.append(StateCenter);
		sb.append(", ProgramName=");
		sb.append(ProgramName);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Register toEntityModel() {
		RegisterImpl registerImpl = new RegisterImpl();

		registerImpl.setID(ID);

		if (FirstName == null) {
			registerImpl.setFirstName(StringPool.BLANK);
		}
		else {
			registerImpl.setFirstName(FirstName);
		}

		if (LastName == null) {
			registerImpl.setLastName(StringPool.BLANK);
		}
		else {
			registerImpl.setLastName(LastName);
		}

		registerImpl.setIsMale(IsMale);

		if (Address == null) {
			registerImpl.setAddress(StringPool.BLANK);
		}
		else {
			registerImpl.setAddress(Address);
		}

		if (City == null) {
			registerImpl.setCity(StringPool.BLANK);
		}
		else {
			registerImpl.setCity(City);
		}

		if (State == null) {
			registerImpl.setState(StringPool.BLANK);
		}
		else {
			registerImpl.setState(State);
		}

		if (Zip == null) {
			registerImpl.setZip(StringPool.BLANK);
		}
		else {
			registerImpl.setZip(Zip);
		}

		if (PhoneNumber == null) {
			registerImpl.setPhoneNumber(StringPool.BLANK);
		}
		else {
			registerImpl.setPhoneNumber(PhoneNumber);
		}

		if (MobileNumber == null) {
			registerImpl.setMobileNumber(StringPool.BLANK);
		}
		else {
			registerImpl.setMobileNumber(MobileNumber);
		}

		if (Email == null) {
			registerImpl.setEmail(StringPool.BLANK);
		}
		else {
			registerImpl.setEmail(Email);
		}

		if (AdditionalMember1 == null) {
			registerImpl.setAdditionalMember1(StringPool.BLANK);
		}
		else {
			registerImpl.setAdditionalMember1(AdditionalMember1);
		}

		if (AdditionalMember2 == null) {
			registerImpl.setAdditionalMember2(StringPool.BLANK);
		}
		else {
			registerImpl.setAdditionalMember2(AdditionalMember2);
		}

		if (AdditionalMember3 == null) {
			registerImpl.setAdditionalMember3(StringPool.BLANK);
		}
		else {
			registerImpl.setAdditionalMember3(AdditionalMember3);
		}

		registerImpl.setIsEmailCampain(IsEmailCampain);
		registerImpl.setIsTemple(IsTemple);
		registerImpl.setIsOnlineNewsWebsite(IsOnlineNewsWebsite);
		registerImpl.setIsRadio(IsRadio);
		registerImpl.setIsYouTube(IsYouTube);
		registerImpl.setIsFacebook(IsFacebook);
		registerImpl.setIsMeetup(IsMeetup);
		registerImpl.setIsOnlineEventCalendar(IsOnlineEventCalendar);
		registerImpl.setIsFlyer(IsFlyer);
		registerImpl.setIsNewspaper(IsNewspaper);
		registerImpl.setIsWordOfMouth(IsWordOfMouth);

		if (Other == null) {
			registerImpl.setOther(StringPool.BLANK);
		}
		else {
			registerImpl.setOther(Other);
		}

		registerImpl.setIsinterestedInVolunteringActivity(IsinterestedInVolunteringActivity);
		registerImpl.setIsinterestedInSatsangActivity(IsinterestedInSatsangActivity);
		registerImpl.setIsinterestedInChildrenActivity(IsinterestedInChildrenActivity);

		if (CreatedDate == Long.MIN_VALUE) {
			registerImpl.setCreatedDate(null);
		}
		else {
			registerImpl.setCreatedDate(new Date(CreatedDate));
		}

		if (UpdatedDate == Long.MIN_VALUE) {
			registerImpl.setUpdatedDate(null);
		}
		else {
			registerImpl.setUpdatedDate(new Date(UpdatedDate));
		}

		if (AdditionalMember1Age == null) {
			registerImpl.setAdditionalMember1Age(StringPool.BLANK);
		}
		else {
			registerImpl.setAdditionalMember1Age(AdditionalMember1Age);
		}

		if (AdditionalMember2Age == null) {
			registerImpl.setAdditionalMember2Age(StringPool.BLANK);
		}
		else {
			registerImpl.setAdditionalMember2Age(AdditionalMember2Age);
		}

		if (AdditionalMember3Age == null) {
			registerImpl.setAdditionalMember3Age(StringPool.BLANK);
		}
		else {
			registerImpl.setAdditionalMember3Age(AdditionalMember3Age);
		}

		if (SpouseFirstName == null) {
			registerImpl.setSpouseFirstName(StringPool.BLANK);
		}
		else {
			registerImpl.setSpouseFirstName(SpouseFirstName);
		}

		if (SpouseLastName == null) {
			registerImpl.setSpouseLastName(StringPool.BLANK);
		}
		else {
			registerImpl.setSpouseLastName(SpouseLastName);
		}

		if (SpousePhoneNumber == null) {
			registerImpl.setSpousePhoneNumber(StringPool.BLANK);
		}
		else {
			registerImpl.setSpousePhoneNumber(SpousePhoneNumber);
		}

		if (MembershipPlan == null) {
			registerImpl.setMembershipPlan(StringPool.BLANK);
		}
		else {
			registerImpl.setMembershipPlan(MembershipPlan);
		}

		if (PaymentMethod == null) {
			registerImpl.setPaymentMethod(StringPool.BLANK);
		}
		else {
			registerImpl.setPaymentMethod(PaymentMethod);
		}

		if (StateCenter == null) {
			registerImpl.setStateCenter(StringPool.BLANK);
		}
		else {
			registerImpl.setStateCenter(StateCenter);
		}

		if (ProgramName == null) {
			registerImpl.setProgramName(StringPool.BLANK);
		}
		else {
			registerImpl.setProgramName(ProgramName);
		}

		registerImpl.resetOriginalValues();

		return registerImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		ID = objectInput.readLong();
		FirstName = objectInput.readUTF();
		LastName = objectInput.readUTF();

		IsMale = objectInput.readBoolean();
		Address = objectInput.readUTF();
		City = objectInput.readUTF();
		State = objectInput.readUTF();
		Zip = objectInput.readUTF();
		PhoneNumber = objectInput.readUTF();
		MobileNumber = objectInput.readUTF();
		Email = objectInput.readUTF();
		AdditionalMember1 = objectInput.readUTF();
		AdditionalMember2 = objectInput.readUTF();
		AdditionalMember3 = objectInput.readUTF();

		IsEmailCampain = objectInput.readBoolean();

		IsTemple = objectInput.readBoolean();

		IsOnlineNewsWebsite = objectInput.readBoolean();

		IsRadio = objectInput.readBoolean();

		IsYouTube = objectInput.readBoolean();

		IsFacebook = objectInput.readBoolean();

		IsMeetup = objectInput.readBoolean();

		IsOnlineEventCalendar = objectInput.readBoolean();

		IsFlyer = objectInput.readBoolean();

		IsNewspaper = objectInput.readBoolean();

		IsWordOfMouth = objectInput.readBoolean();
		Other = objectInput.readUTF();

		IsinterestedInVolunteringActivity = objectInput.readBoolean();

		IsinterestedInSatsangActivity = objectInput.readBoolean();

		IsinterestedInChildrenActivity = objectInput.readBoolean();
		CreatedDate = objectInput.readLong();
		UpdatedDate = objectInput.readLong();
		AdditionalMember1Age = objectInput.readUTF();
		AdditionalMember2Age = objectInput.readUTF();
		AdditionalMember3Age = objectInput.readUTF();
		SpouseFirstName = objectInput.readUTF();
		SpouseLastName = objectInput.readUTF();
		SpousePhoneNumber = objectInput.readUTF();
		MembershipPlan = objectInput.readUTF();
		PaymentMethod = objectInput.readUTF();
		StateCenter = objectInput.readUTF();
		ProgramName = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(ID);

		if (FirstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(FirstName);
		}

		if (LastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(LastName);
		}

		objectOutput.writeBoolean(IsMale);

		if (Address == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(Address);
		}

		if (City == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(City);
		}

		if (State == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(State);
		}

		if (Zip == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(Zip);
		}

		if (PhoneNumber == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(PhoneNumber);
		}

		if (MobileNumber == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(MobileNumber);
		}

		if (Email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(Email);
		}

		if (AdditionalMember1 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(AdditionalMember1);
		}

		if (AdditionalMember2 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(AdditionalMember2);
		}

		if (AdditionalMember3 == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(AdditionalMember3);
		}

		objectOutput.writeBoolean(IsEmailCampain);

		objectOutput.writeBoolean(IsTemple);

		objectOutput.writeBoolean(IsOnlineNewsWebsite);

		objectOutput.writeBoolean(IsRadio);

		objectOutput.writeBoolean(IsYouTube);

		objectOutput.writeBoolean(IsFacebook);

		objectOutput.writeBoolean(IsMeetup);

		objectOutput.writeBoolean(IsOnlineEventCalendar);

		objectOutput.writeBoolean(IsFlyer);

		objectOutput.writeBoolean(IsNewspaper);

		objectOutput.writeBoolean(IsWordOfMouth);

		if (Other == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(Other);
		}

		objectOutput.writeBoolean(IsinterestedInVolunteringActivity);

		objectOutput.writeBoolean(IsinterestedInSatsangActivity);

		objectOutput.writeBoolean(IsinterestedInChildrenActivity);
		objectOutput.writeLong(CreatedDate);
		objectOutput.writeLong(UpdatedDate);

		if (AdditionalMember1Age == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(AdditionalMember1Age);
		}

		if (AdditionalMember2Age == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(AdditionalMember2Age);
		}

		if (AdditionalMember3Age == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(AdditionalMember3Age);
		}

		if (SpouseFirstName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(SpouseFirstName);
		}

		if (SpouseLastName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(SpouseLastName);
		}

		if (SpousePhoneNumber == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(SpousePhoneNumber);
		}

		if (MembershipPlan == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(MembershipPlan);
		}

		if (PaymentMethod == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(PaymentMethod);
		}

		if (StateCenter == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(StateCenter);
		}

		if (ProgramName == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(ProgramName);
		}
	}

	public long ID;
	public String FirstName;
	public String LastName;
	public boolean IsMale;
	public String Address;
	public String City;
	public String State;
	public String Zip;
	public String PhoneNumber;
	public String MobileNumber;
	public String Email;
	public String AdditionalMember1;
	public String AdditionalMember2;
	public String AdditionalMember3;
	public boolean IsEmailCampain;
	public boolean IsTemple;
	public boolean IsOnlineNewsWebsite;
	public boolean IsRadio;
	public boolean IsYouTube;
	public boolean IsFacebook;
	public boolean IsMeetup;
	public boolean IsOnlineEventCalendar;
	public boolean IsFlyer;
	public boolean IsNewspaper;
	public boolean IsWordOfMouth;
	public String Other;
	public boolean IsinterestedInVolunteringActivity;
	public boolean IsinterestedInSatsangActivity;
	public boolean IsinterestedInChildrenActivity;
	public long CreatedDate;
	public long UpdatedDate;
	public String AdditionalMember1Age;
	public String AdditionalMember2Age;
	public String AdditionalMember3Age;
	public String SpouseFirstName;
	public String SpouseLastName;
	public String SpousePhoneNumber;
	public String MembershipPlan;
	public String PaymentMethod;
	public String StateCenter;
	public String ProgramName;
}