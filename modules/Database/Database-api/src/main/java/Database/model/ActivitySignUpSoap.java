/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class ActivitySignUpSoap implements Serializable {
	public static ActivitySignUpSoap toSoapModel(ActivitySignUp model) {
		ActivitySignUpSoap soapModel = new ActivitySignUpSoap();

		soapModel.setID(model.getID());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setLastName(model.getLastName());
		soapModel.setMobileNumber(model.getMobileNumber());
		soapModel.setIsSatsangBiWeeklyMeeting(model.getIsSatsangBiWeeklyMeeting());
		soapModel.setIsBalmukund(model.getIsBalmukund());
		soapModel.setIsBhagvatGitaRecitation(model.getIsBhagvatGitaRecitation());
		soapModel.setIsYoga(model.getIsYoga());
		soapModel.setIs7MindSet(model.getIs7MindSet());
		soapModel.setIsOnlineBhagvatGitaDiscussion(model.getIsOnlineBhagvatGitaDiscussion());
		soapModel.setIsOnlineKidsMeditation(model.getIsOnlineKidsMeditation());
		soapModel.setIsOnlineYouthClub(model.getIsOnlineYouthClub());
		soapModel.setCreatedDate(model.getCreatedDate());
		soapModel.setUpdatedDate(model.getUpdatedDate());

		return soapModel;
	}

	public static ActivitySignUpSoap[] toSoapModels(ActivitySignUp[] models) {
		ActivitySignUpSoap[] soapModels = new ActivitySignUpSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ActivitySignUpSoap[][] toSoapModels(ActivitySignUp[][] models) {
		ActivitySignUpSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ActivitySignUpSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ActivitySignUpSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ActivitySignUpSoap[] toSoapModels(List<ActivitySignUp> models) {
		List<ActivitySignUpSoap> soapModels = new ArrayList<ActivitySignUpSoap>(models.size());

		for (ActivitySignUp model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ActivitySignUpSoap[soapModels.size()]);
	}

	public ActivitySignUpSoap() {
	}

	public long getPrimaryKey() {
		return _ID;
	}

	public void setPrimaryKey(long pk) {
		setID(pk);
	}

	public long getID() {
		return _ID;
	}

	public void setID(long ID) {
		_ID = ID;
	}

	public String getFirstName() {
		return _FirstName;
	}

	public void setFirstName(String FirstName) {
		_FirstName = FirstName;
	}

	public String getLastName() {
		return _LastName;
	}

	public void setLastName(String LastName) {
		_LastName = LastName;
	}

	public String getMobileNumber() {
		return _MobileNumber;
	}

	public void setMobileNumber(String MobileNumber) {
		_MobileNumber = MobileNumber;
	}

	public boolean getIsSatsangBiWeeklyMeeting() {
		return _IsSatsangBiWeeklyMeeting;
	}

	public boolean isIsSatsangBiWeeklyMeeting() {
		return _IsSatsangBiWeeklyMeeting;
	}

	public void setIsSatsangBiWeeklyMeeting(boolean IsSatsangBiWeeklyMeeting) {
		_IsSatsangBiWeeklyMeeting = IsSatsangBiWeeklyMeeting;
	}

	public boolean getIsBalmukund() {
		return _IsBalmukund;
	}

	public boolean isIsBalmukund() {
		return _IsBalmukund;
	}

	public void setIsBalmukund(boolean IsBalmukund) {
		_IsBalmukund = IsBalmukund;
	}

	public boolean getIsBhagvatGitaRecitation() {
		return _IsBhagvatGitaRecitation;
	}

	public boolean isIsBhagvatGitaRecitation() {
		return _IsBhagvatGitaRecitation;
	}

	public void setIsBhagvatGitaRecitation(boolean IsBhagvatGitaRecitation) {
		_IsBhagvatGitaRecitation = IsBhagvatGitaRecitation;
	}

	public boolean getIsYoga() {
		return _IsYoga;
	}

	public boolean isIsYoga() {
		return _IsYoga;
	}

	public void setIsYoga(boolean IsYoga) {
		_IsYoga = IsYoga;
	}

	public boolean getIs7MindSet() {
		return _Is7MindSet;
	}

	public boolean isIs7MindSet() {
		return _Is7MindSet;
	}

	public void setIs7MindSet(boolean Is7MindSet) {
		_Is7MindSet = Is7MindSet;
	}

	public boolean getIsOnlineBhagvatGitaDiscussion() {
		return _IsOnlineBhagvatGitaDiscussion;
	}

	public boolean isIsOnlineBhagvatGitaDiscussion() {
		return _IsOnlineBhagvatGitaDiscussion;
	}

	public void setIsOnlineBhagvatGitaDiscussion(
		boolean IsOnlineBhagvatGitaDiscussion) {
		_IsOnlineBhagvatGitaDiscussion = IsOnlineBhagvatGitaDiscussion;
	}

	public boolean getIsOnlineKidsMeditation() {
		return _IsOnlineKidsMeditation;
	}

	public boolean isIsOnlineKidsMeditation() {
		return _IsOnlineKidsMeditation;
	}

	public void setIsOnlineKidsMeditation(boolean IsOnlineKidsMeditation) {
		_IsOnlineKidsMeditation = IsOnlineKidsMeditation;
	}

	public boolean getIsOnlineYouthClub() {
		return _IsOnlineYouthClub;
	}

	public boolean isIsOnlineYouthClub() {
		return _IsOnlineYouthClub;
	}

	public void setIsOnlineYouthClub(boolean IsOnlineYouthClub) {
		_IsOnlineYouthClub = IsOnlineYouthClub;
	}

	public Date getCreatedDate() {
		return _CreatedDate;
	}

	public void setCreatedDate(Date CreatedDate) {
		_CreatedDate = CreatedDate;
	}

	public Date getUpdatedDate() {
		return _UpdatedDate;
	}

	public void setUpdatedDate(Date UpdatedDate) {
		_UpdatedDate = UpdatedDate;
	}

	private long _ID;
	private String _FirstName;
	private String _LastName;
	private String _MobileNumber;
	private boolean _IsSatsangBiWeeklyMeeting;
	private boolean _IsBalmukund;
	private boolean _IsBhagvatGitaRecitation;
	private boolean _IsYoga;
	private boolean _Is7MindSet;
	private boolean _IsOnlineBhagvatGitaDiscussion;
	private boolean _IsOnlineKidsMeditation;
	private boolean _IsOnlineYouthClub;
	private Date _CreatedDate;
	private Date _UpdatedDate;
}