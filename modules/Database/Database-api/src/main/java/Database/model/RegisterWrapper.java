/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link Register}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Register
 * @generated
 */
@ProviderType
public class RegisterWrapper implements Register, ModelWrapper<Register> {
	public RegisterWrapper(Register register) {
		_register = register;
	}

	@Override
	public Class<?> getModelClass() {
		return Register.class;
	}

	@Override
	public String getModelClassName() {
		return Register.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("FirstName", getFirstName());
		attributes.put("LastName", getLastName());
		attributes.put("IsMale", getIsMale());
		attributes.put("Address", getAddress());
		attributes.put("City", getCity());
		attributes.put("State", getState());
		attributes.put("Zip", getZip());
		attributes.put("PhoneNumber", getPhoneNumber());
		attributes.put("MobileNumber", getMobileNumber());
		attributes.put("Email", getEmail());
		attributes.put("AdditionalMember1", getAdditionalMember1());
		attributes.put("AdditionalMember2", getAdditionalMember2());
		attributes.put("AdditionalMember3", getAdditionalMember3());
		attributes.put("IsEmailCampain", getIsEmailCampain());
		attributes.put("IsTemple", getIsTemple());
		attributes.put("IsOnlineNewsWebsite", getIsOnlineNewsWebsite());
		attributes.put("IsRadio", getIsRadio());
		attributes.put("IsYouTube", getIsYouTube());
		attributes.put("IsFacebook", getIsFacebook());
		attributes.put("IsMeetup", getIsMeetup());
		attributes.put("IsOnlineEventCalendar", getIsOnlineEventCalendar());
		attributes.put("IsFlyer", getIsFlyer());
		attributes.put("IsNewspaper", getIsNewspaper());
		attributes.put("IsWordOfMouth", getIsWordOfMouth());
		attributes.put("Other", getOther());
		attributes.put("IsinterestedInVolunteringActivity",
			getIsinterestedInVolunteringActivity());
		attributes.put("IsinterestedInSatsangActivity",
			getIsinterestedInSatsangActivity());
		attributes.put("IsinterestedInChildrenActivity",
			getIsinterestedInChildrenActivity());
		attributes.put("CreatedDate", getCreatedDate());
		attributes.put("UpdatedDate", getUpdatedDate());
		attributes.put("AdditionalMember1Age", getAdditionalMember1Age());
		attributes.put("AdditionalMember2Age", getAdditionalMember2Age());
		attributes.put("AdditionalMember3Age", getAdditionalMember3Age());
		attributes.put("SpouseFirstName", getSpouseFirstName());
		attributes.put("SpouseLastName", getSpouseLastName());
		attributes.put("SpousePhoneNumber", getSpousePhoneNumber());
		attributes.put("MembershipPlan", getMembershipPlan());
		attributes.put("PaymentMethod", getPaymentMethod());
		attributes.put("StateCenter", getStateCenter());
		attributes.put("ProgramName", getProgramName());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ID = (Long)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		String FirstName = (String)attributes.get("FirstName");

		if (FirstName != null) {
			setFirstName(FirstName);
		}

		String LastName = (String)attributes.get("LastName");

		if (LastName != null) {
			setLastName(LastName);
		}

		Boolean IsMale = (Boolean)attributes.get("IsMale");

		if (IsMale != null) {
			setIsMale(IsMale);
		}

		String Address = (String)attributes.get("Address");

		if (Address != null) {
			setAddress(Address);
		}

		String City = (String)attributes.get("City");

		if (City != null) {
			setCity(City);
		}

		String State = (String)attributes.get("State");

		if (State != null) {
			setState(State);
		}

		String Zip = (String)attributes.get("Zip");

		if (Zip != null) {
			setZip(Zip);
		}

		String PhoneNumber = (String)attributes.get("PhoneNumber");

		if (PhoneNumber != null) {
			setPhoneNumber(PhoneNumber);
		}

		String MobileNumber = (String)attributes.get("MobileNumber");

		if (MobileNumber != null) {
			setMobileNumber(MobileNumber);
		}

		String Email = (String)attributes.get("Email");

		if (Email != null) {
			setEmail(Email);
		}

		String AdditionalMember1 = (String)attributes.get("AdditionalMember1");

		if (AdditionalMember1 != null) {
			setAdditionalMember1(AdditionalMember1);
		}

		String AdditionalMember2 = (String)attributes.get("AdditionalMember2");

		if (AdditionalMember2 != null) {
			setAdditionalMember2(AdditionalMember2);
		}

		String AdditionalMember3 = (String)attributes.get("AdditionalMember3");

		if (AdditionalMember3 != null) {
			setAdditionalMember3(AdditionalMember3);
		}

		Boolean IsEmailCampain = (Boolean)attributes.get("IsEmailCampain");

		if (IsEmailCampain != null) {
			setIsEmailCampain(IsEmailCampain);
		}

		Boolean IsTemple = (Boolean)attributes.get("IsTemple");

		if (IsTemple != null) {
			setIsTemple(IsTemple);
		}

		Boolean IsOnlineNewsWebsite = (Boolean)attributes.get(
				"IsOnlineNewsWebsite");

		if (IsOnlineNewsWebsite != null) {
			setIsOnlineNewsWebsite(IsOnlineNewsWebsite);
		}

		Boolean IsRadio = (Boolean)attributes.get("IsRadio");

		if (IsRadio != null) {
			setIsRadio(IsRadio);
		}

		Boolean IsYouTube = (Boolean)attributes.get("IsYouTube");

		if (IsYouTube != null) {
			setIsYouTube(IsYouTube);
		}

		Boolean IsFacebook = (Boolean)attributes.get("IsFacebook");

		if (IsFacebook != null) {
			setIsFacebook(IsFacebook);
		}

		Boolean IsMeetup = (Boolean)attributes.get("IsMeetup");

		if (IsMeetup != null) {
			setIsMeetup(IsMeetup);
		}

		Boolean IsOnlineEventCalendar = (Boolean)attributes.get(
				"IsOnlineEventCalendar");

		if (IsOnlineEventCalendar != null) {
			setIsOnlineEventCalendar(IsOnlineEventCalendar);
		}

		Boolean IsFlyer = (Boolean)attributes.get("IsFlyer");

		if (IsFlyer != null) {
			setIsFlyer(IsFlyer);
		}

		Boolean IsNewspaper = (Boolean)attributes.get("IsNewspaper");

		if (IsNewspaper != null) {
			setIsNewspaper(IsNewspaper);
		}

		Boolean IsWordOfMouth = (Boolean)attributes.get("IsWordOfMouth");

		if (IsWordOfMouth != null) {
			setIsWordOfMouth(IsWordOfMouth);
		}

		String Other = (String)attributes.get("Other");

		if (Other != null) {
			setOther(Other);
		}

		Boolean IsinterestedInVolunteringActivity = (Boolean)attributes.get(
				"IsinterestedInVolunteringActivity");

		if (IsinterestedInVolunteringActivity != null) {
			setIsinterestedInVolunteringActivity(IsinterestedInVolunteringActivity);
		}

		Boolean IsinterestedInSatsangActivity = (Boolean)attributes.get(
				"IsinterestedInSatsangActivity");

		if (IsinterestedInSatsangActivity != null) {
			setIsinterestedInSatsangActivity(IsinterestedInSatsangActivity);
		}

		Boolean IsinterestedInChildrenActivity = (Boolean)attributes.get(
				"IsinterestedInChildrenActivity");

		if (IsinterestedInChildrenActivity != null) {
			setIsinterestedInChildrenActivity(IsinterestedInChildrenActivity);
		}

		Date CreatedDate = (Date)attributes.get("CreatedDate");

		if (CreatedDate != null) {
			setCreatedDate(CreatedDate);
		}

		Date UpdatedDate = (Date)attributes.get("UpdatedDate");

		if (UpdatedDate != null) {
			setUpdatedDate(UpdatedDate);
		}

		String AdditionalMember1Age = (String)attributes.get(
				"AdditionalMember1Age");

		if (AdditionalMember1Age != null) {
			setAdditionalMember1Age(AdditionalMember1Age);
		}

		String AdditionalMember2Age = (String)attributes.get(
				"AdditionalMember2Age");

		if (AdditionalMember2Age != null) {
			setAdditionalMember2Age(AdditionalMember2Age);
		}

		String AdditionalMember3Age = (String)attributes.get(
				"AdditionalMember3Age");

		if (AdditionalMember3Age != null) {
			setAdditionalMember3Age(AdditionalMember3Age);
		}

		String SpouseFirstName = (String)attributes.get("SpouseFirstName");

		if (SpouseFirstName != null) {
			setSpouseFirstName(SpouseFirstName);
		}

		String SpouseLastName = (String)attributes.get("SpouseLastName");

		if (SpouseLastName != null) {
			setSpouseLastName(SpouseLastName);
		}

		String SpousePhoneNumber = (String)attributes.get("SpousePhoneNumber");

		if (SpousePhoneNumber != null) {
			setSpousePhoneNumber(SpousePhoneNumber);
		}

		String MembershipPlan = (String)attributes.get("MembershipPlan");

		if (MembershipPlan != null) {
			setMembershipPlan(MembershipPlan);
		}

		String PaymentMethod = (String)attributes.get("PaymentMethod");

		if (PaymentMethod != null) {
			setPaymentMethod(PaymentMethod);
		}

		String StateCenter = (String)attributes.get("StateCenter");

		if (StateCenter != null) {
			setStateCenter(StateCenter);
		}

		String ProgramName = (String)attributes.get("ProgramName");

		if (ProgramName != null) {
			setProgramName(ProgramName);
		}
	}

	@Override
	public Database.model.Register toEscapedModel() {
		return new RegisterWrapper(_register.toEscapedModel());
	}

	@Override
	public Database.model.Register toUnescapedModel() {
		return new RegisterWrapper(_register.toUnescapedModel());
	}

	/**
	* Returns the is email campain of this Register.
	*
	* @return the is email campain of this Register
	*/
	@Override
	public boolean getIsEmailCampain() {
		return _register.getIsEmailCampain();
	}

	/**
	* Returns the is facebook of this Register.
	*
	* @return the is facebook of this Register
	*/
	@Override
	public boolean getIsFacebook() {
		return _register.getIsFacebook();
	}

	/**
	* Returns the is flyer of this Register.
	*
	* @return the is flyer of this Register
	*/
	@Override
	public boolean getIsFlyer() {
		return _register.getIsFlyer();
	}

	/**
	* Returns the is male of this Register.
	*
	* @return the is male of this Register
	*/
	@Override
	public boolean getIsMale() {
		return _register.getIsMale();
	}

	/**
	* Returns the is meetup of this Register.
	*
	* @return the is meetup of this Register
	*/
	@Override
	public boolean getIsMeetup() {
		return _register.getIsMeetup();
	}

	/**
	* Returns the is newspaper of this Register.
	*
	* @return the is newspaper of this Register
	*/
	@Override
	public boolean getIsNewspaper() {
		return _register.getIsNewspaper();
	}

	/**
	* Returns the is online event calendar of this Register.
	*
	* @return the is online event calendar of this Register
	*/
	@Override
	public boolean getIsOnlineEventCalendar() {
		return _register.getIsOnlineEventCalendar();
	}

	/**
	* Returns the is online news website of this Register.
	*
	* @return the is online news website of this Register
	*/
	@Override
	public boolean getIsOnlineNewsWebsite() {
		return _register.getIsOnlineNewsWebsite();
	}

	/**
	* Returns the is radio of this Register.
	*
	* @return the is radio of this Register
	*/
	@Override
	public boolean getIsRadio() {
		return _register.getIsRadio();
	}

	/**
	* Returns the is temple of this Register.
	*
	* @return the is temple of this Register
	*/
	@Override
	public boolean getIsTemple() {
		return _register.getIsTemple();
	}

	/**
	* Returns the is word of mouth of this Register.
	*
	* @return the is word of mouth of this Register
	*/
	@Override
	public boolean getIsWordOfMouth() {
		return _register.getIsWordOfMouth();
	}

	/**
	* Returns the is you tube of this Register.
	*
	* @return the is you tube of this Register
	*/
	@Override
	public boolean getIsYouTube() {
		return _register.getIsYouTube();
	}

	/**
	* Returns the isinterested in children activity of this Register.
	*
	* @return the isinterested in children activity of this Register
	*/
	@Override
	public boolean getIsinterestedInChildrenActivity() {
		return _register.getIsinterestedInChildrenActivity();
	}

	/**
	* Returns the isinterested in satsang activity of this Register.
	*
	* @return the isinterested in satsang activity of this Register
	*/
	@Override
	public boolean getIsinterestedInSatsangActivity() {
		return _register.getIsinterestedInSatsangActivity();
	}

	/**
	* Returns the isinterested in voluntering activity of this Register.
	*
	* @return the isinterested in voluntering activity of this Register
	*/
	@Override
	public boolean getIsinterestedInVolunteringActivity() {
		return _register.getIsinterestedInVolunteringActivity();
	}

	@Override
	public boolean isCachedModel() {
		return _register.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _register.isEscapedModel();
	}

	/**
	* Returns <code>true</code> if this Register is is email campain.
	*
	* @return <code>true</code> if this Register is is email campain; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsEmailCampain() {
		return _register.isIsEmailCampain();
	}

	/**
	* Returns <code>true</code> if this Register is is facebook.
	*
	* @return <code>true</code> if this Register is is facebook; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsFacebook() {
		return _register.isIsFacebook();
	}

	/**
	* Returns <code>true</code> if this Register is is flyer.
	*
	* @return <code>true</code> if this Register is is flyer; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsFlyer() {
		return _register.isIsFlyer();
	}

	/**
	* Returns <code>true</code> if this Register is is male.
	*
	* @return <code>true</code> if this Register is is male; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsMale() {
		return _register.isIsMale();
	}

	/**
	* Returns <code>true</code> if this Register is is meetup.
	*
	* @return <code>true</code> if this Register is is meetup; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsMeetup() {
		return _register.isIsMeetup();
	}

	/**
	* Returns <code>true</code> if this Register is is newspaper.
	*
	* @return <code>true</code> if this Register is is newspaper; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsNewspaper() {
		return _register.isIsNewspaper();
	}

	/**
	* Returns <code>true</code> if this Register is is online event calendar.
	*
	* @return <code>true</code> if this Register is is online event calendar; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsOnlineEventCalendar() {
		return _register.isIsOnlineEventCalendar();
	}

	/**
	* Returns <code>true</code> if this Register is is online news website.
	*
	* @return <code>true</code> if this Register is is online news website; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsOnlineNewsWebsite() {
		return _register.isIsOnlineNewsWebsite();
	}

	/**
	* Returns <code>true</code> if this Register is is radio.
	*
	* @return <code>true</code> if this Register is is radio; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsRadio() {
		return _register.isIsRadio();
	}

	/**
	* Returns <code>true</code> if this Register is is temple.
	*
	* @return <code>true</code> if this Register is is temple; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsTemple() {
		return _register.isIsTemple();
	}

	/**
	* Returns <code>true</code> if this Register is is word of mouth.
	*
	* @return <code>true</code> if this Register is is word of mouth; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsWordOfMouth() {
		return _register.isIsWordOfMouth();
	}

	/**
	* Returns <code>true</code> if this Register is is you tube.
	*
	* @return <code>true</code> if this Register is is you tube; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsYouTube() {
		return _register.isIsYouTube();
	}

	/**
	* Returns <code>true</code> if this Register is isinterested in children activity.
	*
	* @return <code>true</code> if this Register is isinterested in children activity; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsinterestedInChildrenActivity() {
		return _register.isIsinterestedInChildrenActivity();
	}

	/**
	* Returns <code>true</code> if this Register is isinterested in satsang activity.
	*
	* @return <code>true</code> if this Register is isinterested in satsang activity; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsinterestedInSatsangActivity() {
		return _register.isIsinterestedInSatsangActivity();
	}

	/**
	* Returns <code>true</code> if this Register is isinterested in voluntering activity.
	*
	* @return <code>true</code> if this Register is isinterested in voluntering activity; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsinterestedInVolunteringActivity() {
		return _register.isIsinterestedInVolunteringActivity();
	}

	@Override
	public boolean isNew() {
		return _register.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _register.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Database.model.Register> toCacheModel() {
		return _register.toCacheModel();
	}

	@Override
	public int compareTo(Database.model.Register register) {
		return _register.compareTo(register);
	}

	@Override
	public int hashCode() {
		return _register.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _register.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new RegisterWrapper((Register)_register.clone());
	}

	/**
	* Returns the additional member1 of this Register.
	*
	* @return the additional member1 of this Register
	*/
	@Override
	public java.lang.String getAdditionalMember1() {
		return _register.getAdditionalMember1();
	}

	/**
	* Returns the additional member1 age of this Register.
	*
	* @return the additional member1 age of this Register
	*/
	@Override
	public java.lang.String getAdditionalMember1Age() {
		return _register.getAdditionalMember1Age();
	}

	/**
	* Returns the additional member2 of this Register.
	*
	* @return the additional member2 of this Register
	*/
	@Override
	public java.lang.String getAdditionalMember2() {
		return _register.getAdditionalMember2();
	}

	/**
	* Returns the additional member2 age of this Register.
	*
	* @return the additional member2 age of this Register
	*/
	@Override
	public java.lang.String getAdditionalMember2Age() {
		return _register.getAdditionalMember2Age();
	}

	/**
	* Returns the additional member3 of this Register.
	*
	* @return the additional member3 of this Register
	*/
	@Override
	public java.lang.String getAdditionalMember3() {
		return _register.getAdditionalMember3();
	}

	/**
	* Returns the additional member3 age of this Register.
	*
	* @return the additional member3 age of this Register
	*/
	@Override
	public java.lang.String getAdditionalMember3Age() {
		return _register.getAdditionalMember3Age();
	}

	/**
	* Returns the address of this Register.
	*
	* @return the address of this Register
	*/
	@Override
	public java.lang.String getAddress() {
		return _register.getAddress();
	}

	/**
	* Returns the city of this Register.
	*
	* @return the city of this Register
	*/
	@Override
	public java.lang.String getCity() {
		return _register.getCity();
	}

	/**
	* Returns the email of this Register.
	*
	* @return the email of this Register
	*/
	@Override
	public java.lang.String getEmail() {
		return _register.getEmail();
	}

	/**
	* Returns the first name of this Register.
	*
	* @return the first name of this Register
	*/
	@Override
	public java.lang.String getFirstName() {
		return _register.getFirstName();
	}

	/**
	* Returns the last name of this Register.
	*
	* @return the last name of this Register
	*/
	@Override
	public java.lang.String getLastName() {
		return _register.getLastName();
	}

	/**
	* Returns the membership plan of this Register.
	*
	* @return the membership plan of this Register
	*/
	@Override
	public java.lang.String getMembershipPlan() {
		return _register.getMembershipPlan();
	}

	/**
	* Returns the mobile number of this Register.
	*
	* @return the mobile number of this Register
	*/
	@Override
	public java.lang.String getMobileNumber() {
		return _register.getMobileNumber();
	}

	/**
	* Returns the other of this Register.
	*
	* @return the other of this Register
	*/
	@Override
	public java.lang.String getOther() {
		return _register.getOther();
	}

	/**
	* Returns the payment method of this Register.
	*
	* @return the payment method of this Register
	*/
	@Override
	public java.lang.String getPaymentMethod() {
		return _register.getPaymentMethod();
	}

	/**
	* Returns the phone number of this Register.
	*
	* @return the phone number of this Register
	*/
	@Override
	public java.lang.String getPhoneNumber() {
		return _register.getPhoneNumber();
	}

	/**
	* Returns the program name of this Register.
	*
	* @return the program name of this Register
	*/
	@Override
	public java.lang.String getProgramName() {
		return _register.getProgramName();
	}

	/**
	* Returns the spouse first name of this Register.
	*
	* @return the spouse first name of this Register
	*/
	@Override
	public java.lang.String getSpouseFirstName() {
		return _register.getSpouseFirstName();
	}

	/**
	* Returns the spouse last name of this Register.
	*
	* @return the spouse last name of this Register
	*/
	@Override
	public java.lang.String getSpouseLastName() {
		return _register.getSpouseLastName();
	}

	/**
	* Returns the spouse phone number of this Register.
	*
	* @return the spouse phone number of this Register
	*/
	@Override
	public java.lang.String getSpousePhoneNumber() {
		return _register.getSpousePhoneNumber();
	}

	/**
	* Returns the state of this Register.
	*
	* @return the state of this Register
	*/
	@Override
	public java.lang.String getState() {
		return _register.getState();
	}

	/**
	* Returns the state center of this Register.
	*
	* @return the state center of this Register
	*/
	@Override
	public java.lang.String getStateCenter() {
		return _register.getStateCenter();
	}

	/**
	* Returns the zip of this Register.
	*
	* @return the zip of this Register
	*/
	@Override
	public java.lang.String getZip() {
		return _register.getZip();
	}

	@Override
	public java.lang.String toString() {
		return _register.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _register.toXmlString();
	}

	/**
	* Returns the created date of this Register.
	*
	* @return the created date of this Register
	*/
	@Override
	public Date getCreatedDate() {
		return _register.getCreatedDate();
	}

	/**
	* Returns the updated date of this Register.
	*
	* @return the updated date of this Register
	*/
	@Override
	public Date getUpdatedDate() {
		return _register.getUpdatedDate();
	}

	/**
	* Returns the ID of this Register.
	*
	* @return the ID of this Register
	*/
	@Override
	public long getID() {
		return _register.getID();
	}

	/**
	* Returns the primary key of this Register.
	*
	* @return the primary key of this Register
	*/
	@Override
	public long getPrimaryKey() {
		return _register.getPrimaryKey();
	}

	@Override
	public void persist() {
		_register.persist();
	}

	/**
	* Sets the additional member1 of this Register.
	*
	* @param AdditionalMember1 the additional member1 of this Register
	*/
	@Override
	public void setAdditionalMember1(java.lang.String AdditionalMember1) {
		_register.setAdditionalMember1(AdditionalMember1);
	}

	/**
	* Sets the additional member1 age of this Register.
	*
	* @param AdditionalMember1Age the additional member1 age of this Register
	*/
	@Override
	public void setAdditionalMember1Age(java.lang.String AdditionalMember1Age) {
		_register.setAdditionalMember1Age(AdditionalMember1Age);
	}

	/**
	* Sets the additional member2 of this Register.
	*
	* @param AdditionalMember2 the additional member2 of this Register
	*/
	@Override
	public void setAdditionalMember2(java.lang.String AdditionalMember2) {
		_register.setAdditionalMember2(AdditionalMember2);
	}

	/**
	* Sets the additional member2 age of this Register.
	*
	* @param AdditionalMember2Age the additional member2 age of this Register
	*/
	@Override
	public void setAdditionalMember2Age(java.lang.String AdditionalMember2Age) {
		_register.setAdditionalMember2Age(AdditionalMember2Age);
	}

	/**
	* Sets the additional member3 of this Register.
	*
	* @param AdditionalMember3 the additional member3 of this Register
	*/
	@Override
	public void setAdditionalMember3(java.lang.String AdditionalMember3) {
		_register.setAdditionalMember3(AdditionalMember3);
	}

	/**
	* Sets the additional member3 age of this Register.
	*
	* @param AdditionalMember3Age the additional member3 age of this Register
	*/
	@Override
	public void setAdditionalMember3Age(java.lang.String AdditionalMember3Age) {
		_register.setAdditionalMember3Age(AdditionalMember3Age);
	}

	/**
	* Sets the address of this Register.
	*
	* @param Address the address of this Register
	*/
	@Override
	public void setAddress(java.lang.String Address) {
		_register.setAddress(Address);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_register.setCachedModel(cachedModel);
	}

	/**
	* Sets the city of this Register.
	*
	* @param City the city of this Register
	*/
	@Override
	public void setCity(java.lang.String City) {
		_register.setCity(City);
	}

	/**
	* Sets the created date of this Register.
	*
	* @param CreatedDate the created date of this Register
	*/
	@Override
	public void setCreatedDate(Date CreatedDate) {
		_register.setCreatedDate(CreatedDate);
	}

	/**
	* Sets the email of this Register.
	*
	* @param Email the email of this Register
	*/
	@Override
	public void setEmail(java.lang.String Email) {
		_register.setEmail(Email);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_register.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_register.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_register.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the first name of this Register.
	*
	* @param FirstName the first name of this Register
	*/
	@Override
	public void setFirstName(java.lang.String FirstName) {
		_register.setFirstName(FirstName);
	}

	/**
	* Sets the ID of this Register.
	*
	* @param ID the ID of this Register
	*/
	@Override
	public void setID(long ID) {
		_register.setID(ID);
	}

	/**
	* Sets whether this Register is is email campain.
	*
	* @param IsEmailCampain the is email campain of this Register
	*/
	@Override
	public void setIsEmailCampain(boolean IsEmailCampain) {
		_register.setIsEmailCampain(IsEmailCampain);
	}

	/**
	* Sets whether this Register is is facebook.
	*
	* @param IsFacebook the is facebook of this Register
	*/
	@Override
	public void setIsFacebook(boolean IsFacebook) {
		_register.setIsFacebook(IsFacebook);
	}

	/**
	* Sets whether this Register is is flyer.
	*
	* @param IsFlyer the is flyer of this Register
	*/
	@Override
	public void setIsFlyer(boolean IsFlyer) {
		_register.setIsFlyer(IsFlyer);
	}

	/**
	* Sets whether this Register is is male.
	*
	* @param IsMale the is male of this Register
	*/
	@Override
	public void setIsMale(boolean IsMale) {
		_register.setIsMale(IsMale);
	}

	/**
	* Sets whether this Register is is meetup.
	*
	* @param IsMeetup the is meetup of this Register
	*/
	@Override
	public void setIsMeetup(boolean IsMeetup) {
		_register.setIsMeetup(IsMeetup);
	}

	/**
	* Sets whether this Register is is newspaper.
	*
	* @param IsNewspaper the is newspaper of this Register
	*/
	@Override
	public void setIsNewspaper(boolean IsNewspaper) {
		_register.setIsNewspaper(IsNewspaper);
	}

	/**
	* Sets whether this Register is is online event calendar.
	*
	* @param IsOnlineEventCalendar the is online event calendar of this Register
	*/
	@Override
	public void setIsOnlineEventCalendar(boolean IsOnlineEventCalendar) {
		_register.setIsOnlineEventCalendar(IsOnlineEventCalendar);
	}

	/**
	* Sets whether this Register is is online news website.
	*
	* @param IsOnlineNewsWebsite the is online news website of this Register
	*/
	@Override
	public void setIsOnlineNewsWebsite(boolean IsOnlineNewsWebsite) {
		_register.setIsOnlineNewsWebsite(IsOnlineNewsWebsite);
	}

	/**
	* Sets whether this Register is is radio.
	*
	* @param IsRadio the is radio of this Register
	*/
	@Override
	public void setIsRadio(boolean IsRadio) {
		_register.setIsRadio(IsRadio);
	}

	/**
	* Sets whether this Register is is temple.
	*
	* @param IsTemple the is temple of this Register
	*/
	@Override
	public void setIsTemple(boolean IsTemple) {
		_register.setIsTemple(IsTemple);
	}

	/**
	* Sets whether this Register is is word of mouth.
	*
	* @param IsWordOfMouth the is word of mouth of this Register
	*/
	@Override
	public void setIsWordOfMouth(boolean IsWordOfMouth) {
		_register.setIsWordOfMouth(IsWordOfMouth);
	}

	/**
	* Sets whether this Register is is you tube.
	*
	* @param IsYouTube the is you tube of this Register
	*/
	@Override
	public void setIsYouTube(boolean IsYouTube) {
		_register.setIsYouTube(IsYouTube);
	}

	/**
	* Sets whether this Register is isinterested in children activity.
	*
	* @param IsinterestedInChildrenActivity the isinterested in children activity of this Register
	*/
	@Override
	public void setIsinterestedInChildrenActivity(
		boolean IsinterestedInChildrenActivity) {
		_register.setIsinterestedInChildrenActivity(IsinterestedInChildrenActivity);
	}

	/**
	* Sets whether this Register is isinterested in satsang activity.
	*
	* @param IsinterestedInSatsangActivity the isinterested in satsang activity of this Register
	*/
	@Override
	public void setIsinterestedInSatsangActivity(
		boolean IsinterestedInSatsangActivity) {
		_register.setIsinterestedInSatsangActivity(IsinterestedInSatsangActivity);
	}

	/**
	* Sets whether this Register is isinterested in voluntering activity.
	*
	* @param IsinterestedInVolunteringActivity the isinterested in voluntering activity of this Register
	*/
	@Override
	public void setIsinterestedInVolunteringActivity(
		boolean IsinterestedInVolunteringActivity) {
		_register.setIsinterestedInVolunteringActivity(IsinterestedInVolunteringActivity);
	}

	/**
	* Sets the last name of this Register.
	*
	* @param LastName the last name of this Register
	*/
	@Override
	public void setLastName(java.lang.String LastName) {
		_register.setLastName(LastName);
	}

	/**
	* Sets the membership plan of this Register.
	*
	* @param MembershipPlan the membership plan of this Register
	*/
	@Override
	public void setMembershipPlan(java.lang.String MembershipPlan) {
		_register.setMembershipPlan(MembershipPlan);
	}

	/**
	* Sets the mobile number of this Register.
	*
	* @param MobileNumber the mobile number of this Register
	*/
	@Override
	public void setMobileNumber(java.lang.String MobileNumber) {
		_register.setMobileNumber(MobileNumber);
	}

	@Override
	public void setNew(boolean n) {
		_register.setNew(n);
	}

	/**
	* Sets the other of this Register.
	*
	* @param Other the other of this Register
	*/
	@Override
	public void setOther(java.lang.String Other) {
		_register.setOther(Other);
	}

	/**
	* Sets the payment method of this Register.
	*
	* @param PaymentMethod the payment method of this Register
	*/
	@Override
	public void setPaymentMethod(java.lang.String PaymentMethod) {
		_register.setPaymentMethod(PaymentMethod);
	}

	/**
	* Sets the phone number of this Register.
	*
	* @param PhoneNumber the phone number of this Register
	*/
	@Override
	public void setPhoneNumber(java.lang.String PhoneNumber) {
		_register.setPhoneNumber(PhoneNumber);
	}

	/**
	* Sets the primary key of this Register.
	*
	* @param primaryKey the primary key of this Register
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_register.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_register.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the program name of this Register.
	*
	* @param ProgramName the program name of this Register
	*/
	@Override
	public void setProgramName(java.lang.String ProgramName) {
		_register.setProgramName(ProgramName);
	}

	/**
	* Sets the spouse first name of this Register.
	*
	* @param SpouseFirstName the spouse first name of this Register
	*/
	@Override
	public void setSpouseFirstName(java.lang.String SpouseFirstName) {
		_register.setSpouseFirstName(SpouseFirstName);
	}

	/**
	* Sets the spouse last name of this Register.
	*
	* @param SpouseLastName the spouse last name of this Register
	*/
	@Override
	public void setSpouseLastName(java.lang.String SpouseLastName) {
		_register.setSpouseLastName(SpouseLastName);
	}

	/**
	* Sets the spouse phone number of this Register.
	*
	* @param SpousePhoneNumber the spouse phone number of this Register
	*/
	@Override
	public void setSpousePhoneNumber(java.lang.String SpousePhoneNumber) {
		_register.setSpousePhoneNumber(SpousePhoneNumber);
	}

	/**
	* Sets the state of this Register.
	*
	* @param State the state of this Register
	*/
	@Override
	public void setState(java.lang.String State) {
		_register.setState(State);
	}

	/**
	* Sets the state center of this Register.
	*
	* @param StateCenter the state center of this Register
	*/
	@Override
	public void setStateCenter(java.lang.String StateCenter) {
		_register.setStateCenter(StateCenter);
	}

	/**
	* Sets the updated date of this Register.
	*
	* @param UpdatedDate the updated date of this Register
	*/
	@Override
	public void setUpdatedDate(Date UpdatedDate) {
		_register.setUpdatedDate(UpdatedDate);
	}

	/**
	* Sets the zip of this Register.
	*
	* @param Zip the zip of this Register
	*/
	@Override
	public void setZip(java.lang.String Zip) {
		_register.setZip(Zip);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof RegisterWrapper)) {
			return false;
		}

		RegisterWrapper registerWrapper = (RegisterWrapper)obj;

		if (Objects.equals(_register, registerWrapper._register)) {
			return true;
		}

		return false;
	}

	@Override
	public Register getWrappedModel() {
		return _register;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _register.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _register.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_register.resetOriginalValues();
	}

	private final Register _register;
}