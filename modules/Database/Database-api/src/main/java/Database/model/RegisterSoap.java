/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class RegisterSoap implements Serializable {
	public static RegisterSoap toSoapModel(Register model) {
		RegisterSoap soapModel = new RegisterSoap();

		soapModel.setID(model.getID());
		soapModel.setFirstName(model.getFirstName());
		soapModel.setLastName(model.getLastName());
		soapModel.setIsMale(model.getIsMale());
		soapModel.setAddress(model.getAddress());
		soapModel.setCity(model.getCity());
		soapModel.setState(model.getState());
		soapModel.setZip(model.getZip());
		soapModel.setPhoneNumber(model.getPhoneNumber());
		soapModel.setMobileNumber(model.getMobileNumber());
		soapModel.setEmail(model.getEmail());
		soapModel.setAdditionalMember1(model.getAdditionalMember1());
		soapModel.setAdditionalMember2(model.getAdditionalMember2());
		soapModel.setAdditionalMember3(model.getAdditionalMember3());
		soapModel.setIsEmailCampain(model.getIsEmailCampain());
		soapModel.setIsTemple(model.getIsTemple());
		soapModel.setIsOnlineNewsWebsite(model.getIsOnlineNewsWebsite());
		soapModel.setIsRadio(model.getIsRadio());
		soapModel.setIsYouTube(model.getIsYouTube());
		soapModel.setIsFacebook(model.getIsFacebook());
		soapModel.setIsMeetup(model.getIsMeetup());
		soapModel.setIsOnlineEventCalendar(model.getIsOnlineEventCalendar());
		soapModel.setIsFlyer(model.getIsFlyer());
		soapModel.setIsNewspaper(model.getIsNewspaper());
		soapModel.setIsWordOfMouth(model.getIsWordOfMouth());
		soapModel.setOther(model.getOther());
		soapModel.setIsinterestedInVolunteringActivity(model.getIsinterestedInVolunteringActivity());
		soapModel.setIsinterestedInSatsangActivity(model.getIsinterestedInSatsangActivity());
		soapModel.setIsinterestedInChildrenActivity(model.getIsinterestedInChildrenActivity());
		soapModel.setCreatedDate(model.getCreatedDate());
		soapModel.setUpdatedDate(model.getUpdatedDate());
		soapModel.setAdditionalMember1Age(model.getAdditionalMember1Age());
		soapModel.setAdditionalMember2Age(model.getAdditionalMember2Age());
		soapModel.setAdditionalMember3Age(model.getAdditionalMember3Age());
		soapModel.setSpouseFirstName(model.getSpouseFirstName());
		soapModel.setSpouseLastName(model.getSpouseLastName());
		soapModel.setSpousePhoneNumber(model.getSpousePhoneNumber());
		soapModel.setMembershipPlan(model.getMembershipPlan());
		soapModel.setPaymentMethod(model.getPaymentMethod());
		soapModel.setStateCenter(model.getStateCenter());
		soapModel.setProgramName(model.getProgramName());

		return soapModel;
	}

	public static RegisterSoap[] toSoapModels(Register[] models) {
		RegisterSoap[] soapModels = new RegisterSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static RegisterSoap[][] toSoapModels(Register[][] models) {
		RegisterSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new RegisterSoap[models.length][models[0].length];
		}
		else {
			soapModels = new RegisterSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static RegisterSoap[] toSoapModels(List<Register> models) {
		List<RegisterSoap> soapModels = new ArrayList<RegisterSoap>(models.size());

		for (Register model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new RegisterSoap[soapModels.size()]);
	}

	public RegisterSoap() {
	}

	public long getPrimaryKey() {
		return _ID;
	}

	public void setPrimaryKey(long pk) {
		setID(pk);
	}

	public long getID() {
		return _ID;
	}

	public void setID(long ID) {
		_ID = ID;
	}

	public String getFirstName() {
		return _FirstName;
	}

	public void setFirstName(String FirstName) {
		_FirstName = FirstName;
	}

	public String getLastName() {
		return _LastName;
	}

	public void setLastName(String LastName) {
		_LastName = LastName;
	}

	public boolean getIsMale() {
		return _IsMale;
	}

	public boolean isIsMale() {
		return _IsMale;
	}

	public void setIsMale(boolean IsMale) {
		_IsMale = IsMale;
	}

	public String getAddress() {
		return _Address;
	}

	public void setAddress(String Address) {
		_Address = Address;
	}

	public String getCity() {
		return _City;
	}

	public void setCity(String City) {
		_City = City;
	}

	public String getState() {
		return _State;
	}

	public void setState(String State) {
		_State = State;
	}

	public String getZip() {
		return _Zip;
	}

	public void setZip(String Zip) {
		_Zip = Zip;
	}

	public String getPhoneNumber() {
		return _PhoneNumber;
	}

	public void setPhoneNumber(String PhoneNumber) {
		_PhoneNumber = PhoneNumber;
	}

	public String getMobileNumber() {
		return _MobileNumber;
	}

	public void setMobileNumber(String MobileNumber) {
		_MobileNumber = MobileNumber;
	}

	public String getEmail() {
		return _Email;
	}

	public void setEmail(String Email) {
		_Email = Email;
	}

	public String getAdditionalMember1() {
		return _AdditionalMember1;
	}

	public void setAdditionalMember1(String AdditionalMember1) {
		_AdditionalMember1 = AdditionalMember1;
	}

	public String getAdditionalMember2() {
		return _AdditionalMember2;
	}

	public void setAdditionalMember2(String AdditionalMember2) {
		_AdditionalMember2 = AdditionalMember2;
	}

	public String getAdditionalMember3() {
		return _AdditionalMember3;
	}

	public void setAdditionalMember3(String AdditionalMember3) {
		_AdditionalMember3 = AdditionalMember3;
	}

	public boolean getIsEmailCampain() {
		return _IsEmailCampain;
	}

	public boolean isIsEmailCampain() {
		return _IsEmailCampain;
	}

	public void setIsEmailCampain(boolean IsEmailCampain) {
		_IsEmailCampain = IsEmailCampain;
	}

	public boolean getIsTemple() {
		return _IsTemple;
	}

	public boolean isIsTemple() {
		return _IsTemple;
	}

	public void setIsTemple(boolean IsTemple) {
		_IsTemple = IsTemple;
	}

	public boolean getIsOnlineNewsWebsite() {
		return _IsOnlineNewsWebsite;
	}

	public boolean isIsOnlineNewsWebsite() {
		return _IsOnlineNewsWebsite;
	}

	public void setIsOnlineNewsWebsite(boolean IsOnlineNewsWebsite) {
		_IsOnlineNewsWebsite = IsOnlineNewsWebsite;
	}

	public boolean getIsRadio() {
		return _IsRadio;
	}

	public boolean isIsRadio() {
		return _IsRadio;
	}

	public void setIsRadio(boolean IsRadio) {
		_IsRadio = IsRadio;
	}

	public boolean getIsYouTube() {
		return _IsYouTube;
	}

	public boolean isIsYouTube() {
		return _IsYouTube;
	}

	public void setIsYouTube(boolean IsYouTube) {
		_IsYouTube = IsYouTube;
	}

	public boolean getIsFacebook() {
		return _IsFacebook;
	}

	public boolean isIsFacebook() {
		return _IsFacebook;
	}

	public void setIsFacebook(boolean IsFacebook) {
		_IsFacebook = IsFacebook;
	}

	public boolean getIsMeetup() {
		return _IsMeetup;
	}

	public boolean isIsMeetup() {
		return _IsMeetup;
	}

	public void setIsMeetup(boolean IsMeetup) {
		_IsMeetup = IsMeetup;
	}

	public boolean getIsOnlineEventCalendar() {
		return _IsOnlineEventCalendar;
	}

	public boolean isIsOnlineEventCalendar() {
		return _IsOnlineEventCalendar;
	}

	public void setIsOnlineEventCalendar(boolean IsOnlineEventCalendar) {
		_IsOnlineEventCalendar = IsOnlineEventCalendar;
	}

	public boolean getIsFlyer() {
		return _IsFlyer;
	}

	public boolean isIsFlyer() {
		return _IsFlyer;
	}

	public void setIsFlyer(boolean IsFlyer) {
		_IsFlyer = IsFlyer;
	}

	public boolean getIsNewspaper() {
		return _IsNewspaper;
	}

	public boolean isIsNewspaper() {
		return _IsNewspaper;
	}

	public void setIsNewspaper(boolean IsNewspaper) {
		_IsNewspaper = IsNewspaper;
	}

	public boolean getIsWordOfMouth() {
		return _IsWordOfMouth;
	}

	public boolean isIsWordOfMouth() {
		return _IsWordOfMouth;
	}

	public void setIsWordOfMouth(boolean IsWordOfMouth) {
		_IsWordOfMouth = IsWordOfMouth;
	}

	public String getOther() {
		return _Other;
	}

	public void setOther(String Other) {
		_Other = Other;
	}

	public boolean getIsinterestedInVolunteringActivity() {
		return _IsinterestedInVolunteringActivity;
	}

	public boolean isIsinterestedInVolunteringActivity() {
		return _IsinterestedInVolunteringActivity;
	}

	public void setIsinterestedInVolunteringActivity(
		boolean IsinterestedInVolunteringActivity) {
		_IsinterestedInVolunteringActivity = IsinterestedInVolunteringActivity;
	}

	public boolean getIsinterestedInSatsangActivity() {
		return _IsinterestedInSatsangActivity;
	}

	public boolean isIsinterestedInSatsangActivity() {
		return _IsinterestedInSatsangActivity;
	}

	public void setIsinterestedInSatsangActivity(
		boolean IsinterestedInSatsangActivity) {
		_IsinterestedInSatsangActivity = IsinterestedInSatsangActivity;
	}

	public boolean getIsinterestedInChildrenActivity() {
		return _IsinterestedInChildrenActivity;
	}

	public boolean isIsinterestedInChildrenActivity() {
		return _IsinterestedInChildrenActivity;
	}

	public void setIsinterestedInChildrenActivity(
		boolean IsinterestedInChildrenActivity) {
		_IsinterestedInChildrenActivity = IsinterestedInChildrenActivity;
	}

	public Date getCreatedDate() {
		return _CreatedDate;
	}

	public void setCreatedDate(Date CreatedDate) {
		_CreatedDate = CreatedDate;
	}

	public Date getUpdatedDate() {
		return _UpdatedDate;
	}

	public void setUpdatedDate(Date UpdatedDate) {
		_UpdatedDate = UpdatedDate;
	}

	public String getAdditionalMember1Age() {
		return _AdditionalMember1Age;
	}

	public void setAdditionalMember1Age(String AdditionalMember1Age) {
		_AdditionalMember1Age = AdditionalMember1Age;
	}

	public String getAdditionalMember2Age() {
		return _AdditionalMember2Age;
	}

	public void setAdditionalMember2Age(String AdditionalMember2Age) {
		_AdditionalMember2Age = AdditionalMember2Age;
	}

	public String getAdditionalMember3Age() {
		return _AdditionalMember3Age;
	}

	public void setAdditionalMember3Age(String AdditionalMember3Age) {
		_AdditionalMember3Age = AdditionalMember3Age;
	}

	public String getSpouseFirstName() {
		return _SpouseFirstName;
	}

	public void setSpouseFirstName(String SpouseFirstName) {
		_SpouseFirstName = SpouseFirstName;
	}

	public String getSpouseLastName() {
		return _SpouseLastName;
	}

	public void setSpouseLastName(String SpouseLastName) {
		_SpouseLastName = SpouseLastName;
	}

	public String getSpousePhoneNumber() {
		return _SpousePhoneNumber;
	}

	public void setSpousePhoneNumber(String SpousePhoneNumber) {
		_SpousePhoneNumber = SpousePhoneNumber;
	}

	public String getMembershipPlan() {
		return _MembershipPlan;
	}

	public void setMembershipPlan(String MembershipPlan) {
		_MembershipPlan = MembershipPlan;
	}

	public String getPaymentMethod() {
		return _PaymentMethod;
	}

	public void setPaymentMethod(String PaymentMethod) {
		_PaymentMethod = PaymentMethod;
	}

	public String getStateCenter() {
		return _StateCenter;
	}

	public void setStateCenter(String StateCenter) {
		_StateCenter = StateCenter;
	}

	public String getProgramName() {
		return _ProgramName;
	}

	public void setProgramName(String ProgramName) {
		_ProgramName = ProgramName;
	}

	private long _ID;
	private String _FirstName;
	private String _LastName;
	private boolean _IsMale;
	private String _Address;
	private String _City;
	private String _State;
	private String _Zip;
	private String _PhoneNumber;
	private String _MobileNumber;
	private String _Email;
	private String _AdditionalMember1;
	private String _AdditionalMember2;
	private String _AdditionalMember3;
	private boolean _IsEmailCampain;
	private boolean _IsTemple;
	private boolean _IsOnlineNewsWebsite;
	private boolean _IsRadio;
	private boolean _IsYouTube;
	private boolean _IsFacebook;
	private boolean _IsMeetup;
	private boolean _IsOnlineEventCalendar;
	private boolean _IsFlyer;
	private boolean _IsNewspaper;
	private boolean _IsWordOfMouth;
	private String _Other;
	private boolean _IsinterestedInVolunteringActivity;
	private boolean _IsinterestedInSatsangActivity;
	private boolean _IsinterestedInChildrenActivity;
	private Date _CreatedDate;
	private Date _UpdatedDate;
	private String _AdditionalMember1Age;
	private String _AdditionalMember2Age;
	private String _AdditionalMember3Age;
	private String _SpouseFirstName;
	private String _SpouseLastName;
	private String _SpousePhoneNumber;
	private String _MembershipPlan;
	private String _PaymentMethod;
	private String _StateCenter;
	private String _ProgramName;
}