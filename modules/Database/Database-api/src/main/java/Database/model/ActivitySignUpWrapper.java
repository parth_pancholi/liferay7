/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ActivitySignUp}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ActivitySignUp
 * @generated
 */
@ProviderType
public class ActivitySignUpWrapper implements ActivitySignUp,
	ModelWrapper<ActivitySignUp> {
	public ActivitySignUpWrapper(ActivitySignUp activitySignUp) {
		_activitySignUp = activitySignUp;
	}

	@Override
	public Class<?> getModelClass() {
		return ActivitySignUp.class;
	}

	@Override
	public String getModelClassName() {
		return ActivitySignUp.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("ID", getID());
		attributes.put("FirstName", getFirstName());
		attributes.put("LastName", getLastName());
		attributes.put("MobileNumber", getMobileNumber());
		attributes.put("IsSatsangBiWeeklyMeeting", getIsSatsangBiWeeklyMeeting());
		attributes.put("IsBalmukund", getIsBalmukund());
		attributes.put("IsBhagvatGitaRecitation", getIsBhagvatGitaRecitation());
		attributes.put("IsYoga", getIsYoga());
		attributes.put("Is7MindSet", getIs7MindSet());
		attributes.put("IsOnlineBhagvatGitaDiscussion",
			getIsOnlineBhagvatGitaDiscussion());
		attributes.put("IsOnlineKidsMeditation", getIsOnlineKidsMeditation());
		attributes.put("IsOnlineYouthClub", getIsOnlineYouthClub());
		attributes.put("CreatedDate", getCreatedDate());
		attributes.put("UpdatedDate", getUpdatedDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long ID = (Long)attributes.get("ID");

		if (ID != null) {
			setID(ID);
		}

		String FirstName = (String)attributes.get("FirstName");

		if (FirstName != null) {
			setFirstName(FirstName);
		}

		String LastName = (String)attributes.get("LastName");

		if (LastName != null) {
			setLastName(LastName);
		}

		String MobileNumber = (String)attributes.get("MobileNumber");

		if (MobileNumber != null) {
			setMobileNumber(MobileNumber);
		}

		Boolean IsSatsangBiWeeklyMeeting = (Boolean)attributes.get(
				"IsSatsangBiWeeklyMeeting");

		if (IsSatsangBiWeeklyMeeting != null) {
			setIsSatsangBiWeeklyMeeting(IsSatsangBiWeeklyMeeting);
		}

		Boolean IsBalmukund = (Boolean)attributes.get("IsBalmukund");

		if (IsBalmukund != null) {
			setIsBalmukund(IsBalmukund);
		}

		Boolean IsBhagvatGitaRecitation = (Boolean)attributes.get(
				"IsBhagvatGitaRecitation");

		if (IsBhagvatGitaRecitation != null) {
			setIsBhagvatGitaRecitation(IsBhagvatGitaRecitation);
		}

		Boolean IsYoga = (Boolean)attributes.get("IsYoga");

		if (IsYoga != null) {
			setIsYoga(IsYoga);
		}

		Boolean Is7MindSet = (Boolean)attributes.get("Is7MindSet");

		if (Is7MindSet != null) {
			setIs7MindSet(Is7MindSet);
		}

		Boolean IsOnlineBhagvatGitaDiscussion = (Boolean)attributes.get(
				"IsOnlineBhagvatGitaDiscussion");

		if (IsOnlineBhagvatGitaDiscussion != null) {
			setIsOnlineBhagvatGitaDiscussion(IsOnlineBhagvatGitaDiscussion);
		}

		Boolean IsOnlineKidsMeditation = (Boolean)attributes.get(
				"IsOnlineKidsMeditation");

		if (IsOnlineKidsMeditation != null) {
			setIsOnlineKidsMeditation(IsOnlineKidsMeditation);
		}

		Boolean IsOnlineYouthClub = (Boolean)attributes.get("IsOnlineYouthClub");

		if (IsOnlineYouthClub != null) {
			setIsOnlineYouthClub(IsOnlineYouthClub);
		}

		Date CreatedDate = (Date)attributes.get("CreatedDate");

		if (CreatedDate != null) {
			setCreatedDate(CreatedDate);
		}

		Date UpdatedDate = (Date)attributes.get("UpdatedDate");

		if (UpdatedDate != null) {
			setUpdatedDate(UpdatedDate);
		}
	}

	@Override
	public Database.model.ActivitySignUp toEscapedModel() {
		return new ActivitySignUpWrapper(_activitySignUp.toEscapedModel());
	}

	@Override
	public Database.model.ActivitySignUp toUnescapedModel() {
		return new ActivitySignUpWrapper(_activitySignUp.toUnescapedModel());
	}

	/**
	* Returns the is7 mind set of this ActivitySignUp.
	*
	* @return the is7 mind set of this ActivitySignUp
	*/
	@Override
	public boolean getIs7MindSet() {
		return _activitySignUp.getIs7MindSet();
	}

	/**
	* Returns the is balmukund of this ActivitySignUp.
	*
	* @return the is balmukund of this ActivitySignUp
	*/
	@Override
	public boolean getIsBalmukund() {
		return _activitySignUp.getIsBalmukund();
	}

	/**
	* Returns the is bhagvat gita recitation of this ActivitySignUp.
	*
	* @return the is bhagvat gita recitation of this ActivitySignUp
	*/
	@Override
	public boolean getIsBhagvatGitaRecitation() {
		return _activitySignUp.getIsBhagvatGitaRecitation();
	}

	/**
	* Returns the is online bhagvat gita discussion of this ActivitySignUp.
	*
	* @return the is online bhagvat gita discussion of this ActivitySignUp
	*/
	@Override
	public boolean getIsOnlineBhagvatGitaDiscussion() {
		return _activitySignUp.getIsOnlineBhagvatGitaDiscussion();
	}

	/**
	* Returns the is online kids meditation of this ActivitySignUp.
	*
	* @return the is online kids meditation of this ActivitySignUp
	*/
	@Override
	public boolean getIsOnlineKidsMeditation() {
		return _activitySignUp.getIsOnlineKidsMeditation();
	}

	/**
	* Returns the is online youth club of this ActivitySignUp.
	*
	* @return the is online youth club of this ActivitySignUp
	*/
	@Override
	public boolean getIsOnlineYouthClub() {
		return _activitySignUp.getIsOnlineYouthClub();
	}

	/**
	* Returns the is satsang bi weekly meeting of this ActivitySignUp.
	*
	* @return the is satsang bi weekly meeting of this ActivitySignUp
	*/
	@Override
	public boolean getIsSatsangBiWeeklyMeeting() {
		return _activitySignUp.getIsSatsangBiWeeklyMeeting();
	}

	/**
	* Returns the is yoga of this ActivitySignUp.
	*
	* @return the is yoga of this ActivitySignUp
	*/
	@Override
	public boolean getIsYoga() {
		return _activitySignUp.getIsYoga();
	}

	@Override
	public boolean isCachedModel() {
		return _activitySignUp.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _activitySignUp.isEscapedModel();
	}

	/**
	* Returns <code>true</code> if this ActivitySignUp is is7 mind set.
	*
	* @return <code>true</code> if this ActivitySignUp is is7 mind set; <code>false</code> otherwise
	*/
	@Override
	public boolean isIs7MindSet() {
		return _activitySignUp.isIs7MindSet();
	}

	/**
	* Returns <code>true</code> if this ActivitySignUp is is balmukund.
	*
	* @return <code>true</code> if this ActivitySignUp is is balmukund; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsBalmukund() {
		return _activitySignUp.isIsBalmukund();
	}

	/**
	* Returns <code>true</code> if this ActivitySignUp is is bhagvat gita recitation.
	*
	* @return <code>true</code> if this ActivitySignUp is is bhagvat gita recitation; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsBhagvatGitaRecitation() {
		return _activitySignUp.isIsBhagvatGitaRecitation();
	}

	/**
	* Returns <code>true</code> if this ActivitySignUp is is online bhagvat gita discussion.
	*
	* @return <code>true</code> if this ActivitySignUp is is online bhagvat gita discussion; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsOnlineBhagvatGitaDiscussion() {
		return _activitySignUp.isIsOnlineBhagvatGitaDiscussion();
	}

	/**
	* Returns <code>true</code> if this ActivitySignUp is is online kids meditation.
	*
	* @return <code>true</code> if this ActivitySignUp is is online kids meditation; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsOnlineKidsMeditation() {
		return _activitySignUp.isIsOnlineKidsMeditation();
	}

	/**
	* Returns <code>true</code> if this ActivitySignUp is is online youth club.
	*
	* @return <code>true</code> if this ActivitySignUp is is online youth club; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsOnlineYouthClub() {
		return _activitySignUp.isIsOnlineYouthClub();
	}

	/**
	* Returns <code>true</code> if this ActivitySignUp is is satsang bi weekly meeting.
	*
	* @return <code>true</code> if this ActivitySignUp is is satsang bi weekly meeting; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsSatsangBiWeeklyMeeting() {
		return _activitySignUp.isIsSatsangBiWeeklyMeeting();
	}

	/**
	* Returns <code>true</code> if this ActivitySignUp is is yoga.
	*
	* @return <code>true</code> if this ActivitySignUp is is yoga; <code>false</code> otherwise
	*/
	@Override
	public boolean isIsYoga() {
		return _activitySignUp.isIsYoga();
	}

	@Override
	public boolean isNew() {
		return _activitySignUp.isNew();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _activitySignUp.getExpandoBridge();
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<Database.model.ActivitySignUp> toCacheModel() {
		return _activitySignUp.toCacheModel();
	}

	@Override
	public int compareTo(Database.model.ActivitySignUp activitySignUp) {
		return _activitySignUp.compareTo(activitySignUp);
	}

	@Override
	public int hashCode() {
		return _activitySignUp.hashCode();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _activitySignUp.getPrimaryKeyObj();
	}

	@Override
	public java.lang.Object clone() {
		return new ActivitySignUpWrapper((ActivitySignUp)_activitySignUp.clone());
	}

	/**
	* Returns the first name of this ActivitySignUp.
	*
	* @return the first name of this ActivitySignUp
	*/
	@Override
	public java.lang.String getFirstName() {
		return _activitySignUp.getFirstName();
	}

	/**
	* Returns the last name of this ActivitySignUp.
	*
	* @return the last name of this ActivitySignUp
	*/
	@Override
	public java.lang.String getLastName() {
		return _activitySignUp.getLastName();
	}

	/**
	* Returns the mobile number of this ActivitySignUp.
	*
	* @return the mobile number of this ActivitySignUp
	*/
	@Override
	public java.lang.String getMobileNumber() {
		return _activitySignUp.getMobileNumber();
	}

	@Override
	public java.lang.String toString() {
		return _activitySignUp.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _activitySignUp.toXmlString();
	}

	/**
	* Returns the created date of this ActivitySignUp.
	*
	* @return the created date of this ActivitySignUp
	*/
	@Override
	public Date getCreatedDate() {
		return _activitySignUp.getCreatedDate();
	}

	/**
	* Returns the updated date of this ActivitySignUp.
	*
	* @return the updated date of this ActivitySignUp
	*/
	@Override
	public Date getUpdatedDate() {
		return _activitySignUp.getUpdatedDate();
	}

	/**
	* Returns the ID of this ActivitySignUp.
	*
	* @return the ID of this ActivitySignUp
	*/
	@Override
	public long getID() {
		return _activitySignUp.getID();
	}

	/**
	* Returns the primary key of this ActivitySignUp.
	*
	* @return the primary key of this ActivitySignUp
	*/
	@Override
	public long getPrimaryKey() {
		return _activitySignUp.getPrimaryKey();
	}

	@Override
	public void persist() {
		_activitySignUp.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_activitySignUp.setCachedModel(cachedModel);
	}

	/**
	* Sets the created date of this ActivitySignUp.
	*
	* @param CreatedDate the created date of this ActivitySignUp
	*/
	@Override
	public void setCreatedDate(Date CreatedDate) {
		_activitySignUp.setCreatedDate(CreatedDate);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_activitySignUp.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_activitySignUp.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_activitySignUp.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the first name of this ActivitySignUp.
	*
	* @param FirstName the first name of this ActivitySignUp
	*/
	@Override
	public void setFirstName(java.lang.String FirstName) {
		_activitySignUp.setFirstName(FirstName);
	}

	/**
	* Sets the ID of this ActivitySignUp.
	*
	* @param ID the ID of this ActivitySignUp
	*/
	@Override
	public void setID(long ID) {
		_activitySignUp.setID(ID);
	}

	/**
	* Sets whether this ActivitySignUp is is7 mind set.
	*
	* @param Is7MindSet the is7 mind set of this ActivitySignUp
	*/
	@Override
	public void setIs7MindSet(boolean Is7MindSet) {
		_activitySignUp.setIs7MindSet(Is7MindSet);
	}

	/**
	* Sets whether this ActivitySignUp is is balmukund.
	*
	* @param IsBalmukund the is balmukund of this ActivitySignUp
	*/
	@Override
	public void setIsBalmukund(boolean IsBalmukund) {
		_activitySignUp.setIsBalmukund(IsBalmukund);
	}

	/**
	* Sets whether this ActivitySignUp is is bhagvat gita recitation.
	*
	* @param IsBhagvatGitaRecitation the is bhagvat gita recitation of this ActivitySignUp
	*/
	@Override
	public void setIsBhagvatGitaRecitation(boolean IsBhagvatGitaRecitation) {
		_activitySignUp.setIsBhagvatGitaRecitation(IsBhagvatGitaRecitation);
	}

	/**
	* Sets whether this ActivitySignUp is is online bhagvat gita discussion.
	*
	* @param IsOnlineBhagvatGitaDiscussion the is online bhagvat gita discussion of this ActivitySignUp
	*/
	@Override
	public void setIsOnlineBhagvatGitaDiscussion(
		boolean IsOnlineBhagvatGitaDiscussion) {
		_activitySignUp.setIsOnlineBhagvatGitaDiscussion(IsOnlineBhagvatGitaDiscussion);
	}

	/**
	* Sets whether this ActivitySignUp is is online kids meditation.
	*
	* @param IsOnlineKidsMeditation the is online kids meditation of this ActivitySignUp
	*/
	@Override
	public void setIsOnlineKidsMeditation(boolean IsOnlineKidsMeditation) {
		_activitySignUp.setIsOnlineKidsMeditation(IsOnlineKidsMeditation);
	}

	/**
	* Sets whether this ActivitySignUp is is online youth club.
	*
	* @param IsOnlineYouthClub the is online youth club of this ActivitySignUp
	*/
	@Override
	public void setIsOnlineYouthClub(boolean IsOnlineYouthClub) {
		_activitySignUp.setIsOnlineYouthClub(IsOnlineYouthClub);
	}

	/**
	* Sets whether this ActivitySignUp is is satsang bi weekly meeting.
	*
	* @param IsSatsangBiWeeklyMeeting the is satsang bi weekly meeting of this ActivitySignUp
	*/
	@Override
	public void setIsSatsangBiWeeklyMeeting(boolean IsSatsangBiWeeklyMeeting) {
		_activitySignUp.setIsSatsangBiWeeklyMeeting(IsSatsangBiWeeklyMeeting);
	}

	/**
	* Sets whether this ActivitySignUp is is yoga.
	*
	* @param IsYoga the is yoga of this ActivitySignUp
	*/
	@Override
	public void setIsYoga(boolean IsYoga) {
		_activitySignUp.setIsYoga(IsYoga);
	}

	/**
	* Sets the last name of this ActivitySignUp.
	*
	* @param LastName the last name of this ActivitySignUp
	*/
	@Override
	public void setLastName(java.lang.String LastName) {
		_activitySignUp.setLastName(LastName);
	}

	/**
	* Sets the mobile number of this ActivitySignUp.
	*
	* @param MobileNumber the mobile number of this ActivitySignUp
	*/
	@Override
	public void setMobileNumber(java.lang.String MobileNumber) {
		_activitySignUp.setMobileNumber(MobileNumber);
	}

	@Override
	public void setNew(boolean n) {
		_activitySignUp.setNew(n);
	}

	/**
	* Sets the primary key of this ActivitySignUp.
	*
	* @param primaryKey the primary key of this ActivitySignUp
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_activitySignUp.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_activitySignUp.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the updated date of this ActivitySignUp.
	*
	* @param UpdatedDate the updated date of this ActivitySignUp
	*/
	@Override
	public void setUpdatedDate(Date UpdatedDate) {
		_activitySignUp.setUpdatedDate(UpdatedDate);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ActivitySignUpWrapper)) {
			return false;
		}

		ActivitySignUpWrapper activitySignUpWrapper = (ActivitySignUpWrapper)obj;

		if (Objects.equals(_activitySignUp,
					activitySignUpWrapper._activitySignUp)) {
			return true;
		}

		return false;
	}

	@Override
	public ActivitySignUp getWrappedModel() {
		return _activitySignUp;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _activitySignUp.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _activitySignUp.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_activitySignUp.resetOriginalValues();
	}

	private final ActivitySignUp _activitySignUp;
}