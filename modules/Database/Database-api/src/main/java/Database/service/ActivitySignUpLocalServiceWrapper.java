/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ActivitySignUpLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ActivitySignUpLocalService
 * @generated
 */
@ProviderType
public class ActivitySignUpLocalServiceWrapper
	implements ActivitySignUpLocalService,
		ServiceWrapper<ActivitySignUpLocalService> {
	public ActivitySignUpLocalServiceWrapper(
		ActivitySignUpLocalService activitySignUpLocalService) {
		_activitySignUpLocalService = activitySignUpLocalService;
	}

	/**
	* Adds the ActivitySignUp to the database. Also notifies the appropriate model listeners.
	*
	* @param activitySignUp the ActivitySignUp
	* @return the ActivitySignUp that was added
	*/
	@Override
	public Database.model.ActivitySignUp addActivitySignUp(
		Database.model.ActivitySignUp activitySignUp) {
		return _activitySignUpLocalService.addActivitySignUp(activitySignUp);
	}

	/**
	* @param firstName
	* @param lastName
	* @param phoneNumber
	* @param isSatsangBiWeeklyMeeting
	* @param isBalmukund
	* @param isBhagvatGitaRecitation
	* @param isYoga
	* @param is7MindSet
	* @param isOnlineBhagvatGitaDiscussion
	* @param isOnlineKidsMeditation
	* @param isOnlineYouthClub
	* @return
	* @throws PortalException
	*/
	@Override
	public Database.model.ActivitySignUp addActivitySignUp(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String phoneNumber, boolean isSatsangBiWeeklyMeeting,
		boolean isBalmukund, boolean isBhagvatGitaRecitation, boolean isYoga,
		boolean is7MindSet, boolean isOnlineBhagvatGitaDiscussion,
		boolean isOnlineKidsMeditation, boolean isOnlineYouthClub)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _activitySignUpLocalService.addActivitySignUp(firstName,
			lastName, phoneNumber, isSatsangBiWeeklyMeeting, isBalmukund,
			isBhagvatGitaRecitation, isYoga, is7MindSet,
			isOnlineBhagvatGitaDiscussion, isOnlineKidsMeditation,
			isOnlineYouthClub);
	}

	/**
	* Creates a new ActivitySignUp with the primary key. Does not add the ActivitySignUp to the database.
	*
	* @param ID the primary key for the new ActivitySignUp
	* @return the new ActivitySignUp
	*/
	@Override
	public Database.model.ActivitySignUp createActivitySignUp(long ID) {
		return _activitySignUpLocalService.createActivitySignUp(ID);
	}

	/**
	* Deletes the ActivitySignUp from the database. Also notifies the appropriate model listeners.
	*
	* @param activitySignUp the ActivitySignUp
	* @return the ActivitySignUp that was removed
	*/
	@Override
	public Database.model.ActivitySignUp deleteActivitySignUp(
		Database.model.ActivitySignUp activitySignUp) {
		return _activitySignUpLocalService.deleteActivitySignUp(activitySignUp);
	}

	/**
	* Deletes the ActivitySignUp with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the ActivitySignUp
	* @return the ActivitySignUp that was removed
	* @throws PortalException if a ActivitySignUp with the primary key could not be found
	*/
	@Override
	public Database.model.ActivitySignUp deleteActivitySignUp(long ID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _activitySignUpLocalService.deleteActivitySignUp(ID);
	}

	/**
	* @param entry
	* @return
	* @throws PortalException
	*/
	@Override
	public Database.model.ActivitySignUp deleteEntry(
		Database.model.ActivitySignUp entry)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _activitySignUpLocalService.deleteEntry(entry);
	}

	@Override
	public Database.model.ActivitySignUp fetchActivitySignUp(long ID) {
		return _activitySignUpLocalService.fetchActivitySignUp(ID);
	}

	/**
	* Returns the ActivitySignUp with the primary key.
	*
	* @param ID the primary key of the ActivitySignUp
	* @return the ActivitySignUp
	* @throws PortalException if a ActivitySignUp with the primary key could not be found
	*/
	@Override
	public Database.model.ActivitySignUp getActivitySignUp(long ID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _activitySignUpLocalService.getActivitySignUp(ID);
	}

	/**
	* Updates the ActivitySignUp in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param activitySignUp the ActivitySignUp
	* @return the ActivitySignUp that was updated
	*/
	@Override
	public Database.model.ActivitySignUp updateActivitySignUp(
		Database.model.ActivitySignUp activitySignUp) {
		return _activitySignUpLocalService.updateActivitySignUp(activitySignUp);
	}

	/**
	* @param activitySignUpId
	* @param firstName
	* @param lastName
	* @param phoneNumber
	* @param isSatsangBiWeeklyMeeting
	* @param isBalmukund
	* @param isBhagvatGitaRecitation
	* @param isYoga
	* @param is7MindSet
	* @param isOnlineBhagvatGitaDiscussion
	* @param isOnlineKidsMeditation
	* @param isOnlineYouthClub
	* @return
	* @throws PortalException
	*/
	@Override
	public Database.model.ActivitySignUp updateActivitySignUp(
		long activitySignUpId, java.lang.String firstName,
		java.lang.String lastName, java.lang.String phoneNumber,
		boolean isSatsangBiWeeklyMeeting, boolean isBalmukund,
		boolean isBhagvatGitaRecitation, boolean isYoga, boolean is7MindSet,
		boolean isOnlineBhagvatGitaDiscussion, boolean isOnlineKidsMeditation,
		boolean isOnlineYouthClub)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _activitySignUpLocalService.updateActivitySignUp(activitySignUpId,
			firstName, lastName, phoneNumber, isSatsangBiWeeklyMeeting,
			isBalmukund, isBhagvatGitaRecitation, isYoga, is7MindSet,
			isOnlineBhagvatGitaDiscussion, isOnlineKidsMeditation,
			isOnlineYouthClub);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _activitySignUpLocalService.getActionableDynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _activitySignUpLocalService.dynamicQuery();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _activitySignUpLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _activitySignUpLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _activitySignUpLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* @return
	*/
	@Override
	public int getActivitySignUpCount() {
		return _activitySignUpLocalService.getActivitySignUpCount();
	}

	/**
	* Returns the number of ActivitySignUps.
	*
	* @return the number of ActivitySignUps
	*/
	@Override
	public int getActivitySignUpsCount() {
		return _activitySignUpLocalService.getActivitySignUpsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public java.lang.String getOSGiServiceIdentifier() {
		return _activitySignUpLocalService.getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _activitySignUpLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _activitySignUpLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _activitySignUpLocalService.dynamicQuery(dynamicQuery, start,
			end, orderByComparator);
	}

	/**
	* @param start
	* @param end
	* @return
	*/
	@Override
	public java.util.List<Database.model.ActivitySignUp> getActivitySignUp(
		int start, int end) {
		return _activitySignUpLocalService.getActivitySignUp(start, end);
	}

	/**
	* Returns a range of all the ActivitySignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ActivitySignUps
	* @param end the upper bound of the range of ActivitySignUps (not inclusive)
	* @return the range of ActivitySignUps
	*/
	@Override
	public java.util.List<Database.model.ActivitySignUp> getActivitySignUps(
		int start, int end) {
		return _activitySignUpLocalService.getActivitySignUps(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _activitySignUpLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _activitySignUpLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public ActivitySignUpLocalService getWrappedService() {
		return _activitySignUpLocalService;
	}

	@Override
	public void setWrappedService(
		ActivitySignUpLocalService activitySignUpLocalService) {
		_activitySignUpLocalService = activitySignUpLocalService;
	}

	private ActivitySignUpLocalService _activitySignUpLocalService;
}