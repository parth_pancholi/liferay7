/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.service.persistence;

import Database.exception.NoSuchRegisterException;

import Database.model.Register;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

/**
 * The persistence interface for the Register service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Database.service.persistence.impl.RegisterPersistenceImpl
 * @see RegisterUtil
 * @generated
 */
@ProviderType
public interface RegisterPersistence extends BasePersistence<Register> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link RegisterUtil} to access the Register persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the Registers where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @return the matching Registers
	*/
	public java.util.List<Register> findByLastName(java.lang.String LastName);

	/**
	* Returns a range of all the Registers where LastName LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LastName the last name
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @return the range of matching Registers
	*/
	public java.util.List<Register> findByLastName(java.lang.String LastName,
		int start, int end);

	/**
	* Returns an ordered range of all the Registers where LastName LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LastName the last name
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching Registers
	*/
	public java.util.List<Register> findByLastName(java.lang.String LastName,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator);

	/**
	* Returns an ordered range of all the Registers where LastName LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LastName the last name
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching Registers
	*/
	public java.util.List<Register> findByLastName(java.lang.String LastName,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first Register in the ordered set where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching Register
	* @throws NoSuchRegisterException if a matching Register could not be found
	*/
	public Register findByLastName_First(java.lang.String LastName,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException;

	/**
	* Returns the first Register in the ordered set where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching Register, or <code>null</code> if a matching Register could not be found
	*/
	public Register fetchByLastName_First(java.lang.String LastName,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator);

	/**
	* Returns the last Register in the ordered set where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching Register
	* @throws NoSuchRegisterException if a matching Register could not be found
	*/
	public Register findByLastName_Last(java.lang.String LastName,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException;

	/**
	* Returns the last Register in the ordered set where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching Register, or <code>null</code> if a matching Register could not be found
	*/
	public Register fetchByLastName_Last(java.lang.String LastName,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator);

	/**
	* Returns the Registers before and after the current Register in the ordered set where LastName LIKE &#63;.
	*
	* @param ID the primary key of the current Register
	* @param LastName the last name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next Register
	* @throws NoSuchRegisterException if a Register with the primary key could not be found
	*/
	public Register[] findByLastName_PrevAndNext(long ID,
		java.lang.String LastName,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException;

	/**
	* Removes all the Registers where LastName LIKE &#63; from the database.
	*
	* @param LastName the last name
	*/
	public void removeByLastName(java.lang.String LastName);

	/**
	* Returns the number of Registers where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @return the number of matching Registers
	*/
	public int countByLastName(java.lang.String LastName);

	/**
	* Returns all the Registers where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @return the matching Registers
	*/
	public java.util.List<Register> findByMobileNumber(
		java.lang.String MobileNumber);

	/**
	* Returns a range of all the Registers where MobileNumber LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param MobileNumber the mobile number
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @return the range of matching Registers
	*/
	public java.util.List<Register> findByMobileNumber(
		java.lang.String MobileNumber, int start, int end);

	/**
	* Returns an ordered range of all the Registers where MobileNumber LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param MobileNumber the mobile number
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching Registers
	*/
	public java.util.List<Register> findByMobileNumber(
		java.lang.String MobileNumber, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator);

	/**
	* Returns an ordered range of all the Registers where MobileNumber LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param MobileNumber the mobile number
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching Registers
	*/
	public java.util.List<Register> findByMobileNumber(
		java.lang.String MobileNumber, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first Register in the ordered set where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching Register
	* @throws NoSuchRegisterException if a matching Register could not be found
	*/
	public Register findByMobileNumber_First(java.lang.String MobileNumber,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException;

	/**
	* Returns the first Register in the ordered set where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching Register, or <code>null</code> if a matching Register could not be found
	*/
	public Register fetchByMobileNumber_First(java.lang.String MobileNumber,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator);

	/**
	* Returns the last Register in the ordered set where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching Register
	* @throws NoSuchRegisterException if a matching Register could not be found
	*/
	public Register findByMobileNumber_Last(java.lang.String MobileNumber,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException;

	/**
	* Returns the last Register in the ordered set where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching Register, or <code>null</code> if a matching Register could not be found
	*/
	public Register fetchByMobileNumber_Last(java.lang.String MobileNumber,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator);

	/**
	* Returns the Registers before and after the current Register in the ordered set where MobileNumber LIKE &#63;.
	*
	* @param ID the primary key of the current Register
	* @param MobileNumber the mobile number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next Register
	* @throws NoSuchRegisterException if a Register with the primary key could not be found
	*/
	public Register[] findByMobileNumber_PrevAndNext(long ID,
		java.lang.String MobileNumber,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator)
		throws NoSuchRegisterException;

	/**
	* Removes all the Registers where MobileNumber LIKE &#63; from the database.
	*
	* @param MobileNumber the mobile number
	*/
	public void removeByMobileNumber(java.lang.String MobileNumber);

	/**
	* Returns the number of Registers where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @return the number of matching Registers
	*/
	public int countByMobileNumber(java.lang.String MobileNumber);

	/**
	* Caches the Register in the entity cache if it is enabled.
	*
	* @param register the Register
	*/
	public void cacheResult(Register register);

	/**
	* Caches the Registers in the entity cache if it is enabled.
	*
	* @param registers the Registers
	*/
	public void cacheResult(java.util.List<Register> registers);

	/**
	* Creates a new Register with the primary key. Does not add the Register to the database.
	*
	* @param ID the primary key for the new Register
	* @return the new Register
	*/
	public Register create(long ID);

	/**
	* Removes the Register with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the Register
	* @return the Register that was removed
	* @throws NoSuchRegisterException if a Register with the primary key could not be found
	*/
	public Register remove(long ID) throws NoSuchRegisterException;

	public Register updateImpl(Register register);

	/**
	* Returns the Register with the primary key or throws a {@link NoSuchRegisterException} if it could not be found.
	*
	* @param ID the primary key of the Register
	* @return the Register
	* @throws NoSuchRegisterException if a Register with the primary key could not be found
	*/
	public Register findByPrimaryKey(long ID) throws NoSuchRegisterException;

	/**
	* Returns the Register with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the Register
	* @return the Register, or <code>null</code> if a Register with the primary key could not be found
	*/
	public Register fetchByPrimaryKey(long ID);

	@Override
	public java.util.Map<java.io.Serializable, Register> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the Registers.
	*
	* @return the Registers
	*/
	public java.util.List<Register> findAll();

	/**
	* Returns a range of all the Registers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @return the range of Registers
	*/
	public java.util.List<Register> findAll(int start, int end);

	/**
	* Returns an ordered range of all the Registers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of Registers
	*/
	public java.util.List<Register> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator);

	/**
	* Returns an ordered range of all the Registers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of Registers
	*/
	public java.util.List<Register> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Register> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the Registers from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of Registers.
	*
	* @return the number of Registers
	*/
	public int countAll();
}