/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for Register. This utility wraps
 * {@link Database.service.impl.RegisterLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see RegisterLocalService
 * @see Database.service.base.RegisterLocalServiceBaseImpl
 * @see Database.service.impl.RegisterLocalServiceImpl
 * @generated
 */
@ProviderType
public class RegisterLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link Database.service.impl.RegisterLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* @param firstName
	* @param lastName
	* @param isMale
	* @param address
	* @param city
	* @param state
	* @param zip
	* @param phoneNumber
	* @param mobileNumber
	* @param email
	* @param additionalMember1
	* @param additionalMember2
	* @param additionalMember3
	* @param isEmailCampain
	* @param isTemple
	* @param isOnlineNewsWebsite
	* @param isRadio
	* @param isYouTube
	* @param isFacebook
	* @param isMeetup
	* @param isOnlineEventCalendar
	* @param isFlyer
	* @param isNewspaper
	* @param isWordOfMouth
	* @param other
	* @param isinterestedInVolunteringActivity
	* @param isinterestedInSatsangActivity
	* @param isinterestedInChildrenActivity
	* @return
	* @throws PortalException
	*/
	public static Database.model.Register RegisterMember(
		java.lang.String firstName, java.lang.String lastName, boolean isMale,
		java.lang.String address, java.lang.String city,
		java.lang.String state, java.lang.String zip,
		java.lang.String phoneNumber, java.lang.String mobileNumber,
		java.lang.String email, java.lang.String additionalMember1,
		java.lang.String additionalMember2, java.lang.String additionalMember3,
		boolean isEmailCampain, boolean isTemple, boolean isOnlineNewsWebsite,
		boolean isRadio, boolean isYouTube, boolean isFacebook,
		boolean isMeetup, boolean isOnlineEventCalendar, boolean isFlyer,
		boolean isNewspaper, boolean isWordOfMouth, java.lang.String other,
		boolean isinterestedInVolunteringActivity,
		boolean isinterestedInSatsangActivity,
		boolean isinterestedInChildrenActivity,
		java.lang.String additionalMember1Age,
		java.lang.String additionalMember2Age,
		java.lang.String additionalMember3Age,
		java.lang.String spouseFirstName, java.lang.String spouseLastName,
		java.lang.String spousePhoneNumber, java.lang.String membershipPlan,
		java.lang.String paymentMethod)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .RegisterMember(firstName, lastName, isMale, address, city,
			state, zip, phoneNumber, mobileNumber, email, additionalMember1,
			additionalMember2, additionalMember3, isEmailCampain, isTemple,
			isOnlineNewsWebsite, isRadio, isYouTube, isFacebook, isMeetup,
			isOnlineEventCalendar, isFlyer, isNewspaper, isWordOfMouth, other,
			isinterestedInVolunteringActivity, isinterestedInSatsangActivity,
			isinterestedInChildrenActivity, additionalMember1Age,
			additionalMember2Age, additionalMember3Age, spouseFirstName,
			spouseLastName, spousePhoneNumber, membershipPlan, paymentMethod);
	}

	/**
	* Adds the Register to the database. Also notifies the appropriate model listeners.
	*
	* @param register the Register
	* @return the Register that was added
	*/
	public static Database.model.Register addRegister(
		Database.model.Register register) {
		return getService().addRegister(register);
	}

	/**
	* Creates a new Register with the primary key. Does not add the Register to the database.
	*
	* @param ID the primary key for the new Register
	* @return the new Register
	*/
	public static Database.model.Register createRegister(long ID) {
		return getService().createRegister(ID);
	}

	/**
	* @param entry
	* @return
	* @throws PortalException
	*/
	public static Database.model.Register deleteEntry(
		Database.model.Register entry)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteEntry(entry);
	}

	/**
	* Deletes the Register from the database. Also notifies the appropriate model listeners.
	*
	* @param register the Register
	* @return the Register that was removed
	*/
	public static Database.model.Register deleteRegister(
		Database.model.Register register) {
		return getService().deleteRegister(register);
	}

	/**
	* Deletes the Register with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the Register
	* @return the Register that was removed
	* @throws PortalException if a Register with the primary key could not be found
	*/
	public static Database.model.Register deleteRegister(long ID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteRegister(ID);
	}

	public static Database.model.Register fetchRegister(long ID) {
		return getService().fetchRegister(ID);
	}

	/**
	* Returns the Register with the primary key.
	*
	* @param ID the primary key of the Register
	* @return the Register
	* @throws PortalException if a Register with the primary key could not be found
	*/
	public static Database.model.Register getRegister(long ID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getRegister(ID);
	}

	/**
	* Updates the Register in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param register the Register
	* @return the Register that was updated
	*/
	public static Database.model.Register updateRegister(
		Database.model.Register register) {
		return getService().updateRegister(register);
	}

	/**
	* @param registerId
	* @param firstName
	* @param lastName
	* @param isMale
	* @param address
	* @param city
	* @param state
	* @param zip
	* @param phoneNumber
	* @param mobileNumber
	* @param email
	* @param additionalMember1
	* @param additionalMember2
	* @param additionalMember3
	* @param isEmailCampain
	* @param isTemple
	* @param isOnlineNewsWebsite
	* @param isRadio
	* @param isYouTube
	* @param isFacebook
	* @param isMeetup
	* @param isOnlineEventCalendar
	* @param isFlyer
	* @param isNewspaper
	* @param isWordOfMouth
	* @param other
	* @param isinterestedInVolunteringActivity
	* @param isinterestedInSatsangActivity
	* @param isinterestedInChildrenActivity
	* @return
	* @throws PortalException
	*/
	public static Database.model.Register updateRegisterMember(
		long registerId, java.lang.String firstName, java.lang.String lastName,
		boolean isMale, java.lang.String address, java.lang.String city,
		java.lang.String state, java.lang.String zip,
		java.lang.String phoneNumber, java.lang.String mobileNumber,
		java.lang.String email, java.lang.String additionalMember1,
		java.lang.String additionalMember2, java.lang.String additionalMember3,
		boolean isEmailCampain, boolean isTemple, boolean isOnlineNewsWebsite,
		boolean isRadio, boolean isYouTube, boolean isFacebook,
		boolean isMeetup, boolean isOnlineEventCalendar, boolean isFlyer,
		boolean isNewspaper, boolean isWordOfMouth, java.lang.String other,
		boolean isinterestedInVolunteringActivity,
		boolean isinterestedInSatsangActivity,
		boolean isinterestedInChildrenActivity,
		java.lang.String additionalMember1Age,
		java.lang.String additionalMember2Age,
		java.lang.String additionalMember3Age,
		java.lang.String spouseFirstName, java.lang.String spouseLastName,
		java.lang.String spousePhoneNumber, java.lang.String membershipPlan,
		java.lang.String paymentMethod)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .updateRegisterMember(registerId, firstName, lastName,
			isMale, address, city, state, zip, phoneNumber, mobileNumber,
			email, additionalMember1, additionalMember2, additionalMember3,
			isEmailCampain, isTemple, isOnlineNewsWebsite, isRadio, isYouTube,
			isFacebook, isMeetup, isOnlineEventCalendar, isFlyer, isNewspaper,
			isWordOfMouth, other, isinterestedInVolunteringActivity,
			isinterestedInSatsangActivity, isinterestedInChildrenActivity,
			additionalMember1Age, additionalMember2Age, additionalMember3Age,
			spouseFirstName, spouseLastName, spousePhoneNumber, membershipPlan,
			paymentMethod);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* @return
	*/
	public static int getRegisterMembersCount() {
		return getService().getRegisterMembersCount();
	}

	/**
	* Returns the number of Registers.
	*
	* @return the number of Registers
	*/
	public static int getRegistersCount() {
		return getService().getRegistersCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	public static java.util.List<Database.model.Register> findByLastNameOrMobile(
		java.lang.String lastName, java.lang.String mobileNumber, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getService()
				   .findByLastNameOrMobile(lastName, mobileNumber, start, end,
			orderByComparator);
	}

	/**
	* @param start
	* @param end
	* @return
	*/
	public static java.util.List<Database.model.Register> getRegisterMembers(
		int start, int end) {
		return getService().getRegisterMembers(start, end);
	}

	/**
	* Returns a range of all the Registers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @return the range of Registers
	*/
	public static java.util.List<Database.model.Register> getRegisters(
		int start, int end) {
		return getService().getRegisters(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static RegisterLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<RegisterLocalService, RegisterLocalService> _serviceTracker =
		ServiceTrackerFactory.open(RegisterLocalService.class);
}