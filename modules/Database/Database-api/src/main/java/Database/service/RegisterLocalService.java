/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.service;

import Database.model.Register;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery;
import com.liferay.portal.kernel.dao.orm.Projection;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.BaseLocalService;
import com.liferay.portal.kernel.service.PersistedModelLocalService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.OrderByComparator;

import java.io.Serializable;

import java.util.List;

/**
 * Provides the local service interface for Register. Methods of this
 * service will not have security checks based on the propagated JAAS
 * credentials because this service can only be accessed from within the same
 * VM.
 *
 * @author Brian Wing Shun Chan
 * @see RegisterLocalServiceUtil
 * @see Database.service.base.RegisterLocalServiceBaseImpl
 * @see Database.service.impl.RegisterLocalServiceImpl
 * @generated
 */
@ProviderType
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
	PortalException.class, SystemException.class})
public interface RegisterLocalService extends BaseLocalService,
	PersistedModelLocalService {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link RegisterLocalServiceUtil} to access the Register local service. Add custom service methods to {@link Database.service.impl.RegisterLocalServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */

	/**
	* @param firstName
	* @param lastName
	* @param isMale
	* @param address
	* @param city
	* @param state
	* @param zip
	* @param phoneNumber
	* @param mobileNumber
	* @param email
	* @param additionalMember1
	* @param additionalMember2
	* @param additionalMember3
	* @param isEmailCampain
	* @param isTemple
	* @param isOnlineNewsWebsite
	* @param isRadio
	* @param isYouTube
	* @param isFacebook
	* @param isMeetup
	* @param isOnlineEventCalendar
	* @param isFlyer
	* @param isNewspaper
	* @param isWordOfMouth
	* @param other
	* @param isinterestedInVolunteringActivity
	* @param isinterestedInSatsangActivity
	* @param isinterestedInChildrenActivity
	* @return
	* @throws PortalException
	*/
	public Register RegisterMember(java.lang.String firstName,
		java.lang.String lastName, boolean isMale, java.lang.String address,
		java.lang.String city, java.lang.String state, java.lang.String zip,
		java.lang.String phoneNumber, java.lang.String mobileNumber,
		java.lang.String email, java.lang.String additionalMember1,
		java.lang.String additionalMember2, java.lang.String additionalMember3,
		boolean isEmailCampain, boolean isTemple, boolean isOnlineNewsWebsite,
		boolean isRadio, boolean isYouTube, boolean isFacebook,
		boolean isMeetup, boolean isOnlineEventCalendar, boolean isFlyer,
		boolean isNewspaper, boolean isWordOfMouth, java.lang.String other,
		boolean isinterestedInVolunteringActivity,
		boolean isinterestedInSatsangActivity,
		boolean isinterestedInChildrenActivity,
		java.lang.String additionalMember1Age,
		java.lang.String additionalMember2Age,
		java.lang.String additionalMember3Age,
		java.lang.String spouseFirstName, java.lang.String spouseLastName,
		java.lang.String spousePhoneNumber, java.lang.String membershipPlan,
		java.lang.String paymentMethod) throws PortalException;

	/**
	* Adds the Register to the database. Also notifies the appropriate model listeners.
	*
	* @param register the Register
	* @return the Register that was added
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Register addRegister(Register register);

	/**
	* Creates a new Register with the primary key. Does not add the Register to the database.
	*
	* @param ID the primary key for the new Register
	* @return the new Register
	*/
	public Register createRegister(long ID);

	/**
	* @param entry
	* @return
	* @throws PortalException
	*/
	public Register deleteEntry(Register entry) throws PortalException;

	/**
	* Deletes the Register from the database. Also notifies the appropriate model listeners.
	*
	* @param register the Register
	* @return the Register that was removed
	*/
	@Indexable(type = IndexableType.DELETE)
	public Register deleteRegister(Register register);

	/**
	* Deletes the Register with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the Register
	* @return the Register that was removed
	* @throws PortalException if a Register with the primary key could not be found
	*/
	@Indexable(type = IndexableType.DELETE)
	public Register deleteRegister(long ID) throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Register fetchRegister(long ID);

	/**
	* Returns the Register with the primary key.
	*
	* @param ID the primary key of the Register
	* @return the Register
	* @throws PortalException if a Register with the primary key could not be found
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Register getRegister(long ID) throws PortalException;

	/**
	* Updates the Register in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param register the Register
	* @return the Register that was updated
	*/
	@Indexable(type = IndexableType.REINDEX)
	public Register updateRegister(Register register);

	/**
	* @param registerId
	* @param firstName
	* @param lastName
	* @param isMale
	* @param address
	* @param city
	* @param state
	* @param zip
	* @param phoneNumber
	* @param mobileNumber
	* @param email
	* @param additionalMember1
	* @param additionalMember2
	* @param additionalMember3
	* @param isEmailCampain
	* @param isTemple
	* @param isOnlineNewsWebsite
	* @param isRadio
	* @param isYouTube
	* @param isFacebook
	* @param isMeetup
	* @param isOnlineEventCalendar
	* @param isFlyer
	* @param isNewspaper
	* @param isWordOfMouth
	* @param other
	* @param isinterestedInVolunteringActivity
	* @param isinterestedInSatsangActivity
	* @param isinterestedInChildrenActivity
	* @return
	* @throws PortalException
	*/
	public Register updateRegisterMember(long registerId,
		java.lang.String firstName, java.lang.String lastName, boolean isMale,
		java.lang.String address, java.lang.String city,
		java.lang.String state, java.lang.String zip,
		java.lang.String phoneNumber, java.lang.String mobileNumber,
		java.lang.String email, java.lang.String additionalMember1,
		java.lang.String additionalMember2, java.lang.String additionalMember3,
		boolean isEmailCampain, boolean isTemple, boolean isOnlineNewsWebsite,
		boolean isRadio, boolean isYouTube, boolean isFacebook,
		boolean isMeetup, boolean isOnlineEventCalendar, boolean isFlyer,
		boolean isNewspaper, boolean isWordOfMouth, java.lang.String other,
		boolean isinterestedInVolunteringActivity,
		boolean isinterestedInSatsangActivity,
		boolean isinterestedInChildrenActivity,
		java.lang.String additionalMember1Age,
		java.lang.String additionalMember2Age,
		java.lang.String additionalMember3Age,
		java.lang.String spouseFirstName, java.lang.String spouseLastName,
		java.lang.String spousePhoneNumber, java.lang.String membershipPlan,
		java.lang.String paymentMethod) throws PortalException;

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public ActionableDynamicQuery getActionableDynamicQuery();

	public DynamicQuery dynamicQuery();

	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public IndexableActionableDynamicQuery getIndexableActionableDynamicQuery();

	/**
	* @throws PortalException
	*/
	@Override
	public PersistedModel deletePersistedModel(PersistedModel persistedModel)
		throws PortalException;

	@Override
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public PersistedModel getPersistedModel(Serializable primaryKeyObj)
		throws PortalException;

	/**
	* @return
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getRegisterMembersCount();

	/**
	* Returns the number of Registers.
	*
	* @return the number of Registers
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public int getRegistersCount();

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public java.lang.String getOSGiServiceIdentifier();

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery);

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end);

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public <T> List<T> dynamicQuery(DynamicQuery dynamicQuery, int start,
		int end, OrderByComparator<T> orderByComparator);

	public List<Register> findByLastNameOrMobile(java.lang.String lastName,
		java.lang.String mobileNumber, int start, int end,
		OrderByComparator orderByComparator) throws SystemException;

	/**
	* @param start
	* @param end
	* @return
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Register> getRegisterMembers(int start, int end);

	/**
	* Returns a range of all the Registers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @return the range of Registers
	*/
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Register> getRegisters(int start, int end);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery);

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public long dynamicQueryCount(DynamicQuery dynamicQuery,
		Projection projection);
}