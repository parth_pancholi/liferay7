/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ActivitySignUp. This utility wraps
 * {@link Database.service.impl.ActivitySignUpLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ActivitySignUpLocalService
 * @see Database.service.base.ActivitySignUpLocalServiceBaseImpl
 * @see Database.service.impl.ActivitySignUpLocalServiceImpl
 * @generated
 */
@ProviderType
public class ActivitySignUpLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link Database.service.impl.ActivitySignUpLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the ActivitySignUp to the database. Also notifies the appropriate model listeners.
	*
	* @param activitySignUp the ActivitySignUp
	* @return the ActivitySignUp that was added
	*/
	public static Database.model.ActivitySignUp addActivitySignUp(
		Database.model.ActivitySignUp activitySignUp) {
		return getService().addActivitySignUp(activitySignUp);
	}

	/**
	* @param firstName
	* @param lastName
	* @param phoneNumber
	* @param isSatsangBiWeeklyMeeting
	* @param isBalmukund
	* @param isBhagvatGitaRecitation
	* @param isYoga
	* @param is7MindSet
	* @param isOnlineBhagvatGitaDiscussion
	* @param isOnlineKidsMeditation
	* @param isOnlineYouthClub
	* @return
	* @throws PortalException
	*/
	public static Database.model.ActivitySignUp addActivitySignUp(
		java.lang.String firstName, java.lang.String lastName,
		java.lang.String phoneNumber, boolean isSatsangBiWeeklyMeeting,
		boolean isBalmukund, boolean isBhagvatGitaRecitation, boolean isYoga,
		boolean is7MindSet, boolean isOnlineBhagvatGitaDiscussion,
		boolean isOnlineKidsMeditation, boolean isOnlineYouthClub)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .addActivitySignUp(firstName, lastName, phoneNumber,
			isSatsangBiWeeklyMeeting, isBalmukund, isBhagvatGitaRecitation,
			isYoga, is7MindSet, isOnlineBhagvatGitaDiscussion,
			isOnlineKidsMeditation, isOnlineYouthClub);
	}

	/**
	* Creates a new ActivitySignUp with the primary key. Does not add the ActivitySignUp to the database.
	*
	* @param ID the primary key for the new ActivitySignUp
	* @return the new ActivitySignUp
	*/
	public static Database.model.ActivitySignUp createActivitySignUp(long ID) {
		return getService().createActivitySignUp(ID);
	}

	/**
	* Deletes the ActivitySignUp from the database. Also notifies the appropriate model listeners.
	*
	* @param activitySignUp the ActivitySignUp
	* @return the ActivitySignUp that was removed
	*/
	public static Database.model.ActivitySignUp deleteActivitySignUp(
		Database.model.ActivitySignUp activitySignUp) {
		return getService().deleteActivitySignUp(activitySignUp);
	}

	/**
	* Deletes the ActivitySignUp with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the ActivitySignUp
	* @return the ActivitySignUp that was removed
	* @throws PortalException if a ActivitySignUp with the primary key could not be found
	*/
	public static Database.model.ActivitySignUp deleteActivitySignUp(long ID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteActivitySignUp(ID);
	}

	/**
	* @param entry
	* @return
	* @throws PortalException
	*/
	public static Database.model.ActivitySignUp deleteEntry(
		Database.model.ActivitySignUp entry)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteEntry(entry);
	}

	public static Database.model.ActivitySignUp fetchActivitySignUp(long ID) {
		return getService().fetchActivitySignUp(ID);
	}

	/**
	* Returns the ActivitySignUp with the primary key.
	*
	* @param ID the primary key of the ActivitySignUp
	* @return the ActivitySignUp
	* @throws PortalException if a ActivitySignUp with the primary key could not be found
	*/
	public static Database.model.ActivitySignUp getActivitySignUp(long ID)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getActivitySignUp(ID);
	}

	/**
	* Updates the ActivitySignUp in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param activitySignUp the ActivitySignUp
	* @return the ActivitySignUp that was updated
	*/
	public static Database.model.ActivitySignUp updateActivitySignUp(
		Database.model.ActivitySignUp activitySignUp) {
		return getService().updateActivitySignUp(activitySignUp);
	}

	/**
	* @param activitySignUpId
	* @param firstName
	* @param lastName
	* @param phoneNumber
	* @param isSatsangBiWeeklyMeeting
	* @param isBalmukund
	* @param isBhagvatGitaRecitation
	* @param isYoga
	* @param is7MindSet
	* @param isOnlineBhagvatGitaDiscussion
	* @param isOnlineKidsMeditation
	* @param isOnlineYouthClub
	* @return
	* @throws PortalException
	*/
	public static Database.model.ActivitySignUp updateActivitySignUp(
		long activitySignUpId, java.lang.String firstName,
		java.lang.String lastName, java.lang.String phoneNumber,
		boolean isSatsangBiWeeklyMeeting, boolean isBalmukund,
		boolean isBhagvatGitaRecitation, boolean isYoga, boolean is7MindSet,
		boolean isOnlineBhagvatGitaDiscussion, boolean isOnlineKidsMeditation,
		boolean isOnlineYouthClub)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService()
				   .updateActivitySignUp(activitySignUpId, firstName, lastName,
			phoneNumber, isSatsangBiWeeklyMeeting, isBalmukund,
			isBhagvatGitaRecitation, isYoga, is7MindSet,
			isOnlineBhagvatGitaDiscussion, isOnlineKidsMeditation,
			isOnlineYouthClub);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* @return
	*/
	public static int getActivitySignUpCount() {
		return getService().getActivitySignUpCount();
	}

	/**
	* Returns the number of ActivitySignUps.
	*
	* @return the number of ActivitySignUps
	*/
	public static int getActivitySignUpsCount() {
		return getService().getActivitySignUpsCount();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static java.lang.String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* @param start
	* @param end
	* @return
	*/
	public static java.util.List<Database.model.ActivitySignUp> getActivitySignUp(
		int start, int end) {
		return getService().getActivitySignUp(start, end);
	}

	/**
	* Returns a range of all the ActivitySignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link Database.model.impl.ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ActivitySignUps
	* @param end the upper bound of the range of ActivitySignUps (not inclusive)
	* @return the range of ActivitySignUps
	*/
	public static java.util.List<Database.model.ActivitySignUp> getActivitySignUps(
		int start, int end) {
		return getService().getActivitySignUps(start, end);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static ActivitySignUpLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ActivitySignUpLocalService, ActivitySignUpLocalService> _serviceTracker =
		ServiceTrackerFactory.open(ActivitySignUpLocalService.class);
}