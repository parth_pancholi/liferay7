/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.service.persistence;

import Database.model.Register;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the Register service. This utility wraps {@link Database.service.persistence.impl.RegisterPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RegisterPersistence
 * @see Database.service.persistence.impl.RegisterPersistenceImpl
 * @generated
 */
@ProviderType
public class RegisterUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Register register) {
		getPersistence().clearCache(register);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Register> findWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Register> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Register> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Register> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Register update(Register register) {
		return getPersistence().update(register);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Register update(Register register,
		ServiceContext serviceContext) {
		return getPersistence().update(register, serviceContext);
	}

	/**
	* Returns all the Registers where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @return the matching Registers
	*/
	public static List<Register> findByLastName(java.lang.String LastName) {
		return getPersistence().findByLastName(LastName);
	}

	/**
	* Returns a range of all the Registers where LastName LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LastName the last name
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @return the range of matching Registers
	*/
	public static List<Register> findByLastName(java.lang.String LastName,
		int start, int end) {
		return getPersistence().findByLastName(LastName, start, end);
	}

	/**
	* Returns an ordered range of all the Registers where LastName LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LastName the last name
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching Registers
	*/
	public static List<Register> findByLastName(java.lang.String LastName,
		int start, int end, OrderByComparator<Register> orderByComparator) {
		return getPersistence()
				   .findByLastName(LastName, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the Registers where LastName LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param LastName the last name
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching Registers
	*/
	public static List<Register> findByLastName(java.lang.String LastName,
		int start, int end, OrderByComparator<Register> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByLastName(LastName, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first Register in the ordered set where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching Register
	* @throws NoSuchRegisterException if a matching Register could not be found
	*/
	public static Register findByLastName_First(java.lang.String LastName,
		OrderByComparator<Register> orderByComparator)
		throws Database.exception.NoSuchRegisterException {
		return getPersistence().findByLastName_First(LastName, orderByComparator);
	}

	/**
	* Returns the first Register in the ordered set where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching Register, or <code>null</code> if a matching Register could not be found
	*/
	public static Register fetchByLastName_First(java.lang.String LastName,
		OrderByComparator<Register> orderByComparator) {
		return getPersistence()
				   .fetchByLastName_First(LastName, orderByComparator);
	}

	/**
	* Returns the last Register in the ordered set where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching Register
	* @throws NoSuchRegisterException if a matching Register could not be found
	*/
	public static Register findByLastName_Last(java.lang.String LastName,
		OrderByComparator<Register> orderByComparator)
		throws Database.exception.NoSuchRegisterException {
		return getPersistence().findByLastName_Last(LastName, orderByComparator);
	}

	/**
	* Returns the last Register in the ordered set where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching Register, or <code>null</code> if a matching Register could not be found
	*/
	public static Register fetchByLastName_Last(java.lang.String LastName,
		OrderByComparator<Register> orderByComparator) {
		return getPersistence().fetchByLastName_Last(LastName, orderByComparator);
	}

	/**
	* Returns the Registers before and after the current Register in the ordered set where LastName LIKE &#63;.
	*
	* @param ID the primary key of the current Register
	* @param LastName the last name
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next Register
	* @throws NoSuchRegisterException if a Register with the primary key could not be found
	*/
	public static Register[] findByLastName_PrevAndNext(long ID,
		java.lang.String LastName, OrderByComparator<Register> orderByComparator)
		throws Database.exception.NoSuchRegisterException {
		return getPersistence()
				   .findByLastName_PrevAndNext(ID, LastName, orderByComparator);
	}

	/**
	* Removes all the Registers where LastName LIKE &#63; from the database.
	*
	* @param LastName the last name
	*/
	public static void removeByLastName(java.lang.String LastName) {
		getPersistence().removeByLastName(LastName);
	}

	/**
	* Returns the number of Registers where LastName LIKE &#63;.
	*
	* @param LastName the last name
	* @return the number of matching Registers
	*/
	public static int countByLastName(java.lang.String LastName) {
		return getPersistence().countByLastName(LastName);
	}

	/**
	* Returns all the Registers where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @return the matching Registers
	*/
	public static List<Register> findByMobileNumber(
		java.lang.String MobileNumber) {
		return getPersistence().findByMobileNumber(MobileNumber);
	}

	/**
	* Returns a range of all the Registers where MobileNumber LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param MobileNumber the mobile number
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @return the range of matching Registers
	*/
	public static List<Register> findByMobileNumber(
		java.lang.String MobileNumber, int start, int end) {
		return getPersistence().findByMobileNumber(MobileNumber, start, end);
	}

	/**
	* Returns an ordered range of all the Registers where MobileNumber LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param MobileNumber the mobile number
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching Registers
	*/
	public static List<Register> findByMobileNumber(
		java.lang.String MobileNumber, int start, int end,
		OrderByComparator<Register> orderByComparator) {
		return getPersistence()
				   .findByMobileNumber(MobileNumber, start, end,
			orderByComparator);
	}

	/**
	* Returns an ordered range of all the Registers where MobileNumber LIKE &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param MobileNumber the mobile number
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching Registers
	*/
	public static List<Register> findByMobileNumber(
		java.lang.String MobileNumber, int start, int end,
		OrderByComparator<Register> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findByMobileNumber(MobileNumber, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first Register in the ordered set where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching Register
	* @throws NoSuchRegisterException if a matching Register could not be found
	*/
	public static Register findByMobileNumber_First(
		java.lang.String MobileNumber,
		OrderByComparator<Register> orderByComparator)
		throws Database.exception.NoSuchRegisterException {
		return getPersistence()
				   .findByMobileNumber_First(MobileNumber, orderByComparator);
	}

	/**
	* Returns the first Register in the ordered set where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching Register, or <code>null</code> if a matching Register could not be found
	*/
	public static Register fetchByMobileNumber_First(
		java.lang.String MobileNumber,
		OrderByComparator<Register> orderByComparator) {
		return getPersistence()
				   .fetchByMobileNumber_First(MobileNumber, orderByComparator);
	}

	/**
	* Returns the last Register in the ordered set where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching Register
	* @throws NoSuchRegisterException if a matching Register could not be found
	*/
	public static Register findByMobileNumber_Last(
		java.lang.String MobileNumber,
		OrderByComparator<Register> orderByComparator)
		throws Database.exception.NoSuchRegisterException {
		return getPersistence()
				   .findByMobileNumber_Last(MobileNumber, orderByComparator);
	}

	/**
	* Returns the last Register in the ordered set where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching Register, or <code>null</code> if a matching Register could not be found
	*/
	public static Register fetchByMobileNumber_Last(
		java.lang.String MobileNumber,
		OrderByComparator<Register> orderByComparator) {
		return getPersistence()
				   .fetchByMobileNumber_Last(MobileNumber, orderByComparator);
	}

	/**
	* Returns the Registers before and after the current Register in the ordered set where MobileNumber LIKE &#63;.
	*
	* @param ID the primary key of the current Register
	* @param MobileNumber the mobile number
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next Register
	* @throws NoSuchRegisterException if a Register with the primary key could not be found
	*/
	public static Register[] findByMobileNumber_PrevAndNext(long ID,
		java.lang.String MobileNumber,
		OrderByComparator<Register> orderByComparator)
		throws Database.exception.NoSuchRegisterException {
		return getPersistence()
				   .findByMobileNumber_PrevAndNext(ID, MobileNumber,
			orderByComparator);
	}

	/**
	* Removes all the Registers where MobileNumber LIKE &#63; from the database.
	*
	* @param MobileNumber the mobile number
	*/
	public static void removeByMobileNumber(java.lang.String MobileNumber) {
		getPersistence().removeByMobileNumber(MobileNumber);
	}

	/**
	* Returns the number of Registers where MobileNumber LIKE &#63;.
	*
	* @param MobileNumber the mobile number
	* @return the number of matching Registers
	*/
	public static int countByMobileNumber(java.lang.String MobileNumber) {
		return getPersistence().countByMobileNumber(MobileNumber);
	}

	/**
	* Caches the Register in the entity cache if it is enabled.
	*
	* @param register the Register
	*/
	public static void cacheResult(Register register) {
		getPersistence().cacheResult(register);
	}

	/**
	* Caches the Registers in the entity cache if it is enabled.
	*
	* @param registers the Registers
	*/
	public static void cacheResult(List<Register> registers) {
		getPersistence().cacheResult(registers);
	}

	/**
	* Creates a new Register with the primary key. Does not add the Register to the database.
	*
	* @param ID the primary key for the new Register
	* @return the new Register
	*/
	public static Register create(long ID) {
		return getPersistence().create(ID);
	}

	/**
	* Removes the Register with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the Register
	* @return the Register that was removed
	* @throws NoSuchRegisterException if a Register with the primary key could not be found
	*/
	public static Register remove(long ID)
		throws Database.exception.NoSuchRegisterException {
		return getPersistence().remove(ID);
	}

	public static Register updateImpl(Register register) {
		return getPersistence().updateImpl(register);
	}

	/**
	* Returns the Register with the primary key or throws a {@link NoSuchRegisterException} if it could not be found.
	*
	* @param ID the primary key of the Register
	* @return the Register
	* @throws NoSuchRegisterException if a Register with the primary key could not be found
	*/
	public static Register findByPrimaryKey(long ID)
		throws Database.exception.NoSuchRegisterException {
		return getPersistence().findByPrimaryKey(ID);
	}

	/**
	* Returns the Register with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the Register
	* @return the Register, or <code>null</code> if a Register with the primary key could not be found
	*/
	public static Register fetchByPrimaryKey(long ID) {
		return getPersistence().fetchByPrimaryKey(ID);
	}

	public static java.util.Map<java.io.Serializable, Register> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the Registers.
	*
	* @return the Registers
	*/
	public static List<Register> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the Registers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @return the range of Registers
	*/
	public static List<Register> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the Registers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of Registers
	*/
	public static List<Register> findAll(int start, int end,
		OrderByComparator<Register> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the Registers.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link RegisterModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of Registers
	* @param end the upper bound of the range of Registers (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of Registers
	*/
	public static List<Register> findAll(int start, int end,
		OrderByComparator<Register> orderByComparator, boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the Registers from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of Registers.
	*
	* @return the number of Registers
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static RegisterPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<RegisterPersistence, RegisterPersistence> _serviceTracker =
		ServiceTrackerFactory.open(RegisterPersistence.class);
}