/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package Database.service.persistence;

import Database.model.ActivitySignUp;

import aQute.bnd.annotation.ProviderType;

import com.liferay.osgi.util.ServiceTrackerFactory;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the ActivitySignUp service. This utility wraps {@link Database.service.persistence.impl.ActivitySignUpPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ActivitySignUpPersistence
 * @see Database.service.persistence.impl.ActivitySignUpPersistenceImpl
 * @generated
 */
@ProviderType
public class ActivitySignUpUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ActivitySignUp activitySignUp) {
		getPersistence().clearCache(activitySignUp);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ActivitySignUp> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ActivitySignUp> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ActivitySignUp> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ActivitySignUp> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ActivitySignUp update(ActivitySignUp activitySignUp) {
		return getPersistence().update(activitySignUp);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ActivitySignUp update(ActivitySignUp activitySignUp,
		ServiceContext serviceContext) {
		return getPersistence().update(activitySignUp, serviceContext);
	}

	/**
	* Caches the ActivitySignUp in the entity cache if it is enabled.
	*
	* @param activitySignUp the ActivitySignUp
	*/
	public static void cacheResult(ActivitySignUp activitySignUp) {
		getPersistence().cacheResult(activitySignUp);
	}

	/**
	* Caches the ActivitySignUps in the entity cache if it is enabled.
	*
	* @param activitySignUps the ActivitySignUps
	*/
	public static void cacheResult(List<ActivitySignUp> activitySignUps) {
		getPersistence().cacheResult(activitySignUps);
	}

	/**
	* Creates a new ActivitySignUp with the primary key. Does not add the ActivitySignUp to the database.
	*
	* @param ID the primary key for the new ActivitySignUp
	* @return the new ActivitySignUp
	*/
	public static ActivitySignUp create(long ID) {
		return getPersistence().create(ID);
	}

	/**
	* Removes the ActivitySignUp with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param ID the primary key of the ActivitySignUp
	* @return the ActivitySignUp that was removed
	* @throws NoSuchActivitySignUpException if a ActivitySignUp with the primary key could not be found
	*/
	public static ActivitySignUp remove(long ID)
		throws Database.exception.NoSuchActivitySignUpException {
		return getPersistence().remove(ID);
	}

	public static ActivitySignUp updateImpl(ActivitySignUp activitySignUp) {
		return getPersistence().updateImpl(activitySignUp);
	}

	/**
	* Returns the ActivitySignUp with the primary key or throws a {@link NoSuchActivitySignUpException} if it could not be found.
	*
	* @param ID the primary key of the ActivitySignUp
	* @return the ActivitySignUp
	* @throws NoSuchActivitySignUpException if a ActivitySignUp with the primary key could not be found
	*/
	public static ActivitySignUp findByPrimaryKey(long ID)
		throws Database.exception.NoSuchActivitySignUpException {
		return getPersistence().findByPrimaryKey(ID);
	}

	/**
	* Returns the ActivitySignUp with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param ID the primary key of the ActivitySignUp
	* @return the ActivitySignUp, or <code>null</code> if a ActivitySignUp with the primary key could not be found
	*/
	public static ActivitySignUp fetchByPrimaryKey(long ID) {
		return getPersistence().fetchByPrimaryKey(ID);
	}

	public static java.util.Map<java.io.Serializable, ActivitySignUp> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the ActivitySignUps.
	*
	* @return the ActivitySignUps
	*/
	public static List<ActivitySignUp> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the ActivitySignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ActivitySignUps
	* @param end the upper bound of the range of ActivitySignUps (not inclusive)
	* @return the range of ActivitySignUps
	*/
	public static List<ActivitySignUp> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the ActivitySignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ActivitySignUps
	* @param end the upper bound of the range of ActivitySignUps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of ActivitySignUps
	*/
	public static List<ActivitySignUp> findAll(int start, int end,
		OrderByComparator<ActivitySignUp> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the ActivitySignUps.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ActivitySignUpModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of ActivitySignUps
	* @param end the upper bound of the range of ActivitySignUps (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of ActivitySignUps
	*/
	public static List<ActivitySignUp> findAll(int start, int end,
		OrderByComparator<ActivitySignUp> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the ActivitySignUps from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of ActivitySignUps.
	*
	* @return the number of ActivitySignUps
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static ActivitySignUpPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ActivitySignUpPersistence, ActivitySignUpPersistence> _serviceTracker =
		ServiceTrackerFactory.open(ActivitySignUpPersistence.class);
}