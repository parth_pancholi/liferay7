package LTPMasterData.portlet;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import LTPDatabase.model.LTPSignUpPreferences;
import LTPDatabase.service.LTPSignUpPreferencesLocalServiceUtil;
import LTPMasterData.constants.LTPMasterDataPortletKeys;

/**
 * @author parth
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=LTPMasterData Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + LTPMasterDataPortletKeys.LTPMasterData,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class LTPMasterDataPortlet extends MVCPortlet {
	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		List<LTPSignUpPreferences> preferenceList= LTPSignUpPreferencesLocalServiceUtil.getLTPSignUpPreferences(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		if(preferenceList.size()>0) {
			renderRequest.setAttribute("preference",preferenceList.get(0));
		}
		renderRequest.setAttribute("isAdmin", themeDisplay.isSignedIn());
		super.render(renderRequest, renderResponse);
	}
	
	@ProcessAction(name = "submitPreferences")
	public void submitPreferences(ActionRequest actionRequest, ActionResponse actionResponse) {
		String morningWalkTitle = actionRequest.getParameter("morningWalkTitle");
		String dinnerPrasadTitle = actionRequest.getParameter("dinnerPrasadTitle");
		String volunteerTitle = actionRequest.getParameter("volunteerTitle");
		Long ltpSignUpPreferenceId = Validator.isNotNull(actionRequest.getParameter("id"))? Long.parseLong(actionRequest.getParameter("id")) : 0L;
		try {
			LTPSignUpPreferencesLocalServiceUtil.addUpdateLTPSignUpPreferences(morningWalkTitle, dinnerPrasadTitle, volunteerTitle, ltpSignUpPreferenceId);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}