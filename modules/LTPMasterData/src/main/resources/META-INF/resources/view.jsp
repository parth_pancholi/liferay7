<%@ include file="/init.jsp"%>
<portlet:actionURL name="submitPreferences" var="submitPreferencesURL" />
<c:if test="${isAdmin eq true}">
	<aui:form action="${submitPreferencesURL}" method="post" name="registerForm"
		cssClass="container-fluid-1280">
		<aui:fieldset-group markupView="lexicon">
			<aui:fieldset>
				<h1 align="center">LTP Signup Section Title Configuration</h1>
			</aui:fieldset>
			<aui:fieldset>
				<aui:input label="Morning Walk Section Title" name="morningWalkTitle" type="text"
					placeholder="Enter your first name" value="${preference.morningWalkTitle}">
					<aui:validator name="required"></aui:validator>
					<aui:validator name="maxLength">500</aui:validator>
				</aui:input>
				<aui:input label="Dinner Prasad Section Title" name="dinnerPrasadTitle" type="text"
					placeholder="Enter your last name" value="${preference.dinnerPrasadTitle}">
					<aui:validator name="required"></aui:validator>
					<aui:validator name="maxLength">500</aui:validator>
				</aui:input>
				<aui:input label="Volunteer Section Title" name="volunteerTitle" type="text"
					placeholder="Enter your last name" value="${preference.volunteerTitle}">
					<aui:validator name="required"></aui:validator>
					<aui:validator name="maxLength">500</aui:validator>
				</aui:input>
				<aui:input name="id" type="hidden" value="${preference.ID}" />
			</aui:fieldset>
		</aui:fieldset-group>
		<aui:button-row>
			<aui:button type="submit"
				value='Update' primary="<%=true%>" />
		</aui:button-row>
	</aui:form>
</c:if>
<c:if test="${isAdmin ne true}">
	Please login via admin account to use this portlet.
</c:if>