<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="RegisterForm.portlet.RegisterConfiguration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>

<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%><%@
taglib
	uri="http://liferay.com/tld/portlet" prefix="liferay-portlet"%><%@
taglib
	uri="http://liferay.com/tld/theme" prefix="liferay-theme"%><%@
taglib
	uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
	RegisterConfiguration registerConfiguration = (RegisterConfiguration) renderRequest
			.getAttribute(RegisterConfiguration.class.getName());

	String headerText = StringPool.BLANK;

	if (Validator.isNotNull(registerConfiguration)) {
		headerText = portletPreferences.getValue("headerText", registerConfiguration.headerText());
	}
%>
<style>
@media print {
	#content, #page {
		width: 100%;
		margin: 0;
		float: none;
	}

	/** Setting margins */
	@page {
		margin: 2cm
	}

	/* Or: */
	@page :left {
		margin: 1cm;
	}
	@page :right {
		margin: 1cm;
	}
	body {
		font: 13pt Georgia, "Times New Roman", Times, serif;
		line-height: 1.3;
		background: #fff !important;
		color: #000;
	}
	h1 {
		font-size: 24pt;
	}
	div.button-holder, #footer, ul.control-menu-nav{
		display: none;
	}
}
</style>