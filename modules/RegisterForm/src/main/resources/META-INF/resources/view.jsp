<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ include file="/init.jsp"%>

<portlet:renderURL var="viewRegisterMemberURL">
	<portlet:param name="mvcPath" value="/listing.jsp"></portlet:param>
</portlet:renderURL>

<portlet:actionURL name="RegisterUser" var="registerUserURL" />

<aui:nav cssClass="container-fluid-1280">
	<aui:button-row>
		<c:if test="${afterUpdate eq 'true'}">
			<aui:a href="javascript:void(0)" onClick="window.print()"
				cssClass="btn btn-primary btn-default">
				<span class="lfr-btn-label">Print</span>
			</aui:a>
		</c:if>
		<c:if test="${isAdmin eq true}">
			<aui:a href="<%=viewRegisterMemberURL%>"
				cssClass="btn btn-primary btn-default">
				<span class="lfr-btn-label">Show Registered Users List</span>
			</aui:a>
		</c:if>
	</aui:button-row>
</aui:nav>

<aui:form action="${registerUserURL}" method="post" name="registerForm"
	cssClass="container-fluid-1280">
	<aui:fieldset-group markupView="lexicon">
		<aui:fieldset>
			<!--START TODO Get from the portlet preference -->
			<h1 align="center">Some Header</h1>
			<!--END TODO Get from the portlet preference -->
			<h3 align="center">Registration Form</h3>
		</aui:fieldset>
		<aui:fieldset>
			<aui:row>
				<aui:col width="50">
					<aui:input label="First Name" name="firstName" type="text"
						placeholder="Enter your first name" value="${member.firstName}">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="50">
					<aui:input label="Last Name" name="lastName" type="text"
						placeholder="Enter your last name" value="${member.lastName}">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:col>
			</aui:row>
			<aui:select label="Gender" name="isMale" value="${member.isMale}"
				required="true">
				<aui:option label="Male" value="true" />
				<aui:option label="Female" value="false" />
			</aui:select>
			<aui:input label="Street Address" name="address" type="text"
				placeholder="Enter your street address" value="${member.address}">
				<aui:validator name="required"></aui:validator>
				<aui:validator name="maxLength">255</aui:validator>
			</aui:input>
			<aui:row>
				<aui:col width="50">
					<aui:input label="City" name="city" type="text"
						placeholder="Enter your city" value="${member.city}">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="maxLength">255</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="25">
					<aui:input label="State Code" name="state" type="text"
						placeholder="Enter your state code" value="${member.state}">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="maxLength">2</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="25">
					<aui:input label="ZIP" name="zip" type="text"
						placeholder="Enter your zip" value="${member.zip}">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="maxLength">5</aui:validator>
					</aui:input>
				</aui:col>
			</aui:row>
			<aui:row>
				<aui:col width="50">
					<aui:input label="PhoneNumber" name="workNumber" type="text"
						placeholder="Enter your phoneNumber" value="${member.phoneNumber}">
						<aui:validator name="number"></aui:validator>
						<aui:validator name="maxLength">10</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="50">
					<aui:input label="MobileNumber" name="mobileNumber" type="text"
						placeholder="Enter your mobileNumber"
						value="${member.mobileNumber}">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="number"></aui:validator>
						<aui:validator name="maxLength">10</aui:validator>
					</aui:input>
				</aui:col>
			</aui:row>
			<aui:input label="Email" name="email" type="text"
				placeholder="Enter your email" value="${member.email}">
				<aui:validator name="required"></aui:validator>
				<aui:validator name="email"></aui:validator>
			</aui:input>
		</aui:fieldset>
	</aui:fieldset-group>
	<aui:fieldset-group markupView="lexicon">
		<h4 align="center">Spouse Details:</h4>
		<aui:fieldset>
			<aui:row>
				<aui:col width="50">
					<aui:input label="Spouse First Name" name="spouseFirstName"
						type="text" placeholder="Please enter spouse first name" value="${member.spouseFirstName}">
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="50">
					<aui:input label="Spouse Last Name" name="spouseLastName"
						type="text" placeholder="Please enter spouse last name" value="${member.spouseLastName}">
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:col>
			</aui:row>
			<aui:input label="Phone Number" name="spousePhoneNumber" type="text"
				placeholder="Please enter spouse phone number" value="${member.spousePhoneNumber}">
				<aui:validator name="number"></aui:validator>
				<aui:validator name="maxLength">10</aui:validator>
			</aui:input>
		</aui:fieldset>
	</aui:fieldset-group>
	<aui:fieldset-group markupView="lexicon">
		<h4 align="center">Other Participating family members (under the
			age of 18):</h4>
		<aui:fieldset>
			<aui:row>
				<aui:col width="50">
					<aui:input label="Child 1" name="additionalMember1" type="text"
						value="${member.additionalMember1}"
						placeholder="Please enter child 1 name">
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="50">
					<aui:input label="Child 1 Age" name="additionalMember1Age"
						type="text" placeholder="Please enter child 1 age" value="${member.additionalMember1Age}">
						<aui:validator name="number"></aui:validator>
						<aui:validator
							errorMessage="Please enter the valid child age below 18 years."
							name="range">[2,17]</aui:validator>
					</aui:input>
				</aui:col>
			</aui:row>
			<aui:row>
				<aui:col width="50">
					<aui:input label="Child 2" name="additionalMember2" type="text"
						value="${member.additionalMember2}"
						placeholder="Please enter child 2 name">
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="50">
					<aui:input label="Child 2 Age" name="additionalMember2Age"
						type="text" placeholder="Please enter child 2 age" value="${member.additionalMember2Age}">
						<aui:validator name="number"></aui:validator>
						<aui:validator
							errorMessage="Please enter the valid child age below 18 years."
							name="range">[2,17]</aui:validator>
					</aui:input>
				</aui:col>
			</aui:row>
			<aui:row>
				<aui:col width="50">
					<aui:input label="Child 3" name="additionalMember3" type="text"
						value="${member.additionalMember3}"
						placeholder="Please enter child 3 name">
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="50">
					<aui:input label="Child 3 Age" name="additionalMember3Age"
						type="text" placeholder="Please enter child 3 age" value="${member.additionalMember3Age}">
						<aui:validator name="number"></aui:validator>
						<aui:validator
							errorMessage="Please enter the valid child age below 18 years."
							name="range">[2,17]</aui:validator>
					</aui:input>
				</aui:col>
			</aui:row>
		</aui:fieldset>
	</aui:fieldset-group>
	<aui:fieldset-group markupView="lexicon">
		<h4 align="center">How did you learn about us? [Please select all
			that applies]</h4>
		<aui:fieldset>
			<aui:row>
				<aui:col width="20">
					<aui:input name="isEmailCampain" type="checkbox"
						label="Email Campaign" checked="${member.isEmailCampain}" />
				</aui:col>
				<aui:col width="20">
					<aui:input name="isTemple" checked="${member.isTemple}"
						type="checkbox" label="Temple" />
				</aui:col>
				<aui:col width="20">
					<aui:input name="isOnlineNewsWebsite"
						checked="${member.isOnlineNewsWebsite}" type="checkbox"
						label="Online News/Website" />
				</aui:col>
				<aui:col width="20">
					<aui:input name="isRadio" checked="${member.isRadio}"
						type="checkbox" label="Radio" />
				</aui:col>
				<aui:col width="20">
					<aui:input name="isYouTube" checked="${member.isYouTube}"
						type="checkbox" label="YouTube" />
				</aui:col>
			</aui:row>
			<aui:row>
				<aui:col width="20">
					<aui:input name="isFacebook" checked="${member.isFacebook}"
						type="checkbox" label="Facebook" />
				</aui:col>
				<aui:col width="20">
					<aui:input name="isMeetup" checked="${member.isMeetup}"
						type="checkbox" label="Meetup" />
				</aui:col>
				<aui:col width="20">
					<aui:input name="isOnlineEventCalendar"
						checked="${member.isOnlineEventCalendar}" type="checkbox"
						label="Online Event Calendar" />
				</aui:col>
				<aui:col width="20">
					<aui:input name="isFlyer" checked="${member.isFlyer}"
						type="checkbox" label="Flyer" />
				</aui:col>
				<aui:col width="20">
					<aui:input name="isNewspaper" checked="${member.isNewspaper}"
						type="checkbox" label="Newspaper" />
				</aui:col>
			</aui:row>
			<aui:row>
				<aui:col width="20">
					<aui:input name="isWordOfMouth" type="checkbox"
						checked="${member.isWordOfMouth}" label="Word Of Mouth" />
				</aui:col>
				<aui:col width="20">
					<aui:input label="Other" name="otherCheckbox" type="checkbox"
						checked='${member.other != null && member.other != ""}' />
				</aui:col>
				<aui:col width="60">
					<aui:input label="" name="other" type="text"
						placeholder="Please mention other here" value="${member.other}">
						<aui:validator name="required">
			                function() {
			                        return AUI.$('#<portlet:namespace />otherCheckbox').prop('checked');
			                }
			        	</aui:validator>
					</aui:input>
				</aui:col>
			</aui:row>
		</aui:fieldset>
	</aui:fieldset-group>
	<aui:fieldset-group markupView="lexicon">
		<h4 align="center">Please check all the boxes below that interest you.</h4>
		<aui:fieldset>
			<aui:input name="isinterestedInVolunteringActivity" type="checkbox"
				label="Interested In Voluntering Activity"
				checked="${member.isinterestedInVolunteringActivity}" />
			<aui:input name="isinterestedInSatsangActivity" type="checkbox"
				label="Interested In Satsang Activity(devotional, chanting, scriptual studies etc.)"
				checked="${member.isinterestedInSatsangActivity}" />
			<aui:input name="isinterestedInChildrenActivity" type="checkbox"
				label="Interested In Balmukund Children Activity(Specially designed program to expose children to Indian values & culture)"
				checked="${member.isinterestedInChildrenActivity}" />
			<aui:input name="id" type="hidden" value="${member.ID}" />
		</aui:fieldset>
	</aui:fieldset-group>
	<aui:fieldset-group markupView="lexicon">
		<h4 align="center">Do you want to become a life member?</h4>
		<aui:fieldset>
			<aui:row>
				<aui:col width="50">
					<aui:select label="Yes, I want to become a" name="membershipPlan" value="${member.membershipPlan}">
						<aui:option label="Select" value="" />
						<aui:option label="Founder Member - $5000" value="Founder Member - $5000" />
						<aui:option label="Patron Member - $2500" value="Patron Member - $2500" />
						<aui:option label="Esteemed Member - $1000" value="Esteemed Member - $1000" />
						<aui:option label="Life Member - $500" value="Life Member - $500" />	
					</aui:select>
				</aui:col>
				<aui:col width="50">
					<aui:select label="Payment Method" name="paymentMethod" value="${member.paymentMethod}">
						<aui:option label="Select" value="" />
						<aui:option label="cheque" value="cheque" />
						<aui:option label="Online Payment" value="Online Payment" />
					</aui:select>
				</aui:col>
			</aui:row>
		</aui:fieldset>
	</aui:fieldset-group>
	<hr>
	<p align="justify">Lorem Ipsum is simply dummy text of the printing
		and typesetting industry. Lorem Ipsum has been the industry's standard
		dummy text ever since the 1500s, when an unknown printer took a galley
		of type and scrambled it to make a type specimen book. It has survived
		not only five centuries, but also the leap into electronic
		typesetting, remaining essentially unchanged. It was popularised in
		the 1960s with the release of Letraset sheets containing Lorem Ipsum
		passages, and more recently with desktop publishing software like
		Aldus PageMaker including versions of Lorem Ipsum.</p>
	<hr>
	<p>Signature : ____________________________</p>
	<p>Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
		_____________________________</p>
	<hr>
	<p align="center">For registration during local programs, please
		turn in the form at the book stall, else email to:</p>
	<aui:button-row>
		<aui:button type="submit"
			value='${member.ID ne null? "update" : "submit" }' primary="<%=true%>" />
	</aui:button-row>
</aui:form>