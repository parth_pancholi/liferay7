<%@ include file="/init.jsp"%>
<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css" />
<script type="text/javascript" charset="utf8"
	src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<portlet:renderURL var="addRegisterMemberURL">
	<portlet:param name="mvcPath" value="/view.jsp"></portlet:param>
</portlet:renderURL>
<portlet:renderURL var="updateRegisterMemberURL">
	<portlet:param name="mvcPath" value="/view.jsp"></portlet:param>
</portlet:renderURL>
<portlet:resourceURL var="downoadcsvURL" id="downloadCSV" />
<portlet:resourceURL var="deleteUserURL" id="deleteUser" />
<aui:nav cssClass="container-fluid-1280">
	<aui:button-row>
		<aui:a href="<%=addRegisterMemberURL%>"
			cssClass="btn btn-primary btn-default">
			<span class="lfr-btn-label">Register New User</span>
		</aui:a>
		<aui:a href="<%=downoadcsvURL%>" target="_blank"
			cssClass="btn btn-primary btn-default">
			<span class="lfr-btn-label">Download CSV</span>
		</aui:a>
	</aui:button-row>
</aui:nav>
<table id="userList" class="display container-fluid-1280">
	<thead>
		<tr>
			<th>Full Name</th>
			<th>Email Address</th>
			<th>Mobile Number</th>
			<th>Created Date</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${members}" var="user">
			<tr class="${user.getID()}">
				<td>${user.getFirstName()}&nbsp;${user.getLastName()}</td>
				<td>${user.getEmail()}</td>
				<td>${user.getMobileNumber()}</td>
				<td>
					<fmt:formatDate pattern = "MM/dd/yyyy"  value = "${user.getCreatedDate()}" />
				</td>
				<td>
					<aui:a href="${updateRegisterMemberURL}&${renderResponse.getNamespace()}userId=${user.getID()}"
						cssClass="btn btn-primary btn-default">
						<span class="lfr-btn-label">Edit User</span>
					</aui:a>
					<aui:a href="#" onclick="deleteUser(${user.getID()})"
						cssClass="btn btn-primary btn-default">
						<span class="lfr-btn-label">Delete User</span>
					</aui:a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script type="text/javascript">
	function deleteUser(userId){
		$.ajax({
			url: "${deleteUserURL}&${renderResponse.getNamespace()}userId="+userId, 
			success: function(result){
				location.reload();
		  	}
		});
	}
	$(document).ready(function() {
		var userTable=$('#userList').DataTable({
							"oLanguage" : {
								"sSearch" : "Search: ",
								"sEmptyTable" : "<div class='portlet-msg-alert'>No User Found</div>" // default message for no data
							}
						});
	});
</script>