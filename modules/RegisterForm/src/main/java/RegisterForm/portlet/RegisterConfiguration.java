package RegisterForm.portlet;

import aQute.bnd.annotation.metatype.Meta;


@Meta.OCD(id = "RegisterForm.portlet.RegisterConfiguration")
public interface RegisterConfiguration {
	
	@Meta.AD(required = false)
    public String headerText();

}
