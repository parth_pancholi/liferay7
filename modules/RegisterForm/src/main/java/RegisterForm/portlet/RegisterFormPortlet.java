package RegisterForm.portlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;

import com.liferay.portal.configuration.metatype.bnd.util.ConfigurableUtil;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.PortletResponseUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.CharPool;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import Database.model.Register;
import Database.service.RegisterLocalServiceUtil;
import RegisterForm.constants.RegisterFormPortletKeys;

/**
 * @author test
 */
@Component(configurationPid = "RegisterForm.portlet.RegisterConfiguration", immediate = true, property = {
		"com.liferay.portlet.display-category=category.Custom", "com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=RegisterForm Portlet", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + RegisterFormPortletKeys.RegisterForm, "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class RegisterFormPortlet extends MVCPortlet {

	public static String[] columnNames = { "ID", "FirstName", "LastName", "Email", "MobileNumber", "Gender", "Address",
			"City", "State", "Zip", "PhoneNumber", "Spouse Name", "Spouse Phone Number", "Child Name(Child Age)",
			"How did you learn about us?", "Activities intrested in", "Life Membership", "Payment Method", "Created Date" };
	public static final String CSV_SEPARATOR = ",";

	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		renderRequest.setAttribute(RegisterConfiguration.class.getName(), _registerConfiguration);
		super.doView(renderRequest, renderResponse);
	}

	@ProcessAction(name = "RegisterUser")
	public void AddOrUpdateUser(ActionRequest actionRequest, ActionResponse actionResponse) {
		String firstName = actionRequest.getParameter("firstName");
		String lastName = actionRequest.getParameter("lastName");
		boolean isMale = Boolean.parseBoolean(actionRequest.getParameter("isMale"));
		String address = actionRequest.getParameter("address");
		String city = actionRequest.getParameter("city");
		String state = actionRequest.getParameter("state");
		String zip = actionRequest.getParameter("zip");
		String phoneNumber = actionRequest.getParameter("workNumber");
		String mobileNumber = actionRequest.getParameter("mobileNumber");
		String email = actionRequest.getParameter("email");
		String spouseFirstName = actionRequest.getParameter("spouseFirstName");
		String spouseLastName = actionRequest.getParameter("spouseLastName");
		String spousePhoneNumber = actionRequest.getParameter("spousePhoneNumber");
		String additionalMember1 = actionRequest.getParameter("additionalMember1");
		String additionalMember1Age = actionRequest.getParameter("additionalMember1Age");
		String additionalMember2 = actionRequest.getParameter("additionalMember2");
		String additionalMember2Age = actionRequest.getParameter("additionalMember2Age");
		String additionalMember3 = actionRequest.getParameter("additionalMember3");
		String additionalMember3Age = actionRequest.getParameter("additionalMember3Age");
		boolean isEmailCampain = Boolean.parseBoolean(actionRequest.getParameter("isEmailCampain"));
		boolean isTemple = Boolean.parseBoolean(actionRequest.getParameter("isTemple"));
		boolean isOnlineNewsWebsite = Boolean.parseBoolean(actionRequest.getParameter("isOnlineNewsWebsite"));
		boolean isRadio = Boolean.parseBoolean(actionRequest.getParameter("isRadio"));
		boolean isYouTube = Boolean.parseBoolean(actionRequest.getParameter("isYouTube"));
		boolean isFacebook = Boolean.parseBoolean(actionRequest.getParameter("isFacebook"));
		boolean isMeetup = Boolean.parseBoolean(actionRequest.getParameter("isMeetup"));
		boolean isOnlineEventCalendar = Boolean.parseBoolean(actionRequest.getParameter("isOnlineEventCalendar"));
		boolean isFlyer = Boolean.parseBoolean(actionRequest.getParameter("isFlyer"));
		boolean isNewspaper = Boolean.parseBoolean(actionRequest.getParameter("isNewspaper"));
		boolean isWordOfMouth = Boolean.parseBoolean(actionRequest.getParameter("isWordOfMouth"));
		boolean otherCheckbox = Boolean.parseBoolean(actionRequest.getParameter("otherCheckbox"));
		boolean isinterestedInVolunteringActivity = Boolean
				.parseBoolean(actionRequest.getParameter("isinterestedInVolunteringActivity"));
		boolean isinterestedInSatsangActivity = Boolean
				.parseBoolean(actionRequest.getParameter("isinterestedInSatsangActivity"));
		boolean isinterestedInChildrenActivity = Boolean
				.parseBoolean(actionRequest.getParameter("isinterestedInChildrenActivity"));

		String membershipPlan = actionRequest.getParameter("membershipPlan");
		String paymentMethod = actionRequest.getParameter("paymentMethod");
		try {
			if (Validator.isNotNull(actionRequest.getParameter("id"))) {
				RegisterLocalServiceUtil.updateRegisterMember(Long.parseLong(actionRequest.getParameter("id")),
						firstName, lastName, isMale, address, city, state, zip, phoneNumber, mobileNumber, email,
						additionalMember1, additionalMember2, additionalMember3, isEmailCampain, isTemple,
						isOnlineNewsWebsite, isRadio, isYouTube, isFacebook, isMeetup, isOnlineEventCalendar, isFlyer,
						isNewspaper, isWordOfMouth,
						otherCheckbox ? actionRequest.getParameter("other") : StringPool.BLANK,
						isinterestedInVolunteringActivity, isinterestedInSatsangActivity,
						isinterestedInChildrenActivity, additionalMember1Age, additionalMember2Age,
						additionalMember3Age, spouseFirstName, spouseLastName, spousePhoneNumber, membershipPlan,
						paymentMethod);
			} else {
				RegisterLocalServiceUtil.RegisterMember(firstName, lastName, isMale, address, city, state, zip,
						phoneNumber, mobileNumber, email, additionalMember1, additionalMember2, additionalMember3,
						isEmailCampain, isTemple, isOnlineNewsWebsite, isRadio, isYouTube, isFacebook, isMeetup,
						isOnlineEventCalendar, isFlyer, isNewspaper, isWordOfMouth,
						otherCheckbox ? actionRequest.getParameter("other") : StringPool.BLANK,
						isinterestedInVolunteringActivity, isinterestedInSatsangActivity,
						isinterestedInChildrenActivity, additionalMember1Age, additionalMember2Age,
						additionalMember3Age, spouseFirstName, spouseLastName, spousePhoneNumber, membershipPlan,
						paymentMethod);
			}
			actionResponse.setRenderParameter("afterUpdate", "true");
		} catch (PortalException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		// TODO Auto-generated method stub
		if ("downloadCSV".equals(resourceRequest.getResourceID())) {
			// generateCSV();
			byte[] bytes = generateCSV();
			String fileName = "Users.csv";
			String contentType = ContentTypes.APPLICATION_TEXT;
			PortletResponseUtil.sendFile(resourceRequest, resourceResponse, fileName, bytes, contentType);
		} else if ("deleteUser".equals(resourceRequest.getResourceID())) {
			try {
				Register user = RegisterLocalServiceUtil
						.deleteRegister(Long.parseLong(resourceRequest.getParameter("userId")));
				System.out.println("User Deleted Successfully...!!" + user);
				resourceResponse.createRenderURL().addProperty("mvcPath", "/listing.jsp");
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private byte[] generateCSV() {
		StringBundler sb = new StringBundler();
		for (String columnName : columnNames) {
			sb.append(getCSVFormattedValue(columnName));
			sb.append(CSV_SEPARATOR);
		}
		sb.setIndex(sb.index() - 1);
		sb.append(CharPool.NEW_LINE);

		List<Register> usersList = RegisterLocalServiceUtil.getRegisters(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
		for (Register user : usersList) {
			sb.append(getCSVFormattedValue(String.valueOf(user.getID())));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(String.valueOf(user.getFirstName())));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(String.valueOf(user.getLastName())));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(String.valueOf(user.getEmail())));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(String.valueOf(user.getMobileNumber())));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(String.valueOf(user.getIsMale() ? "Male" : "Female")));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(String.valueOf(user.getAddress())));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(String.valueOf(user.getCity())));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(String.valueOf(user.getState())));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(String.valueOf(user.getZip())));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(
					String.valueOf(Validator.isNotNull(user.getPhoneNumber()) ? user.getPhoneNumber() : "")));
			sb.append(CSV_SEPARATOR);
			sb.append(
					getCSVFormattedValue(String.valueOf(Validator.isNotNull(user.getSpouseFirstName())
							? user.getSpouseFirstName() + StringPool.SPACE
									+ (Validator.isNotNull(user.getSpouseLastName()) ? user.getSpouseLastName() : "")
							: "")));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(
					String.valueOf(Validator.isNotNull(user.getSpousePhoneNumber()) ? user.getSpousePhoneNumber() : "")));
			sb.append(CSV_SEPARATOR);
			String additionalMember = (Validator.isNotNull(user.getAdditionalMember1())
					? user.getAdditionalMember1() + "("
							+ (Validator.isNotNull(user.getAdditionalMember1Age()) ? user.getAdditionalMember1Age()
									: "N/A")
							+ ")" + ","
					: "")
					+ (Validator.isNotNull(user.getAdditionalMember2()) ? user.getAdditionalMember2() + "("
							+ (Validator.isNotNull(user.getAdditionalMember2Age()) ? user.getAdditionalMember2Age()
									: "N/A")
							+ ")" + "," : "")
					+ (Validator.isNotNull(user.getAdditionalMember3()) ? user.getAdditionalMember3() + "("
							+ (Validator.isNotNull(user.getAdditionalMember3Age()) ? user.getAdditionalMember3Age()
									: "N/A")
							+ ")" : "");
			if (additionalMember.endsWith(",")) {
				additionalMember = additionalMember.substring(0, additionalMember.length() - 1);
			}
			sb.append(getCSVFormattedValue(String.valueOf(additionalMember)));
			sb.append(CSV_SEPARATOR);
			String learnAboutUs = (user.getIsEmailCampain() ? "Email Champign," : "")
					+ (user.getIsTemple() ? "Temple," : "")
					+ (user.getIsOnlineNewsWebsite() ? "Online News/Website," : "")
					+ (user.getIsRadio() ? "Radio," : "") + (user.getIsYouTube() ? "Youtube," : "")
					+ (user.getIsFacebook() ? "Facebook," : "") + (user.getIsMeetup() ? "Meetup," : "")
					+ (user.getIsOnlineEventCalendar() ? "Online Event Calendar," : "")
					+ (user.getIsFlyer() ? "Flyer," : "") + (user.getIsNewspaper() ? "Newspaper," : "")
					+ (user.getIsWordOfMouth() ? "Word of mouth," : "")
					+ (Validator.isNotNull(user.getOther()) ? user.getOther() : "");
			if (learnAboutUs.endsWith(",")) {
				learnAboutUs = learnAboutUs.substring(0, learnAboutUs.length() - 1);
			}
			sb.append(getCSVFormattedValue(String.valueOf(learnAboutUs)));
			sb.append(CSV_SEPARATOR);
			String intrestedActivity = (user.getIsinterestedInVolunteringActivity() ? "Volentering Activity," : "")
					+ (user.getIsinterestedInSatsangActivity() ? "Satsang Activity," : "")
					+ (user.getIsinterestedInChildrenActivity() ? "Children Activity" : "");
			if (intrestedActivity.endsWith(",")) {
				intrestedActivity = intrestedActivity.substring(0, intrestedActivity.length() - 1);
			}
			sb.append(getCSVFormattedValue(intrestedActivity));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(user.getMembershipPlan()));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(user.getPaymentMethod()));
			sb.append(CSV_SEPARATOR);
			sb.append(getCSVFormattedValue(String.valueOf(user.getCreatedDate())));
			sb.append(CSV_SEPARATOR);
			sb.setIndex(sb.index() - 1);
			sb.append(CharPool.NEW_LINE);
		}

		return sb.toString().getBytes();
	}

	protected static String getCSVFormattedValue(String value) {
		StringBundler sb = new StringBundler(3);
		sb.append(CharPool.QUOTE);
		sb.append(StringUtil.replace(value, CharPool.QUOTE, StringPool.DOUBLE_QUOTE));
		sb.append(CharPool.QUOTE);
		return sb.toString();
	}

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		// Get all the data for the register page.
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		if (Validator.isNotNull(renderRequest.getParameter("userId"))) {
			try {
				renderRequest.setAttribute("member",
						RegisterLocalServiceUtil.getRegister(Long.parseLong(renderRequest.getParameter("userId"))));
			} catch (NumberFormatException | PortalException e) {
				e.printStackTrace();
			}
		}
		if(Validator.isNotNull(renderRequest.getParameter("afterUpdate"))) {
			renderRequest.setAttribute("afterUpdate", renderRequest.getParameter("afterUpdate"));
		}
		renderRequest.setAttribute("members",
				RegisterLocalServiceUtil.getRegisters(QueryUtil.ALL_POS, QueryUtil.ALL_POS));
		renderRequest.setAttribute("isAdmin", themeDisplay.isSignedIn());
		super.render(renderRequest, renderResponse);
	}

	public String getHeaderText(Map labels) {
		return (String) labels.get(_registerConfiguration.headerText());
	}

	@Activate
	@Modified
	protected void activate(Map<Object, Object> properties) {
		_registerConfiguration = ConfigurableUtil.createConfigurable(RegisterConfiguration.class, properties);
	}

	private volatile RegisterConfiguration _registerConfiguration;

}