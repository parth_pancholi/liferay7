<%@ include file="/init.jsp"%>

<portlet:actionURL name="ltpSignUp" var="ltpSignUpURL" />
<portlet:renderURL var="viewLTPSignUpDataURL">
	<portlet:param name="mvcPath" value="/listing.jsp"></portlet:param>
</portlet:renderURL>


<c:if test="${isAdmin eq true}">
	<aui:nav cssClass="container-fluid-1280">
		<aui:button-row>
			<aui:a href="<%=viewLTPSignUpDataURL%>"
				cssClass="btn btn-primary btn-default">
				<span class="lfr-btn-label">View LTPSignUp List</span>
			</aui:a>
		</aui:button-row>
	</aui:nav>
	<aui:nav cssClass="container-fluid-1280">
		<table id="searchUser" class="display container-fluid-1280">
			<thead>
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Mobile Number</th>
					<th>Created Date</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${searchUsers}" var="user">
					<tr class="${user.getID()}">
						<td>${user.getFirstName()}</td>
						<td>${user.getLastName()}</td>
						<td>${user.getMobileNumber()}</td>
						<td><fmt:formatDate pattern="MM/dd/yyyy"
								value="${user.getCreatedDate()}" /></td>
						<td><aui:a href="javascript:void(0)"
								onClick="loadUser('${user.getFirstName()}', '${user.getLastName()}', '${user.getMobileNumber()}')"
								cssClass="btn btn-primary btn-default">
								<span class="lfr-btn-label">Load User</span>
							</aui:a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</aui:nav>
</c:if>
<hr>
<aui:form action="${ltpSignUpURL}" method="post" name="ltpSignUpForm"
	cssClass="container-fluid-1280">
	<aui:fieldset-group markupView="lexicon">
		<aui:fieldset>
			<h3 align="center">LTP Signup Form</h3>
		</aui:fieldset>
		<aui:fieldset>
			<aui:row>
				<aui:col width="33">
					<label>First Name:  <span id="labelFirstName">${basicLTPSignUpData.firstName}</span></label>
					<aui:input label="First Name" name="firstName" type="hidden"
						value="${basicLTPSignUpData.firstName}"
						placeholder="Enter your first name" readonly="true">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="33">
					<label>Last Name:  <span id="labelLastName">${basicLTPSignUpData.lastName}</span></label>
					<aui:input label="Last Name" name="lastName" type="hidden"
						value="${basicLTPSignUpData.lastName}"
						placeholder="Enter your last name" readonly="true">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="maxLength">75</aui:validator>
					</aui:input>
				</aui:col>
				<aui:col width="33">
					<label>MobileNumber:  <span id="labelMobileNumber">${basicLTPSignUpData.mobileNumber}</span></label>
					<aui:input label="MobileNumber" name="mobileNumber" type="hidden"
						value="${basicLTPSignUpData.mobileNumber}"
						placeholder="Enter your mobileNumber" readonly="true">
						<aui:validator name="required"></aui:validator>
						<aui:validator name="number"></aui:validator>
						<aui:validator name="maxLength">10</aui:validator>
					</aui:input>
				</aui:col>
			</aui:row>
			<aui:input name="id" type="hidden" value="${basicLTPSignUpData.ID}" />
		</aui:fieldset>
	</aui:fieldset-group>
	<aui:fieldset-group markupView="lexicon">
		<aui:fieldset>
			<h1 align="center">Morning Walk</h1>
			<h3 align="center">${preferences.morningWalkTitle}</h3>
		</aui:fieldset>
		<aui:fieldset>
			<c:forEach var="morningWalk" items="${morningWalkList}">
				<c:set var="eventDate">
					<fmt:formatDate pattern="MM/dd/yyyy"
						value="${morningWalk.eventDate}" />
				</c:set>
				<aui:row>
					<aui:col width="20">
						<aui:input name="morningWalkEvent${morningWalk.ID}"
							checked="${selectedMorningWalkList.contains(morningWalk.ID)}"
							type="checkbox" label='${eventDate}' />
					</aui:col>
					<aui:col width="80">
						<label>${morningWalk.extraText}</label>
					</aui:col>
				</aui:row>
			</c:forEach>
		</aui:fieldset>
	</aui:fieldset-group>
	<aui:fieldset-group markupView="lexicon">
		<aui:fieldset>
			<h1 align="center">Dinner Prasad</h1>
			<h3 align="center">${preferences.dinnerPrasadTitle}</h3>
		</aui:fieldset>
		<aui:fieldset>
			<c:forEach var="dinnerPrasad" items="${dinnerPrasadList}">
				<c:set var="eventDate">
					<fmt:formatDate pattern="MM/dd/yyyy"
						value="${dinnerPrasad.eventDate}" />
				</c:set>
				<aui:row>
					<aui:col width="20">
						<aui:input name="dinnerPrasadEvent${dinnerPrasad.ID}"
							checked="${selectedDinnerPrasadList.contains(dinnerPrasad.ID)}"
							type="checkbox" label="${eventDate}" />
					</aui:col>
					<aui:col width="80">
						<aui:input name="dinnerPrasadItems${dinnerPrasad.ID}"
							value="${selectedDinnerPrasadItemList.get(dinnerPrasad.ID) }"
							type="textarea"
							placeholder="Enter all the prasad Items you wish to bring to the event."
							label="" />
					</aui:col>
				</aui:row>
			</c:forEach>
		</aui:fieldset>
	</aui:fieldset-group>
	<aui:fieldset-group markupView="lexicon">
		<aui:fieldset>
			<h1 align="center">Volunteer</h1>
			<h3 align="center">${preferences.volunteerTitle}</h3>
		</aui:fieldset>
		<aui:fieldset>
			<c:forEach var="volunteer" items="${volunteerList}">
				<c:set var="eventDate">
					<fmt:formatDate pattern="MM/dd/yyyy" value="${volunteer.eventDate}" />
				</c:set>
				<aui:row>
					<aui:col width="20">
						<aui:input name="volunteerEvent${volunteer.ID}"
							checked="${selectedVolunteerList.contains(volunteer.ID)}"
							type="checkbox" label="${eventDate}" />
					</aui:col>
				</aui:row>
			</c:forEach>
		</aui:fieldset>
	</aui:fieldset-group>
	<aui:button-row>
		<aui:button type="submit" value='submit' primary="<%=true%>" />
	</aui:button-row>
</aui:form>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						var userTable = $('#searchUser')
								.DataTable(
										{
											"bLengthChange" : false,
											"oLanguage" : {
												"sSearch" : "Search by your name:",
												"sEmptyTable" : "<div class='portlet-msg-alert'>No data Found</div>",
												"sZeroRecords": "No matching records found. <br/>  Not a registered member. Please register on the welcome page."
											}
										});
						var condition = '${basicLTPSignUpData.ID}';
						if (condition != "") {
							$('#searchUser').parents('div.dataTables_wrapper')
									.first().hide();
						}
						$('#searchUser_filter input').keyup(function(event) {
							if (event.target.value !== "") {
								$("#searchUser").show();
								$('#searchUser_length').show();
								$("#searchUser + .dataTables_info").show();
								$("#searchUser_paginate").show();
							}else{
								$("#searchUser").hide();
								$('#searchUser_length').hide();
								$("#searchUser + .dataTables_info").hide();
								$("#searchUser_paginate").hide();
								$("form[id$='ltpSignUpForm']").hide();
							}
						});
						$("#searchUser").hide();
						$('#searchUser_length').hide();
						$("#searchUser + .dataTables_info").hide();
						$("#searchUser_paginate").hide();
						if(!'${basicLTPSignUpData.ID}')
							$("form[id$='ltpSignUpForm']").hide();
					});
</script>
<aui:script>
	function loadUser(firstName, lastName, mobileNumber) {
		$('input[name$="firstName"]').val(firstName);
		$('input[name$="lastName"]').val(lastName);
		$('input[name$="mobileNumber"]').val(mobileNumber);
		$('#labelFirstName').text(firstName);
		$('#labelLastName').text(lastName);
		$('#labelMobileNumber').text(mobileNumber);
		$("form[id$='ltpSignUpForm']").show();
	}
</aui:script>
	





