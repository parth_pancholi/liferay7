package LTPSignupForm.portlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.ProcessAction;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

import Database.service.RegisterLocalServiceUtil;
import LTPDatabase.model.LTPSignUpMasterData;
import LTPDatabase.model.LTPSignUp_LTPMasterData;
import LTPDatabase.service.LTPSignUpLocalServiceUtil;
import LTPDatabase.service.LTPSignUpMasterDataLocalServiceUtil;
import LTPDatabase.service.LTPSignUpPreferencesLocalServiceUtil;
import LTPDatabase.service.LTPSignUp_LTPMasterDataLocalServiceUtil;
import LTPSignupForm.constants.LTPSignupFormPortletKeys;

/**
 * @author parth
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true", "javax.portlet.display-name=LTPSignupForm Portlet",
		"javax.portlet.init-param.template-path=/", "javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + LTPSignupFormPortletKeys.LTPSignupForm,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class LTPSignupFormPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		renderRequest.setAttribute("isAdmin", themeDisplay.isSignedIn());
		if (!"/listing.jsp".equals(renderRequest.getParameter("mvcPath"))) {
			renderRequest.setAttribute("searchUsers",
					RegisterLocalServiceUtil.getRegisterMembers(QueryUtil.ALL_POS, QueryUtil.ALL_POS));
			renderRequest.setAttribute("preferences", LTPSignUpPreferencesLocalServiceUtil
					.getLTPSignUpPreferences(QueryUtil.ALL_POS, QueryUtil.ALL_POS).get(0));
			List<LTPSignUpMasterData> morningWalkList = new ArrayList<LTPSignUpMasterData>();
			List<LTPSignUpMasterData> dinnerPrasadList = new ArrayList<LTPSignUpMasterData>();
			List<LTPSignUpMasterData> volunteerList = new ArrayList<LTPSignUpMasterData>();
			List<Long> selectedMorningWalkList = new ArrayList<Long>();
			List<Long> selectedDinnerPrasadList = new ArrayList<Long>();
			List<Long> selectedVolunteerList = new ArrayList<Long>();
			Map<Long,String> selectedDinnerPrasadItemList = new HashMap<Long,String>();
			for (LTPSignUpMasterData ltpSignUpMasterData : LTPSignUpMasterDataLocalServiceUtil
					.getLTPSignUpMasterDatas(QueryUtil.ALL_POS, QueryUtil.ALL_POS)) {
				// Get only the event which are greater than current day.
				if (new Date().getTime() - ltpSignUpMasterData.getEventDate().getTime() < 0) {
					if (ltpSignUpMasterData.getIsMorningWalk())
						morningWalkList.add(ltpSignUpMasterData);
					else if (ltpSignUpMasterData.getIsDinnerPrasad())
						dinnerPrasadList.add(ltpSignUpMasterData);
					else if (ltpSignUpMasterData.getIsVolunteer())
						volunteerList.add(ltpSignUpMasterData);
				}
			}
			if ("update".equals(renderRequest.getParameter("type"))) {
				try {
					renderRequest.setAttribute("basicLTPSignUpData",
							LTPSignUpLocalServiceUtil.getLTPSignUp(Long.parseLong(renderRequest.getParameter("id"))));
					List<LTPSignUp_LTPMasterData> ltpSignUp_LTPMasterDataList = LTPSignUp_LTPMasterDataLocalServiceUtil.findByLTPSignUpID(Long.parseLong(renderRequest.getParameter("id")));
					for (LTPSignUp_LTPMasterData ltpSignUp_LTPMasterData :  ltpSignUp_LTPMasterDataList) {
						if("morningWalkEvent".equals(ltpSignUp_LTPMasterData.getDinnerPrasadItem2())) {
							selectedMorningWalkList.add(ltpSignUp_LTPMasterData.getLTPMasterDataID());
						}else if("dinnerPrasadEvent".equals(ltpSignUp_LTPMasterData.getDinnerPrasadItem2())) {
							selectedDinnerPrasadList.add(ltpSignUp_LTPMasterData.getLTPMasterDataID());
							selectedDinnerPrasadItemList.put(ltpSignUp_LTPMasterData.getLTPMasterDataID(), ltpSignUp_LTPMasterData.getDinnerPrasadItem1());
						}else if("volunteerEvent".equals(ltpSignUp_LTPMasterData.getDinnerPrasadItem2())) {
							selectedVolunteerList.add(ltpSignUp_LTPMasterData.getLTPMasterDataID());
						}
					}
					renderRequest.setAttribute("selectedMorningWalkList", selectedMorningWalkList);
					renderRequest.setAttribute("selectedDinnerPrasadList", selectedDinnerPrasadList);
					renderRequest.setAttribute("selectedVolunteerList", selectedVolunteerList);
					renderRequest.setAttribute("selectedDinnerPrasadItemList", selectedDinnerPrasadItemList);
				} catch (NumberFormatException | PortalException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			renderRequest.setAttribute("morningWalkList", morningWalkList);
			renderRequest.setAttribute("dinnerPrasadList", dinnerPrasadList);
			renderRequest.setAttribute("volunteerList", volunteerList);
		} else {
			// Parameters setting for the listing part.
			renderRequest.setAttribute("data",
					LTPSignUpLocalServiceUtil.getLTPSignUps(QueryUtil.ALL_POS, QueryUtil.ALL_POS));
		}

		
		super.render(renderRequest, renderResponse);
	}

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		// TODO Auto-generated method stub
		try {
			LTPSignUpLocalServiceUtil.deleteLTPSignUpWithDependents(Long.parseLong(resourceRequest.getParameter("id")));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		super.serveResource(resourceRequest, resourceResponse);
	}

	@ProcessAction(name = "ltpSignUp")
	public void ltpSignUp(ActionRequest actionRequest, ActionResponse actionResponse) {
		String firstName = actionRequest.getParameter("firstName");
		String lastName = actionRequest.getParameter("lastName");
		String mobileNumber = actionRequest.getParameter("mobileNumber");
		Long id = Validator.isNotNull(actionRequest.getParameter("id")) ? Long.parseLong(actionRequest.getParameter("id")) : 0L;

		// Get the data from the frontend form based on the dynamic fields for
		// the events.
		String[] checkboxNames = actionRequest.getParameter("checkboxNames").split(",");
		Map<Integer, String> ltpMap = new HashMap<>();
		for (int i = 0; i < checkboxNames.length; i++) {
			String string = actionRequest.getParameter(checkboxNames[i]);
			if ("true".equals(string)) {
				if (checkboxNames[i].contains("morningWalkEvent")) {
					ltpMap.put(Integer.parseInt(checkboxNames[i].replaceAll("morningWalkEvent", "")), "morningWalkEvent##");
				} else if (checkboxNames[i].contains("dinnerPrasadEvent")) {
					ltpMap.put(Integer.parseInt(checkboxNames[i].replaceAll("dinnerPrasadEvent", "")), "dinnerPrasadEvent##" + actionRequest
							.getParameter("dinnerPrasadItems" + checkboxNames[i].replaceAll("dinnerPrasadEvent", "")));
				} else if (checkboxNames[i].contains("volunteerEvent")) {
					ltpMap.put(Integer.parseInt(checkboxNames[i].replaceAll("volunteerEvent", "")), "volunteerEvent##");
				}
			}
		}
		try {
			if (id != 0L) {
				LTPSignUpLocalServiceUtil.updateLTPSignUp(firstName, lastName, mobileNumber, ltpMap,
						id);
			} else {
				// TODO Call the add method
				LTPSignUpLocalServiceUtil.addLTPSignUp(firstName, lastName, mobileNumber, ltpMap);
			}
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}