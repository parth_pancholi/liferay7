<%@ include file="/init.jsp"%>

<style>
	.dataTables_filter input{
 	    width: 500px;
   		height: 36px;
	}
</style>
<portlet:renderURL var="addLTPSignUpURL">
	<portlet:param name="mvcPath" value="/view.jsp"></portlet:param>
</portlet:renderURL>

<portlet:renderURL var="updateLTPSignUpURL">
	<portlet:param name="mvcPath" value="/view.jsp"></portlet:param>
	<portlet:param name="type" value="update"></portlet:param>
</portlet:renderURL>

<portlet:resourceURL var="deleteURL" id="delete" />

<aui:nav cssClass="container-fluid-1280">
	<aui:button-row>
		<aui:a href="<%=addLTPSignUpURL%>"
			cssClass="btn btn-primary btn-default">
			<span class="lfr-btn-label">New LTP SignUp</span>
		</aui:a>
	</aui:button-row>
</aui:nav>
<table id="ltpSignUpList" class="display container-fluid-1280">
	<thead>
		<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Mobile Number</th>
			<th>Create Date</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		<c:forEach items="${data}" var="ltpData">
			<tr class="${ltpData.getID()}">
				<td>${ltpData.getFirstName()}</td>
				<td>${ltpData.getLastName()}</td>
				<td>${ltpData.getMobileNumber()}</td>
				<td>
					<fmt:formatDate pattern = "MM/dd/yyyy"  value = "${ltpData.getCreatedDate()}" />
				</td>
				<td>
					<aui:a href="${updateLTPSignUpURL}&${renderResponse.getNamespace()}id=${ltpData.getID()}"
						cssClass="btn btn-primary btn-default">
						<span class="lfr-btn-label">Edit</span>
					</aui:a>
					<aui:a href="#" onclick="deleteltp(${ltpData.getID()})"
						cssClass="btn btn-primary btn-default">
						<span class="lfr-btn-label">Delete</span>
					</aui:a>
				</td>
			</tr>
		</c:forEach>
	</tbody>
</table>
<script type="text/javascript">
	function deleteltp(id){
		$.ajax({
			url: "${deleteURL}&${renderResponse.getNamespace()}id="+id, 
			success: function(result){
				location.reload();
		  	}
		});
	}
	$(document).ready(function() {
		var userTable=$('#ltpSignUpList').DataTable({
							"oLanguage" : {
								"sSearch" : "Search:",
								"sEmptyTable" : "<div class='portlet-msg-alert'>No data Found</div>"
							}
						});
	});
</script>